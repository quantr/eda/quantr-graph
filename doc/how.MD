# How to Use Quantr Logic

## Quantr Logic Canvas Layout

<img src = "https://gitlab.com/quantr/eda/quantr-logic/-/raw/b5d3e6257f062a800689f2df43b6f075306c9637/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20142757.png" style="center">

## Toolbar
![image](https://gitlab.com/quantr/eda/quantr-logic/-/raw/b5d3e6257f062a800689f2df43b6f075306c9637/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20143122.png)

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/3dae758a9221fefa8c30d05742a00ae6ee69d2a5/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144223.png"  width="80" height="" style="margin-left:25;">  New 
    The new button allows users to create a new canvas for other circuit building, it would launch a new canvas everytime when the New button is presssed.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/3dae758a9221fefa8c30d05742a00ae6ee69d2a5/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144239.png"  width="80" height="" style="margin-left:25;">  Open
    Users can open their previous ql files from the open button.
- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144245.png"  width="80" height="" style="margin-left:25;">  Save
     We allow users to save their logic design in Quantr Logic to on their local computer.
- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/3dae758a9221fefa8c30d05742a00ae6ee69d2a5/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144250.png"  width="150" height="" style="margin-left:25;">  Save to Quantr
     Users may also choose to save their files on our Quantr server for further use.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/3dae758a9221fefa8c30d05742a00ae6ee69d2a5/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144255.png"  width="80" height="" style="margin-left:25;">  Setting
    Users can change the settings according to their preference like the grid color and the window size.
- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144306.png"  width="150" height="" style="margin-left:25;">  Zoom in zoom out
     Our default view size for the canvas is 100%, users may adjust the size of the canvas according to users preference by pressing the magnifier button.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/04d8bcba675510e7c2889aa3a0476ff05ce36611/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20164735.png"  width="79" height="" style="margin-left:25;">  Clear
     The clear button allow users to clear all the components put on canvas, which users can create a new curcuit on the same canvas.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144332.png"  width="80" height="" style="margin-left:25;">  Grid ON/OFF
     User can choose whether the grid is visible or not according to users' preference.
- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144437.png"  width="80" height="" style="margin-left:25;">  Simulate
    The circuit would be simulated once user presses the simulate button.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144445.png"  width="80" height="" style="margin-left:25;">  Reset
     All the components would be reset to its initial state when the reset button is clicked.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/04d8bcba675510e7c2889aa3a0476ff05ce36611/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20164743.png"  width="80" height="" style="margin-left:25;">  Wire
     By clicking the wire button, users can connect the components by a wire. Users can also connect the comopnents by right clicking.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144518.png"  width="80" height="" style="margin-left:25;">  Auto
    The auto button allows users to connect components by clicking the two input/output pins.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144526.png"  width="80" height="" style="margin-left:25;">  Delete
    Users can delete a specific components by clicking the delete button or pressing the delete key. Users can directly delete by pressing the key Shift+Delete.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144529.png"  width="100" height="" style="margin-left:25;">  Show Level
    Users can get to know the level of each component on canvas by clicking the show level button to know how the simulation works.
- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144533.png"  width="100" height="" style="margin-left:25;">  Auto Position
    After placing the components on canvas, the atuo position button helps to format the place for each components according to their levels. The smaller the level, the closer to the left.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144538.png"  width="150" height="" style="margin-left:25;">  Orientation
     Users can adjust the orientation for each component by clicking different orientation button or by changing it through the property table at the lower left-hand corner of Quantr Logic.

- #### <img src="https://gitlab.com/quantr/eda/quantr-logic/-/raw/bad7713b375fbed57042d69456699136708e1149/doc/screen%20Capture%20for%20Intro/Screenshot%202023-08-16%20144543.png"  width="80" height="" style="margin-left:25;">  Convert
    Users can input a boolean algebra on the convert button, and it will generate the corresponding logic circuit on the canvas.