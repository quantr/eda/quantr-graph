/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.graph;

import hk.quantr.logic.data.gate.AndGate;
import hk.quantr.logic.data.gate.Port;
import org.junit.Test;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class TestRefFormatter {

	@Test
	public void test() {
		Port p = new Port(new AndGate("AND"), "test");
		for (int i = 0; i < 300; i++) {
			p.refFormatter(i);
			System.out.println("ref[" + (i + 1) + "]: " + p.ref);
			p.ref = "";
		}
	}
}
