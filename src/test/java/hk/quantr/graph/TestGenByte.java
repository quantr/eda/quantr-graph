///*
// * Copyright 2023 Peter (peter@quantr.hk).
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package hk.quantr.graph;
//
//import java.io.IOException;
//import java.lang.reflect.InvocationTargetException;
//import org.junit.Test;
//import org.objectweb.asm.ClassReader;
//import org.objectweb.asm.ClassVisitor;
//import org.objectweb.asm.ClassWriter;
//import org.objectweb.asm.MethodVisitor;
//import org.objectweb.asm.Opcodes;
//import static org.objectweb.asm.Opcodes.ASM7;
//
///**
// *
// * @author Peter (peter@quantr.hk)
// */
//class OwnClassLoader extends ClassLoader {
//
//	public Class<?> defineClass(String name, byte[] b) {
//		return defineClass(name, b, 0, b.length);
//	}
//}
//
//public class TestGenByte {
//
//	private Class<?> createClass(byte[] b) {
//		return new OwnClassLoader().defineClass("hk.quantr.graph.Simulator", b);
//	}
//
//	@Test
//	public void test() throws IOException, InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
////		Simulator simulator = new Simulator();
//		ClassReader cr = new ClassReader("hk.quantr.graph.Simulator");
//		ClassWriter cw = new ClassWriter(0);
//		ClassVisitor cv = new ClassVisitor(ASM7, cw) {
//			@Override
//			public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
//				if ("or".equals(name)) {
//					return null;
//				}
//				return super.visitMethod(access, name, desc, signature, exceptions);
//			}
//
//			@Override
//			public void visitEnd() {
//				MethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "or", "(II)I", null, null);
//				mv.visitCode();
////				mv.visitVarInsn(Opcodes.ILOAD, 1);
////				mv.visitVarInsn(Opcodes.ILOAD, 2);
////				mv.visitInsn(Opcodes.IOR);
////				mv.visitInsn(Opcodes.IRETURN);
//				mv.visitVarInsn(Opcodes.ILOAD, 1);
//				mv.visitIntInsn(Opcodes.BIPUSH, 110);
//				mv.visitInsn(Opcodes.IADD);
//				mv.visitInsn(Opcodes.IRETURN);
//				mv.visitEnd();
//				mv.visitMaxs(4, 4);
//			}
//		};
//		cr.accept(cv, 0);
//		byte bytes[] = cw.toByteArray();
////		Class c = loadClass("hk.quantr.graph.Simulator", bytes);
////		Class<?> c = new OwnClassLoader().defineClass("hk.quantr.graph.Simulator", bytes);
//		Class<Simulator> c = (Class<Simulator>) new OwnClassLoader().defineClass("hk.quantr.graph.Simulator", bytes);
//		Object simulator = c.getDeclaredConstructor().newInstance();
//		Object r = simulator.getClass().getMethod("or", int.class, int.class).invoke(simulator, 1, 2);
//		System.out.println(r);
//	}
//}
