/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.graph;

import hk.quantr.logic.data.gate.Vertex;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestLoadingComponents {

	@Test
	public void test() throws MalformedURLException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
		URL url = new URL("https://www.quantr.foundation/wp-json/quantr-foundation-wordpress-widgets/componentLibrary");
		String content = IOUtils.toString(url.openStream(), "UTF-8");
		System.out.println(content);
		JSONArray arr = new JSONArray(content);
		for (int i = 0; i < arr.length(); i++) {
			String name = arr.getJSONObject(i).getString("name");
			String icon = arr.getJSONObject(i).getString("icon");

			System.out.println("name=" + name);
			System.out.println("icon=" + icon);
		}

		URL[] urls = {new URL("https://www.quantr.foundation/componentLibrary/TTL-1.0.jar")};
		URLClassLoader cl = URLClassLoader.newInstance(urls);
		Class<Vertex> vertexClass = (Class<Vertex>) cl.loadClass("hk.quantr.ttl.TTL7411");

		Class<? extends Vertex> sub = vertexClass.asSubclass(Vertex.class);
		Constructor<? extends Vertex> con = sub.getConstructor(String.class);
		Vertex v1 = con.newInstance("peter");
	}
}
