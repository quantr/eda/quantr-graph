/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.graph;

import hk.quantr.routingalgo.lee.Path;
import hk.quantr.routingalgo.lee.VerifyGrid;

/**
 *
 * @author ken
 */
public class TestSpeed {

	public static void main(String args[]) {
		VerifyGrid grid = new VerifyGrid(70);

		for (int x = 0; x < grid.nodeLoc.length; x++) {
			for (int y = 0; y < grid.nodeLoc[0].length; y++) {
				grid.nodeLoc[x][y].applied = false;
			}
		}
//		for (Vertex v : data.vertices) {
//			for (int x = v.x; x <= v.x + v.width && x < grid.nodeLoc.length; x++) {
//				for (int y = v.y; y <= v.y + v.height && y < grid.nodeLoc[0].length; y++) {
//					grid.nodeLoc[x][y].applied = true;
//				}
//			}
//		}
		Path p = grid.findPath(new hk.quantr.routingalgo.lee.Point(10, 20), new hk.quantr.routingalgo.lee.Point(30, 40), true);
	}
}
