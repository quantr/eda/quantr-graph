/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.graph;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestLoadComponent {

	@Test
	public void test() throws IOException, URISyntaxException, ClassNotFoundException {
		String jarPath = Paths.get("../ttl7411/target/TTL7411-1.0.jar").toString();
		JarFile jarFile = new JarFile(jarPath);
		Enumeration<JarEntry> e = jarFile.entries();

		URL[] urls = {new URL("jar:file:" + jarPath + "!/")};
		URLClassLoader cl = URLClassLoader.newInstance(urls);

		while (e.hasMoreElements()) {
			JarEntry je = e.nextElement();
			System.out.println(">" + je);
			if (je.getName().equals("META-INF/MANIFEST.MF")) {
				InputStream input = jarFile.getInputStream(je);
				String content = IOUtils.toString(input, "utf-8");
				System.out.println(content);
			}
			if (je.getName().equals("component.xml")) {
				System.out.println("-".repeat(100));
				InputStream input = jarFile.getInputStream(je);
				String content = IOUtils.toString(input, "utf-8");
				System.out.println(content);
				System.out.println("-".repeat(100));
			}
			if (je.isDirectory() || !je.getName().endsWith(".class")) {
				continue;
			}
//			String className = je.getName().substring(0, je.getName().length() - 6);
//			className = className.replace('/', '.');
//			Class c = cl.loadClass(className);
//			MyInterface clsInstance = (MyInterface) c.newInstance();
//			clsInstance.play();
		}
	}
}
