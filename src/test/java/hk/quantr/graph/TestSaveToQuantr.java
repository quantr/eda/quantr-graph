/*
 * Copyright 2023 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.graph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSaveToQuantr {

	@Test
	public void test() {
		try {
			HttpPost post = new HttpPost("https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/api/quantr-logic/saveProject.php");
			final List<NameValuePair> params = new ArrayList();
			params.add(new BasicNameValuePair("username", "fuck"));
			params.add(new BasicNameValuePair("username", "fuck"));
			post.setEntity(new UrlEncodedFormEntity(params));

			try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
				String result = EntityUtils.toString(response.getEntity());
				System.out.println(result);
			}
		} catch (IOException | ParseException ex) {
			ex.printStackTrace();
		}
	}
}
