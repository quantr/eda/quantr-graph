/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.graph;

import hk.quantr.logic.arduino.ArduinoServer;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpConnectTimeoutException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import org.junit.Test;

/**
 *
 * @author ken
 */
public class TestArduinoServer {

	@Test
	public void test() throws Exception {
//		ArduinoServer s = new ArduinoServer();
//		while (true) {
//			s.receiveData();
//			s.sendData();
//
//		}
		System.out.println(get());
	}

	public String get() throws Exception, HttpConnectTimeoutException {
//		System.out.println(InetAddress.getByName(ArduinoServer.serverUrlString.toString()).isReachable(0));
		HttpClient client = HttpClient.newHttpClient();
		String signal = "hello";
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(ArduinoServer.serverUrlString + "," + signal + ",Quantr-logic"))
				.timeout(Duration.of(1000, ChronoUnit.MILLIS))
				.build();
		var response
				= client.send(request, BodyHandlers.ofString());
		return response.body();
	}
}
