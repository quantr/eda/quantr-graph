package hk.quantr.logic.data.graphics;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("imageComponent")
public class ImageComponent extends Vertex { // in progress

	public String encodedImage;
	String format;
	@XStreamOmitField
	public BufferedImage image;

	public ImageComponent(String name) {
		super(name, 0, 0, 10, 10);
		this.properties.remove("Label");
		this.properties.put("Name", name);
		this.properties.put("Width", 10);
		this.properties.put("Height", 10);
		this.properties.put("Border", "enable");
	}

	@Override
	public void updateProperty() {
		super.updateProperty();
		if (this.properties.get("Width") != null && (int) this.properties.get("Width") > 0) {
			this.width = (int) this.properties.get("Width");
		}
		if (this.properties.get("Height") != null && (int) this.properties.get("Height") > 0) {
			this.height = (int) this.properties.get("Height");
		}
	}

	@Override
	public String getTypeName() {
		return "Image Component";
	}

	public void upload() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Supported File Format", "jpeg", "jpg", "png", "bmp", "webp", "gif"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JPEG Files", "jpeg", "jpg"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG Files", "png"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("BMP Files", "bmp"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("WEBP Files", "webp"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("GIF Files", "gif"));
		int result = fileChooser.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			try {
				BufferedImage img = ImageIO.read(selectedFile);

				if (img != null) {
					String[] str = selectedFile.getPath().split("\\.");
					this.format = str[str.length - 1];
					System.out.println(this.format);
					setImage(img, this.format);
				} else {
					JOptionPane.showMessageDialog(null, "File Format Is Not Supported!", "Failed to Load Image", JOptionPane.INFORMATION_MESSAGE);
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error loading image: " + ex.getMessage());
			}
		}
	}

	public void setImage(BufferedImage img, String format) {
		this.image = img;
		this.format = format;
		try {
			this.encodedImage = encodeImage(img, this.format);
		} catch (Exception ex) {
			Logger.getLogger(ImageComponent.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println("encoded:\n" + encodedImage);
		decodeImage(encodedImage);
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g.setColor(Color.black);
		if (image != null) {
			if (encodedImage == null) {
				encodedImage = encodeImage(image, this.format);
			}
		} else if (encodedImage != null) {
			image = decodeImage(encodedImage);
		}
		g.drawImage(image, x * gridSize, y * gridSize, width * gridSize, height * gridSize, null);
		if (properties.get("Border").equals("enable")) {
			g.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		}
		super.paint(g);
	}

	@Override
	public void eval() {
	}

	public static String encodeImage(BufferedImage image, String format) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, format, outputStream);
			byte[] imageBytes = outputStream.toByteArray();
			String encodedImage = Base64.getEncoder().encodeToString(imageBytes);
			outputStream.close();
			return encodedImage;
		} catch (IOException ex) {
			Logger.getLogger(ImageComponent.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public static BufferedImage decodeImage(String encodedImage) {
		byte[] imageBytes = Base64.getDecoder().decode(encodedImage);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(imageBytes);
		BufferedImage image;
		try {
			image = ImageIO.read(inputStream);
			inputStream.close();
//			ImageIO.write(image, "png", new File("/home/yin/simTest/test.jpeg"));
			return image;
		} catch (IOException ex) {
			Logger.getLogger(ImageComponent.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
}
