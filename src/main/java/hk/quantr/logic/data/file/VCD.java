/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.file;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.basic.Clock;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("vcd")
public class VCD extends Vertex {

//	@XStreamOmitField
	public ArrayList<Port> wires = new ArrayList<>();
	public boolean defined = false;
	public int refCnt = 0;
	long time = 0, clock = 0;
	public String timeUnit = "1s";
	private String dataToDump = "";
	private boolean timeFlagDumped = false;
	ArrayList<Output> out = new ArrayList();
	boolean ticked = true;

	public VCD(String name) {
		super(name, 2, 0, 4, 4);
		out.add(new Output(this, this.name + " output 0"));
		out.add(new Output(this, this.name + " output 1"));
		this.inputs.get(0).setLocation(2, 0);
		this.inputs.get(1).setLocation(0, 2);
		this.inputs.get(0).name = "clk";
		this.properties.put("Module", name);
		this.properties.put("No. Of Inputs", 1);
		this.properties.put("Trigger", "positive");
		this.properties.put("input 1", this.inputs.get(1).name);
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(new Color(255, 140, 0));
		g2.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.drawPolyline(new int[]{x * gridSize + width / 4 * gridSize + gridSize / 3, x * gridSize + width / 2 * gridSize, x * gridSize + width * 3 / 4 * gridSize - gridSize / 3}, new int[]{y * gridSize, (y + 1) * gridSize, y * gridSize}, 3);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 2));
		g2.drawString("VCD", x * gridSize + gridSize / 3, y * gridSize + height / 2 * gridSize + gridSize * 3 / 5);
		for (int i = 0; i < this.inputs.size(); i++) {
			inputs.get(i).paint(g, gridSize);
		}
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "VCD";
	}

	@Override
	public void eval() {
		if (this.inputs.get(0).value != this.inputs.get(0).preValue) {
			ticked = true;
			clock++;
			this.inputs.get(0).preValue = this.inputs.get(0).value;
		}
		if (this.properties.get("Trigger").equals("event")) {
			if (ticked) {
				ticked = false;
				timeFlagDumped = false;
				for (int i = 0; i < this.inputs.size(); i++) {
					if (!this.portConnectedTo(this.inputs.get(i)).isEmpty()) {
						if (out.get(i).value != this.inputs.get(i).value || !this.portConnectedTo(this.inputs.get(i)).get(0).vcdDefined) {
							dump(i);
							this.portConnectedTo(this.inputs.get(i)).get(0).vcdDefined = true;
						}
					}
				}
				time++;
			}
		} else {
			if (out.get(0).value != clock && !(this.properties.get("Trigger").equals("positive") ^ clock % 2 == 1)) {
				timeFlagDumped = false;
				for (int i = 0; i < this.inputs.size(); i++) {
					if (!this.portConnectedTo(this.inputs.get(i)).isEmpty()) {
						if (out.get(i).value != this.inputs.get(i).value || !this.portConnectedTo(this.inputs.get(i)).get(0).vcdDefined) {
							if (i == 0 && this.portConnectedTo(this.inputs.get(i)).get(0).vcdDefined) {
								continue;
							}
							dump(i);
							this.portConnectedTo(this.inputs.get(i)).get(0).vcdDefined = true;
						}
					}
				}
				time++;
			}
			out.get(0).value = clock;
		}
	}

	public void dump(int i) {
		if (!timeFlagDumped) {
			dataToDump += "#" + time + "\n";
			timeFlagDumped = true;
		}
		dataToDump += (this.inputs.get(i).bits > 1 ? "b" : "") + Long.toBinaryString(this.portConnectedTo(this.inputs.get(i)).get(0).parent instanceof Clock ? this.inputs.get(i).value % 2 : this.inputs.get(i).value) + (this.inputs.get(i).bits > 1 ? " " : "") + this.portConnectedTo(this.inputs.get(i)).get(0).ref + "\n";
		out.get(i).value = this.inputs.get(i).value;
		System.out.println("dataToDump: " + dataToDump);
	}

	public String wiresToString() {
		String str = "";
		for (Port w : wires) {
			str += "$var wire " + w.bits + ' ' + w.ref + ' ' + w.name.replaceAll(" ", "_") + (w.bits > 1 ? " [" + (w.bits - 1) + ":0]" : "") + " $end\n";
		}
		return str;
	}

	public String initVCDFile() {
		return "$scope module " + name.replaceAll(" ", "_") + " $end\n" + wiresToString() + "$upscope $end\n";
	}

	@Override
	public void updateProperty() {
		int noOfPin = (int) this.properties.get("No. Of Inputs") + 1;
		for (int i = 1; i < this.inputs.size(); i++) {
			this.inputs.get(i).name = (String) this.properties.get("input " + i);
		}
		if (noOfPin > this.inputs.size()) {
			for (int i = this.inputs.size() + 1; i <= noOfPin; i++) {
				this.inputs.add(new Input(this, this.name + " input " + (i - 1)));
				out.add(new Output(this, this.name + " output " + (i - 1)));
				this.properties.put("input " + (i - 1), (this.inputs.get(i - 1).name != null ? this.inputs.get(i - 1).name : this.portConnectedTo(this.inputs.get(i - 1)).get(0).name));
			}
		} else {
			for (int i = this.inputs.size() - 1; i >= noOfPin; i--) {
				this.inputs.get(i).edges.clear();
				this.inputs.remove(i);
				out.get(i).edges.clear();
				out.remove(i);
				this.properties.remove("input " + i);
			}
		}
//		this.setSize(this.width, Math.max((this.inputs.size() - 1) - (this.inputs.size() - 1) % 2, 4));

		if (this.inputs.size() - 1 == 1) {
			this.setSize(this.width, 4);
		} else {
			this.setSize(this.width, this.inputs.size() < 4 ? 4 : this.inputs.size());
		}

		int cnt = 1;
		for (int i = 1; i < this.inputs.size(); i++) {
			this.inputs.get(i).deltaX = 0;
			if (this.inputs.size() == 2) {
				this.inputs.get(i).deltaY = 2;
			} else {
				this.inputs.get(i).deltaY = i;//cnt++ + (cnt > (this.inputs.size() - 1) / 2 ? (this.inputs.size() - 1) % 2 ^ 1 : 0) + (this.inputs.size() - 1 <= 3 ? (this.inputs.size() - 1 < 2 ? 2 : 1) : 0);
			}
			if (!this.portConnectedTo(this.inputs.get(i)).isEmpty()) {
				this.inputs.get(i).bits = this.portConnectedTo(this.inputs.get(i)).get(0).bits;
			}
		}
		this.timeUnit = (!this.portConnectedTo(this.inputs.get(0)).isEmpty() && this.portConnectedTo(this.inputs.get(0)).get(0).parent instanceof Clock) ? getTimeUnit(((Clock) this.portConnectedTo(this.inputs.get(0)).get(0).parent).hertz) : "1s";
	}

	public String getTimeUnit(int hertz) {
		String[] str = {"s", "ms", "us", "ns", "ps", "fs"};
		return String.format("%.0f%s", (1 + 1e-18) / hertz * Math.pow(1000, (short) Math.ceil(Math.log(hertz) / Math.log(1000))), str[(short) Math.ceil(Math.log(hertz) / Math.log(1000))]);
	}

	@Override
	public void simulateInit() {
		this.defined = false;
		refCnt = 0;
		time = 0;
		timeUnit = "1s";
		dataToDump = "";
		this.wires.clear();
		super.simulateInit();
		for (int i = 0; i < this.inputs.size(); i++) {
			if (!this.portConnectedTo(this.inputs.get(i)).isEmpty()) {
				this.portConnectedTo(this.inputs.get(i)).get(0).vcdDefined = false;
			}
		}
	}

	public synchronized String getDataToDump() {
		String str = dataToDump;
		dataToDump = "";
		return str;
	}
}
