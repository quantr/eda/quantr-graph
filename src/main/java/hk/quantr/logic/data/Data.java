/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import hk.quantr.logic.arduino.ArduinoHeltecWifiOled;
import hk.quantr.logic.arduino.ArduinoSerial;
import hk.quantr.logic.arduino.ArduinoSerialOled;
import hk.quantr.logic.arduino.ArduinoWifi;
import hk.quantr.logic.arduino.ArduinoWifiOled;
import hk.quantr.logic.data.arithmetic.Adder;
import hk.quantr.logic.data.arithmetic.Shifter;
import hk.quantr.logic.data.arithmetic.Divider;
import hk.quantr.logic.data.arithmetic.Multiplier;
import hk.quantr.logic.data.arithmetic.Negator;
import hk.quantr.logic.data.arithmetic.Subtractor;
import hk.quantr.logic.data.basic.BitExtender;
import hk.quantr.logic.data.basic.Button;
import hk.quantr.logic.data.basic.Clock;
import hk.quantr.logic.data.basic.Combiner;
import hk.quantr.logic.data.basic.Constant;
import hk.quantr.logic.data.basic.DipSwitch;
import hk.quantr.logic.data.basic.Led;
import hk.quantr.logic.data.basic.LedBar;
import hk.quantr.logic.data.basic.Pin;
import hk.quantr.logic.data.basic.Probe;
import hk.quantr.logic.data.basic.RGBLed;
import hk.quantr.logic.data.basic.Random;
import hk.quantr.logic.data.basic.Splitter;
import hk.quantr.logic.data.basic.Switch;
import hk.quantr.logic.data.basic.Tunnel;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.data.flipflop.DFlipflop;
import hk.quantr.logic.data.flipflop.DLatch;
import hk.quantr.logic.data.flipflop.JKFlipflop;
import hk.quantr.logic.data.flipflop.Ram;
import hk.quantr.logic.data.flipflop.SRFlipflop;
import hk.quantr.logic.data.flipflop.TFlipflop;
import hk.quantr.logic.data.gate.AndGate;
import hk.quantr.logic.data.gate.Buffer;
import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.EvenParity;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Module;
import hk.quantr.logic.data.gate.NandGate;
import hk.quantr.logic.data.gate.NorGate;
import hk.quantr.logic.data.gate.NotGate;
import hk.quantr.logic.data.gate.OddParity;
import hk.quantr.logic.data.gate.OrGate;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Point;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.TriStateBuffer;
import hk.quantr.logic.data.gate.TriStateNotBuffer;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.logic.data.gate.XnorGate;
import hk.quantr.logic.data.gate.XorGate;
import hk.quantr.logic.data.graphics.ImageComponent;
import hk.quantr.logic.data.output.EightSegmentDisplay;
import hk.quantr.logic.data.output.HexDisplay;
import hk.quantr.logic.data.output.TTY;
import hk.quantr.logic.data.plexer.BitSelector;
import hk.quantr.logic.data.plexer.Decoder;
import hk.quantr.logic.data.plexer.Demultiplexer;
import hk.quantr.logic.data.plexer.Encoder;
import hk.quantr.logic.data.plexer.Multiplexer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("data")
public class Data implements Cloneable {

	public String projectName;
	public HashMap<String, Module> modules = new HashMap<>();

	@Override
	public Object clone() throws CloneNotSupportedException {
		Data d = new Data();
		for (String key : this.modules.keySet()) {
			d.modules.put(key, (Module) this.modules.clone());
		}
		return d;
	}

	public void save(File file) {
		String xml = getXML();

		try {
			IOUtils.write(xml, new FileOutputStream(file), "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getXML() {
		XStream xstream = new XStream();
		xstream.autodetectAnnotations(true);
		for (int i = 0; i < Edge.getFieldsToOmit().length; i++) {
			xstream.omitField(Edge.class, Edge.getFieldsToOmit()[i]);
		}
		xstream.omitField(ArduinoSerial.class, "port");
		xstream.omitField(ArduinoSerialOled.class, "port");
		xstream.omitField(ArduinoWifi.class, "port");
		xstream.omitField(ArduinoWifiOled.class, "port");
		xstream.omitField(ArduinoHeltecWifiOled.class, "port");
		String xml = xstream.toXML(this);
		return xml;
	}

	public static Data load(String ql) {
		XStream xstream = new XStream(new StaxDriver());
		xstream.autodetectAnnotations(true);
		xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
		xstream.processAnnotations(new Class[]{Adder.class, Divider.class, Multiplier.class, Negator.class, Shifter.class, Subtractor.class, BitExtender.class, Button.class,
			Clock.class, Combiner.class, Constant.class, DipSwitch.class, Led.class, LedBar.class, Pin.class, Probe.class, RGBLed.class, Random.class,
			Splitter.class, Switch.class, Tunnel.class, VCD.class, DFlipflop.class, DLatch.class, JKFlipflop.class, Ram.class, SRFlipflop.class, TFlipflop.class, AndGate.class,
			Buffer.class, Data.class, Edge.class, EvenParity.class, Input.class, Module.class, NandGate.class, NorGate.class, NotGate.class,
			OddParity.class, OrGate.class, Output.class, Point.class, Port.class, TriStateBuffer.class, TriStateNotBuffer.class, Vertex.class, XnorGate.class, XorGate.class,
			EightSegmentDisplay.class, HexDisplay.class, TTY.class, BitSelector.class, Decoder.class, Demultiplexer.class, Encoder.class, Multiplexer.class, ImageComponent.class});

		xstream.allowTypesByWildcard(new String[]{"hk.quantr.logic.**"});
		xstream.ignoreUnknownElements();
		try {
			FileUtils.writeStringToFile(new File("/Users/peter/Desktop/fuck.ql"), ql, "utf-8");
		} catch (IOException ex) {
			Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
		}
		Data data = (Data) xstream.fromXML(ql);
		return data;
	}

	public static Data load(File file) {
		XStream xstream = new XStream(new StaxDriver());
		xstream.autodetectAnnotations(true);
		xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
		xstream.processAnnotations(new Class[]{Adder.class, Divider.class, Multiplier.class, Negator.class, Shifter.class, Subtractor.class, BitExtender.class, Button.class,
			Clock.class, Combiner.class, Constant.class, DipSwitch.class, Led.class, LedBar.class, Pin.class, Probe.class, RGBLed.class, Random.class,
			Splitter.class, Switch.class, Tunnel.class, VCD.class, DFlipflop.class, DLatch.class, JKFlipflop.class, Ram.class, SRFlipflop.class, TFlipflop.class, AndGate.class,
			Buffer.class, Data.class, Edge.class, EvenParity.class, Input.class, Module.class, NandGate.class, NorGate.class, NotGate.class,
			OddParity.class, OrGate.class, Output.class, Point.class, Port.class, TriStateBuffer.class, TriStateNotBuffer.class, Vertex.class, XnorGate.class, XorGate.class,
			EightSegmentDisplay.class, HexDisplay.class, TTY.class, BitSelector.class, Decoder.class, Demultiplexer.class, Encoder.class, Multiplexer.class, ImageComponent.class});

		xstream.allowTypesByWildcard(new String[]{"hk.quantr.logic.**"});
		xstream.ignoreUnknownElements();
		Data data = (Data) xstream.fromXML(file);
		return data;
	}

	@Override
	public String toString() {
		String str = "Data{\n";
		for (Module m : this.modules.values()) {
			str += "  " + m.name + "{\n";
			for (Vertex v : m.vertices) {
				str += "    " + v.name + " [" + v.x + '/' + v.y + ']' + '\n';
			}
			str += "  }\n";
		}
		str += "}";
		return str;
	}

}
