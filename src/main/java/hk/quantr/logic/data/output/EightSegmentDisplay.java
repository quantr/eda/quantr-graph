/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.output;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import static hk.quantr.logic.LogicGateDrawer.init;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("eightSegmentDisplay")
public class EightSegmentDisplay extends Vertex {

	public EightSegmentDisplay(String name) {
		super(name, 8, 0, 5, 6);
		properties.put("Name", name);
		inputs.get(0).setLocation(1, 0); //segment G
		inputs.get(1).setLocation(2, 0); //segment F
		inputs.get(2).setLocation(3, 0); //segment A
		inputs.get(3).setLocation(4, 0); //segment B
		inputs.get(4).setLocation(1, 6); //segment E
		inputs.get(5).setLocation(2, 6); //segment D
		inputs.get(6).setLocation(3, 6); //segment C
		inputs.get(7).setLocation(4, 6); //decimal point
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize + gridSize / 2, y * gridSize, width * gridSize - gridSize, height * gridSize);

		if (this.inputs.get(0).value == 0) {// segment G
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize * 9 / 5, gridSize * 3 / 10, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize * 9 / 5, gridSize * 3 / 10, gridSize / 4, gridSize / 4);
		}

		if (this.inputs.get(1).value == 0) {//segment F
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		}

		if (this.inputs.get(2).value == 0) { //segment A
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize * 9 / 5, gridSize * 3 / 10, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize * 9 / 5, gridSize * 3 / 10, gridSize / 4, gridSize / 4);
		}
		if (this.inputs.get(3).value == 0) { //segment B
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		}
		if (this.inputs.get(4).value == 0) { //segment E
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		}
		if (this.inputs.get(5).value == 0) { //segment D
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize * 9 / 5, gridSize * 3 / 10, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize * 9 / 5, gridSize * 3 / 10, gridSize / 4, gridSize / 4);

		}
		if (this.inputs.get(6).value == 0) { //segment C
			g2.setColor(Color.lightGray);
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		} else {
			g2.setColor(Color.red);
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize * 3 / 10, gridSize * 9 / 5, gridSize / 4, gridSize / 4);
		}
		if (this.inputs.get(7).value == 0) { //decimal point
			g2.setColor(Color.lightGray);
			g2.fillOval((x + 4) * gridSize - gridSize / 4, (y + 5) * gridSize - gridSize / 4, gridSize / 2, gridSize / 2);
		} else {
			g2.setColor(Color.red);
			g2.fillOval((x + 4) * gridSize - gridSize / 4, (y + 5) * gridSize - gridSize / 4, gridSize / 2, gridSize / 2);
		}
		for (int i = 0; i < this.inputs.size(); i++) {
			this.inputs.get(i).paint(g, gridSize);
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width + 1)) * gridSize - gridSize * 4 / 10, (y + (this.height / 2)) * gridSize + gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "8 Segment Display";
	}

	@Override
	public void eval() {
	}
}
