/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.output;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("hexDisplay")
public class HexDisplay extends Vertex {

	public boolean dot = true;

	public HexDisplay(String name) {
		super(name, 2, 0, 5, 6);
		properties.put("Name", name);
		properties.put("Decimal Point", Boolean.toString(dot));
		inputs.get(0).bits = 4;
		inputs.get(0).setLocation(2, 6);
		inputs.get(1).setLocation(4, 6);
		inputs.get(0).value = -1;
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize + gridSize / 2, y * gridSize, width * gridSize - gridSize, height * gridSize);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize + gridSize / 2, y * gridSize, width * gridSize - gridSize, height * gridSize);
		g2.setColor(new Color(170, 170, 170, 150));
		g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
		g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
		g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
		g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
		g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
		g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
		g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		if (dot) {
			g2.fillOval((x + 4) * gridSize, (y + 5) * gridSize, gridSize / 2, gridSize / 2); //dot
		}
		g2.setColor(Color.red);
		if (this.inputs.get(0).value == 0) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
		} else if (this.inputs.get(0).value == 1) {
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
		} else if (this.inputs.get(0).value == 2) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 3) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 4) {
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 5) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 6) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 7) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
		} else if (this.inputs.get(0).value == 8) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 9) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 10) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 11) {
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 12) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
		} else if (this.inputs.get(0).value == 13) {
			g2.fillRoundRect((x + 3) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //b
			g2.fillRoundRect((x + 3) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //c
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 14) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 5) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //d
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		} else if (this.inputs.get(0).value == 15) {
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 1) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //a
			g2.fillRoundRect((x + 1) * gridSize, (y + 3) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //e
			g2.fillRoundRect((x + 1) * gridSize, (y + 1) * gridSize + gridSize / 5, gridSize / 10 * 3, gridSize / 5 * 8, gridSize / 5, gridSize / 5); //f
			g2.fillRoundRect((x + 1) * gridSize + gridSize / 5, (y + 3) * gridSize, gridSize / 5 * 8, gridSize / 10 * 3, gridSize / 5, gridSize / 5); //g
		}
		if (dot && this.inputs.get(1).value == 1) {
			g2.fillOval((x + 4) * gridSize, (y + 5) * gridSize, gridSize / 2, gridSize / 2); //dot
		}
		for (int i = 0; i < (dot ? 2 : 1); i++) {
			this.inputs.get(i).paint(g, gridSize);
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize, (y - 1) * gridSize + gridSize * 4 / 10);
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Hex Display";
	}

	@Override
	public void eval() {
	}

	@Override
	public void updateProperty() {
		this.dot = this.properties.get("Decimal Point").equals("true");
	}
}
