/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.output;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author michelle
 */
@XStreamAlias("tty")
public class TTY extends Vertex {

	ArrayList<Character> text = new ArrayList();

	public TTY(String name) {
		super(name, 4, 0, 29, 12);
		properties.put("Name", name);
		inputs.get(0).setLocation(0, 1); // ascii code
		inputs.get(0).bits = 7;
		inputs.get(1).setLocation(0, 5); // clock
		inputs.get(2).setLocation(0, 3); // write enable
		inputs.get(3).setLocation(0, 10); // clear
		text.add('|');
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, 4 * gridSize, height * gridSize);
		g2.fillRect((x + 4) * gridSize, y * gridSize, (width - 4) * gridSize, height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect((x + 4) * gridSize, y * gridSize, (width - 4) * gridSize, height * gridSize);
		g2.drawRect(x * gridSize, y * gridSize, 4 * gridSize, height * gridSize);
		g2.setFont(new Font("monospaced", Font.PLAIN, gridSize / 5 * 6));
		String temp = "";
		for (int i = (this.text.size() / 32 >= 6 ? (this.text.size() / 32 - 6) * 32 : 0); i < this.text.size(); i++) {
			if (i % 32 == 0 && i != 0) {
				g2.drawString(temp, (x + 5) * gridSize, (y) * gridSize + (1 + i / 32 * 3 - (this.text.size() / 32 >= 6 ? (this.text.size() / 32 - 6) : 0) * 3) * gridSize / 2);
				temp = "";
			}
			temp += this.text.get(i);
		}
		g2.drawString(temp, (x + 5) * gridSize, (y) * gridSize + (4 + (this.text.size() / 32 * 3 - (this.text.size() % 32 == 0 ? 1 : 0) * 3) - (this.text.size() / 32 >= 6 ? (this.text.size() / 32 - 6) : 0) * 3) * gridSize / 2);
		g2.setFont(new Font("arial", Font.PLAIN, gridSize));
		g2.drawString("ascii", this.inputs.get(0).getAbsolutionX() * gridSize + gridSize / 2, this.inputs.get(0).getAbsolutionY() * gridSize + gridSize / 3);
		g2.drawString("enable", this.inputs.get(2).getAbsolutionX() * gridSize + gridSize / 2, this.inputs.get(2).getAbsolutionY() * gridSize + gridSize / 3);
		g2.drawString("clear", this.inputs.get(3).getAbsolutionX() * gridSize + gridSize / 2, this.inputs.get(3).getAbsolutionY() * gridSize + gridSize / 3);
		g2.drawPolyline(new int[]{x * gridSize, (x + 1) * gridSize, x * gridSize}, new int[]{y * gridSize + 9 * gridSize / 2, (y + 5) * gridSize, y * gridSize + 11 * gridSize / 2}, 3);
		g2.setColor(Color.blue);
		int ovalSize = gridSize / 10;
		g2.fillOval(this.inputs.get(1).getAbsolutionX() * gridSize - ovalSize / 2, this.inputs.get(1).getAbsolutionY() * gridSize - ovalSize / 2, ovalSize, ovalSize);
		this.inputs.get(0).paint(g, gridSize);
		this.inputs.get(2).paint(g, gridSize);
		this.inputs.get(3).paint(g, gridSize);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize, (y - 1) * gridSize + gridSize * 4 / 10);
		super.paint(g);
	}

	@Override
	public void simulateInit() {
		super.simulateInit();
		this.text.clear();
		this.text.add('|');
	}

	@Override
	public void eval() {
		if (this.inputs.get(1).preValue == 0 && this.inputs.get(1).value == 1) {
			if (this.inputs.get(2).value == 1) {
				if (this.inputs.get(0).value >= 32 && this.inputs.get(0).value <= 126) {
					this.text.add(this.text.size() - 1, (char) this.inputs.get(0).value);
				} else if (this.inputs.get(0).value == 8) {
					if (this.text.size() > 1) {
						this.text.remove(this.text.size() - 2);
					}
				} else if (this.inputs.get(0).value == 10) {
					int temp = this.text.size() - 1;
					for (int i = 1; i <= 32 - temp % 32; i++) {
						this.text.add(this.text.size() - 1, ' ');
					}
				}
			}
		}
		this.inputs.get(1).preValue = this.inputs.get(1).value;

		if (this.inputs.get(3).value == 1) {
			this.text.clear();
			this.text.add('|');
		}
	}

	@Override
	public void eval2() {
		if (this.inputs.get(1).preValue == 0 && this.inputs.get(1).value == 1) {
			if (this.inputs.get(2).value == 1) {
				if (this.inputs.get(0).value >= 32 && this.inputs.get(0).value <= 126) {
					this.text.add(1, (char) this.inputs.get(0).value);
				} else if (this.inputs.get(0).value == 8) {
					if (this.text.size() > 1) {
						this.text.remove(1);
					}
				} else if (this.inputs.get(0).value == 10) {
					int temp = this.text.size() - 1;
					for (int i = 1; i <= 32 - temp % 32; i++) {
						this.text.add(1, ' ');
					}
				}
			}
		}
		this.inputs.get(1).preValue = this.inputs.get(1).value;

		if (this.inputs.get(3).value == 1) {
			this.text.clear();
			this.text.add('|');
		}
		if (this.text.size() > 1000) {
			this.text.remove(0);
			this.text.remove(1);
		}
	}
}
