
/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.flipflop;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@XStreamAlias("jkFlipflop")
public class JKFlipflop extends Vertex {

	public JKFlipflop(String name) {
		super(name, 5, 2, 6, 6);
		properties.put("Name", name);
		//input 0 = J, input 1 = Enable, input 2 = K, input 3 = preset, input 4 = clear
		inputs.get(0).setLocation(0, 1);
		inputs.get(1).setLocation(0, 3);
		inputs.get(2).setLocation(0, 5);
		inputs.get(3).setLocation(3, 0);
		inputs.get(4).setLocation(3, 6);
		outputs.get(0).setLocation(width, height / 2 - 2);
		outputs.get(1).setLocation(width, height / 2 + 2);
		outputs.get(1).name += "_bar";
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		LogicGateDrawer.drawJKFlipflop(g, this, this.x * gridSize, this.y * gridSize, this.gridSize, stroke);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 5, (y + (this.height / 2) + 1) * gridSize - gridSize / 2);
		super.paint(g);
	}

	@Override
	public void eval() {
		//input 0 = J, input 1 = Clock, input 2 = K, input 3 = preset, input 4 = clear
		if (this.inputs.get(3).preValue == 0 && this.inputs.get(4).preValue == 1) { // preset/clear = 01
			this.outputs.get(0).value = 0;
			this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
		} else if (this.inputs.get(3).preValue == 1 && this.inputs.get(4).preValue == 0) { // preset/clear = 10
			this.outputs.get(0).value = 1;
			this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
		} else if (this.inputs.get(3).preValue == 1 && this.inputs.get(4).preValue == 1) { // present/clear = 11
			if (this.inputs.get(1).preValue == 0 && this.inputs.get(1).value == 1) { //clk positive edge
				if (this.inputs.get(0).preValue == 0 && this.inputs.get(2).preValue == 0) { // JK = 00
					this.outputs.get(0).value = outputs.get(0).preValue;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				} else if (this.inputs.get(0).preValue == 0 && this.inputs.get(2).preValue == 1) {// JK = 01
					this.outputs.get(0).value = 0;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				} else if (this.inputs.get(0).preValue == 1 && this.inputs.get(2).preValue == 0) {// JK = 10
					this.outputs.get(0).value = 1;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				} else if (this.inputs.get(0).preValue == 1 && this.inputs.get(2).preValue == 1) {// JK = 11
					this.outputs.get(0).value = outputs.get(0).preValue ^ 1;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				}
			}
		} else if (this.inputs.get(3).preValue == 0 && this.inputs.get(4).preValue == 0) { // present/clear = 00
			if (this.inputs.get(1).preValue == 0 && this.inputs.get(1).value == 1) { //clk positive edge
				if (this.inputs.get(0).preValue == 0 && this.inputs.get(2).preValue == 0) { // JK = 00
					this.outputs.get(0).value = outputs.get(0).preValue;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				} else if (this.inputs.get(0).preValue == 0 && this.inputs.get(2).preValue == 1) {// JK = 01
					this.outputs.get(0).value = 0;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				} else if (this.inputs.get(0).preValue == 1 && this.inputs.get(2).preValue == 0) {// JK = 10
					this.outputs.get(0).value = 1;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				} else if (this.inputs.get(0).preValue == 1 && this.inputs.get(2).preValue == 1) {// JK = 11
					this.outputs.get(0).value = outputs.get(0).preValue ^ 1;
					this.outputs.get(1).value = this.outputs.get(0).value ^ 1;
				}
			}
		}

		this.inputs.get(0).preValue = this.inputs.get(0).value;
		this.inputs.get(1).preValue = this.inputs.get(1).value;
		this.inputs.get(2).preValue = this.inputs.get(2).value;
		this.inputs.get(3).preValue = this.inputs.get(3).value;
		this.inputs.get(4).preValue = this.inputs.get(4).value;
		this.outputs.get(0).preValue = this.outputs.get(0).value;
		this.outputs.get(1).preValue = this.outputs.get(1).value;

		this.outputs.get(1).value = this.outputs.get(0).value ^ 1;

		super.eval();
	}

	@Override
	public void updateProperty() {
	}

	@Override
	public String getTypeName() {
		return "JK FlipFlop";
	}
}
