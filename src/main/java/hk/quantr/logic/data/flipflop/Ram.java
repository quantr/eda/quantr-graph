/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.flipflop;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("ram")
public class Ram extends Vertex {

	public int addressBitWidth = 8;
	public int dataBitWidth = 8;
	final String[] hex = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f "};
	long[] data = new long[(int) Math.pow(2, 8)];
	private int page = 1;
	int col = 0;

	public Ram(String name) {
		super(name, 4, 1, 20, 27);
		properties.put("Name", name);
		properties.put("Address Bit Width", addressBitWidth);
		properties.put("Data Bit Width", dataBitWidth);
		properties.put("Page", page);
		inputs.get(0).setLocation(0, 3); //address
		inputs.get(0).bits = 8;
		inputs.get(1).setLocation(0, 5); //data
		inputs.get(1).bits = 8;
		inputs.get(2).setLocation(0, 7); //read or write
		inputs.get(3).setLocation(0, 9); //clock
		outputs.get(0).setLocation(width, 5);
		outputs.get(0).bits = 8;
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 6 / 5));
		g2.drawString("RAM " + Integer.toString((int) Math.pow(2, addressBitWidth)) + " x " + Integer.toString(dataBitWidth), x * gridSize + (22 - ("RAM " + Integer.toString((int) Math.pow(2, addressBitWidth)) + " x " + Integer.toString(dataBitWidth)).length()) * gridSize / 2, y * gridSize + 3 * gridSize / 2);
		g2.drawLine(x * gridSize, (y + 2) * gridSize, (x + width) * gridSize, (y + 2) * gridSize);
		g2.drawString(Integer.toString(this.addressBitWidth), (x + 1) * gridSize, y * gridSize + 7 * gridSize / 2);
		g2.drawString(Integer.toString(this.dataBitWidth), (x + 1) * gridSize, y * gridSize + 11 * gridSize / 2);
		g2.drawString(Integer.toString(this.dataBitWidth), (x + width - 1 - Integer.toString(this.dataBitWidth).length()) * gridSize, y * gridSize + 11 * gridSize / 2);
		g2.drawPolyline(new int[]{x * gridSize, (x + 1) * gridSize, x * gridSize}, new int[]{y * gridSize + 17 * gridSize / 2, (y + 9) * gridSize, y * gridSize + 19 * gridSize / 2}, 3);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.PLAIN, gridSize * 6 / 5));
		g2.drawString("Addr", (x + 1 + Integer.toString(this.addressBitWidth).length()) * gridSize, y * gridSize + 7 * gridSize / 2);
		g2.drawString("Data In", (x + 1 + Integer.toString(this.dataBitWidth).length()) * gridSize, y * gridSize + 11 * gridSize / 2);
		g2.drawString("Data Out", (x + width - 7 - Integer.toString(this.dataBitWidth).length()) * gridSize, y * gridSize + 11 * gridSize / 2);
		g2.drawString("Write / Read", (x + 1) * gridSize, y * gridSize + 15 * gridSize / 2);

		g2.setColor(Color.black);
		g2.setFont(new Font("arial", Font.PLAIN, gridSize));
		col = (28 - this.addressBitWidth / 4 + (this.addressBitWidth % 4 != 0 ? 1 : 0)) / (dataBitWidth / 4 + (dataBitWidth % 4 != 0 ? 1 : 0) + 1);
		for (int i = 0; i < 8; i++) {
			String ram = "";
			if (((page - 1) * col * 8 + i * col) < this.data.length) {
				ram += toHex((page - 1) * col * 8 + i * col, addressBitWidth) + " | ";
				for (int j = 0; j < col; j++) {
					if (((page - 1) * col * 8 + j + i * col) < this.data.length && ((page - 1) * col * 8 + j + i * col) >= 0) {
						ram += toHex(data[(page - 1) * col * 8 + j + i * col], dataBitWidth) + " ";
					}
				}
			}
			g2.drawString(ram, (x + 2) * gridSize, (y + i * 2) * gridSize + 19 * gridSize / 2);
		}
		g2.setStroke(new BasicStroke(this.gridSize / 7, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2.drawRect((x + 5) * gridSize, (y + height) * gridSize - 5 * gridSize / 2, 4 * gridSize, 2 * gridSize);
		g2.drawRect((x + 11) * gridSize, (y + height) * gridSize - 5 * gridSize / 2, 4 * gridSize, 2 * gridSize);
		g2.setFont(new Font("arial", Font.PLAIN, gridSize));
		g2.drawString("Prev", (x + 6) * gridSize, (y + height - 1) * gridSize);
		g2.drawString("Next", (x + 12) * gridSize, (y + height - 1) * gridSize);
		g2.setColor(Color.blue);
		int ovalSize = gridSize / 10;
		g2.fillOval(this.inputs.get(3).getAbsolutionX() * gridSize - ovalSize / 2, this.inputs.get(3).getAbsolutionY() * gridSize - ovalSize / 2, ovalSize, ovalSize);
		ovalSize = gridSize * 2 / 3;
		for (int i = 0; i < this.inputs.size() - 1; i++) {
			g2.fillOval(this.inputs.get(i).getAbsolutionX() * gridSize - ovalSize / 2, this.inputs.get(i).getAbsolutionY() * gridSize - ovalSize / 2, ovalSize, ovalSize);
		}
		g2.setColor(Color.green);
		g2.fillOval(this.outputs.get(0).getAbsolutionX() * gridSize - ovalSize / 2, this.outputs.get(0).getAbsolutionY() * gridSize - ovalSize / 2, ovalSize, ovalSize);
		g2.setStroke(stroke);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize, (y - 1) * gridSize + gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public void eval() {
		if (this.inputs.get(3).preValue == 0 && this.inputs.get(3).value == 1) { // clk positive edge
			if (this.inputs.get(2).value == 1) {// read
				this.outputs.get(0).value = data[(int) this.inputs.get(0).value] & ((int) Math.pow(2, this.dataBitWidth) - 1);
			} else {
				data[(int) this.inputs.get(0).value] = this.inputs.get(1).value;
			}
		}
		this.inputs.get(3).preValue = this.inputs.get(3).value;
		super.eval();
	}

	@Override
	public void simulateInit() {
		super.simulateInit();
		data = new long[(int) Math.pow(2, this.addressBitWidth)];
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		if (this.addressBitWidth != (int) properties.get("Address Bit Width")) {
			page = 0;
			if ((int) properties.get("Address Bit Width") > 28) {
				properties.put("Address Bit Width", 28);
			} else if ((int) properties.get("Address Bit Width") == 0) {
				properties.put("Address Bit Width", 1);
			}
			long[] temp = new long[(int) Math.pow(2, (int) properties.get("Address Bit Width"))];
			System.arraycopy(this.data, 0, temp, 0, (int) Math.pow(2, (int) properties.get("Address Bit Width") > this.addressBitWidth ? this.addressBitWidth : (int) properties.get("Address Bit Width")));
			this.addressBitWidth = (int) properties.get("Address Bit Width");
			this.inputs.get(0).bits = this.addressBitWidth;
			this.inputs.get(0).value = 0;
			this.data = temp;
		}
		if (this.dataBitWidth != (int) properties.get("Data Bit Width")) {
			page = 1;
		}
		if (this.page != (int) properties.get("Page")) {
			if ((((int) properties.get("Page")) * 8 * col) < this.data.length) {
				page = (int) properties.get("Page");
			} else {
				if (this.data.length / 8 / col > 0) {
					page = this.data.length / 8 / col;
				} else {
					page = 1;
				}
				properties.put("Page", page);
			}
		}
		if ((int) properties.get("Data Bit Width") != 64) {
			if ((int) properties.get("Data Bit Width") > 64) {
				this.dataBitWidth = 64;
				this.properties.put("Data Bit Width", 64);
			} else {
				this.dataBitWidth = (int) properties.get("Data Bit Width");
			}
		}
		this.outputs.get(0).bits = this.dataBitWidth;
		this.inputs.get(1).bits = this.dataBitWidth;
		this.outputs.get(0).value = 0;
	}

	private String toHex(long value, int bitWidth) {
		String temp = "";
		while (bitWidth > 0) {
			temp = hex[(int) value & ((int) Math.pow(2, bitWidth) - 1) & 15] + temp;
			value = value >> (bitWidth >= 4 ? 4 : bitWidth);
			bitWidth -= 4;
		}
		return temp;
	}

	public void pageClicked(int x, int y) {
		if (x > (this.x + 5) * gridSize && x < (this.x + 9) * gridSize && y > (this.y + height) * gridSize - 5 * gridSize / 2 && y < (this.y + height) * gridSize - 1 * gridSize / 2) {
			if (page <= 1) {
				return;
			} else {
				page--;
			}
			properties.put("Page", page);
		} else if (x > (this.x + 11) * gridSize && x < (this.x + 15) * gridSize && y > (this.y + height) * gridSize - 5 * gridSize / 2 && y < (this.y + height) * gridSize - 1 * gridSize / 2) {
			if ((page) * 8 * col >= this.data.length) {
				return;
			} else {
				page++;
			}
			properties.put("Page", page);
		}
	}

	@Override
	public String getTypeName() {
		return "Ram";
	}
}
