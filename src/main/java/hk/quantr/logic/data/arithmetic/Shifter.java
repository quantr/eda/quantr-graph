/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.arithmetic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("shifter")
public class Shifter extends Vertex {

	public Shifter(String name) {
		super(name, 2, 1, 4, 4);
		properties.put("Name", name);
		properties.put("Bits", 4);
		properties.put("Shift Type", "logical left");
		this.outputs.get(0).bits = 4;
		this.inputs.get(0).bits = 4;
		this.inputs.get(1).bits = 2;
		this.inputs.get(0).setLocation(0, 1);	// value
		this.inputs.get(1).setLocation(0, 3);	// dist
		this.outputs.get(0).setLocation(4, 2);	// result
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		LogicGateDrawer.drawShifter(g, this, this.x * gridSize, this.y * gridSize, this.gridSize, stroke, (String) properties.get("Shift Type"));
		if (isSelected) {
			g.setColor(Color.blue);
			g.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
	}

	@Override
	public String getTypeName() {
		return "Shifter";
	}

	@Override
	public void eval() {
		if (properties.get("Shift Type").equals("logical left")) {
			this.outputs.get(0).value = (this.inputs.get(0).value << this.inputs.get(1).value) & ((1 << this.outputs.get(0).bits) - 1);
		} else if (properties.get("Shift Type").equals("logical right")) {
			this.outputs.get(0).value = (this.inputs.get(0).value >> this.inputs.get(1).value) & ((1 << this.outputs.get(0).bits) - 1);
		} else if (properties.get("Shift Type").equals("arithmetic right")) {
//			System.out.println("shift: " + ((this.inputs.get(0).value >>> this.inputs.get(1).value) & ((1 << this.outputs.get(0).bits) - 1)));
			this.outputs.get(0).value = (this.inputs.get(0).value >>> this.inputs.get(1).value) & ((1 << this.outputs.get(0).bits) - 1);
		}
//		System.out.println("Shifter: " + this.outputs.get(0).value);
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.outputs.get(0).bits = (int) this.properties.get("Bits");
		this.inputs.get(0).bits = (int) this.properties.get("Bits");
		this.inputs.get(1).bits = (int) Math.ceil(Math.log(this.inputs.get(0).bits) / Math.log(2));
	}
}
