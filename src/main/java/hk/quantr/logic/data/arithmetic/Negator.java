/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.arithmetic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("negator")
public class Negator extends Vertex {

	public Negator(String name) {
		super(name, 1, 1, 4, 4);
		properties.put("Name", name);
		properties.put("Bits", 4);
		this.outputs.get(0).bits = 4;
		this.inputs.get(0).bits = 4;
		this.inputs.get(0).setLocation(0, 2);	// value
		this.outputs.get(0).setLocation(4, 2);	// result
	}

	@Override
	public void paint(Graphics g) {
//		if (this.inputs.get(0).bits == 1) {
//			System.out.println("bits == 1");
////			Thread.sleep(1);
//			int x = 1 / 0;
//		}
//		System.out.println(this.inputs.get(0).bits);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		LogicGateDrawer.drawNegator(g, this, this.x * gridSize, this.y * gridSize, this.gridSize, stroke);
		if (isSelected) {
			g.setColor(Color.blue);
			g.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
	}

	@Override
	public String getTypeName() {
		return "Negator";
	}

	@Override
	public void eval() {
		this.outputs.get(0).value = (long) ((Math.pow(2, this.outputs.get(0).bits) - this.inputs.get(0).value) % Math.pow(2, this.outputs.get(0).bits));
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.outputs.get(0).bits = (int) this.properties.get("Bits");
		this.inputs.get(0).bits = (int) this.properties.get("Bits");
	}
}
