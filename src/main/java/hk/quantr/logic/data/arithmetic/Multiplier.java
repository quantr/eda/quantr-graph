/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.arithmetic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("multiplier")
public class Multiplier extends Vertex {

	public Multiplier(String name) {
		super(name, 3, 2, 4, 4);
		properties.put("Name", name);
		properties.put("Bits", 4);
		this.outputs.get(0).bits = 4;
		this.outputs.get(1).bits = 4;
		this.inputs.get(0).bits = 4;
		this.inputs.get(1).bits = 4;
		this.inputs.get(2).bits = 4;
		this.inputs.get(0).setLocation(0, 1);	// A
		this.inputs.get(1).setLocation(0, 3);	// B
		this.inputs.get(2).setLocation(2, 0);	// cin
		this.outputs.get(0).setLocation(4, 2);	// lower
		this.outputs.get(1).setLocation(2, 4);	// upper
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		LogicGateDrawer.drawMultiplier(g, this, this.x * gridSize, this.y * gridSize, this.gridSize, stroke);
		if (isSelected) {
			g.setColor(Color.blue);
			g.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
	}

	@Override
	public String getTypeName() {
		return "Multiplier";
	}

	@Override
	public void eval() {
		System.out.println("called");
		this.outputs.get(0).value = this.inputs.get(0).value * this.inputs.get(1).value + this.inputs.get(2).value;
		long upper = 0, lower = this.outputs.get(0).value;
		if (lower >= Math.pow(2, this.outputs.get(0).bits)) {
			for (int i = this.outputs.get(0).bits * 2 - 1; i >= this.outputs.get(0).bits; i--) {
				if (lower >= Math.pow(2, i)) {
					lower -= (long) Math.pow(2, i);
					upper += (long) Math.pow(2, i - this.outputs.get(0).bits);
				}
			}
		}
		this.outputs.get(0).value = lower;
		this.outputs.get(1).value = upper;
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.outputs.get(0).bits = (int) this.properties.get("Bits");
		this.outputs.get(1).bits = (int) this.properties.get("Bits");
		this.inputs.get(0).bits = (int) this.properties.get("Bits");
		this.inputs.get(1).bits = (int) this.properties.get("Bits");
		this.inputs.get(2).bits = (int) this.properties.get("Bits");
	}
}
