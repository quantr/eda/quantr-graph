/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.BDDBuilder;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.nio.CharBuffer;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("pin")
public class Pin extends Vertex {

	public String radix = "bin";
	public int base = 2;
	char[] bin = new char[64];
	char[] hex = new char[16];
	char[] dec = new char[20];
//	int[] data = new int[(int) Math.pow(2, 8)];

	public Pin(String name) {
		super(name, 0, 1, 2, 2);
		properties.put("Name", name);
//		this.outputs.get(0).bits = 4;
		properties.put("Data Bits", this.outputs.get(0).bits);
		properties.put("Orientation", "east");
		properties.put("Radix", radix);
		properties.put("Value", this.outputs.get(0).value);
		for (int i = 0; i < 64; i++) {
			bin[i] = '0';
		}
		for (int i = 0; i < 16; i++) {
			hex[i] = '0';
		}
		for (int i = 0; i < 20; i++) {
			dec[i] = '0';
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(new Color(0, 60, 0));
		g2.fillRoundRect(x * gridSize + gridSize * 1 / 10, y * gridSize + gridSize * 1 / 10, width * gridSize - gridSize * 2 / 10, height * gridSize - gridSize * 2 / 10, gridSize * 2 - gridSize * 2 / 10, gridSize * 2 - gridSize * 2 / 10);
		g2.setColor(Color.black);
		g2.drawRect(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize);
		g2.drawString(Long.toUnsignedString(this.outputs.get(0).value), x * gridSize, y * gridSize - gridSize / 5);
		outputs.get(0).paint(g2, gridSize);

		g2.setFont(new Font("monospaced", Font.BOLD, gridSize * 2 - gridSize / 2));
		g2.setColor(Color.white);
		for (int i = 0; i < Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base)); i++) {
			if (radix.equals("bin")) {
				g.drawString(Character.toString(bin[63 - i]), this.x * gridSize + gridSize / 5 * 2 + 2 * (((int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base))) - i - 1) * gridSize + gridSize * 2 / 10, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			} else if (radix.equals("hex")) {
				g.drawString(Character.toString(hex[15 - i]), this.x * gridSize + gridSize / 5 * 2 + 2 * (((int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base))) - i - 1) * gridSize + gridSize * 2 / 10, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			} else {
				g.drawString(Character.toString(dec[19 - i]), this.x * gridSize + gridSize / 5 * 2 + 2 * (((int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base))) - i - 1) * gridSize + gridSize * 2 / 10, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			}
		}

		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		if (properties.get("Orientation").equals("east")) {
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawString((String) properties.get("Label"), (x + width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
		}
		if (isSelected) {
			g2.setColor(Color.blue);
			g2.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
	}

	@Override
	public String getTypeName() {
		return "Pin";
	}

	// can't handle 64-bit unsigned yet
	public void clicked(int x) {
		x = (x - (x / gridSize) * gridSize > (x / gridSize) * gridSize + gridSize - x)
				? (x / gridSize) * gridSize + gridSize : (x / gridSize) * gridSize;
		x /= gridSize;
		int bits = (int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base));
		if (radix.equals("bin")) {
			for (int i = 0; i < bits; i++) {
				if (x == this.x + (bits - i - 1) * 2 + 1) {
					if (Long.parseLong(Character.toString(bin[63 - i]), base) == base - 1 || this.outputs.get(0).value + Math.pow(base, i) >= Math.pow(2, this.outputs.get(0).bits)) {
						bin[63 - i] = '0';
					} else {
						bin[63 - i]++;
					}
				}
			}
			this.outputs.get(0).value = Long.parseUnsignedLong(CharBuffer.wrap(bin), 0, bin.length, base);
		} else if (radix.equals("hex")) {
			this.outputs.get(0).value++;
			if (this.outputs.get(0).value == Math.pow(2, this.outputs.get(0).bits)) {
				this.outputs.get(0).value = 0;
			}
			long temp = Long.parseUnsignedLong(Long.toUnsignedString(this.outputs.get(0).value));
			for (int i = 0; i < 16 - bits; i++) {
				hex[i] = '0';
			}
			for (int i = 16 - bits; i < 16; i++) {
				hex[i] = (char) (temp / Math.pow(base, 15 - i) + '0');
				hex[i] += hex[i] > '9' ? 39 : 0;
				temp %= Math.pow(base, 15 - i);
			}
		} else {
			this.outputs.get(0).value++;
			if (this.outputs.get(0).value == Math.pow(2, this.outputs.get(0).bits)) {
				this.outputs.get(0).value = 0;
			}
			long temp = Long.parseUnsignedLong(Long.toUnsignedString(this.outputs.get(0).value));
			for (int i = 0; i < 20 - bits; i++) {
				dec[i] = '0';
			}
			for (int i = 20 - bits; i < 20; i++) {
				dec[i] = (char) (temp / Math.pow(base, 19 - i) + '0');
				temp %= Math.pow(base, 19 - i);
			}
		}
	}

	// can't handle 64-bit unsigned yet
	public void changeRadix(String newRadix) {
		radix = newRadix;
		base = radix.equals("bin") ? 2 : (radix.equals("dec") ? 10 : 16);
		int bits = (int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base));
		long temp = this.outputs.get(0).value;
		for (int i = 0; i < 64 - ((int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(2))); i++) {
			bin[i] = '0';
		}
		for (int i = 64 - ((int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(2))); i < 64; i++) {
			bin[i] = (char) ((temp / Math.pow(2, 63 - i)) % 2 + '0');
			temp %= Math.pow(2, 63 - i);
		}
		this.outputs.get(0).value = Long.parseUnsignedLong(CharBuffer.wrap(bin), 0, bin.length, 2);
		temp = this.outputs.get(0).value;
		switch (radix) {
			case "hex":
				for (int i = 0; i < 16 - bits; i++) {
					hex[i] = '0';
				}
				for (int i = 16 - bits; i < 16; i++) {
					hex[i] = (char) ((temp / Math.pow(base, 15 - i)) % base + '0');
					hex[i] += hex[i] > '9' ? 39 : 0;
					temp %= Math.pow(base, 15 - i);
				}
				this.outputs.get(0).value = Long.parseUnsignedLong(CharBuffer.wrap(hex), 0, hex.length, base);
				break;
			case "dec":
				for (int i = 0; i < 20 - bits; i++) {
					dec[i] = '0';
				}
				for (int i = 20 - bits; i < 20; i++) {
					dec[i] = (char) ((temp / Math.pow(base, 19 - i)) % base + '0');
					temp %= Math.pow(base, 19 - i);
				}
				this.outputs.get(0).value = Long.parseUnsignedLong(CharBuffer.wrap(dec), 0, dec.length, base);
				break;
		}
		this.properties.put("Value", this.outputs.get(0).value);
	}

	@Override
	public void eval() {
		super.eval();
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 1, 0, 0.5f}, {0, 0.5f, 0, 1}, {0, 0, 0, 0.5f}, {0, 0.5f, 0, 0}};
	}

	@Override
	public void updateProperty() {
		this.outputs.get(0).bits = (int) this.properties.get("Data Bits");
		this.outputs.get(0).value = (long) this.properties.get("Value");
		this.changeRadix((String) this.properties.get("Radix"));
		this.width = (int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(this.base)) * 2;
//		this.outputs.get(0).setLocation(this.width, (this.height / 2));
		rotate(false);
	} //update property in recalculateLine() if instant update is wanted

	@Override
	public void simulateInit() {
		super.simulateInit();
		this.changeRadix(this.radix);
	}
}
