/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("switch")
public class Switch extends Vertex {

	private boolean switchState = false;

	public Switch(String name) {
		super(name, 1, 1, 4, 5);
		properties.put("Name", name);
		inputs.get(0).setLocation(2, 0);
		outputs.get(0).setLocation(2, 5);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
//		g2.setStroke(new BasicStroke(this.gridSize / 7, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

		if (switchState) {
			g2.setColor(Color.white);
			g2.fillPolygon(new int[]{x * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 4, x * gridSize + gridSize / 4}, new int[]{y * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (y + 1) * gridSize + gridSize / 4, (y + 1) * gridSize + gridSize / 4}, 4);
			g2.fillPolygon(new int[]{x * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{(y + 1) * gridSize + gridSize / 4, (y + 1) * gridSize + gridSize / 4, (y + 4) * gridSize + gridSize / 2, (y + 4) * gridSize + gridSize / 2}, 4);
			g2.setColor(Color.black);
			g2.drawPolygon(new int[]{x * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 4, x * gridSize + gridSize / 4}, new int[]{y * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (y + 1) * gridSize + gridSize / 4, (y + 1) * gridSize + gridSize / 4}, 4);
			g2.drawLine((x + 3) * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 2, (y + 4) * gridSize + gridSize / 2);
			g2.drawPolygon(new int[]{x * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{(y + 1) * gridSize + gridSize / 4, (y + 1) * gridSize + gridSize / 4, (y + 4) * gridSize + gridSize / 2, (y + 4) * gridSize + gridSize / 2}, 4);
			g2.drawLine(x * gridSize + gridSize * 2 / 5, (y + 3) * gridSize - gridSize / 10, (x + 3) * gridSize + gridSize * 2 / 5, (y + 3) * gridSize - gridSize / 10);
			g2.setFont(new Font("arial", Font.PLAIN, gridSize));
			g2.drawString("O", (x + 2) * gridSize - gridSize / 2, (y + 3) * gridSize - gridSize / 2);
			g2.drawString("I", (x + 2) * gridSize - gridSize / 4, (y + 4) * gridSize);
		} else {
			g2.setColor(Color.white);
			g2.fillPolygon(new int[]{x * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 4, x * gridSize + gridSize / 4}, new int[]{y * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (y + 4) * gridSize - gridSize / 4, (y + 4) * gridSize - gridSize / 4}, 4);
			g2.fillPolygon(new int[]{x * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{(y + 4) * gridSize - gridSize / 4, (y + 4) * gridSize - gridSize / 4, (y + 4) * gridSize + gridSize / 2, (y + 4) * gridSize + gridSize / 2}, 4);
			g2.setColor(Color.black);
			g2.drawPolygon(new int[]{x * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 4, x * gridSize + gridSize / 4}, new int[]{y * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (y + 4) * gridSize - gridSize / 4, (y + 4) * gridSize - gridSize / 4}, 4);
			g2.drawLine((x + 3) * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize / 2, (y + 4) * gridSize + gridSize / 2);
			g2.drawPolygon(new int[]{x * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 4, (x + 3) * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{(y + 4) * gridSize - gridSize / 4, (y + 4) * gridSize - gridSize / 4, (y + 4) * gridSize + gridSize / 2, (y + 4) * gridSize + gridSize / 2}, 4);
			g2.drawLine(x * gridSize + gridSize * 2 / 5, (y + 2) * gridSize + gridSize / 2, (x + 3) * gridSize + gridSize * 3 / 10, (y + 2) * gridSize + gridSize / 2);
			g2.setFont(new Font("arial", Font.PLAIN, gridSize));
			g2.drawString("O", (x + 2) * gridSize - gridSize / 2, (y + 2) * gridSize - gridSize * 3 / 10);
			g2.drawString("I", (x + 2) * gridSize - gridSize / 4, (y + 3) * gridSize + gridSize / 2);
		}
		inputs.get(0).paint(g, gridSize);
		outputs.get(0).paint(g, gridSize);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, (y + (this.height / 2) + 1) * gridSize - gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Switch";
	}

	@Override
	public void eventTriggerEval() {
		this.eval();
	}

	@Override
	public void eval() {
		if (!switchState) {
			this.outputs.get(0).value = 0;
		} else {
			this.outputs.get(0).value = this.inputs.get(0).value;
		}
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output || p.parent instanceof Tunnel) {
				this.inputs.get(0).bits = p.bits;
				this.outputs.get(0).bits = p.bits;
			}
		}
//		this.orientation = (String) properties.get("Orientation");
	}

	public void switchClicked(int x, int y) {
		if (x > (this.x * gridSize + gridSize / 4) && x < ((this.x + 3) * gridSize + gridSize / 4) && y > (this.y * gridSize + gridSize / 2) && y < (this.y + 4) * gridSize + gridSize / 2) {
			switchState = !switchState;
		}
	}
}
