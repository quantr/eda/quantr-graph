/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("splitter")
public class Splitter extends Vertex {

	public Splitter(String name) {
		super(name, 1, 4, 2, 4);
		properties.put("Name", name);
		properties.put("Orientation", "east");
		this.inputs.get(0).bits = 4;
		this.inputs.get(0).setLocation(0, this.inputs.get(0).bits);
		this.outputs.get(0).setLocation(2, 0);
		this.outputs.get(1).setLocation(2, 1);
		this.outputs.get(2).setLocation(2, 2);
		this.outputs.get(3).setLocation(2, 3);
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);

		if (properties.get("Orientation").equals("east")) {
			for (int i = 0; i < outputs.size(); i++) {
				g2.setColor(Color.gray);
//                g2.setFont(new Font("arial", Font.PLAIN, gridSize / 2));
//                g2.drawString((Integer.toString(i)), (x + 1) * gridSize - gridSize / 5, (y + i) * gridSize - gridSize / 10);
				g2.drawLine(x * gridSize + gridSize / 2, (y + i) * gridSize, (x + 2) * gridSize, (y + i) * gridSize);
				outputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{(y + this.inputs.get(0).bits) * gridSize, (y + this.inputs.get(0).bits) * gridSize - gridSize / 2, y * gridSize}, 3);
			g2.setStroke(stroke);
			this.inputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - gridSize));
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			for (int i = 0; i < outputs.size(); i++) {
				g2.setColor(Color.gray);
//                g2.setFont(new Font("arial", Font.PLAIN, gridSize / 2));
//                g2.drawString((Integer.toString(i)), (x + 1) * gridSize, (y + i) * gridSize - gridSize / 10);
				g2.drawLine(x * gridSize, (y + i) * gridSize, (x + 1) * gridSize + gridSize / 2, (y + i) * gridSize);
				outputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{(x + 2) * gridSize, (x + 1) * gridSize + gridSize / 2, (x + 1) * gridSize + gridSize / 2}, new int[]{(y + this.inputs.get(0).bits) * gridSize, (y + this.inputs.get(0).bits) * gridSize - gridSize / 2, y * gridSize}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x + width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - gridSize));
			this.inputs.get(0).paint(g, gridSize);
			super.paint(g);
		} else if (properties.get("Orientation").equals("north")) {
			for (int i = 0; i < outputs.size(); i++) {
				g2.setColor(Color.gray);
//                g2.setFont(new Font("arial", Font.PLAIN, gridSize / 2));
//                g2.drawString((Integer.toString(i)), (x + 1 + i) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5);
				g2.drawLine((x + 1 + i) * gridSize, y * gridSize, (x + 1 + i) * gridSize, (y + 1) * gridSize + gridSize / 2);
				outputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize / 2, (x + this.inputs.get(0).bits) * gridSize}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize - gridSize / 2, (y + 2) * gridSize - gridSize / 2}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x + 1) * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
			this.inputs.get(0).paint(g, gridSize);
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			for (int i = 0; i < outputs.size(); i++) {
				g2.setColor(Color.gray);
//                g2.setFont(new Font("arial", Font.PLAIN, gridSize / 2));
//                g2.drawString((Integer.toString(i)), (x + 1 + i) * gridSize + gridSize / 5,  y * gridSize + gridSize / 2);
				g2.drawLine((x + 1 + i) * gridSize, y * gridSize + gridSize / 2, (x + 1 + i) * gridSize, (y + 2) * gridSize);
				outputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize / 2, (x + this.inputs.get(0).bits) * gridSize}, new int[]{y * gridSize, y * gridSize + gridSize / 2, y * gridSize + gridSize / 2}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x + 1) * gridSize, y * gridSize);
			this.inputs.get(0).paint(g, gridSize);
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "Splitter";
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 0, 0, 1}, {0, 0, 0, 0}, {2, 0, 0, 1}, {0, 0, 2, 0}};
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");

		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output) {
				if (this.inputs.get(0).bits != p.bits) {
					if (this.inputs.get(0).bits > p.bits) {
						for (int i = this.inputs.get(0).bits - 1; i >= p.bits; i--) {
							outputs.get(i).edges.clear();
							outputs.remove(i);
						}
					} else if (this.inputs.get(0).bits < p.bits) {
						for (int i = this.inputs.get(0).bits; i < p.bits; i++) {
							outputs.add(new Output(this, "Output " + i));
						}
					}
				}
				this.inputs.get(0).bits = p.bits;
			} else if (p.parent instanceof Tunnel) {
				this.inputs.get(0).bits = p.bits;
				if (this.inputs.get(0).bits > this.outputs.size()) {
					for (int i = outputs.size(); i < this.inputs.get(0).bits; i++) {
						outputs.add(new Output(this, "Output " + i));
					}
					for (int i = this.inputs.get(0).bits - 1; i >= p.bits; i--) {
						outputs.get(i).edges.clear();
						outputs.remove(i);
					}
				} else {
					for (int i = this.inputs.get(0).bits - 1; i >= p.bits; i--) {
						outputs.get(i).edges.clear();
						outputs.remove(i);
					}
				}
			}
		}
		if (this.properties.get("Orientation").equals("west")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(0, i);
			}
			this.height = this.inputs.get(0).bits;
			this.width = 2;
			rotate(true);
//			this.inputs.get(0).setLocation(2, inputs.get(0).bits);
		} else if (this.properties.get("Orientation").equals("east")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(2, i);
			}
			this.height = this.inputs.get(0).bits;
			this.width = 2;
			rotate(true);
//			this.inputs.get(0).setLocation(0, inputs.get(0).bits);
		} else if (this.properties.get("Orientation").equals("north")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(i + 1, 0);
			}
			this.height = 2;
			this.width = this.inputs.get(0).bits;
			rotate(true);
//			this.inputs.get(0).setLocation(0, 2);
		} else if (this.properties.get("Orientation").equals("south")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(i + 1, 2);
			}
			this.height = 2;
			this.width = this.inputs.get(0).bits;
			rotate(true);
//			this.inputs.get(0).setLocation(0, 0);
		}
	}

	@Override
	public void eval() {
		for (int i = 0; i < outputs.size(); i++) {
			this.outputs.get(i).value = (this.inputs.get(0).value & (int) Math.pow(2, i)) != 0 ? 1 : 0;
		}
		super.eval();
	}
}
