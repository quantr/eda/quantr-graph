/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.QuantrGraphCanvas;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author michelle
 */
@XStreamAlias("tunnel")
public class Tunnel extends Vertex {

	public int databits = 1;
	public String label = " ";
	ArrayList<Tunnel> tunnelGroup = new ArrayList();
	public boolean hasTunnelOutput = false;

	public Tunnel(String name) {
		super(name, 1, 0, 2, 4);
		properties.put("Name", name);
		properties.put("Orientation", "west");
		properties.put("Data Bits", databits);
		inputs.get(0).setLocation(0, 2);
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.black);

		if (properties.get("Orientation").equals("north")) {
			g2.setColor(Color.white);
			g2.fillPolygon(new int[]{x * gridSize + gridSize / 2, (x + 2) * gridSize, (x + 4) * gridSize - gridSize / 2, (x + 4) * gridSize - gridSize / 2, x * gridSize + gridSize / 2}, new int[]{y * gridSize + gridSize / 2, y * gridSize, y * gridSize + gridSize / 2, (y + 2) * gridSize - gridSize * 1 / 4, (y + 2) * gridSize - gridSize * 1 / 4}, 5);
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, (x + 2) * gridSize, y * gridSize);
			g2.drawLine((x + 2) * gridSize, y * gridSize, (x + 4) * gridSize - gridSize / 2, y * gridSize + gridSize / 2);
			g2.drawLine(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, x * gridSize + gridSize / 2, (y + 2) * gridSize);
			g2.drawLine((x + 4) * gridSize - gridSize / 2, y * gridSize + gridSize / 2, (x + 4) * gridSize - gridSize / 2, (y + 2) * gridSize);
			g2.drawLine(x * gridSize + gridSize / 2, (y + 2) * gridSize - gridSize * 1 / 4, (x + 4) * gridSize - gridSize / 2, (y + 2) * gridSize - gridSize * 1 / 4);

			g2.setFont(new Font("arial", Font.BOLD, 14));
			if (!label.isEmpty()) {
				g2.drawString(label, (x + 2) * gridSize - gridSize * 2 / 5, (y + 1) * gridSize + gridSize * 2 / 5);
			}
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(0).paint(g, gridSize);
				super.paint(g);
			}
		} else if (properties.get("Orientation").equals("east")) {
			g2.setColor(Color.white);
			g2.fillPolygon(new int[]{x * gridSize + gridSize * 1 / 4, (x + 1) * gridSize + gridSize / 2, (x + 2) * gridSize, (x + 1) * gridSize + gridSize / 2, x * gridSize + gridSize * 1 / 4}, new int[]{(y + 1) * gridSize - gridSize / 2, (y + 1) * gridSize - gridSize / 2, (y + 2) * gridSize, (y + 3) * gridSize + gridSize / 2, (y + 3) * gridSize + gridSize / 2}, 5);
			g2.setColor(Color.black);
			g2.drawLine((x + 1) * gridSize + gridSize / 2, (y + 1) * gridSize - gridSize / 2, (x + 2) * gridSize, (y + 2) * gridSize);
			g2.drawLine(x * gridSize, (y + 1) * gridSize - gridSize / 2, (x + 1) * gridSize + gridSize / 2, (y + 1) * gridSize - gridSize / 2);
			g2.drawLine(x * gridSize + gridSize * 1 / 4, (y + 1) * gridSize - gridSize / 2, x * gridSize + gridSize * 1 / 4, (y + 3) * gridSize + gridSize / 2);
			g2.drawLine(x * gridSize, (y + 3) * gridSize + gridSize / 2, (x + 1) * gridSize + gridSize / 2, (y + 3) * gridSize + gridSize / 2);
			g2.drawLine((x + 2) * gridSize, (y + 2) * gridSize, (x + 1) * gridSize + gridSize / 2, (y + 3) * gridSize + gridSize / 2);

			g2.setFont(new Font("arial", Font.BOLD, 14));
			if (!label.isEmpty()) {
				g2.drawString(label, (x + 1) * gridSize - gridSize * 2 / 5, (y + 2) * gridSize + gridSize / 2);
			}
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(0).paint(g, gridSize);
				super.paint(g);
			}
		} else if (properties.get("Orientation").equals("south")) {
			g2.setColor(Color.white);
			g2.fillPolygon(new int[]{x * gridSize + gridSize / 2, (x + 4) * gridSize - gridSize / 2, (x + 4) * gridSize - gridSize / 2, (x + 2) * gridSize, x * gridSize + gridSize / 2}, new int[]{y * gridSize + gridSize * 1 / 4, y * gridSize + gridSize * 1 / 4, (y + 1) * gridSize + gridSize / 2, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 2}, 5);
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize + gridSize / 2, y * gridSize, x * gridSize + gridSize / 2, (y + 1) * gridSize + gridSize / 2);
			g2.drawLine((x + 4) * gridSize - gridSize / 2, y * gridSize, (x + 4) * gridSize - gridSize / 2, (y + 1) * gridSize + gridSize / 2);
			g2.drawLine(x * gridSize + gridSize / 2, y * gridSize + gridSize * 1 / 4, (x + 4) * gridSize - gridSize / 2, y * gridSize + gridSize * 1 / 4);
			g2.drawLine(x * gridSize + gridSize / 2, (y + 1) * gridSize + gridSize / 2, (x + 2) * gridSize, (y + 2) * gridSize);
			g2.drawLine((x + 2) * gridSize, (y + 2) * gridSize, (x + 4) * gridSize - gridSize / 2, (y + 1) * gridSize + gridSize / 2);

			g2.setFont(new Font("arial", Font.BOLD, 14));
			if (!label.isEmpty()) {
				g2.drawString(label, (x + 2) * gridSize - gridSize * 2 / 5, (y + 1) * gridSize + gridSize / 2);
			}
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(0).paint(g, gridSize);
				super.paint(g);
			}
		} else if (properties.get("Orientation").equals("west")) {
			g2.setColor(Color.white);
			g2.fillPolygon(new int[]{x * gridSize, x * gridSize + gridSize / 2, (x + 2) * gridSize - gridSize * 1 / 4, (x + 2) * gridSize - gridSize * 1 / 4, x * gridSize + gridSize / 2}, new int[]{(y + 2) * gridSize, (y + 1) * gridSize - gridSize / 2, (y + 1) * gridSize - gridSize / 2, (y + 3) * gridSize + gridSize / 2, (y + 3) * gridSize + gridSize / 2}, 5);
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize + gridSize / 2, (y + 1) * gridSize - gridSize / 2, x * gridSize, (y + 2) * gridSize);
			g2.drawLine(x * gridSize + gridSize / 2, (y + 1) * gridSize - gridSize / 2, (x + 2) * gridSize, (y + 1) * gridSize - gridSize / 2);
			g2.drawLine((x + 2) * gridSize - gridSize * 1 / 4, (y + 1) * gridSize - gridSize / 2, (x + 2) * gridSize - gridSize * 1 / 4, (y + 3) * gridSize + gridSize / 2);
			g2.drawLine(x * gridSize + gridSize / 2, (y + 3) * gridSize + gridSize / 2, (x + 2) * gridSize, (y + 3) * gridSize + gridSize / 2);
			g2.drawLine(x * gridSize, (y + 2) * gridSize, x * gridSize + gridSize / 2, (y + 3) * gridSize + gridSize / 2);

			g2.setFont(new Font("arial", Font.BOLD, 14));
			if (!label.isEmpty()) {
				g2.drawString(label, (x + 1) * gridSize - gridSize * 2 / 5, (y + 2) * gridSize + gridSize / 2);
			}
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(0).paint(g, gridSize);
				super.paint(g);
			}
		}
	}

	@Override
	public String getTypeName() {
		return "Tunnel";
	}

	@Override
	public void eval() {
//		System.out.println(name + (this.inputs.get(0).state || inputs.get(0).value != 0));
		boolean hasTunnelOutput = false;
		Output temp = null;
		int count = 0;
		for (Port p : this.outputConnectedTo()) {
			if (p instanceof Output) {
				count++;
				temp = (Output) p;
				hasTunnelOutput = true;
			}
		}
		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output && count == 1) {
				hasTunnelOutput = false;
			}
		}
		if (count == 1 && temp != null) {
			this.inputs.get(0).bits = temp.bits;
			this.inputs.get(0).value = temp.value;
			for (Port p : this.outputConnectedTo()) {
				if (p instanceof Input) {
					if (p.bits == temp.bits) {
						if (this.label.equals(label)) {
							p.state = temp.value == 1 ? "on" : "off";
							p.value = temp.value;
						}
					}
				}
			}
		} else if (count == 0) {
			hasTunnelOutput = false;
		}
		this.hasTunnelOutput = hasTunnelOutput;
	}

	@Override
	public ArrayList<Port> outputConnectedTo() {
		ArrayList<Port> temp = new ArrayList();
		for (int i = 0; i < tunnelGroup.size(); i++) {
			for (int j = 0; j < tunnelGroup.get(i).portConnectedTo(tunnelGroup.get(i).inputs.get(0)).size(); j++) {
				if (!temp.contains(tunnelGroup.get(i).portConnectedTo(tunnelGroup.get(i).inputs.get(0)).get(j))) {
					temp.add(tunnelGroup.get(i).portConnectedTo(tunnelGroup.get(i).inputs.get(0)).get(j));
				}
			}
		}
		temp.addAll(this.portConnectedTo(this.inputs.get(0)));
		return temp;
	}

	@Override
	public void updateProperty(QuantrGraphCanvas canvas) {
		System.out.println("updated");
		this.name = (String) properties.get("Name");
		if (this.properties.get("Orientation").equals("west")) {
			this.inputs.get(0).setLocation(0, 2);
			this.height = 4;
			this.width = 2;
		} else if (this.properties.get("Orientation").equals("east")) {
			this.inputs.get(0).setLocation(2, 2);
			this.height = 4;
			this.width = 2;
		} else if (this.properties.get("Orientation").equals("north")) {
			this.inputs.get(0).setLocation(2, 0);
			this.height = 2;
			this.width = 4;
		} else if (this.properties.get("Orientation").equals("south")) {
			this.inputs.get(0).setLocation(2, 2);
			this.height = 2;
			this.width = 4;
		}
		this.databits = (int) properties.get("Data Bits");
		this.inputs.get(0).bits = this.databits;
		this.label = (String) properties.get("Label");
		tunnelGroup.clear();
		for (Vertex v : canvas.module.vertices) {
			if (v instanceof Tunnel && v != this) {
				if (this.label.equals(((Tunnel) v).label)) {
					if (!this.tunnelGroup.contains(((Tunnel) v))) {
						((Tunnel) v).tunnelGroup.add(this);
						this.tunnelGroup.add(((Tunnel) v));
					}
				}
			}
		}
	}

	@Override
	public void updateProperty() {

	}

	@Override
	public void simulateInit() {
		super.simulateInit();
		this.hasTunnelOutput = false;
	}
}
