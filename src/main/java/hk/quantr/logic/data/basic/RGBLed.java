/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("rgbLed")
public class RGBLed extends Vertex {

	public String activeHighLow = "High";
	public boolean nextstate = false;
	public boolean presentstate = false;

	public RGBLed(String name) {
		super(name, 3, 0, 2, 2);
		properties.put("Name", name);
		properties.put("Orientation", "west");
		properties.put("Active High/Low", activeHighLow);

		this.inputs.get(0).setLocation(1, 0); // input0 = red input
		this.inputs.get(0).bits = 1;
		this.inputs.get(1).setLocation(0, 1); // input1 = green input
		this.inputs.get(1).bits = 1;
		this.inputs.get(2).setLocation(1, 2); // input2 = Blue input
		this.inputs.get(2).bits = 1;
	}

	public void paint(Graphics g) {

		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		if (activeHighLow.equals("High")) {
			int red = 0, green = 0, blue = 0;
			if (this.inputs.get(0).preValue == 1) {
				red = (int) ((255L * this.inputs.get(0).value) / (long) (Math.pow(2, this.inputs.get(0).bits) - 1));
			}
			if (this.inputs.get(1).preValue == 1) {
				green = (int) ((255L * this.inputs.get(1).value) / (long) (Math.pow(2, this.inputs.get(1).bits) - 1));
			}
			if (this.inputs.get(2).preValue == 1) {
				blue = (int) ((255L * this.inputs.get(2).value) / (long) (Math.pow(2, this.inputs.get(2).bits) - 1));
			}
			if (!this.nextstate && this.level == 0) {
				red = 211;
				green = 211;
				blue = 211;
			}
			g2.setColor(new Color(red, green, blue));
			g2.fillArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);
			g2.setColor(Color.black);
			g2.drawArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);
			for (int z = 0; z < inputs.size(); z++) {
				inputs.get(z).paint(g, gridSize);
			}
			properties.put("Red Color depth", red);
			properties.put("Green Color depth", green);
			properties.put("Blue Color depth", blue);
			super.paint(g);
		} else if (activeHighLow.equals("Low")) {
			int red = 0, green = 0, blue = 0;
			if (this.inputs.get(0).preValue == 0) {
				green = 255;
			}
			if (this.inputs.get(1).preValue == 0) {
				red = 255;
			}
			if (this.inputs.get(2).preValue == 0) {
				blue = 255;
			}
			if (!this.nextstate && this.level == -1) {
				red = 211;
				green = 211;
				blue = 211;
			}
			g2.setColor(new Color(red, green, blue));
			g2.fillArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);
			g2.setColor(Color.black);
			g2.drawArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);
			for (int z = 0; z < inputs.size(); z++) {
				inputs.get(z).paint(g, gridSize);
			}
			properties.put("Red Color depth", red);
			properties.put("Green Color depth", green);
			properties.put("Blue Color depth", blue);
			super.paint(g);
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		if (properties.get("Orientation").equals("east")) {
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
		}
	}

	public void update() {
		this.nextstate = this.presentstate;
		this.inputs.get(0).preValue = this.inputs.get(0).value;
		this.inputs.get(1).preValue = this.inputs.get(1).value;
		this.inputs.get(2).preValue = this.inputs.get(2).value;
	}

	@Override
	public String getTypeName() {
		return "RGB LED";
	}

	@Override
	public void eval() {
		this.presentstate = this.inputs.get(0).value + this.inputs.get(1).value + this.inputs.get(2).value != 0;
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		this.activeHighLow = (String) properties.get("Active High/Low");

		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output) {
				if (this.inputs.get(0).bits != p.bits) {
					this.inputs.get(0).bits = p.bits;
				}
			}
		}
		for (Port p : this.portConnectedTo(this.inputs.get(1))) {
			if (p instanceof Output) {
				if (this.inputs.get(1).bits != p.bits) {
					this.inputs.get(1).bits = p.bits;
				}
			}
		}
		for (Port p : this.portConnectedTo(this.inputs.get(2))) {
			if (p instanceof Output) {
				if (this.inputs.get(2).bits != p.bits) {
					this.inputs.get(2).bits = p.bits;
				}
			}
		}
		if (properties.get("Orientation").equals("west")) {
			this.inputs.get(0).setLocation(1, 0);
			this.inputs.get(1).setLocation(0, 1);
			this.inputs.get(2).setLocation(1, 2);
		} else if (properties.get("Orientation").equals("east")) {
			this.inputs.get(0).setLocation(1, 2);
			this.inputs.get(1).setLocation(2, 1);
			this.inputs.get(2).setLocation(1, 0);
		} else if (properties.get("Orientation").equals("north")) {
			this.inputs.get(0).setLocation(2, 1);
			this.inputs.get(1).setLocation(1, 0);
			this.inputs.get(2).setLocation(0, 1);
		} else if (properties.get("Orientation").equals("south")) {
			this.inputs.get(0).setLocation(0, 1);
			this.inputs.get(1).setLocation(1, 2);
			this.inputs.get(2).setLocation(2, 1);
		}
	}
}
