/*
 * Copyright 2023 chanhuiwah.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.ibm.icu.impl.TextTrieMap.Output;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author chanhuiwah
 */
@XStreamAlias("ledBar")
public class LedBar extends Vertex {

	private boolean connectionState = false;
	public boolean nextstate = false;
	public boolean presentstate = false;

	public LedBar(String name) {
		super(name, 8, 0, 9, 3);
		properties.put("Name", name);
		properties.put("Orientation", "south");
		properties.put("Number of Segments", this.inputs.size());
		this.inputs.get(0).setLocation(1, 3);
		this.inputs.get(1).setLocation(2, 3);
		this.inputs.get(2).setLocation(3, 3);
		this.inputs.get(3).setLocation(4, 3);
		this.inputs.get(4).setLocation(5, 3);
		this.inputs.get(5).setLocation(6, 3);
		this.inputs.get(6).setLocation(7, 3);
		this.inputs.get(7).setLocation(8, 3);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);

		if (properties.get("Orientation").equals("north")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, (this.inputs.size() + 1) * gridSize, height * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, (this.inputs.size() + 1) * gridSize, height * gridSize);

			for (int i = 0; i < inputs.size(); i++) {
				if (this.inputs.get(i).value == 0) {
					g2.setColor(Color.lightGray);
					g2.fillRect((x + i) * gridSize + gridSize * 7 / 10, y * gridSize + gridSize / 2, gridSize * 3 / 5, gridSize * 2);
				} else {
					g2.setColor(Color.green);
					g2.fillRect((x + i) * gridSize + gridSize * 7 / 10, y * gridSize + gridSize / 2, gridSize * 3 / 5, gridSize * 2);
				}
				inputs.get(i).paint(g, gridSize);
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize + gridSize * 2 / 10, (y + (this.height + 1)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("east")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, width * gridSize, (this.inputs.size() + 1) * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, width * gridSize, (this.inputs.size() + 1) * gridSize);

			for (int i = 0; i < inputs.size(); i++) {
				if (this.inputs.get(i).value == 0) {
					g2.setColor(Color.lightGray);
					g2.fillRect(x * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 7 / 10, gridSize * 2, gridSize * 3 / 5);
				} else {
					g2.setColor(Color.green);
					g2.fillRect(x * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 7 / 10, gridSize * 2, gridSize * 3 / 5);
				}
				inputs.get(i).paint(g, gridSize);
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, (y + (this.height / 2)) * gridSize + gridSize * 2 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, (this.inputs.size() + 1) * gridSize, height * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, (this.inputs.size() + 1) * gridSize, height * gridSize);

			for (int i = 0; i < inputs.size(); i++) {
				if (this.inputs.get(i).value == 0) {
					g2.setColor(Color.lightGray);
					g2.fillRect((x + i) * gridSize + gridSize * 7 / 10, y * gridSize + gridSize / 2, gridSize * 3 / 5, gridSize * 2);
				} else {
					g2.setColor(Color.green);
					g2.fillRect((x + i) * gridSize + gridSize * 7 / 10, y * gridSize + gridSize / 2, gridSize * 3 / 5, gridSize * 2);
				}
				inputs.get(i).paint(g, gridSize);
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize, (y - 1) * gridSize + gridSize * 2 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, width * gridSize, (this.inputs.size() + 1) * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, width * gridSize, (this.inputs.size() + 1) * gridSize);

			for (int i = 0; i < inputs.size(); i++) {
				if (this.inputs.get(i).value == 0) {
					g2.setColor(Color.lightGray);
					g2.fillRect(x * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 7 / 10, gridSize * 2, gridSize * 3 / 5);
				} else {
					g2.setColor(Color.green);
					g2.fillRect(x * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 7 / 10, gridSize * 2, gridSize * 3 / 5);
				}
				inputs.get(i).paint(g, gridSize);
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width + 1)) * gridSize + gridSize * 2 / 10, (y + (this.height / 2)) * gridSize + gridSize * 2 / 10);
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "LED Bar";
	}

	public void update() {
		this.nextstate = this.presentstate;
		for (int i = 0; i < inputs.size(); i++) {
			this.inputs.get(i).preValue = this.inputs.get(i).value;
		}
	}

	@Override
	public void eval() {
		for (int i = 0; i < inputs.size(); i++) {
			this.inputs.get(i).preValue = this.inputs.get(i).value;
		}
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");

		if (this.inputs.size() != (int) properties.get("Number of Segments")) {
			if (this.inputs.size() > (int) properties.get("Number of Segments")) {
				for (int i = this.inputs.size() - 1; i >= (int) properties.get("Number of Segments"); i--) {
					inputs.get(i).edges.clear();
					inputs.remove(i);
				}
			} else if (this.inputs.size() < (int) properties.get("Number of Segments")) {
				for (int i = this.inputs.size(); i < (int) properties.get("Number of Segments"); i++) {
					inputs.add(new Input(this, "Input " + i));
				}
			}
		}
		if (properties.get("Orientation").equals("north")) {
			for (int i = 0; i < inputs.size(); i++) {
				this.inputs.get(i).setLocation(i + 1, 0);
			}
			this.width = this.inputs.size() + 1;
			this.height = 3;
		} else if (properties.get("Orientation").equals("east")) {
			for (int i = 0; i < inputs.size(); i++) {
				this.inputs.get(i).setLocation(3, i + 1);
			}
			this.width = 3;
			this.height = this.inputs.size() + 1;
		} else if (properties.get("Orientation").equals("south")) {
			for (int i = 0; i < inputs.size(); i++) {
				this.inputs.get(i).setLocation(i + 1, 3);
			}
			this.width = this.inputs.size() + 1;
			this.height = 3;
		} else if (properties.get("Orientation").equals("west")) {
			for (int i = 0; i < inputs.size(); i++) {
				this.inputs.get(i).setLocation(0, i + 1);
			}
			this.width = 3;
			this.height = this.inputs.size() + 1;
		}
	}
}
