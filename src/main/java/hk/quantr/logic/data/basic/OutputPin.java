/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.github.javabdd.BDD;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class OutputPin extends Vertex {

	public OutputPin(String name) {
		super(name, 1, 0, 3, 2);
		this.inputs.get(0).setLocation(0, 1);
		this.properties.put("Name", name);
		this.properties.put("Radix", "dec");
		this.properties.put("Orientation", "east");
	}

	@Override
	public String getTypeName() {
		return "Output Pin";
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		double rotate = 0;
		int tempLength = this.inputs.get(0).bits;
		if (this.properties.get("Radix").equals("dec")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits * Math.log10(2));
		} else if (this.properties.get("Radix").equals("hex")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits / Math.log10(16) * Math.log10(2));
		}
		String temp = (this.properties.get("Radix").equals("dec") ? Long.toUnsignedString(inputs.get(0).value, 10) : (this.properties.get("Radix").equals("bin") ? Long.toBinaryString(inputs.get(0).value) : Long.toHexString(inputs.get(0).value)));
		String str = "";
		for (int i = 0; i < tempLength - temp.length(); i++) {
			str += "0";
		}
		str += temp;
		int drawX = this.x * this.gridSize;
		int drawY = this.y * this.gridSize;
		int drawWidth = (this.width > this.height ? this.width : this.height);
		int drawHeight = (this.width > this.height ? this.height : this.width);
		if (((String) this.properties.get("Orientation")).equals("west")) {
			rotate = 0.5;
			drawX += this.width * this.gridSize / 2;
			drawY += this.height * this.gridSize / 2;
		} else if (((String) this.properties.get("Orientation")).equals("north")) {
			rotate = 0.75;
			drawX += this.height * this.gridSize / 2;
			drawY += this.height * this.gridSize / 2;
		} else if (((String) this.properties.get("Orientation")).equals("south")) {
			rotate = 0.25;
			drawX += this.width * this.gridSize / 2;
			drawY += this.width * this.gridSize / 2;
		}
		AffineTransform backup = g2.getTransform();
		AffineTransform trans = new AffineTransform();
		trans.rotate((float) Math.toRadians(rotate * 360), drawX, drawY);
		g2.transform(trans);
		g2.setColor(new Color(215, 215, 215));
		g2.fillPolygon(new int[]{x * gridSize, x * gridSize, (x + drawWidth - 1) * gridSize, (x + drawWidth) * gridSize, (x + drawWidth - 1) * gridSize, x * gridSize}, new int[]{y * gridSize, (y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize, y * gridSize, y * gridSize}, 6);
		g2.setColor(Color.black);
		g2.drawPolygon(new int[]{x * gridSize, x * gridSize, (x + drawWidth - 1) * gridSize, (x + drawWidth) * gridSize, (x + drawWidth - 1) * gridSize, x * gridSize}, new int[]{y * gridSize, (y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize, y * gridSize, y * gridSize}, 6);
		g2.setFont(new Font("monospaced", Font.BOLD, gridSize * 2 - gridSize / 2));
		for (int i = 0; i < str.length(); i++) {
			g2.drawString(Character.toString(str.charAt(str.length() - 1 - i)), this.x * gridSize + gridSize / 5 * 2 + 2 * (drawWidth / 2 - i - 1) * gridSize + gridSize * 2 / 10, this.y * gridSize + drawHeight * gridSize * 7 / 10);
		}

		g2.setTransform(backup);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), x * gridSize, this.y * gridSize - gridSize * 6 / 10);
		if (((String) this.properties.get("Orientation")).equals("west")) {
			this.width = str.length() * 2 + 1;
			this.height = 2;
		} else if (((String) this.properties.get("Orientation")).equals("north") || (((String) this.properties.get("Orientation")).equals("south"))) {
			this.height = str.length() * 2 + 1;
			this.width = 2;
		}
		inputs.get(0).paint(g, gridSize);
		super.paint(g);
	}



	@Override
	public void eval() {
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 0, 1, 0}, {1, 0, 0, 0}, {0, 1, 1, 0}, {1, 0, 0, 1}};
	}

	@Override
	public void updateProperty() {
		ArrayList<Port> temp = this.portConnectedTo(this.inputs.get(0));
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i) instanceof Output) {
				this.inputs.get(0).bits = this.portConnectedTo(this.inputs.get(0)).get(i).bits;
			}
		}

		int tempLength = this.inputs.get(0).bits;
		if (this.properties.get("Radix").equals("dec")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits * Math.log10(2));
		} else if (this.properties.get("Radix").equals("hex")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits / Math.log10(16) * Math.log10(2));
		}

		if (properties.get("Orientation").equals("east")) {
			this.width = tempLength * 2 + 1;
			this.height = 2;
		} else if (properties.get("Orientation").equals("south")) {
			this.height = tempLength * 2 + 1;
			this.width = 2;
		} else if (properties.get("Orientation").equals("west")) {
			this.width = tempLength * 2 + 1;
			this.height = 2;
		} else if (properties.get("Orientation").equals("north")) {
			this.height = tempLength * 2 + 1;
			this.width = 2;
		}
		rotate(true);
	}// update property in recalculateLine() if instant update is wanted
}
