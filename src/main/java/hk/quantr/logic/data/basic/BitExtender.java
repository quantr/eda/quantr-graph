/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("bitExtender")
public class BitExtender extends Vertex {

	public BitExtender(String name) {
		super(name, 1, 1, 4, 4);
		properties.put("Name", name);
		properties.put("Input Bits", 1);
		properties.put("Output Bits", 4);
		this.outputs.get(0).bits = 4;
		this.inputs.get(0).bits = 1;
		this.inputs.get(0).setLocation(0, 2);	// origin
		this.outputs.get(0).setLocation(4, 2);	// extended
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		LogicGateDrawer.drawBitExtender(g, this, this.x * gridSize, this.y * gridSize, this.gridSize, stroke);
		if (isSelected) {
			g.setColor(Color.blue);
			g.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.7f)));
		g2.drawString((String) properties.get("Label"), x * gridSize + gridSize / 2, (y - 1) * gridSize + gridSize / 2);

	}

	@Override
	public String getTypeName() {
		return "Bit Extender";
	}

	@Override
	public void eval() {
		this.outputs.get(0).value = this.inputs.get(0).value;
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.outputs.get(0).bits = (int) this.properties.get("Output Bits");
		this.inputs.get(0).bits = (int) this.properties.get("Input Bits");
		for (Port p : this.outputConnectedTo()) {
			if (p instanceof Input) {
				p.parent.updateProperty();
			}
		}
	}
}
