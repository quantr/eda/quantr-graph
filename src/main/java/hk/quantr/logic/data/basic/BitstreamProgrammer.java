/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("bitstreamProgrammer")
public class BitstreamProgrammer extends Vertex {

	public boolean nextstate = false;
	public boolean presentstate = false;

	public BitstreamProgrammer(String name) {
		super(name, 1, 0, 2, 2);
		properties.put("Name", name);
		properties.put("Orientation", "west");
	}

	public void paint(Graphics g) {

		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);

		if (inputs.get(0).value==0) {
			g2.setColor(Color.lightGray);
			g2.fillArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);
		} else {
			g2.setColor(Color.green);
			g2.fillArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);
		}
		g2.setColor(Color.black);
		g2.drawArc(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize, 0, 360);

		for (int z = 0; z < inputs.size(); z++) {
			inputs.get(z).paint(g, gridSize);
		}

		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		if (properties.get("Orientation").equals("east")) {
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
		}

		super.paint(g);
	}

	public void update() {
	}

	@Override
	public String getTypeName() {
		return "LED";
	}

	@Override
	public void eval() {

	}

	@Override
	public void eval2() {
	}

	@Override
	public void updateProperty() {
		if (this.properties.get("Orientation").equals("north")) {
			this.inputs.get(0).setLocation(1, 0);
		} else if (this.properties.get("Orientation").equals("east")) {
			this.inputs.get(0).setLocation(2, 1);
		} else if (this.properties.get("Orientation").equals("south")) {
			this.inputs.get(0).setLocation(1, 2);
		} else if (this.properties.get("Orientation").equals("west")) {
			this.inputs.get(0).setLocation(0, 1);
		}
	}
}
