/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.logic.data.file.VCD;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("clock")
public class Clock extends Vertex {

	public int hertz = 1;
	public boolean presentstate = false;
	private long lastSystemTime = 0;
//	public long cnt = -1;

	public Clock(String name) {
		super(name, 0, 1, 2, 2);
		properties.put("Name", name);
		properties.put("Orientation", "east");
		properties.put("Hertz", 1);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(Color.black);
		g.drawString(String.valueOf(hertz), outputs.get(0).getAbsolutionX() * gridSize, outputs.get(0).getAbsolutionY() * gridSize);
		g2.drawRect(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize);

		outputs.get(0).paint(g2, gridSize);

		g.setFont(new Font("arial", Font.BOLD, gridSize * 2));
		g.drawString(Integer.toString((int) (this.outputs.get(0).value & 1)), this.x * gridSize + width * gridSize / 5, this.y * gridSize + height * gridSize * 7 / 8);

		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
		if (properties.get("Orientation").equals("east")) {
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
		}

		if (isSelected) {
			g2.setColor(Color.blue);
			g2.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
	}

	@Override
	public String getTypeName() {
		return "Clock";
	}

	@Override
	public void eval() {
		if (System.nanoTime() - lastSystemTime > 1000000000L / this.hertz) {
			lastSystemTime = System.nanoTime();
			this.outputs.get(0).value = this.outputs.get(0).value ^ 1;
			super.eval();
		}
	}

	@Override
	public void eval2() {
		if (System.nanoTime() - lastSystemTime > 1000000000L / this.hertz) {
			lastSystemTime = System.nanoTime();
			this.outputs.get(0).value = this.outputs.get(0).value ^ 1;
		}
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 1, 0, 0.5f}, {0, 0.5f, 0, 1}, {0, 0, 0, 0.5f}, {0, 0.5f, 0, 0}};
	}

	@Override
	public void updateProperty() {
		this.hertz = (int) this.properties.get("Hertz");
		for (Port p : this.outputConnectedTo()) {
			if (p instanceof Input) {
				p.parent.updateProperty();
			}
		}
		rotate(false);
	}

	@Override
	public void simulateInit() {
		super.simulateInit();
		this.outputs.get(0).value = 0;
		this.lastSystemTime = System.nanoTime();
	}
}
