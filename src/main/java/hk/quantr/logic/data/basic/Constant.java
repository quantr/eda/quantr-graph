/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("constant")
public class Constant extends Vertex {

	public Constant(String name) {
		super(name, 0, 1, 2, 2);
		this.properties.put("Bits", 1);
		this.properties.put("Orientation", "east");
		this.properties.put("Constant", "0x1");
		this.outputs.get(0).value = 1;
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(new Color(211, 211, 211));
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		if (this.outputs.get(0).value > 0) {
			g2.setColor(new Color(0, 220, 0));
		} else {
			g2.setColor(new Color(0, 100, 0));
		}
		g.setFont(new Font("arial", Font.BOLD, gridSize * 2));
		int n = (int) Math.ceil((1e-20 + this.outputs.get(0).bits) / 4);
		for (int i = 0; i < n; i++) {
			g.drawString(Character.toString(String.format("%0" + n + "X", this.outputs.get(0).value).charAt(n - i - 1)), this.x * gridSize + gridSize / 5 * 2 + 2 * (((int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(16))) - i - 1) * gridSize, this.y * gridSize + height * gridSize * 7 / 8);
		}
		outputs.get(0).paint(g2, gridSize);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		if (properties.get("Orientation").equals("east")) {
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawString((String) properties.get("Label"), (x + width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
		}
		if (isSelected) {
			g2.setColor(Color.blue);
			g2.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
	}

	@Override
	public String getTypeName() {
		return "Constant";
	}

	@Override
	public void eval() {
		super.eval();
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 1, 0, 0.5f}, {0, 0.5f, 0, 1}, {0, 0, 0, 0.5f}, {0, 0.5f, 0, 0}};
	}

	@Override
	public void updateProperty() {
		this.outputs.get(0).bits = (int) this.properties.get("Bits");
		if (Long.parseLong(((String) this.properties.get("Constant")).replace("0x", ""), 16) < pow(2, (int) this.properties.get("Bits"))) {
			this.outputs.get(0).value = Long.parseLong(((String) this.properties.get("Constant")).replace("0x", ""), 16);
		} else {
			this.properties.put("Constant", "0x" + String.format("%0" + Math.ceil((this.outputs.get(0).bits + 1e-20) / 4) + "X", this.outputs.get(0).value));
		}
		this.width = (int) Math.ceil(Math.log(Math.pow(2, this.outputs.get(0).bits)) / Math.log(16)) * 2;
		rotate(false);
//		this.outputs.get(0).setLocation(this.width, (this.height / 2));
	}

	@Override
	public void simulateInit() {
		for (Input input : this.inputs) {
			input.value = 0;
			input.preValue = -1;
		}
		for (int j = 0; j < this.outputs.size(); j++) {
			this.outputs.get(j).ports.clear();
			for (Port p : this.portConnectedTo(this.outputs.get(j))) {
				if (p instanceof Input) {
					this.outputs.get(j).ports.add((Input) p);
				}
			}
		}
	}

}
