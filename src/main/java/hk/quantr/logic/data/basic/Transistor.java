/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("transistor")
public class Transistor extends Vertex {

	private boolean transistorState = false;

	public Transistor(String name) {
		super(name, 2, 1, 2, 4);
		properties.put("Name", name);
		properties.put("Orientation", "north");
		properties.put("Data Bits", 1);
		inputs.get(0).setLocation(0, 4);
		inputs.get(1).setLocation(2, 2);
		inputs.get(1).bits = 2;
		outputs.get(0).setLocation(0, 0);
	}

	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);

		//P-Type Transistor
		if (properties.get("Orientation").equals("east")) {
			if (inputs.get(1).value == 1) {
				g2.setColor(Color.green);
				g2.fillOval((x + 2) * gridSize - gridSize * 3 / 10, y * gridSize, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize, y * gridSize + gridSize * 3 / 5, (x + 3) * gridSize, y * gridSize + gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize + gridSize / 5, (x + 3) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, (x + 1) * gridSize + gridSize / 5, (x + 1) * gridSize + gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{(x + 4) * gridSize, (x + 3) * gridSize - gridSize / 5, (x + 3) * gridSize - gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˃", (x + 2) * gridSize - gridSize / 5, (y + 2) * gridSize);
			} else {
				g2.setColor(Color.black);
				g2.drawOval((x + 2) * gridSize - gridSize * 3 / 10, y * gridSize, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize, y * gridSize + gridSize * 3 / 5, (x + 3) * gridSize, y * gridSize + gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize + gridSize / 5, (x + 3) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, (x + 1) * gridSize + gridSize / 5, (x + 1) * gridSize + gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{(x + 4) * gridSize, (x + 3) * gridSize - gridSize / 5, (x + 3) * gridSize - gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˃", (x + 2) * gridSize - gridSize / 5, (y + 2) * gridSize);
			}
		} else if (properties.get("Orientation").equals("south")) {
			if (inputs.get(1).value == 1) {
				g2.setColor(Color.green);
				g2.fillOval((x + 1) * gridSize + gridSize * 2 / 5, (y + 2) * gridSize - gridSize * 3 / 10, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize + gridSize * 2 / 5, (y + 1) * gridSize, (x + 1) * gridSize + gridSize * 2 / 5, (y + 3) * gridSize);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize - gridSize / 5, (x + 1) * gridSize - gridSize / 5, (y + 3) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{y * gridSize, (y + 1) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{(y + 4) * gridSize, (y + 3) * gridSize - gridSize / 5, (y + 3) * gridSize - gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˅", x * gridSize, (y + 3) * gridSize - gridSize / 2);
			} else {
				g2.setColor(Color.black);
				g2.drawOval((x + 1) * gridSize + gridSize * 2 / 5, (y + 2) * gridSize - gridSize * 3 / 10, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize + gridSize * 2 / 5, (y + 1) * gridSize, (x + 1) * gridSize + gridSize * 2 / 5, (y + 3) * gridSize);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize - gridSize / 5, (x + 1) * gridSize - gridSize / 5, (y + 3) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{y * gridSize, (y + 1) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{(y + 4) * gridSize, (y + 3) * gridSize - gridSize / 5, (y + 3) * gridSize - gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˅", x * gridSize, (y + 3) * gridSize - gridSize / 2);
			}
		} else if (properties.get("Orientation").equals("west")) {
			if (inputs.get(1).value == 1) {
				g2.setColor(Color.green);
				g2.fillOval((x + 2) * gridSize - gridSize * 3 / 10, y * gridSize, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize, y * gridSize + gridSize * 3 / 5, (x + 3) * gridSize, y * gridSize + gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize + gridSize / 5, (x + 3) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, (x + 1) * gridSize + gridSize / 5, (x + 1) * gridSize + gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{(x + 4) * gridSize, (x + 3) * gridSize - gridSize / 5, (x + 3) * gridSize - gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˂", (x + 2) * gridSize - gridSize / 5, (y + 2) * gridSize);
			} else {
				g2.setColor(Color.black);
				g2.drawOval((x + 2) * gridSize - gridSize * 3 / 10, y * gridSize, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize, y * gridSize + gridSize * 3 / 5, (x + 3) * gridSize, y * gridSize + gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize + gridSize / 5, (x + 3) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, (x + 1) * gridSize + gridSize / 5, (x + 1) * gridSize + gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{(x + 4) * gridSize, (x + 3) * gridSize - gridSize / 5, (x + 3) * gridSize - gridSize / 5}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˂", (x + 2) * gridSize - gridSize / 5, (y + 2) * gridSize);
			}
		} else if (properties.get("Orientation").equals("north")) {
			if (inputs.get(1).value == 1) {
				g2.setColor(Color.green);
				g2.fillOval((x + 1) * gridSize + gridSize * 2 / 5, (y + 2) * gridSize - gridSize * 3 / 10, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize + gridSize * 2 / 5, (y + 1) * gridSize, (x + 1) * gridSize + gridSize * 2 / 5, (y + 3) * gridSize);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize - gridSize / 5, (x + 1) * gridSize - gridSize / 5, (y + 3) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{y * gridSize, (y + 1) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{(y + 4) * gridSize, (y + 3) * gridSize - gridSize / 5, (y + 3) * gridSize - gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˄", x * gridSize, (y + 3) * gridSize - gridSize / 2);
			} else {
				g2.setColor(Color.black);
				g2.drawOval((x + 1) * gridSize + gridSize * 2 / 5, (y + 2) * gridSize - gridSize * 3 / 10, gridSize * 3 / 5, gridSize * 3 / 5);
				g2.drawLine((x + 1) * gridSize + gridSize * 2 / 5, (y + 1) * gridSize, (x + 1) * gridSize + gridSize * 2 / 5, (y + 3) * gridSize);
				g2.drawLine((x + 1) * gridSize - gridSize / 5, (y + 1) * gridSize - gridSize / 5, (x + 1) * gridSize - gridSize / 5, (y + 3) * gridSize + gridSize / 5);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{y * gridSize, (y + 1) * gridSize + gridSize / 5, (y + 1) * gridSize + gridSize / 5}, 3);
				g2.drawPolyline(new int[]{x * gridSize, x * gridSize, (x + 1) * gridSize - gridSize / 5}, new int[]{(y + 4) * gridSize, (y + 3) * gridSize - gridSize / 5, (y + 3) * gridSize - gridSize / 5}, 3);
				g2.setFont(new Font("arial", Font.BOLD, gridSize));
				g2.drawString("˄", x * gridSize, (y + 3) * gridSize - gridSize / 2);
			}
		}

		inputs.get(0).paint(g, gridSize);
		inputs.get(1).paint(g, gridSize);
		outputs.get(0).paint(g, gridSize);
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Transistor";
	}

	@Override
	public void eval() {
		if (inputs.get(1).value == 1) {
			this.outputs.get(0).value = this.inputs.get(0).value;
		} else if (inputs.get(1).value == 2) {
			this.outputs.get(0).value = 0;
		} else {
			return;
		}
		super.eval();
	}

	@Override
	public void updateProperty() {
		super.updateProperty();
		int databits = (int) properties.get("Data Bits");
		inputs.get(0).bits = databits;
		outputs.get(0).bits = databits;
		if (this.properties.get("Orientation").equals("north")) {
			inputs.get(0).setLocation(0, 4);
			inputs.get(1).setLocation(2, 2);
			outputs.get(0).setLocation(0, 0);
			this.height = 4;
			this.width = 2;
		} else if (this.properties.get("Orientation").equals("east")) {
			inputs.get(0).setLocation(0, 2);
			inputs.get(1).setLocation(2, 0);
			outputs.get(0).setLocation(4, 2);
			this.height = 2;
			this.width = 4;
		} else if (this.properties.get("Orientation").equals("south")) {
			inputs.get(0).setLocation(0, 0);
			inputs.get(1).setLocation(2, 2);
			outputs.get(0).setLocation(0, 4);
			this.height = 4;
			this.width = 2;
		} else if (this.properties.get("Orientation").equals("west")) {
			inputs.get(0).setLocation(4, 2);
			inputs.get(1).setLocation(2, 0);
			outputs.get(0).setLocation(0, 2);
			this.height = 2;
			this.width = 4;
		}
	}
}
