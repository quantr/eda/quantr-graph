/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("probe")
public class Probe extends Vertex {

	public Probe(String name) {
		super(name, 1, 0, 2, 2);
		this.inputs.get(0).setLocation(0, 1);
		this.properties.put("Name", name);
		this.properties.put("Orientation", "west");
		this.properties.put("Radix", "dec");
	}

	@Override
	public String getTypeName() {
		return "Probe";
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(new Color(215, 215, 215));
		g2.fillRoundRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize, gridSize * 2, gridSize * 5);
		g2.setColor(Color.black);
		g2.drawRoundRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize, gridSize * 2, gridSize * 5);
		g2.setFont(new Font("monospaced", Font.BOLD, gridSize * 2 - gridSize / 2));
		int tempLength = this.inputs.get(0).bits;
		if (this.properties.get("Radix").equals("dec")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits * Math.log10(2));
		} else if (this.properties.get("Radix").equals("hex")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits / Math.log10(16) * Math.log10(2));
		}
		String temp = (this.properties.get("Radix").equals("dec") ? Long.toUnsignedString(inputs.get(0).value, 10) : (this.properties.get("Radix").equals("bin") ? Long.toBinaryString(inputs.get(0).value) : Long.toHexString(inputs.get(0).value)));
		String str = "";
		for (int i = 0; i < tempLength - temp.length(); i++) {
			str += "0";
		}
		str += temp;
		for (int i = 0; i < str.length(); i++) {
			g2.drawString(Character.toString(str.charAt(str.length() - 1 - i)), this.x * gridSize + gridSize / 5 * 2 + 2 * (this.width / 2 - i - 1) * gridSize + gridSize * 2 / 10, this.y * gridSize + height * gridSize * 7 / 10);
		}
		inputs.get(0).paint(g, gridSize);
		if (isSelected) {
			g2.setColor(Color.blue);
			g2.drawRect(this.x * gridSize - gridSize / 5, this.y * gridSize - gridSize / 5, width * gridSize + gridSize / 5 * 2, height * gridSize + gridSize / 5 * 2);
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		if (properties.get("Orientation").equals("east")) {
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawString((String) properties.get("Label"), (x + width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
		}
	}

	@Override
	public void eval() {
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 1, 0, 0.5f}, {0, 0.5f, 0, 1}, {0, 0, 0, 0.5f}, {0, 0.5f, 0, 0}};
	}

	@Override
	public void updateProperty() {
		ArrayList<Port> temp = this.portConnectedTo(this.inputs.get(0));
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i) instanceof Output) {
				this.inputs.get(0).bits = this.portConnectedTo(this.inputs.get(0)).get(i).bits;
				break;
			}
		}

		int tempLength = this.inputs.get(0).bits;
		if (this.properties.get("Radix").equals("dec")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits * Math.log10(2));
		} else if (this.properties.get("Radix").equals("hex")) {
			tempLength = (int) Math.ceil(this.inputs.get(0).bits / Math.log10(16) * Math.log10(2));
		}
		this.width = tempLength * 2;
		this.rotate(true);
	}// update property in recalculateLine() if instant update is wanted
}
