/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("button")
public class Button extends Vertex {

	private boolean buttonState = false;

	public Button(String name) {
		super(name, 0, 1, 3, 3);
		properties.put("Name", name);
		properties.put("Orientation", "east");
		this.outputs.get(0).setLocation(3, 2);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		if (properties.get("Orientation").equals("east") || properties.get("Orientation").equals("south")) {
			if (buttonState) {//on
				g2.setColor(Color.white);
				g2.fillRect((x + 1) * gridSize, (y + 1) * gridSize, gridSize * 2, gridSize * 2);
				g2.setColor(Color.red);
				g2.fillOval((x + 1) * gridSize + gridSize / 2, (y + 1) * gridSize + gridSize / 2, gridSize, gridSize);
				g2.setColor(Color.black);
				g2.drawRect((x + 1) * gridSize, (y + 1) * gridSize, gridSize * 2, gridSize * 2);
				g2.drawOval((x + 1) * gridSize + gridSize / 2, (y + 1) * gridSize + gridSize / 2, gridSize, gridSize);
			} else { //off
				g2.setColor(Color.white);
				g2.fillRect(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, gridSize * 2, gridSize * 2);
				g2.setColor(Color.lightGray);
				g2.fillPolygon(new int[]{(x + 2) * gridSize + gridSize / 2, (x + 3) * gridSize, (x + 3) * gridSize, (x + 2) * gridSize + gridSize / 2}, new int[]{y * gridSize + gridSize / 2, (y + 1) * gridSize, (y + 3) * gridSize, (y + 2) * gridSize + gridSize / 2}, 4);
				g2.fillPolygon(new int[]{x * gridSize + gridSize / 2, (x + 2) * gridSize + gridSize / 2, (x + 3) * gridSize, (x + 1) * gridSize}, new int[]{(y + 2) * gridSize + gridSize / 2, (y + 2) * gridSize + gridSize / 2, (y + 3) * gridSize, (y + 3) * gridSize}, 4);
				g2.fillOval((x + 1) * gridSize, (y + 1) * gridSize, gridSize, gridSize);
				g2.setColor(Color.black);
				g2.drawRect(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, gridSize * 2, gridSize * 2);
				g2.drawPolygon(new int[]{(x + 2) * gridSize + gridSize / 2, (x + 3) * gridSize, (x + 3) * gridSize, (x + 2) * gridSize + gridSize / 2}, new int[]{y * gridSize + gridSize / 2, (y + 1) * gridSize, (y + 3) * gridSize, (y + 2) * gridSize + gridSize / 2}, 4);
				g2.drawPolygon(new int[]{x * gridSize + gridSize / 2, (x + 2) * gridSize + gridSize / 2, (x + 3) * gridSize, (x + 1) * gridSize}, new int[]{(y + 2) * gridSize + gridSize / 2, (y + 2) * gridSize + gridSize / 2, (y + 3) * gridSize, (y + 3) * gridSize}, 4);
				g2.drawOval((x + 1) * gridSize, (y + 1) * gridSize, gridSize, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			super.paint(g);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
		} else if (properties.get("Orientation").equals("north") || properties.get("Orientation").equals("west")) {
			if (buttonState) {//on
				g2.setColor(Color.white);
				g2.fillRect(x * gridSize, y * gridSize, gridSize * 2, gridSize * 2);
				g2.setColor(Color.red);
				g2.fillOval(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, gridSize, gridSize);
				g2.setColor(Color.black);
				g2.drawRect(x * gridSize, y * gridSize, gridSize * 2, gridSize * 2);
				g2.drawOval(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, gridSize, gridSize);
			} else { //off
				g2.setColor(Color.white);
				g2.fillRect(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, gridSize * 2, gridSize * 2);
				g2.setColor(Color.lightGray);
				g2.fillPolygon(new int[]{x * gridSize, x * gridSize + gridSize / 2, x * gridSize + gridSize / 2, x * gridSize}, new int[]{y * gridSize, y * gridSize + gridSize / 2, (y + 2) * gridSize + gridSize / 2, (y + 2) * gridSize}, 4);
				g2.fillPolygon(new int[]{x * gridSize, (x + 2) * gridSize, (x + 2) * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{y * gridSize, y * gridSize, y * gridSize + gridSize / 2, y * gridSize + gridSize / 2}, 4);
				g2.fillOval((x + 1) * gridSize, (y + 1) * gridSize, gridSize, gridSize);
				g2.setColor(Color.black);
				g2.drawRect(x * gridSize + gridSize / 2, y * gridSize + gridSize / 2, gridSize * 2, gridSize * 2);
				g2.drawPolygon(new int[]{x * gridSize, x * gridSize + gridSize / 2, x * gridSize + gridSize / 2, x * gridSize}, new int[]{y * gridSize, y * gridSize + gridSize / 2, (y + 2) * gridSize + gridSize / 2, (y + 2) * gridSize}, 4);
				g2.drawPolygon(new int[]{x * gridSize, (x + 2) * gridSize, (x + 2) * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{y * gridSize, y * gridSize, y * gridSize + gridSize / 2, y * gridSize + gridSize / 2}, 4);
				g2.drawOval((x + 1) * gridSize, (y + 1) * gridSize, gridSize, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			super.paint(g);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (this.x + 3) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 5));
		}
	}

	@Override
	public String getTypeName() {
		return "Button";
	}

	@Override
	public void eval() {
		super.eval();
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{3, 0, 2, 0}, {2, 0, 3, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}};
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		rotate(false);
//		if (properties.get("Orientation").equals("north")) {
//			this.outputs.get(0).setLocation(1, 0);
//		} else if (properties.get("Orientation").equals("east")) {
//			this.outputs.get(0).setLocation(3, 2);
//		} else if (properties.get("Orientation").equals("south")) {
//			this.outputs.get(0).setLocation(2, 3);
//		} else if (properties.get("Orientation").equals("west")) {
//			this.outputs.get(0).setLocation(0, 1);
//		}

	}

	public void buttonPressed(int x, int y) {
		if (x > (this.x * gridSize + gridSize / 2) && x < ((this.x + 3) * gridSize) && y > (this.y * gridSize + gridSize / 2) && y < (this.y + 3) * gridSize) {
			this.outputs.get(0).value = 1;
		}
	}

	public void buttonReleased() {
		this.outputs.get(0).value = 0;
	}
}
