/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashSet;

/**
 *
 * @author michelle
 */
@XStreamAlias("combiner")
public class Combiner extends Vertex {

	public Combiner(String name) {
		super(name, 4, 1, 2, 4);
		this.outputs.get(0).bits = 4;
		properties.put("Name", name);
		properties.put("Data Bits", this.outputs.get(0).bits);
		properties.put("Orientation", "west");
		this.outputs.get(0).setLocation(2, this.outputs.get(0).bits);
		this.inputs.get(0).setLocation(0, 0);
		this.inputs.get(1).setLocation(0, 1);
		this.inputs.get(2).setLocation(0, 2);
		this.inputs.get(3).setLocation(0, 3);
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);

		if (properties.get("Orientation").equals("west")) {
			for (int i = 0; i < inputs.size(); i++) {
				g2.setColor(Color.gray);
				g2.drawLine(x * gridSize, (y + i) * gridSize, (x + 1) * gridSize + gridSize / 2, (y + i) * gridSize);
				inputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{(x + 1) * gridSize + gridSize / 2, (x + 1) * gridSize + gridSize / 2, (x + 2) * gridSize}, new int[]{y * gridSize, (y + this.outputs.get(0).bits) * gridSize - gridSize / 2, (y + this.outputs.get(0).bits) * gridSize}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x + width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - gridSize));
			this.outputs.get(0).paint(g, gridSize);
			super.paint(g);

		} else if (properties.get("Orientation").equals("east")) {
			for (int i = 0; i < inputs.size(); i++) {
				g2.setColor(Color.gray);
				g2.drawLine(x * gridSize + gridSize / 2, (y + i) * gridSize, (x + 2) * gridSize, (y + i) * gridSize);
				inputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize / 2, x * gridSize + gridSize / 2}, new int[]{(y + this.outputs.get(0).bits) * gridSize, (y + this.outputs.get(0).bits) * gridSize - gridSize / 2, y * gridSize}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - gridSize));
			this.outputs.get(0).paint(g, gridSize);
			super.paint(g);

		} else if (properties.get("Orientation").equals("north")) {
			for (int i = 0; i < inputs.size(); i++) {
				g2.setColor(Color.gray);
				g2.drawLine((x + 1 + i) * gridSize, y * gridSize, (x + 1 + i) * gridSize, (y + 1) * gridSize + gridSize / 2);
				inputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize / 2, (x + this.outputs.get(0).bits) * gridSize}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize - gridSize / 2, (y + 2) * gridSize - gridSize / 2}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x + 1) * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
			this.outputs.get(0).paint(g, gridSize);
			super.paint(g);

		} else if (properties.get("Orientation").equals("south")) {
			for (int i = 0; i < inputs.size(); i++) {
				g2.setColor(Color.gray);
				g2.drawLine((x + 1 + i) * gridSize, y * gridSize + gridSize / 2, (x + 1 + i) * gridSize, (y + 2) * gridSize);
				inputs.get(i).paint(g, gridSize);
			}
			g2.setStroke(new BasicStroke(this.gridSize / 3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize / 2, (x + this.outputs.get(0).bits) * gridSize}, new int[]{y * gridSize, y * gridSize + gridSize / 2, y * gridSize + gridSize / 2}, 3);
			g2.setStroke(stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int)(gridSize *1.4f)));
			g2.drawString((String) properties.get("Label"), (x + 1) * gridSize, y * gridSize);
			this.outputs.get(0).paint(g, gridSize);
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "Combiner";
	}

	@Override
	public void eval() {
		int temp = 0;
		for (int i = 0; i < inputs.size(); i++) {
			if (this.inputs.get(i).value == 1) {
				temp += (long) Math.pow(2, i);
			}
		}
		outputs.get(0).value = temp;
		super.eval();
	}

	@Override
	public float[][] getOrientPrefix() {
		return new float[][]{{0, 0, 0, 1}, {0, 0, 0, 0}, {2, 0, 0, 1}, {0, 0, 2, 0}};
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");

		if (this.outputs.get(0).bits != (int) properties.get("Data Bits")) {
			if (this.outputs.get(0).bits > (int) properties.get("Data Bits")) {
				for (int i = this.outputs.get(0).bits - 1; i >= (int) properties.get("Data Bits"); i--) {
					inputs.get(i).edges.clear();
					inputs.remove(i);
				}
			} else if (this.outputs.get(0).bits < (int) properties.get("Data Bits")) {
				for (int i = this.outputs.get(0).bits; i < (int) properties.get("Data Bits"); i++) {
					inputs.add(new Input(this, "Input " + i));
				}
			}
		}
		this.outputs.get(0).bits = (int) properties.get("Data Bits");
		if (this.properties.get("Orientation").equals("west")) {
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(i).setLocation(0, i);
			}
			this.width = 2;
			this.height = this.outputs.get(0).bits;
//			this.outputs.get(0).setLocation(2, outputs.get(0).bits);
			rotate(false);
		} else if (this.properties.get("Orientation").equals("east")) {
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(i).setLocation(2, i);
			}
			this.width = 2;
			this.height = this.outputs.get(0).bits;
//			this.outputs.get(0).setLocation(0, outputs.get(0).bits);
			rotate(false);
		} else if (this.properties.get("Orientation").equals("north")) {
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(i).setLocation(i + 1, 0);
			}
			this.height = 2;
			this.width = this.outputs.get(0).bits;
//			this.outputs.get(0).setLocation(0, 2);
			rotate(false);
		} else if (this.properties.get("Orientation").equals("south")) {
			for (int i = 0; i < inputs.size(); i++) {
				inputs.get(i).setLocation(i + 1, 2);
			}
			this.height = 2;
			this.width = this.outputs.get(0).bits;
//			this.outputs.get(0).setLocation(0, 0);
			rotate(false);
		}
	}
}
