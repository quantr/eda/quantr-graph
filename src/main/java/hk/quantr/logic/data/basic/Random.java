/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author michelle
 */
@XStreamAlias("random")
public class Random extends Vertex {

	long min = 0;
	long max = 100;

	public Random(String name) {
		super(name, 3, 1, 4, 4);
		this.properties.put("Name", name);
		this.properties.put("Trigger", "positive");
		this.inputs.get(0).setLocation(0, 1); // clock
		this.inputs.get(1).setLocation(0, 2); // lower range
		this.inputs.get(2).setLocation(0, 3); // upper range
		this.outputs.get(0).setLocation(4, 2);
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.drawPolyline(new int[]{x * gridSize, x * gridSize + gridSize, x * gridSize}, new int[]{y * gridSize + gridSize / 2, y * gridSize + gridSize, y * gridSize + gridSize * 3 / 2}, 3);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 5 / 2));
		g2.drawString("?", x * gridSize + gridSize * 4 / 3, y * gridSize + gridSize * 3);
		for (int i = 0; i < this.inputs.size(); i++) {
			this.inputs.get(i).paint(g2, gridSize);
		}
		this.outputs.get(0).paint(g2, gridSize);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public void eval() {
		if ((this.properties.get("Trigger").equals("positive") && this.inputs.get(0).preValue == 0 && this.inputs.get(0).value == 1)
				|| (this.properties.get("Trigger").equals("negative") && this.inputs.get(0).preValue == 1 && this.inputs.get(0).value == 0)
				|| (this.properties.get("Trigger").equals("event") && this.inputs.get(0).preValue != this.inputs.get(0).value)) {
			this.outputs.get(0).value = (long) ThreadLocalRandom.current().nextInt((int) (Math.max(this.inputs.get(2).value, this.inputs.get(1).value) + 1 - Math.min(this.inputs.get(2).value, this.inputs.get(1).value))) + Math.min(this.inputs.get(2).value, this.inputs.get(1).value);
		}
		super.eval();
	}

	@Override
	public void updateProperty() {
		for (int i = 1; i < this.inputs.size(); i++) {
			if (!this.portConnectedTo(this.inputs.get(i)).isEmpty()) {
				this.inputs.get(i).bits = this.portConnectedTo(this.inputs.get(i)).get(0).bits;
			}
		}
		this.outputs.get(0).bits = Math.max(this.inputs.get(1).bits, this.inputs.get(2).bits);
	}
}
