/*
 * Copyright 2023 chanhuiwah.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.basic;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author chanhuiwah
 */
@XStreamAlias("dipSwitch")
public class DipSwitch extends Vertex {

	public DipSwitch(String name) {
		super(name, 0, 8, 9, 4);
		properties.put("Name", name);
		properties.put("Orientation", "north");
		properties.put("Number of Switch", this.outputs.size());
		this.outputs.get(0).setLocation(1, 0);
		this.outputs.get(1).setLocation(2, 0);
		this.outputs.get(2).setLocation(3, 0);
		this.outputs.get(3).setLocation(4, 0);
		this.outputs.get(4).setLocation(5, 0);
		this.outputs.get(5).setLocation(6, 0);
		this.outputs.get(6).setLocation(7, 0);
		this.outputs.get(7).setLocation(8, 0);
	}

	public void paint(Graphics g) {

		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		if (properties.get("Orientation").equals("north")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, (this.outputs.size() + 1) * gridSize, height * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, (this.outputs.size() + 1) * gridSize, height * gridSize);

			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).paint(g, gridSize);
				g2.setColor(Color.white);
				g2.setFont(new Font("arial", Font.PLAIN, gridSize / 5 * 4));
				g2.drawString(Integer.toString(i + 1), (x + i + 1) * gridSize - gridSize * 1 / 5, (y + 1) * gridSize);
				g2.setColor(Color.lightGray);
				g2.fillRoundRect((x + i) * gridSize + gridSize * 6 / 10, y * gridSize + gridSize * 3 / 2, (int) (gridSize * 0.8f), gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
				g2.setColor(Color.white);
				g2.fillOval((x + i) * gridSize + (gridSize * 6 / 10 + 1), y * gridSize + gridSize * 27 / 10, (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				if (this.outputs.get(i).value == 1) {
					g2.setColor(new Color(50, 210, 50)); // green
					g2.fillRoundRect((x + i) * gridSize + gridSize * 6 / 10, y * gridSize + gridSize * 3 / 2, (int) (gridSize * 0.8f), gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval((x + i) * gridSize + (gridSize * 6 / 10 + 1), y * gridSize + gridSize * 3 / 2 + 1, (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				}
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize + gridSize * 2 / 10, (y + (this.height + 1)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("east")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, width * gridSize, (this.outputs.size() + 1) * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, width * gridSize, (this.outputs.size() + 1) * gridSize);

			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).paint(g, gridSize);
				g2.setColor(Color.white);
				g2.setFont(new Font("arial", Font.PLAIN, gridSize / 5 * 4));
				g2.drawString(Integer.toString(i + 1), (x + 3) * gridSize, (y + i + 1) * gridSize + gridSize / 4);
				if (this.outputs.get(i).value == 1) {
					g2.setColor(new Color(50, 210, 50)); // green
					g2.fillRoundRect(x * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 6 / 10, gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval(x * gridSize + gridSize / 2 + gridSize * 2 - ((int) (gridSize * 0.8f) - 2) - 1, (y + i) * gridSize + (gridSize * 6 / 10 + 1), (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				} else {
					g2.setColor(Color.lightGray);
					g2.fillRoundRect(x * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 6 / 10, gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval(x * gridSize + gridSize / 2 + 1, (y + i) * gridSize + (gridSize * 6 / 10 + 1), (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				}
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, (y + (this.height / 2)) * gridSize + gridSize * 2 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, (this.outputs.size() + 1) * gridSize, height * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, (this.outputs.size() + 1) * gridSize, height * gridSize);

			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).paint(g, gridSize);
				g2.setColor(Color.white);
				g2.setFont(new Font("arial", Font.PLAIN, gridSize / 5 * 4));
				g2.drawString(Integer.toString(i + 1), (x + i + 1) * gridSize - gridSize / 5, (y + 4) * gridSize - gridSize / 2);
				if (this.outputs.get(i).value == 1) {
					g2.setColor(new Color(50, 210, 50)); // green
					g2.fillRoundRect((x + i) * gridSize + gridSize * 6 / 10, y * gridSize + gridSize / 2, (int) (gridSize * 0.8f), gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval((x + i) * gridSize + (gridSize * 6 / 10 + 1), y * gridSize + gridSize / 2 + gridSize * 2 - ((int) (gridSize * 0.8f) - 2) - 1, (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				} else {
					g2.setColor(Color.lightGray);
					g2.fillRoundRect((x + i) * gridSize + gridSize * 6 / 10, y * gridSize + gridSize / 2, (int) (gridSize * 0.8f), gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval((x + i) * gridSize + (gridSize * 6 / 10 + 1), y * gridSize + gridSize / 2 + 1, (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				}
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize, (y - 1) * gridSize + gridSize * 2 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			g2.setColor(Color.darkGray);
			g2.fillRect(x * gridSize, y * gridSize, width * gridSize, (this.outputs.size() + 1) * gridSize);
			g2.setColor(Color.black);
			g2.drawRect(x * gridSize, y * gridSize, width * gridSize, (this.outputs.size() + 1) * gridSize);

			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).paint(g, gridSize);
				g2.setColor(Color.white);
				g2.setFont(new Font("arial", Font.PLAIN, gridSize / 5 * 4));
				g2.drawString(Integer.toString(i + 1), (x + 1) * gridSize - gridSize * 2 / 5, (y + i + 1) * gridSize + gridSize / 4);
				if (this.outputs.get(i).value == 1) {
					g2.setColor(new Color(50, 210, 50)); // green
					g2.fillRoundRect((x + 1) * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 6 / 10, gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval((x + 1) * gridSize + gridSize / 2 + 1, (y + i) * gridSize + (gridSize * 6 / 10 + 1), (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				} else {
					g2.setColor(Color.lightGray);
					g2.fillRoundRect((x + 1) * gridSize + gridSize / 2, (y + i) * gridSize + gridSize * 6 / 10, gridSize * 2, (int) (gridSize * 0.8f), (int) (gridSize * 0.8f), (int) (gridSize * 0.8f));
					g2.setColor(Color.white);
					g2.fillOval((x + 1) * gridSize + gridSize / 2 + gridSize * 2 - ((int) (gridSize * 0.8f) - 2) - 1, (y + i) * gridSize + (gridSize * 6 / 10 + 1), (int) (gridSize * 0.8f) - 2, (int) (gridSize * 0.8f) - 2);
				}
			}
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width + 1)) * gridSize + gridSize * 2 / 10, (y + (this.height / 2)) * gridSize + gridSize * 2 / 10);
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "Dip Switch";
	}

	@Override
	public void eval() {
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		if (this.outputs.size() != (int) properties.get("Number of Switch")) {
			if (this.outputs.size() > (int) properties.get("Number of Switch")) {
				for (int i = this.outputs.size() - 1; i >= (int) properties.get("Number of Switch"); i--) {
					outputs.get(i).edges.clear();
					outputs.remove(i);
				}
			} else if (this.outputs.size() < (int) properties.get("Number of Switch")) {
				for (int i = this.outputs.size(); i < (int) properties.get("Number of Switch"); i++) {
					outputs.add(new Output(this, "Output " + i));
				}
			}
		}

		if (this.properties.get("Orientation").equals("north")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(i + 1, 0);
			}
			this.height = 4;
			this.width = this.outputs.size() + 1;
		} else if (this.properties.get("Orientation").equals("east")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(4, i + 1);
			}
			this.height = this.outputs.size() + 1;
			this.width = 4;
		} else if (this.properties.get("Orientation").equals("south")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(i + 1, 4);
			}
			this.height = 4;
			this.width = this.outputs.size() + 1;
		} else if (this.properties.get("Orientation").equals("west")) {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).setLocation(0, i + 1);
			}
			this.height = this.outputs.size() + 1;
			this.width = 4;
		}
	}

	public void dipSwitchClicked(int x, int y) {
		if (this.properties.get("Orientation").equals("north") || this.properties.get("Orientation").equals("south")) {
			int mouseX = x - this.x * gridSize;
			int mouseY = y - this.y * gridSize;
			mouseX -= gridSize * 3 / 5;
			if (mouseX % gridSize >= 0 && mouseX % gridSize <= (int) (gridSize * 0.8f)) {
				if (mouseX / gridSize <= this.outputs.size()) {
					this.outputs.get(mouseX / gridSize).value = this.outputs.get(mouseX / gridSize).value ^ 1;
				}
			}
		} else if (this.properties.get("Orientation").equals("east") || this.properties.get("Orientation").equals("west")) {
			int mouseX = x - this.x * gridSize;
			int mouseY = y - this.y * gridSize;
			mouseY -= gridSize * 3 / 5;
			if (mouseY % gridSize >= 0 && mouseY % gridSize <= (int) (gridSize * 0.8f)) {
				if (mouseY / gridSize <= this.outputs.size()) {
					this.outputs.get(mouseY / gridSize).value = this.outputs.get(mouseY / gridSize).value ^ 1;
				}
			}
		}
	}
}
