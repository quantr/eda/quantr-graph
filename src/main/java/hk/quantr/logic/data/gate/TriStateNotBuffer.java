/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michelle
 */
@XStreamAlias("triStateNotBuffer")
public class TriStateNotBuffer extends Vertex {

	public String orientation = "east";

	public TriStateNotBuffer(String name) {
		super(name, 2, 1, 3, 2);
		properties.put("Name", name);
		properties.put("Data Bits", 1);
		properties.put("Orientation", orientation);
		inputs.get(0).setLocation(0, 1);
		inputs.get(1).setLocation(1, 2);
		outputs.get(0).setLocation(3, 1);

	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		if (orientation.equals("east")) {
			g2.setColor(Color.blue);
			g2.drawPolyline(new int[]{(x + 1) * gridSize, (x + 1) * gridSize}, new int[]{(y + 1) * gridSize + gridSize / 2, (y + 2) * gridSize}, 2);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{2 * gridSize - gridSize / 10 + x * gridSize, gridSize / 10 * 2 + x * gridSize, gridSize / 10 * 2 + x * gridSize, 2 * gridSize - gridSize / 10 + x * gridSize},
					new int[]{gridSize + y * gridSize, y * gridSize + gridSize / 10 * 2, 2 * gridSize - gridSize / 10 * 2 + y * gridSize, gridSize + y * gridSize}, 4);
			g2.drawOval((x + 2) * gridSize, y * gridSize + gridSize / 2, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			inputs.get(1).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (orientation.equals("west")) {
			g2.setColor(Color.blue);
			g2.drawPolyline(new int[]{(x + 2) * gridSize, (x + 2) * gridSize}, new int[]{y * gridSize, y * gridSize + gridSize / 2}, 2);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{(x + 3) * gridSize, (x + 1) * gridSize, (x + 3) * gridSize, (x + 3) * gridSize}, new int[]{y * gridSize, (y + 1) * gridSize, (y + 2) * gridSize, y * gridSize}, 4);
			g2.drawOval(x * gridSize, y * gridSize + gridSize / 2, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			inputs.get(1).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (orientation.equals("north")) {
			g2.setColor(Color.blue);
			g2.drawPolyline(new int[]{(x + 1) * gridSize + gridSize / 2, (x + 2) * gridSize}, new int[]{(y + 2) * gridSize, (y + 2) * gridSize}, 2);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{(x + 2) * gridSize, (x + 1) * gridSize, x * gridSize, (x + 2) * gridSize}, new int[]{(y + 3) * gridSize, (y + 1) * gridSize, (y + 3) * gridSize, (y + 3) * gridSize}, 4);
			g2.drawOval(x * gridSize + gridSize / 2, y * gridSize, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			inputs.get(1).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			super.paint(g);
		} else if (orientation.equals("south")) {
			g2.setColor(Color.blue);
			g2.drawPolyline(new int[]{x * gridSize + gridSize / 2, x * gridSize}, new int[]{(y + 1) * gridSize, (y + 1) * gridSize}, 2);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, (x + 2) * gridSize, (x + 1) * gridSize, x * gridSize}, new int[]{y * gridSize, y * gridSize, (y + 2) * gridSize, y * gridSize}, 4);
			g2.drawOval(x * gridSize + gridSize / 2, (y + 2) * gridSize, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			inputs.get(1).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			super.paint(g);
		}
	}

	@Override
	public void eval() {
		if (inputs.get(1).value != 0) {
			long temp = inputs.get(0).value;
			this.outputs.get(0).value = (temp ^ (((1L << (this.outputs.get(0).bits - 1)) - 1 + (1L << (this.outputs.get(0).bits - 1)))));
			super.eval();
		}
	}

	@Override
	public void updateProperty() {
		super.updateProperty();
		int databits = (int) properties.get("Data Bits");
		inputs.get(0).bits = databits;
		outputs.get(0).bits = databits;
		this.orientation = (String) properties.get("Orientation");
		if (orientation.equals("east")) {
			this.inputs.get(0).setLocation(0, 1);
			this.inputs.get(1).setLocation(1, 2);
			this.outputs.get(0).setLocation(3, 1);
			this.height = 2;
			this.width = 3;
		} else if (orientation.equals("west")) {
			this.inputs.get(0).setLocation(3, 1);
			this.inputs.get(1).setLocation(2, 0);
			this.outputs.get(0).setLocation(0, 1);
			this.height = 2;
			this.width = 3;
		} else if (orientation.equals("north")) {
			this.inputs.get(0).setLocation(1, 3);
			this.inputs.get(1).setLocation(2, 2);
			this.outputs.get(0).setLocation(1, 0);
			this.height = 3;
			this.width = 2;
		} else if (orientation.equals("south")) {
			this.inputs.get(0).setLocation(1, 0);
			this.inputs.get(1).setLocation(0, 1);
			this.outputs.get(0).setLocation(1, 3);
			this.height = 3;
			this.width = 2;
		}
	}
}
