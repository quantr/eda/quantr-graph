/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("oddParity")
public class OddParity extends Vertex {

	public OddParity(String name) {
		super(name, 2, 1, 4, 4);
		properties.put("Name", name);
		properties.put("No. of Inputs", 2);
		properties.put("Data Bits", 1);
		inputs.get(0).setLocation(0, 1);
		inputs.get(1).setLocation(0, 3);
		outputs.get(0).setLocation(4, 2);
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("Odd", x * gridSize + (int) (gridSize * 0.8f), (y + height / 2) * gridSize + gridSize * 3 / 5);

		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).paint(g, gridSize);
		}
		outputs.get(0).paint(g, gridSize);
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public void eval() {
		outputs.get(0).value = inputs.get(0).value;
		for (int i = 1; i < inputs.size(); i++) {
			outputs.get(0).value = ~(inputs.get(i).value ^ outputs.get(0).value);
		}
		super.eval();
	}

	@Override
	public void updateProperty() {
		outputs.get(0).bits = (int) properties.get("Data Bits");
		if ((int) properties.get("No. of Inputs") >= 2) {
			if (this.inputs.size() > (int) properties.get("No. of Inputs")) {
				for (int i = this.inputs.size() - 1; i > (int) properties.get("No. of Inputs") - 1; i--) {
					inputs.get(i).edges.clear();
					inputs.remove(i);
				}
			} else if (this.inputs.size() < (int) properties.get("No. of Inputs")) {
				for (int i = this.inputs.size(); i < (int) properties.get("No. of Inputs"); i++) {
					inputs.add(new Input(this, "Output " + i));
				}
			}
		} else {
			properties.put("No. of Inputs", 2);
			inputs.get(0).bits = 2;
			for (int i = this.inputs.size() - 1; i > (int) properties.get("No. of Inputs") - 1; i--) {
				inputs.get(i).edges.clear();
				inputs.remove(i);
			}
		}
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).setLocation(0, 1 + i + ((i >= this.inputs.size() / 2 && this.inputs.size() % 2 == 0) ? 1 : 0));
			inputs.get(i).bits = outputs.get(0).bits;
		}
		height = inputs.size() - inputs.size() % 2 + 2;
		this.outputs.get(0).setLocation(this.width, this.height / 2);
	}
}
