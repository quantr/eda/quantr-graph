/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.github.javabdd.BDD;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.engine.Simulate;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("norGate")
public class NorGate extends Vertex {

	public NorGate(String name) {
		super(name, 2, 1, 6, 4);
		properties.put("Name", name);
		properties.put("No. Of Inputs", 2);
		properties.put("Data Bits", 1);
		properties.put("Orientation", "east");
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		if (properties.get("Orientation").equals("east")) {
			LogicGateDrawer.drawNorGate(g, this, x * gridSize, y * gridSize, this.gridSize, stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y + (this.height / 2)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			g2.setColor(Color.blue);
			g2.drawLine((x + this.width / 2 - 1) * gridSize, y * gridSize, (x + this.width / 2 - 1) * gridSize, (y + 1) * gridSize - gridSize * 2 / 10);
			g2.drawLine((x + this.width / 2 + 1) * gridSize, y * gridSize, (x + this.width / 2 + 1) * gridSize, (y + 1) * gridSize - gridSize * 2 / 10);
			if (this.inputs.size() % 2 == 1) {
				g2.drawLine((x + this.width / 2) * gridSize, y * gridSize, (x + this.width / 2) * gridSize, (y + 1) * gridSize);
			}
			g2.setColor(Color.black);
			g2.drawArc((x + this.width / 2 - 2) * gridSize, (y - 1) * gridSize, 4 * gridSize, 2 * gridSize, 0, -180);
			g2.drawArc((x + this.width / 2 + 2) * gridSize - 4 * gridSize, (y - 6) * gridSize, 9 * gridSize, 12 * gridSize, 180, 56);
			g2.drawArc((x + this.width / 2 - 3) * gridSize - 4 * gridSize, (y - 6) * gridSize, 9 * gridSize, 12 * gridSize, 0, -56);
			g2.drawOval((x + this.width / 2) * gridSize - gridSize / 2, (y + 5) * gridSize, gridSize, gridSize);
			g2.drawLine(x * gridSize, y * gridSize + gridSize * 2 / 10, (x + this.width / 2 - 2) * gridSize, y * gridSize + gridSize * 2 / 10);
			g2.drawLine((x + this.width / 2 + 2) * gridSize, y * gridSize + gridSize * 2 / 10, (x + this.width) * gridSize, y * gridSize + gridSize * 2 / 10);
			for (int i = 0; i < this.inputs.size(); i++) {
				inputs.get(i).paint(g, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize + gridSize / 5, (y + (this.height / 2)) * gridSize - gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			g2.setColor(Color.blue);
			g2.drawLine((x + 5) * gridSize + gridSize * 2 / 10, (y + this.height / 2 - 1) * gridSize, (x + 6) * gridSize, (y + this.height / 2 - 1) * gridSize);
			g2.drawLine((x + 5) * gridSize + gridSize * 2 / 10, (y + this.height / 2 + 1) * gridSize, (x + 6) * gridSize, (y + this.height / 2 + 1) * gridSize);
			if (this.inputs.size() % 2 == 1) {
				g2.drawLine((x + 5) * gridSize, (y + this.height / 2) * gridSize, (x + 6) * gridSize, (y + this.height / 2) * gridSize);
			}
			g2.setColor(Color.black);
			g2.drawArc((x + 5) * gridSize, (y + this.height / 2 - 2) * gridSize, 2 * gridSize, 4 * gridSize, 90, 180);
			g2.drawArc(x * gridSize, (y + this.height / 2 - 2) * gridSize, 12 * gridSize, 9 * gridSize, 90, 56);
			g2.drawArc(x * gridSize, (y + this.height / 2 - 7) * gridSize, 12 * gridSize, 9 * gridSize, -90, -56);
			g2.drawOval(x * gridSize, (y + this.height / 2) * gridSize - gridSize / 2, gridSize, gridSize);
			g2.drawLine((x + 6) * gridSize - gridSize * 2 / 10, y * gridSize, (x + 6) * gridSize - gridSize * 2 / 10, (y + this.height / 2 - 2) * gridSize);
			g2.drawLine((x + 6) * gridSize - gridSize * 2 / 10, (y + this.height) * gridSize, (x + 6) * gridSize - gridSize * 2 / 10, (y + this.height / 2 + 2) * gridSize);
			for (int i = 0; i < this.inputs.size(); i++) {
				inputs.get(i).paint(g, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize, (y + (this.height / 2)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("north")) {
			g2.setColor(Color.blue);
			g2.drawLine((x + this.width / 2 - 1) * gridSize, (y + 5) * gridSize + gridSize * 2 / 10, (x + this.width / 2 - 1) * gridSize, (y + 6) * gridSize);
			g2.drawLine((x + this.width / 2 + 1) * gridSize, (y + 5) * gridSize + gridSize * 2 / 10, (x + this.width / 2 + 1) * gridSize, (y + 6) * gridSize);
			if (this.inputs.size() % 2 == 1) {
				g2.drawLine((x + this.width / 2) * gridSize, (y + 5) * gridSize, (x + this.width / 2) * gridSize, (y + 6) * gridSize);
			}
			g2.setColor(Color.black);
			g2.drawArc((x + this.width / 2 - 2) * gridSize, (y + 5) * gridSize, 4 * gridSize, 2 * gridSize, 0, 180);
			g2.drawArc((x + this.width / 2 + 2) * gridSize - 4 * gridSize, y * gridSize, 9 * gridSize, 12 * gridSize, -180, -56);
			g2.drawArc((x + this.width / 2 - 3) * gridSize - 4 * gridSize, y * gridSize, 9 * gridSize, 12 * gridSize, 0, 56);
			g2.drawOval((x + this.width / 2) * gridSize - gridSize / 2, y * gridSize, gridSize, gridSize);
			g2.drawLine(x * gridSize, (y + 6) * gridSize - gridSize * 2 / 10, (x + this.width / 2 - 2) * gridSize, (y + 6) * gridSize - gridSize * 2 / 10);
			g2.drawLine((x + this.width / 2 + 2) * gridSize, (y + 6) * gridSize - gridSize * 2 / 10, (x + this.width) * gridSize, (y + 6) * gridSize - gridSize * 2 / 10);
			for (int i = 0; i < this.inputs.size(); i++) {
				inputs.get(i).paint(g, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize + gridSize / 5, (y + (this.height / 2) + 1) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "Nor Gate";
	}

	@Override
	public void eval() {
		long temp = inputs.get(0).value;
		for (int i = 1; i < this.inputs.size(); i++) {
			temp = temp | this.inputs.get(i).value;
		}
		this.outputs.get(0).value = (temp ^ (((1L << (this.outputs.get(0).bits - 1)) - 1 + (1L << (this.outputs.get(0).bits - 1)))));
		super.eval();
	}

	@Override
	public void updateProperty() {
		super.updateProperty();
		int databits = (int) properties.get("Data Bits");
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).bits = databits;
		}
		outputs.get(0).bits = databits;
	}
}
