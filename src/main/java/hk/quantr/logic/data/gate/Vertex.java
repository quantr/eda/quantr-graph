 /*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.QuantrGraphCanvas;
import hk.quantr.logic.data.basic.OutputPin;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.engine.Simulate;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("vertex")
public abstract class Vertex implements Simulate, Cloneable {

//	public ArrayList<Boolean> timeLine = new ArrayList();
	public ArrayList<Output> outputs = new ArrayList();
	public boolean isSelected;
	public int gridSize = 10;
	public String name;
	public ArrayList<Input> inputs = new ArrayList();
	public LinkedHashMap<String, Object> properties = new LinkedHashMap();
	public int x;
	public int y;
	public int width;
	public int height;
	public int level;
	public boolean showLevel;
	public long simTime = 0;
	public int initX = -1;
	public int initY = -1;

	public Vertex(String name, int inputPins, int outputPins, int width, int height) {
		setSize(width, height);
		this.name = name;
		properties.put("Label", "");
		int cnt = 0;
		for (int i = -inputPins / 2; i <= inputPins / 2; i++) {
			if (inputPins % 2 == 0 && i == 0) {
				continue;
			}
			Input in = new Input(this, this.name + " input " + cnt++);
			in.setLocation(0, this.height / 2 + i);
			inputs.add(in);
		}
		for (int i = 0; i < outputPins; i++) {
			Output output = new Output(this, this.name + " output");
			output.setLocation(width, height / 2);
			outputs.add(output);
		}
	}

	public ArrayList<Port> portConnectedTo(Port port) {
		ArrayList<Port> temp = new ArrayList<Port>();
		for (Edge e : port.edges) {
			for (Port p : e.findConnectedPorts(port)) {
				if (!temp.contains(p)) {
					temp.add(p);
				}
			}
		}
		temp.remove(port);
		return temp;
	}

	public ArrayList<Port> outputConnectedTo() {
		ArrayList<Port> temp = new ArrayList();
		for (Output o : this.outputs) {
			for (Edge e : o.edges) {
				for (Port p : e.findConnectedPorts()) {
					if (!temp.contains(p) && p != o) {
						temp.add(p);
					}
				}
			}
		}
		return temp;
	}

	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getY() {
		return y;
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void paint(Graphics g) {
		if (isSelected) {
			g.setColor(Color.blue);
			g.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		}
		g.setColor(Color.red);
		g.setFont(new Font("arial", Font.BOLD, 10));

		if (showLevel) {
			for (Input input : inputs) {
				g.drawString(String.valueOf(input.level), input.getAbsolutionX() * gridSize - 10, input.getAbsolutionY() * gridSize);
				g.drawString(String.valueOf(input.state), input.getAbsolutionX() * gridSize - 15, input.getAbsolutionY() * gridSize - 10);
			}
			g.drawString(String.valueOf(level), x * gridSize - 10, y * gridSize);
			for (int i = 0; i < outputs.size(); i++) {
				g.drawString(String.valueOf(level), outputs.get(i).getAbsolutionX() * gridSize + 10, outputs.get(i).getAbsolutionY() * gridSize);
				g.drawString(String.valueOf(outputs.get(i).state), outputs.get(i).getAbsolutionX() * gridSize + 5, outputs.get(i).getAbsolutionY() * gridSize - 10);

			}
		}
	}

	public String getTypeName() {
		return getClass().getSimpleName();
	}

	public void updateProperty(QuantrGraphCanvas quantrGraphCanvas1) {
		if (this instanceof VCD) {
			this.name = (String) this.properties.get("Module");
		} else {
			this.name = (String) this.properties.get("Name");
		}
	}

	public void updateProperty() {
		if (this instanceof AndGate || this instanceof NandGate || this instanceof NorGate || this instanceof OrGate || this instanceof XnorGate || this instanceof XorGate) {
			int noOfPin = (int) this.properties.get("No. Of Inputs");
			if (noOfPin > this.inputs.size()) {
				for (int i = this.inputs.size() + 1; i <= noOfPin; i++) {
					this.inputs.add(new Input(this, this.name + " input " + i));
				}
			} else {
				for (int i = this.inputs.size() - 1; i >= noOfPin; i--) {
					this.inputs.get(i).edges.clear();
					this.inputs.remove(i);
				}
			}

			if (properties.get("Orientation").equals("east")) {
				this.setSize(getwidthConstant(), Math.max(this.inputs.size() - this.inputs.size() % 2, (this instanceof NotGate ? 2 : 4)));
				int cnt = 0;
				for (Input in : this.inputs) {
					in.deltaX = 0;
					in.deltaY = cnt++ + (cnt > this.inputs.size() / 2 ? this.inputs.size() % 2 ^ 1 : 0) + (this.inputs.size() <= 3 ? 1 : 0);
				}
				this.outputs.get(0).deltaX = this.width;
				this.outputs.get(0).deltaY = this.height / 2;
			} else if (properties.get("Orientation").equals("south")) {
				this.setSize(Math.max(this.inputs.size() - this.inputs.size() % 2, (this instanceof NotGate ? 2 : 4)), getwidthConstant());
				int cnt = 0;
				for (Input in : this.inputs) {
					in.deltaX = cnt++ + (cnt > this.inputs.size() / 2 ? this.inputs.size() % 2 ^ 1 : 0) + (this.inputs.size() <= 3 ? 1 : 0);
					in.deltaY = 0;
				}
				this.outputs.get(0).deltaX = this.width / 2;
				this.outputs.get(0).deltaY = this.height;
			} else if (properties.get("Orientation").equals("west")) {
				this.setSize(getwidthConstant(), Math.max(this.inputs.size() - this.inputs.size() % 2, (this instanceof NotGate ? 2 : 4)));
				int cnt = 0;
				for (Input in : this.inputs) {
					in.deltaX = width;
					in.deltaY = cnt++ + (cnt > this.inputs.size() / 2 ? this.inputs.size() % 2 ^ 1 : 0) + (this.inputs.size() <= 3 ? 1 : 0);
				}
				this.outputs.get(0).deltaX = 0;
				this.outputs.get(0).deltaY = this.height / 2;
			} else if (properties.get("Orientation").equals("north")) {
				this.setSize(Math.max(this.inputs.size() - this.inputs.size() % 2, (this instanceof NotGate ? 2 : 4)), getwidthConstant());
				int cnt = 0;
				for (Input in : this.inputs) {
					in.deltaX = cnt++ + (cnt > this.inputs.size() / 2 ? this.inputs.size() % 2 ^ 1 : 0) + (this.inputs.size() <= 3 ? 1 : 0);
					in.deltaY = this.height;
				}
				this.outputs.get(0).deltaX = this.width / 2;
				this.outputs.get(0).deltaY = 0;
			}

		}
	}

	public float[][] getOrientPrefix() {
		/* orientPrefix : 
		[0][] for east, [1][]-south, [2][]-west, [3][]-north
		Override to getOrientPrefix to set the numbers
		the x-y distance between location-fixed port and the location of the vertex
		so the vertex can rotate with the wanted fixed port without disconnecting wire, e.g. Pin and Probe
		and no need many hard code
		[][0] and [][1] for x-coordinate adjustment
		[][2] and [][3] for y-coordinate adjustment
		[0] and [2] are constant int for adjustment
		[1] is width, [3] is height
		 */
		return null; // always return null if this method is not overrided
		// return new float[4][4]; return this in override with initalize values
	}

	public void rotate(boolean inputPortCentralized) {
		float[][] orientPrefix = getOrientPrefix();
		Port port;
		if (inputPortCentralized) {
			port = inputs.get(0);
		} else {
			port = outputs.get(0);
		}
		if (orientPrefix == null) {
			return;
		}
		if (properties.get("Orientation").equals("east")) {
			x = port.getAbsolutionX() - (int) (orientPrefix[0][0] + width * orientPrefix[0][1]);
			y = port.getAbsolutionY() - (int) (orientPrefix[0][2] + height * orientPrefix[0][3]);
			port.setLocation((int) (orientPrefix[0][0] + width * orientPrefix[0][1]), (int) (orientPrefix[0][2] + height * orientPrefix[0][3]));
		} else if (properties.get("Orientation").equals("south")) {
			x = port.getAbsolutionX() - (int) (orientPrefix[1][0] + width * orientPrefix[1][1]);
			y = port.getAbsolutionY() - (int) (orientPrefix[1][2] + height * orientPrefix[1][3]);
			port.setLocation((int) (orientPrefix[1][0] + width * orientPrefix[1][1]), (int) (orientPrefix[1][2] + height * orientPrefix[1][3]));
		} else if (properties.get("Orientation").equals("west")) {
			x = port.getAbsolutionX() - (int) (orientPrefix[2][0] + width * orientPrefix[2][1]);
			y = port.getAbsolutionY() - (int) (orientPrefix[2][2] + height * orientPrefix[2][3]);
			port.setLocation((int) (orientPrefix[2][0] + width * orientPrefix[2][1]), (int) (orientPrefix[2][2] + height * orientPrefix[2][3]));
		} else if (properties.get("Orientation").equals("north")) {
			x = port.getAbsolutionX() - (int) (orientPrefix[3][0] + width * orientPrefix[3][1]);
			y = port.getAbsolutionY() - (int) (orientPrefix[3][2] + height * orientPrefix[3][3]);
			port.setLocation((int) (orientPrefix[3][0] + width * orientPrefix[3][1]), (int) (orientPrefix[3][2] + height * orientPrefix[3][3]));
		}
	}

	public int getwidthConstant() {
		if (this instanceof AndGate) {
			return 5;
		} else if (this instanceof NandGate) {
			return 6;
		} else if (this instanceof OrGate) {
			return 5;
		} else if (this instanceof NorGate) {
			return 6;
		} else if (this instanceof XorGate) {
			return 6;
		} else if (this instanceof XnorGate) {
			return 7;
		}
		return 0;
	}

	public void integerToBinary(ArrayList<Boolean> bit, int value) {
		while (value > Math.pow(2, bit.size())) {

		}
	}

	@Override
	public void eventTriggerEval() {
		if (this.inputs.isEmpty()) {
			this.eval();
			return;
		}
		for (int i = 0; i < this.inputs.size(); i++) {
			if (this.inputs.get(i).value != this.inputs.get(i).preValue) {
				this.eval();
				return;
			}
		}

	}

	@Override
	public void eval() {
//		for (int i = 0; i < this.outputs.size(); i++) {
//			for (Port p : this.portConnectedTo(this.outputs.get(i))) {
//				if (p instanceof Input) {
//					if (p.bits == this.outputs.get(i).bits) {
////						p.state = this.outputs.get(0).value == 0 ? "off" : "on";
//						p.value = this.outputs.get(i).value;
//					}
//				}
//			}
//		}

		for (int i = 0; i < this.outputs.size(); i++) {
			for (Input p : this.outputs.get(i).ports) {
				if (p.bits == this.outputs.get(i).bits) {
//						p.state = this.outputs.get(0).value == 0 ? "off" : "on";
					p.value = this.outputs.get(i).value;
				}

			}
		}
		for (int i = 0; i < this.inputs.size(); i++) {
			this.inputs.get(i).preValue = this.inputs.get(i).value;
		}
	}

	@Override
	public void simulateInit() {
//		this.timeLine = new ArrayList();

		for (Input input : this.inputs) {
			input.value = 0;
			input.preValue = -1;
		}
		for (int j = 0; j < this.outputs.size(); j++) {
			this.outputs.get(j).ports.clear();
			for (Port p : this.portConnectedTo(this.outputs.get(j))) {
				if (p instanceof Input) {
					this.outputs.get(j).ports.add((Input) p);
				}
			}
		}
	}

	public void reset() {
		for (Input input : this.inputs) {
			input.value = 0;
			input.preValue = -1;
		}
		for (Output output : this.outputs) {
			output.value = 0;
			output.preValue = -1;
		}
	}

	public void eval2() {

	}

	public boolean loopFeedback(Set<Vertex> visited, Vertex start, Vertex last, boolean begin) {
		if (!visited.contains(last)) {
			visited.add(last);
		} else {
			return true;
		}
		for (Input port : last.inputs) {
			if (port.getConnectedOutput() != null) {
				Vertex next = port.getConnectedOutput().parent;
				if (start == last && !(start instanceof OutputPin) && !begin) {
					return true;
				} else {
					return loopFeedback(visited, start, next, false);
				}
			}
		}
		visited.remove(last);
		return false;
	}

	public boolean hasFeedback() {
		Set<Vertex> visited = new HashSet();
		return loopFeedback(visited, this, this, true);
	}

	// buggy
	public int log(int b, long x) {
		int result = 0;
		if (!((x & 1 % 2) == 1 ^ x > 0)) {
			result = 1;
		}
		System.out.println("x : " + (Long.parseUnsignedLong(Long.toUnsignedString(x)) > 1));
		while (Long.parseUnsignedLong(Long.toUnsignedString(x)) > 1) {
			x = Long.divideUnsigned(Long.parseUnsignedLong(Long.toUnsignedString(x)), b);
			System.out.println("x: " + x);
			result++;
		}
		return result;
	}

	public long pow(int a, int n) {
		long res = 1;
		for (int i = 0; i < n; i++) {
			res = Long.parseUnsignedLong(Long.toUnsignedString(Long.parseUnsignedLong(Long.toUnsignedString(res)) * a));
		}
		return res;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Vertex temp = (Vertex) super.clone();
		temp.name = this.name + "_copy";
		temp.inputs = new ArrayList<>();
		temp.outputs = new ArrayList<>();
		temp.properties = new LinkedHashMap();
		int cnt = 0;
		for (int i = -this.inputs.size() / 2; i <= this.inputs.size() / 2; i++) {
			if (this.inputs.size() % 2 == 0 && i == 0) {
				continue;
			}
			Input in = new Input(temp, temp.name + " input " + cnt++);
			in.setLocation(0, this.height / 2 + i);
			temp.inputs.add(in);
		}
		for (int i = 0; i < this.outputs.size(); i++) {
			Output output = new Output(temp, temp.name + " output");
			output.setLocation(width, height / 2);
			temp.outputs.add(output);
		}
		temp.setLocation(this.x + 1, this.y + 1);
		boolean isFirstKey = true;
		for (String key : this.properties.keySet()) {
			if (isFirstKey) {
				isFirstKey = false;
				temp.properties.put(key, temp.name);
			} else {
				temp.properties.put(key, this.properties.get(key));
			}
		}
		for (int i = 0; i < this.inputs.size(); i++) {
			temp.inputs.get(i).setLocation(this.inputs.get(i).deltaX, this.inputs.get(i).deltaY);
			temp.inputs.get(i).bits = this.inputs.get(i).bits;
		}
		for (int i = 0; i < this.outputs.size(); i++) {
			temp.outputs.get(i).setLocation(this.outputs.get(i).deltaX, this.outputs.get(i).deltaY);
			temp.outputs.get(i).bits = this.outputs.get(i).bits;
		}
		Field[] fields = temp.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(this);
				if (value instanceof char[]) {
					try {
						field.set(temp, Arrays.copyOf((char[]) value, ((char[]) value).length));
					} catch (IllegalArgumentException | IllegalAccessException ex) {
						Logger.getLogger(Vertex.class.getName()).log(Level.SEVERE, null, ex);
					}
				} else if (value instanceof List<?>) {
					try {
						field.set(temp, new ArrayList<>((List<?>) value));
					} catch (IllegalArgumentException | IllegalAccessException ex) {
						Logger.getLogger(Vertex.class.getName()).log(Level.SEVERE, null, ex);
					}
				} else if (value instanceof java.util.Random) {
					field.set(temp, new java.util.Random());

				} else {
					field.set(temp, value);
				}
			} catch (IllegalArgumentException | IllegalAccessException ex) {
				Logger.getLogger(Vertex.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		temp.simulateInit();
		return temp;
	}
}
