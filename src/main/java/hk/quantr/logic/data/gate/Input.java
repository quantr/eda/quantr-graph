/*
 * Copyright 2023 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author peter
 */
@XStreamAlias("input")
public class Input extends Port {

	public int dataPortIndex = -1;

	public Input(Vertex parent, String name) {
		super(parent, name);
	}

	public void paint(Graphics g, int gridSize) {
		g.setColor(Color.blue);
		float ovalSize = gridSize * 0.4f * (getConnectionState() ? 4 : 1) * (this.vertexPort != null && !this.vertexPort.edges.isEmpty() ? 2 : 1);
		g.fillOval((int) (getAbsolutionX() * gridSize - ovalSize * 0.5f), (int) (getAbsolutionY() * gridSize - ovalSize * 0.5f), (int) ovalSize, (int) ovalSize);
	}

	public boolean contains(String valueOf) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	public Output getConnectedOutput() {
		for (int i = 0; i < this.parent.portConnectedTo(this).size(); i++) {
			if (this.parent.portConnectedTo(this).get(i) instanceof Output) {
				return (Output) this.parent.portConnectedTo(this).get(i);
			}
		}
		return null;
	}
}
