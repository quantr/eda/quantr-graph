/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("buffer")
public class Buffer extends Vertex {

	public Buffer(String name) {
		super(name, 1, 1, 2, 2);
		properties.put("Name", name);
		properties.put("Data Bits", 1);
		properties.put("Orientation", "east");
		inputs.get(0).setLocation(0, 1);
		outputs.get(0).setLocation(2, 1);
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		if (properties.get("Orientation").equals("east")) {
			g2.drawPolyline(new int[]{2 * gridSize - gridSize / 10 + x * gridSize, gridSize / 10 * 2 + x * gridSize, gridSize / 10 * 2 + x * gridSize, 2 * gridSize - gridSize / 10 + x * gridSize},
					new int[]{gridSize + y * gridSize, y * gridSize + gridSize / 10 * 2, 2 * gridSize - gridSize / 10 * 2 + y * gridSize, gridSize + y * gridSize}, 4);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			g2.drawPolyline(new int[]{(x + 2) * gridSize, x * gridSize, (x + 2) * gridSize, (x + 2) * gridSize}, new int[]{y * gridSize, (y + 1) * gridSize, (y + 2) * gridSize, y * gridSize}, 4);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
			super.paint(g);
		} else if (properties.get("Orientation").equals("north")) {
			g2.drawPolyline(new int[]{(x + 2) * gridSize, (x + 1) * gridSize, x * gridSize, (x + 2) * gridSize}, new int[]{(y + 2) * gridSize, y * gridSize, (y + 2) * gridSize, (y + 2) * gridSize}, 4);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			g2.drawPolyline(new int[]{x * gridSize, (x + 2) * gridSize, (x + 1) * gridSize, x * gridSize}, new int[]{y * gridSize, y * gridSize, (y + 2) * gridSize, y * gridSize}, 4);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			super.paint(g);
		}
	}

	@Override
	public void eval() {
		this.outputs.get(0).value = inputs.get(0).value;
		super.eval();
	}

	@Override
	public void updateProperty() {
		int databits = (int) properties.get("Data Bits");
		inputs.get(0).bits = databits;
		outputs.get(0).bits = databits;
		if (properties.get("Orientation").equals("east")) {
			this.inputs.get(0).setLocation(0, 1);
			this.outputs.get(0).setLocation(2, 1);
		} else if (properties.get("Orientation").equals("west")) {
			this.inputs.get(0).setLocation(2, 1);
			this.outputs.get(0).setLocation(0, 1);
		} else if (properties.get("Orientation").equals("north")) {
			this.inputs.get(0).setLocation(1, 2);
			this.outputs.get(0).setLocation(1, 0);
		} else if (properties.get("Orientation").equals("south")) {
			this.inputs.get(0).setLocation(1, 0);
			this.outputs.get(0).setLocation(1, 2);
		}
	}
}
