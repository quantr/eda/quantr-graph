/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("port")
public class Port {

	public Vertex parent;
	public String name;
	public int deltaX;
	public int deltaY;
	public int x;
	public int y;
	public ArrayList<Edge> edges = new ArrayList();
	private boolean connected = false;
	@XStreamOmitField
	public Port vertexPort = null;
	public int level = -1;
	public String state = "off";
//	public boolean selected = false;
//	public boolean state = false;
//	public boolean presentstate = false;
	public int bits = 1;
	public long value = 0;
	public long preValue = -1;
	public String ref = "";
	public boolean vcdDefined = false;

	public Port(Vertex v, String name) {
		this.parent = v;
		this.name = name;
	}

	public Port(String name) {
		this.name = name;
	}

	public Port(String name, int x, int y) {
		this.name = name;
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return name + ", " + deltaX + " / " + deltaY;
	}

	public void setLocation(int deltaX, int deltaY) {
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}

	public int getAbsolutionX() {
		if (parent == null) {
			return x;
		} else {
			return parent.x + deltaX;
		}
	}

	public int getAbsolutionY() {
		if (parent == null) {
			return y;
		} else {
			return parent.y + deltaY;
		}
	}

	public String refFormatter(int refCnt) {
		if (refCnt < 94) {
			this.ref += Character.toString((char) (refCnt + 33));
		} else {
			int quotient = (refCnt - 94) / 93;
			int remainder = (refCnt - 94) % 93;
			this.ref += Character.toString((char) (33 + remainder)) + refFormatter(quotient);
		}
		System.out.println("ref: " + this.ref);
		return this.ref;
	}

	public void setStatus(String s) {
		if (s.equals("c")) {
			state = "c";
		} else if (s.equals("on")) {
			state = "on";
		} else if (s.equals("off")) {
			state = "off";
		} else if (s.equals("bit")) {
			state = "bit";
		}
	}

	public void connect() {
		connected = true;
	}

	public void disconnect() {
		connected = false;
	}

	public boolean getConnectionState() {
		return connected;
	}

	public void addEdge(Edge e, int i) {

	}

	public double getDistanceAutoEdge(int x, int y) {
		return Point2D.distance(x, y, this.getAbsolutionX(), this.getAbsolutionY());
	}

	public void paint(Graphics g, int gridSize) {
		int ovalSize = gridSize * 2 / 3;
		g.fillOval((int) (x * gridSize - ovalSize * 0.5f), (int) (y * gridSize - ovalSize * 0.5f), ovalSize, ovalSize);
	}
}
