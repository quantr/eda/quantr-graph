/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import hk.quantr.logic.data.basic.Clock;
import hk.quantr.logic.data.basic.Tunnel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("edge")
public class Edge {

	public Point start, end;
	public String name;
	public Port startPort, endPort;
	public int gridSize = 10;
	public boolean isSelected = false;
	private int identity;
//	@XStreamImplicit
//	@XStreamOmitField

	public Edge(String name, Port startPort, Port endPort) {
		this.name = name;
		this.endPort = endPort;
		this.startPort = startPort;
		start = new Point(startPort.x, startPort.y);
		end = new Point(endPort.x, endPort.y);
		this.name += " " + start + "to" + end;
	}

	public ArrayList<Edge> findConnectedEdges() {
		ArrayList<Port> ports = new ArrayList();
		ArrayList<Edge> edges = new ArrayList();
		if (this.startPort != null) {
			ports.add(startPort);
		}
		if (this.endPort.vertexPort != null) {
			ports.add(endPort);
		}
		ArrayList<Edge> finished = new ArrayList();
		this.searchGridPorts(this, this, ports, finished);
		for (Port p : ports) {
			for (Edge e : p.edges) {
				if (!edges.contains(e) && e != this) {
					edges.add(e);
				}
			}
		}
		return edges;
	}

	public ArrayList<Port> findConnectedPorts() {
		ArrayList<Port> ports = new ArrayList();
		if (this.startPort.vertexPort != null) {
			ports.add(startPort.vertexPort);
		}
		if (this.endPort.vertexPort != null) {
			ports.add(endPort.vertexPort);
		}
		ArrayList<Edge> finished = new ArrayList();
		this.searchPorts(this, this, ports, finished);
		return ports;
	}

	public ArrayList<Port> findConnectedPorts(Port port) {
		ArrayList<Port> ports = new ArrayList();
		if (this.startPort.vertexPort != null) {
			ports.add(startPort.vertexPort);
		}
		if (this.endPort.vertexPort != null) {
			ports.add(endPort.vertexPort);
		}
		ArrayList<Edge> finished = new ArrayList();
		this.searchPorts(this, this, ports, finished);
		ports.add(port);
		return ports;
	}

	public void searchGridPorts(Edge e, Edge last, ArrayList<Port> ports, ArrayList<Edge> finished) {
		if (finished.contains(e)) {
			return;
		} else {
			finished.add(e);
		}
		for (Edge temp : e.startPort.edges) {
			if (last != temp) {
				if (temp.startPort != null) {
					if (!ports.contains(temp.startPort)) {
						ports.add(temp.startPort);
					}
				}
				if (!temp.startPort.edges.isEmpty()) {
					searchGridPorts(temp, e, ports, finished);
				}
				if (temp.endPort != null) {
					if (!ports.contains(temp.endPort)) {
						ports.add(temp.endPort);
					}
				}
				if (!temp.endPort.edges.isEmpty()) {
					searchGridPorts(temp, e, ports, finished);
				}
			}
		}
		for (Edge temp : e.endPort.edges) {
			if (last != temp) {
				if (temp.startPort != null) {
					if (!ports.contains(temp.startPort)) {
						ports.add(temp.startPort);
					}
				}
				if (!temp.startPort.edges.isEmpty()) {
					searchGridPorts(temp, e, ports, finished);
				}
				if (temp.endPort != null) {
					if (!ports.contains(temp.endPort)) {
						ports.add(temp.endPort);
					}
				}
				if (!temp.endPort.edges.isEmpty()) {
					searchGridPorts(temp, e, ports, finished);
				}
			}
		}
	}

	public void searchPorts(Edge e, Edge last, ArrayList<Port> ports, ArrayList<Edge> finished) {
		if (finished.contains(e)) {
			return;
		} else {
			finished.add(e);
		}
		for (Edge temp : e.startPort.edges) {
			if (last != temp) {
				if (temp.startPort.vertexPort != null) {
					if (!ports.contains(temp.startPort.vertexPort)) {
						ports.add(temp.startPort.vertexPort);
					}
				}
				if (!temp.startPort.edges.isEmpty()) {
					searchPorts(temp, e, ports, finished);
				}
				if (temp.endPort.vertexPort != null) {
					if (!ports.contains(temp.endPort.vertexPort)) {
						ports.add(temp.endPort.vertexPort);
					}
				}
				if (!temp.endPort.edges.isEmpty()) {
					searchPorts(temp, e, ports, finished);
				}
			}
		}
		for (Edge temp : e.endPort.edges) {
			if (last != temp) {
				if (temp.startPort.vertexPort != null) {
					if (!ports.contains(temp.startPort.vertexPort)) {
						ports.add(temp.startPort.vertexPort);
					}
				}
				if (!temp.startPort.edges.isEmpty()) {
					searchPorts(temp, e, ports, finished);
				}
				if (temp.endPort.vertexPort != null) {
					if (!ports.contains(temp.endPort.vertexPort)) {
						ports.add(temp.endPort.vertexPort);
					}
				}
				if (!temp.endPort.edges.isEmpty()) {
					searchPorts(temp, e, ports, finished);
				}
			}
		}
	}

//	public void removePort(Port port) {
//		if (port == this.startPort) {
//			this.startPort = null;
//			this.startVertex = null;
//		} else if (port == this.endPort) {
//			this.endPort = null;
//			this.endVertex = null;
//		}
//	}
//
//	public void removeVertex(Vertex vertex) {
//		if (vertex == this.startVertex) {
//			this.startPort = null;
//			this.startVertex = null;
//		} else if (vertex == this.endVertex) {
//			this.endPort = null;
//			this.endVertex = null;
//		}
//	}
//
//	public void addPort(Port port) {
//		if (port.getAbsolutionX() == this.start.x && port.getAbsolutionY() == this.start.y) {
//			this.startPort = port;
//			this.startVertex = port.parent;
//		} else if (port.getAbsolutionX() == this.end.x && port.getAbsolutionY() == this.end.y) {
//			this.endPort = port;
//			this.endVertex = port.parent;
//		}
//	}
//
//	public Point findPoint(Port port) {
//
//		if (port.parent == this.startVertex && port == this.startPort) {
//			return new Point(this.start.x, this.start.y);
//		} else if (port == this.endPort && port.parent == this.endVertex) {
//			return new Point(this.end.x, this.end.y);
//		}
//		return null;
//	}
	public Port findPort(Port vertexPort) {
		if (vertexPort.vertexPort == this.startPort) {
			return this.startPort;
		} else if (vertexPort.vertexPort == this.endPort) {
			return this.endPort;
		}
		return null;
	}

//	public Point findOppositePoint(Port port) {
//
//		if (port.parent == this.startVertex && port == this.startPort) {
//			return new Point(this.end.x, this.end.y);
//		} else if (port == this.endPort && port.parent == this.endVertex) {
//			return new Point(this.start.x, this.start.y);
//		}
//		return null;
//	}
//
//	public Vertex findOppositeVertex(Port port) {
//
//		if (port.parent == this.startVertex && port == this.startPort) {
//			return endVertex;
//		} else if (port == this.endPort && port.parent == this.endVertex) {
//			return startVertex;
//		}
//		return null;
//	}
	public boolean isHorizontal() {
		return (start.y == end.y);
	}

	public void paint(Graphics g, int gridSize) {
		int count = 0;
		int bit = -1;
		Port tempOutput = new Port(null, null);
		for (Port p : this.findConnectedPorts()) {
			if (bit == -1) {
				bit = p.bits;
			} else {
				if (bit != p.bits) {
					count = -1;
					g.setColor(Color.red);
					for (Port port : this.findConnectedPorts()) {
						g.setFont(new Font("arial", Font.BOLD, 20));
						g.drawString(Integer.toString(port.bits) + " bits", port.getAbsolutionX() * gridSize, port.getAbsolutionY() * gridSize);
					}
					g.setColor(Color.orange);
					break;
				}
			}
			if (p instanceof Output || (p.parent instanceof Tunnel && ((Tunnel) p.parent).hasTunnelOutput)) {
				count++;
				tempOutput = p;
			}
		}
		if (isSelected) {
			g.setColor(Color.blue);
		} else if (count >= 2) {
			g.setColor(Color.red);  //when the edge is connected to more than 2 outputs
		} else if (count == 1 && (tempOutput.value > 0)) {
			g.setColor(Color.green); //has signal
		} else if (count >= 0) {
			g.setColor(Color.black); // no signal
		}
//		for (int i = 0; i < points.size() - 1; i++) {
		g.drawLine(start.x * gridSize, start.y * gridSize, end.x * gridSize, end.y * gridSize);
		if (this.startPort.getConnectionState()) {
			this.startPort.paint(g, gridSize);
		}
		if (this.endPort.getConnectionState()) {
			this.endPort.paint(g, gridSize);
		}
//		}

	}

	public void setID(int id) {
		this.identity = id;
	}

	public int getID() {
		return this.identity;
	}

	public static String[] getFieldsToOmit() {
		return new String[]{"startVertex", "endVertex", "startPort", "endPort", "edgeConnected", "gridSize", "isSelected", "identity"};
	}

	public String toString() {
		return ("(" + this.start.x + ", " + this.start.y + ")" + "to " + "(" + this.end.x + ", " + this.end.y + ")");
	}

	@Override
	public boolean equals(Object obj) {
		Edge e = (Edge) obj;
		if (e.startPort == this.startPort && e.endPort == this.endPort) {
			return true;
		} else if (e.startPort == this.endPort && e.endPort == this.startPort) {
			return true;
		}
		return false;
	}

}
