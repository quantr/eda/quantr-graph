/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.github.javabdd.BDD;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.engine.Simulate;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
@XStreamAlias("notGate")
public class NotGate extends Vertex {

	public NotGate(String name) {
		super(name, 1, 1, 3, 2);
		properties.put("Name", name);
		properties.put("Data Bits", 1);
		properties.put("Orientation", "east");
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		if (properties.get("Orientation").equals("east")) {
			LogicGateDrawer.drawNotGate(g, x * gridSize, y * gridSize, this.gridSize, stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), x * gridSize, (y + this.height + 1) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			g2.setStroke(stroke);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{(x + 3) * gridSize, (x + 1) * gridSize, (x + 3) * gridSize, (x + 3) * gridSize}, new int[]{y * gridSize, (y + 1) * gridSize, (y + 2) * gridSize, y * gridSize}, 4);
			g2.drawOval(x * gridSize, y * gridSize + gridSize / 2, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), x * gridSize, (y - 1) * gridSize);
			super.paint(g);
		} else if (properties.get("Orientation").equals("north")) {
			g2.setStroke(stroke);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{(x + 2) * gridSize, (x + 1) * gridSize, x * gridSize, (x + 2) * gridSize}, new int[]{(y + 3) * gridSize, (y + 1) * gridSize, (y + 3) * gridSize, (y + 3) * gridSize}, 4);
			g2.drawOval(x * gridSize + gridSize / 2, y * gridSize, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x - ((String) properties.get("Label")).length()) * gridSize, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			g2.setStroke(stroke);
			g2.setColor(Color.black);
			g2.drawPolyline(new int[]{x * gridSize, (x + 2) * gridSize, (x + 1) * gridSize, x * gridSize}, new int[]{y * gridSize, y * gridSize, (y + 2) * gridSize, y * gridSize}, 4);
			g2.drawOval(x * gridSize + gridSize / 2, (y + 2) * gridSize, gridSize, gridSize);
			inputs.get(0).paint(g, gridSize);
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + this.width) * gridSize + gridSize / 2, this.y * gridSize + (height * gridSize * 8 / 10 - 2));
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "Not gate";
	}

	@Override
	public void eval() {
		long temp = inputs.get(0).value;
		this.outputs.get(0).value = (temp ^ (((1L << (this.outputs.get(0).bits - 1)) - 1 + (1L << (this.outputs.get(0).bits - 1)))));
		super.eval();
	}

	@Override
	public void updateProperty() {
		int databits = (int) properties.get("Data Bits");
		inputs.get(0).bits = databits;
		outputs.get(0).bits = databits;
		if (properties.get("Orientation").equals("east")) {
			this.inputs.get(0).setLocation(0, 1);
			this.outputs.get(0).setLocation(3, 1);
			this.height = 2;
			this.width = 3;
		} else if (properties.get("Orientation").equals("west")) {
			this.inputs.get(0).setLocation(3, 1);
			this.outputs.get(0).setLocation(0, 1);
			this.height = 2;
			this.width = 3;
		} else if (properties.get("Orientation").equals("north")) {
			this.inputs.get(0).setLocation(1, 3);
			this.outputs.get(0).setLocation(1, 0);
			this.height = 3;
			this.width = 2;
		} else if (properties.get("Orientation").equals("south")) {
			this.inputs.get(0).setLocation(1, 0);
			this.outputs.get(0).setLocation(1, 3);
			this.height = 3;
			this.width = 2;
		}
	}
}
