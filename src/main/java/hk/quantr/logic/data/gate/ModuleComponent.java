/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.QuantrGraphCanvas;
import hk.quantr.logic.data.basic.OutputPin;
import hk.quantr.logic.data.basic.Pin;
import hk.quantr.logic.data.graphics.ImageComponent;
import static hk.quantr.logic.data.graphics.ImageComponent.decodeImage;
import static hk.quantr.logic.data.graphics.ImageComponent.encodeImage;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class ModuleComponent extends Vertex {

	Module module;

	@XStreamOmitField
	public QuantrGraphCanvas canvas;
	@XStreamOmitField
	public BufferedImage icon;
	public String encodedIcon;
	String format;
	private ArrayList<Vertex> temp = new ArrayList();
	private HashMap<Pin, Input> inputMap = new HashMap();
	private HashMap<OutputPin, Output> outputMap = new HashMap();

	public ModuleComponent(String name, Module module) {
		super(name, 1, 1, 4, 4);
		this.canvas = new QuantrGraphCanvas();
		this.module = module;
		this.properties.put("Icon", "centered name");
		this.properties.put("width", width);
		this.properties.put("height", height);
		this.updateProperty();
	}

	@Override
	public String getTypeName() {
		return "Module Component";
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x * gridSize, y * gridSize, width * gridSize, height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 2));
		if (this.properties.get("Icon").equals("centered name")) {
			g2.drawString(module.name, x * gridSize + width * gridSize / 2 - module.name.length() * gridSize / 2, y * gridSize + height * gridSize / 2 + gridSize / 2);
		} else if (this.properties.get("Icon").equals("module circuit")) {
			g2.drawString(module.name, x * gridSize - 1, y * gridSize - 1);
			g2.drawImage(this.canvas.image, x * gridSize, y * gridSize, width * gridSize, height * gridSize, null);
		} else {
			if (icon == null && encodedIcon != null) {
				icon = decodeImage(encodedIcon);
			}
			g2.drawImage(icon, x * gridSize, y * gridSize, width * gridSize, height * gridSize, null);
		}
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).paint(g, gridSize);
		}
		for (int i = 0; i < outputs.size(); i++) {
			outputs.get(i).paint(g, gridSize);
		}
		super.paint(g);
	}

	@Override
	public void eval() {
		for (int i = 0; i < inputMap.keySet().toArray().length; i++) {
			((Pin) inputMap.keySet().toArray()[i]).outputs.get(0).value = inputMap.get((Pin) inputMap.keySet().toArray()[i]).value;
			((Pin) inputMap.keySet().toArray()[i]).outputs.get(0).state = inputMap.get((Pin) inputMap.keySet().toArray()[i]).state;
//			((Pin) inputMap.keySet().toArray()[i]).outputs.get(0).status = inputMap.get((Pin) inputMap.keySet().toArray()[i]).status;
		}

		for (Vertex v : temp) {
			v.eval();
		}
		for (int i = 0; i < outputMap.keySet().toArray().length; i++) {
			outputMap.get((OutputPin) outputMap.keySet().toArray()[i]).value = ((OutputPin) outputMap.keySet().toArray()[i]).inputs.get(0).value;
//			outputMap.get((OutputPin) outputMap.keySet().toArray()[i]).state = ((OutputPin) outputMap.keySet().toArray()[i]).inputs.get(0).state;
//			outputMap.get((OutputPin) outputMap.keySet().toArray()[i]).status = ((OutputPin) outputMap.keySet().toArray()[i]).inputs.get(0).status;
		}
		super.eval();
	}

	@Override
	public void simulateInit() {
		super.simulateInit();
		canvas = new QuantrGraphCanvas();
		for (Vertex v : module.vertices) {
			try {
				canvas.module.vertices.add((Vertex) v.clone());
				canvas.module.vertices.get(canvas.module.vertices.size() - 1).setLocation(v.x, v.y);
			} catch (CloneNotSupportedException ex) {
				Logger.getLogger(ModuleComponent.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		for (Edge e : module.edges) {
			Edge e1 = new Edge("virtualwire", canvas.getGrid().findPort(e.start.x, e.start.y), canvas.getGrid().findPort(e.end.x, e.end.y));
			canvas.getGrid().addEdge(e1);
		}
		canvas.recalculateLines();
		temp = canvas.calculateLevel(canvas.calculateLevel());
		for (Vertex v : temp) {
			if (!(v instanceof ModuleComponent)) {
				v.simulateInit();
			}
		}
		this.handleModule();
		this.canvas.setImage();
	}

	public void handleModule() {
		inputMap.clear();
		outputMap.clear();
		ArrayList<Pin> pins = new ArrayList();
		ArrayList<OutputPin> outputPins = new ArrayList();
		for (Vertex v : this.canvas.module.vertices) {
			if (v instanceof Pin) {
				pins.add((Pin) v);
			} else if (v instanceof OutputPin) {
				outputPins.add((OutputPin) v);
			}
		}
		pins.sort(Comparator.comparing(Vertex::getY));
		outputPins.sort(Comparator.comparing(Vertex::getY));
		for (int i = 0; i < this.inputs.size(); i++) {
			inputMap.put(pins.get(i), this.inputs.get(i));
		}
		for (int i = 0; i < this.outputs.size(); i++) {
			outputMap.put(outputPins.get(i), this.outputs.get(i));
		}
	}

	@Override
	public void updateProperty() {
		int noOfInputs = 0;
		int noOfOutputs = 0;
		for (Vertex v : module.vertices) {
			if (v instanceof Pin) {
				inputMap.put((Pin) v, null);
				noOfInputs++;
			} else if (v instanceof OutputPin) {
				outputMap.put((OutputPin) v, null);
				noOfOutputs++;
			}
		}
		if (noOfInputs > this.inputs.size()) {
			for (int i = this.inputs.size() + 1; i <= noOfInputs; i++) {
				this.inputs.add(new Input(this, this.name + " input " + i));
			}
		} else {
			for (int i = this.inputs.size() - 1; i >= noOfInputs; i--) {
				this.inputs.get(i).edges.clear();
				this.inputs.remove(i);
			}
		}
		if (noOfOutputs > this.outputs.size()) {
			for (int i = this.outputs.size() + 1; i <= noOfOutputs; i++) {
				this.outputs.add(new Output(this, this.name + " output " + i));
			}
		} else {
			for (int i = this.outputs.size() - 1; i >= noOfOutputs; i--) {
				this.outputs.get(i).edges.clear();
				this.outputs.remove(i);
			}
		}
		this.setSize(Math.max(module.name.length() + 2, Math.max((int) this.properties.get("width"), 4)), Math.max(Math.max(Math.max(this.outputs.size(), this.inputs.size()) - 1, 4), (int) this.properties.get("height")));
		int cnt = 0;
		for (int i = -this.inputs.size() / 2; i < (this.inputs.size() + 1) / 2; i++) {
			this.inputs.get(cnt).deltaX = 0;
			this.inputs.get(cnt++).deltaY = (height + 1) / 2 + i + (i >= 0 && height % 2 == 0 && this.inputs.size() % 2 == 0 ? 1 : 0);
		}
		cnt = 0;
		for (int i = -this.outputs.size() / 2; i < (this.outputs.size() + 1) / 2; i++) {
			this.outputs.get(cnt).deltaX = width;
			this.outputs.get(cnt++).deltaY = (height + 1) / 2 + i + (i >= 0 && height % 2 == 0 && this.outputs.size() % 2 == 0 ? 1 : 0);
		}
		if (this.properties.get("Icon").equals("module circuit")) {
			simulateInit();
//			this.canvas.setImage();
			icon = null;
		} else if (this.properties.get("Icon").equals("custom icon") && icon == null) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileFilter(new FileNameExtensionFilter("Supported File Format", "jpeg", "jpg", "png", "bmp", "webp", "gif"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JPEG Files", "jpeg", "jpg"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG Files", "png"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("BMP Files", "bmp"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("WEBP Files", "webp"));
			fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("GIF Files", "gif"));
			int result = fileChooser.showOpenDialog(null);
			if (result == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fileChooser.getSelectedFile();
				try {
					if ((icon = ImageIO.read(selectedFile)) != null) {
						String[] str = selectedFile.getPath().split("\\.");
						this.format = str[str.length - 1];
						System.out.println(this.format);
						setImage(icon, this.format);
					} else {
						JOptionPane.showMessageDialog(null, "File Format Is Not Supported!", "Failed to Load Image", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Error loading image: " + ex.getMessage());
				}
			}
		} else if (this.properties.get("Icon").equals("centered name")) {
			icon = null;
		} else {
			if (icon != null) {
				if (encodedIcon == null) {
					encodedIcon = encodeImage(icon, this.format);
				}
			}
		}

	}

	public void setImage(BufferedImage icon, String format) {
		this.icon = icon;
		this.format = format;
		try {
			this.encodedIcon = encodeImage(icon, this.format);
		} catch (Exception ex) {
			Logger.getLogger(ImageComponent.class.getName()).log(Level.SEVERE, null, ex);
		}
//		System.out.println("encoded:\n" + encodedIcon);
		decodeImage(encodedIcon);
	}
}
