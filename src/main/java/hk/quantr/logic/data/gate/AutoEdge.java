///*
// * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package hk.quantr.logic.data.gate;
//
//import com.thoughtworks.xstream.annotations.XStreamAlias;
//import hk.quantr.logic.algo.lee.Path;
//import hk.quantr.logic.data.basic.Clock;
//import hk.quantr.logic.data.basic.Tunnel;
//import hk.quantr.routingalgo.lee.IPath;
//import java.awt.Color;
//import java.awt.Font;
//import java.awt.Graphics;
//
///**
// *
// * @author RonaldPark <pkc6krt22@gmail.com>
// */
//@XStreamAlias("autoEdge")
//public class AutoEdge extends BaseEdge implements Cloneable{
//
//	public String name;
//	public boolean fromWire = false;
//	public int x, y;
//
//	public AutoEdge(String name, Vertex startVertex, Port startPort, Vertex endVertex, Port endPort) {
//		this.name = name;
//		this.startVertex = startVertex;
//		this.endVertex = endVertex;
//		this.startPort = startPort;
//		if(startPort!=null){
//			startPort.edges.add(this);
//		}
//		this.endPort = endPort;
//		if(endPort!=null){
//			endPort.edges.add(this);
//		}
//		System.out.println(startVertex + "  " + startPort.parent);
//		System.out.println(findConnectedPorts());
//	}
//
//	public AutoEdge(String name, Point edgePoint, Vertex startVertex, Port startPort) {
//		this.name = name;
//		this.fromWire = true;
//		this.x = edgePoint.x;
//		this.y = edgePoint.y;
//		this.startVertex = startVertex;
//		this.endVertex = null;
//		this.startPort = startPort;
//		this.endPort = null;
//	}
//
//	@Override
//	public String toString() {
//		return name + "{" + startPort + "<->" + endPort + '}';
//	}
//}
