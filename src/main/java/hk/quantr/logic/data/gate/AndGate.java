/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("andGate")
public class AndGate extends Vertex {

	public AndGate(String name) {
		super(name, 2, 1, 5, 4);
		properties.put("Name", name);
		properties.put("No. Of Inputs", 2);
		properties.put("Data Bits", 1);
		properties.put("Orientation", "east");
	}

	@Override
	public void paint(Graphics g) {
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		Graphics2D g2 = LogicGateDrawer.init(g);
		g2.setStroke(stroke);
		if (properties.get("Orientation").equals("east")) {
			LogicGateDrawer.drawAndGate(g, this, this.x * gridSize, this.y * gridSize, this.gridSize, stroke);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2)) * gridSize - gridSize / 5, (y + (this.height / 2)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("south")) {
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize, y * gridSize + gridSize * 2 / 10, (x + this.width) * gridSize, y * gridSize + gridSize * 2 / 10);
			g2.drawLine((x + this.width / 2 - 2) * gridSize, y * gridSize + gridSize * 2 / 10, (x + this.width / 2 - 2) * gridSize, (y + 2) * gridSize + gridSize / 2);
			g2.drawLine((x + this.width / 2 + 2) * gridSize, y * gridSize + gridSize * 2 / 10, (x + this.width / 2 + 2) * gridSize, (y + 2) * gridSize + gridSize / 2);
			g2.drawArc((x + (this.inputs.size() > 5 ? this.inputs.size() / 2 - 2 : 0)) * gridSize, y * gridSize, 4 * gridSize, 5 * gridSize, 0, -180);
			for (int i = 0; i < this.inputs.size(); i++) {
				inputs.get(i).paint(g, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize + gridSize / 5, (y + (this.height / 2)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("west")) {
			g2.setColor(Color.black);
			g2.drawLine((x + 5) * gridSize - gridSize * 2 / 10, y * gridSize, (x + 5) * gridSize - gridSize * 2 / 10, (y + this.height) * gridSize);
			g2.drawLine((x + 2) * gridSize, (y + this.height / 2 - 2) * gridSize, (x + 5) * gridSize - gridSize * 2 / 10, (y + this.height / 2 - 2) * gridSize);
			g2.drawLine((x + 2) * gridSize, (y + this.height / 2 + 2) * gridSize, (x + 5) * gridSize - gridSize * 2 / 10, (y + this.height / 2 + 2) * gridSize);
			g2.drawArc(x * gridSize, (y + (this.inputs.size() > 5 ? this.inputs.size() / 2 - 2 : 0)) * gridSize, 4 * gridSize, 4 * gridSize, 90, 180);
			for (int i = 0; i < this.inputs.size(); i++) {
				inputs.get(i).paint(g, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2)) * gridSize - gridSize / 5, (y + (this.height / 2)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		} else if (properties.get("Orientation").equals("north")) {
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize, (y + 5) * gridSize - gridSize * 2 / 10, (x + this.width) * gridSize, (y + 5) * gridSize - gridSize * 2 / 10);
			g2.drawLine((x + this.width / 2 - 2) * gridSize, (y + 2) * gridSize, (x + this.width / 2 - 2) * gridSize, (y + 5) * gridSize - gridSize * 2 / 10);
			g2.drawLine((x + this.width / 2 + 2) * gridSize, (y + 2) * gridSize, (x + this.width / 2 + 2) * gridSize, (y + 5) * gridSize - gridSize * 2 / 10);
			g2.drawArc((x + (this.inputs.size() > 5 ? this.inputs.size() / 2 - 2 : 0)) * gridSize, y * gridSize, 4 * gridSize, 4 * gridSize, 0, 180);
			for (int i = 0; i < this.inputs.size(); i++) {
				inputs.get(i).paint(g, gridSize);
			}
			outputs.get(0).paint(g, gridSize);
			g2.setColor(Color.darkGray);
			g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
			g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize + gridSize / 5, (y + (this.height / 2)) * gridSize + gridSize * 4 / 10);
			super.paint(g);
		}
	}

	@Override
	public String getTypeName() {
		return "And Gate";
	}

	@Override
	public void eval() {
		long temp = inputs.get(0).value;
		for (int i = 1; i < this.inputs.size(); i++) {
			temp = temp & this.inputs.get(i).value;
		}
		this.outputs.get(0).value = temp;
		super.eval();
	}

	@Override
	public void eval2() {
		long temp = inputs.get(0).value;
		for (int i = 1; i < this.inputs.size(); i++) {
			temp = temp & this.inputs.get(i).value;
		}
		this.outputs.get(0).value = temp;
	}

	@Override
	public void updateProperty() {
		super.updateProperty();
		int databits = (int) properties.get("Data Bits");
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).bits = databits;
		}
		outputs.get(0).bits = databits;
	}
}
