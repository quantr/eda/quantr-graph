/*
 * Copyright 2023 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author peter
 */
@XStreamAlias("output")
public class Output extends Port {

	public ArrayList<Input> ports = new ArrayList();
	public int dataIndex = -1;

	public Output(Vertex parent, String name) {
		super(parent, name);
	}

	public void paint(Graphics g, int gridSize) {
		g.setColor(Color.green);
		float ovalSize = gridSize * 0.4f * (getConnectionState() ? 4 : 1) * (this.vertexPort != null && !this.vertexPort.edges.isEmpty() ? 2 : 1);
		g.fillOval((int) (getAbsolutionX() * gridSize - ovalSize * 0.5f), (int) (getAbsolutionY() * gridSize - ovalSize * 0.5f), (int) ovalSize, (int) ovalSize);
	}
}
