///*
// * Copyright 2023 ken.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package hk.quantr.logic.data.gate;
//
//import com.thoughtworks.xstream.annotations.XStreamAlias;
//import com.thoughtworks.xstream.annotations.XStreamOmitField;
//import java.util.ArrayList;
//
///**
// *
// * @author ken
// */
//@XStreamAlias("baseEdge")
//public abstract class BaseEdge implements Cloneable {
//
//	@XStreamOmitField
//	public Vertex startVertex;
//	public Vertex endVertex;
//	public Port startPort;
////	@XStreamOmitField
//	public Port endPort;
//	public ArrayList<BaseEdge> edgeConnected = new ArrayList();
//	public int gridSize = 10;
//	public boolean isSelected = false;
////	public String[] = {}
//
//	public ArrayList<Port> findConnectedPorts() {
//		ArrayList<Port> ports = new ArrayList();
//		if (this.startPort != null) {
//			if (this.startPort.parent == startVertex) {
//				ports.add(startPort);
//			}
//		}
//		if (this.endPort != null) {
//			if (this.endPort.parent == endVertex) {
//				ports.add(endPort);
//			}
//		}
//		ArrayList<BaseEdge> finished = new ArrayList();
//		this.findPorts(this, this, this, ports, finished);
//		System.out.println(ports);
//		return ports;
//	}
//
//	public ArrayList<Port> findConnectedPorts(Port port) {
//		ArrayList<Port> ports = new ArrayList();
//		if (this.startPort != null) {
//			if (this.startPort.parent == startVertex) {
//				ports.add(startPort);
//			}
//		}
//		if (this.endPort != null) {
//			if (this.endPort.parent == endVertex) {
//				ports.add(endPort);
//			}
//		}
//		ArrayList<BaseEdge> finished = new ArrayList();
//		this.findPorts(this, this, this, ports, finished);
//		ports.remove(port);
//		return ports;
//	}
//
//	public void findPorts(BaseEdge e, BaseEdge last, BaseEdge edge, ArrayList<Port> ports, ArrayList<BaseEdge> finished) {
//		if (finished.contains(e)) {
//			return;
//		} else {
//			finished.add(e);
//		}
//		if (!e.edgeConnected.isEmpty()) {
//			for (BaseEdge temp : e.edgeConnected) {
//				if (last != temp) {
//					if (temp.startPort != null && temp.startVertex != null) {
//						if (!ports.contains(temp.startPort)) {
//							ports.add(temp.startPort);
//						}
//					}
//					if (temp.endPort != null && temp.endVertex != null) {
//						if (!ports.contains(temp.endPort)) {
//							ports.add(temp.endPort);
//						}
//					}
//					if (!temp.edgeConnected.isEmpty()) {
//						findPorts(temp, e, edge, ports, finished);
//					}
//				}
//			}
//		}
//	}
//
//	public Port findPort(Vertex vertex) {
//		if (vertex == this.startVertex) {
//			for (Input input : vertex.inputs) {
//				if (input == this.startPort) {
//					return input;
//				}
//			}
//			if (vertex.outputs.get(0) == this.startPort) {
//				return vertex.outputs.get(0);
//			}
//		} else if (vertex == this.endVertex) {
//			for (Input input : vertex.inputs) {
//				if (input == this.endPort) {
//					return input;
//				}
//			}
//			if (vertex.outputs.get(0) == this.endPort) {
//				return vertex.outputs.get(0);
//			}
//		}
//		return null;
//	}
//
//	public static String[] getFieldsToOmit() {
//		return new String[]{"startVertex", "endVertex", "startPort", "endPort", "edgeConnected", "gridSize", "isSelected"};
//	}
//}
