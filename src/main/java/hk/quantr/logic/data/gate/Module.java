/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.gate;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Rectangle;
import java.util.ArrayList;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("module")
public class Module implements Cloneable {

	public String name;
	public ArrayList<Vertex> vertices = new ArrayList();
	public ArrayList<Edge> edges = new ArrayList();

	public Module(String name) {
		this.name = name;
	}

	public void clear() {
		vertices.clear();
		edges.clear();
	}

	public boolean isEmpty() {
		return vertices.isEmpty() && edges.isEmpty();
	}

	public boolean isOverlap(int x, int y, int width, int height) {
		for (Vertex v : vertices) {
			Rectangle r = new Rectangle(v.x, v.y, v.width, v.height);
			Rectangle r2 = new Rectangle(x, y, width, height);
			if (r.intersects(r2)) {
				return true;
			}
		}
		return false;
	}

	public boolean checkEquality(Module m) {
		if (m.edges.size() != this.edges.size()) {
			return false;
		} else if (m.vertices.size() != this.vertices.size()) {
			return false;
		}
		for (int i = 0; i < this.vertices.size(); i++) {
			if (m.vertices.get(i).x != this.vertices.get(i).x) {
				System.out.println("x not same");
				return false;
			} else if (m.vertices.get(i).y != this.vertices.get(i).y) {
				System.out.println("y not same");
				return false;
			} else if (m.vertices.get(i).properties != this.vertices.get(i).properties) {
				System.out.println("property not same");
				return false;
			}
		}
		return true;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Module m = new Module(this.name);
		m.edges = (ArrayList<Edge>) this.edges.clone();
		m.vertices = (ArrayList<Vertex>) this.vertices.clone();
		return m;
	}
}
