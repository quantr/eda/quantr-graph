/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.plexer;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author michelle
 */
@XStreamAlias("multiplexer")
public class Multiplexer extends Vertex {

	public Multiplexer(String name) {
		super(name, 5, 1, 6, 6);
		properties.put("Name", name);
		properties.put("Data Bits", 1);
		properties.put("No. of Bits", "2");
		properties.put("Include Input Enable", "no");

		this.inputs.get(0).setLocation(3, 6);// select port always with index 0
		inputs.get(0).bits = 2;
		//enable port always with index 1 
		this.inputs.get(1).setLocation(0, 1);
		this.inputs.get(1).name = this.name + "bit 0";

		this.inputs.get(2).setLocation(0, 2);
		this.inputs.get(2).name = this.name + "bit 1";

		this.inputs.get(3).setLocation(0, 3);
		this.inputs.get(3).name = this.name + "bit 2";

		this.inputs.get(4).setLocation(0, 4);
		this.inputs.get(4).name = this.name + "bit 3";

		this.outputs.get(0).setLocation(6, 3);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect((x + 1) * gridSize, y * gridSize, 4 * gridSize, (this.inputs.size() - (this.inputs.get(1).name.contains("Enable") ? 1 : 0)) * gridSize);
		g2.setColor(Color.black);
		g2.drawRect((x + 1) * gridSize, y * gridSize, 4 * gridSize, (this.inputs.size() - +(this.inputs.get(1).name.contains("Enable") ? 1 : 0)) * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString(Integer.toString((int) Math.pow(2, inputs.get(0).bits)) + " to 1", (x + 2) * gridSize - gridSize * 2 / 5, (y + (inputs.size() - (this.inputs.get(1).name.contains("Enable") ? 1 : 0)) / 2) * gridSize);
		g2.drawString("MUX", (x + 2) * gridSize, (y + (inputs.size() - (this.inputs.get(1).name.contains("Enable") ? 1 : 0)) / 2) * gridSize + gridSize * 7 / 10);
		g2.drawLine((x + width) * gridSize, (outputs.get(0).getAbsolutionY()) * gridSize, (x + width - 1) * gridSize, (outputs.get(0).getAbsolutionY()) * gridSize);
		outputs.get(0).paint(g, gridSize);
		g2.setColor(Color.black);
		g2.drawLine((x + inputs.get(0).deltaX) * gridSize, (y + height) * gridSize, (x + inputs.get(0).deltaX) * gridSize, (y + height - 1) * gridSize);
		inputs.get(0).paint(g, gridSize);
		if (this.inputs.get(1).name.contains("Enable")) {
			g2.setColor(Color.black);
			g2.drawLine((x + inputs.get(1).deltaX) * gridSize, (y + height) * gridSize, (x + inputs.get(1).deltaX) * gridSize, (y + height - 1) * gridSize);
			g2.setColor(Color.gray);
			g2.drawString("E", x * gridSize + inputs.get(1).deltaX * gridSize - gridSize * 1 / 5, (y + inputs.get(1).deltaY) * gridSize - gridSize * 6 / 5);
			inputs.get(1).paint(g, gridSize);
		}
		for (int i = 1 + (this.inputs.get(1).name.contains("Enable") ? 1 : 0); i < inputs.size(); i++) {
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize, (y - (this.inputs.get(1).name.contains("Enable") ? 1 : 0) + i) * gridSize, (x + 1) * gridSize, (y - (this.inputs.get(1).name.contains("Enable") ? 1 : 0) + i) * gridSize);
			inputs.get(i).paint(g, gridSize);
		}
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString("S", x * gridSize + inputs.get(0).deltaX * gridSize - gridSize * 1 / 5, (y + inputs.get(0).deltaY) * gridSize - gridSize * 6 / 5);

		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 2 - gridSize * 6 / 10));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);

		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Multiplexer";
	}

	@Override
	public void eval() {
		if (inputs.get(1).name.contains("Enable")) {
			if (inputs.get(1).value != 0) {
				outputs.get(0).value = inputs.get((int) inputs.get(0).value + 2).value;
			} else {
				outputs.get(0).value = 0;
			}
		} else {
			outputs.get(0).value = inputs.get((int) inputs.get(0).value + 1).value;
		}
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		int data = (int) properties.get("Data Bits");
		int num = Integer.parseInt((String) properties.get("No. of Bits"));
		outputs.get(0).bits = data;
		if (properties.get("Include Input Enable").equals("yes")) {
			if (!inputs.get(1).name.contains("Enable")) {
				inputs.add(1, new Input(this, "Enable"));
			}
			inputs.get(1).setLocation(2, this.height);
			inputs.get(0).setLocation(4, this.height);
		} else {
			inputs.get(0).setLocation(3, this.height);
		}

		if (this.inputs.size() - 1 - (this.inputs.get(1).name.contains("Enable") ? 1 : 0) != (int) Math.pow(2, num)) {
			this.inputs.get(0).bits = num;
			this.height = (int) Math.pow(2, num) + 2;

			if (this.inputs.size() > (int) Math.pow(2, num)) { // remove port
				for (int i = this.inputs.size() - 1; i > (int) Math.pow(2, num) - (this.inputs.get(1).name.contains("Enable") ? 1 : 0); i--) {
					inputs.get(i).edges.clear();
					inputs.remove(i);
				}
			} else if (this.inputs.size() - 1 - (this.inputs.get(1).name.contains("Enable") ? 1 : 0) < (int) Math.pow(2, num)) { // add port
				for (int i = this.inputs.size() - 1; i < (int) Math.pow(2, num) + (this.inputs.get(1).name.contains("Enable") ? 1 : 0); i++) {
					inputs.add(new Input(this, "Input " + i));
					inputs.get(inputs.size() - 1).setLocation(0, i + (this.inputs.get(1).name.contains("Enable") ? 0 : 1));
					System.out.println("added" + (i + 1));
				}
			}
			outputs.get(0).setLocation(6, (this.inputs.size() - 1) / 2);
		}
		if (properties.get("Include Input Enable").equals("yes")) {
			if (!inputs.get(1).name.contains("Enable")) {
				inputs.add(1, new Input(this, "Enable"));
			}
			inputs.get(1).setLocation(2, this.height);
			inputs.get(0).setLocation(4, this.height);
		} else {
			inputs.get(0).setLocation(3, this.height);
		}
	}
}
