/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.plexer;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("bitSelector")
public class BitSelector extends Vertex {

	public BitSelector(String name) {
		super(name, 2, 1, 6, 5);
		properties.put("Name", name);
		properties.put("Data Bits", 2);
		properties.put("Output Bits", 1);
		this.inputs.get(0).setLocation(0, 2);
		this.inputs.get(1).setLocation(3, 5);
		this.outputs.get(0).setLocation(6, 2);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect((x + 1) * gridSize, y * gridSize, (width - 2) * gridSize, (height - 1) * gridSize);
		g2.setColor(Color.black);
		g2.drawRect((x + 1) * gridSize, y * gridSize, (width - 2) * gridSize, (height - 1) * gridSize);
		g2.drawLine(inputs.get(0).getAbsolutionX() * gridSize, inputs.get(0).getAbsolutionY() * gridSize, (inputs.get(0).getAbsolutionX() + 1) * gridSize, inputs.get(0).getAbsolutionY() * gridSize);
		g2.drawLine(outputs.get(0).getAbsolutionX() * gridSize, outputs.get(0).getAbsolutionY() * gridSize, (outputs.get(0).getAbsolutionX() - 1) * gridSize, outputs.get(0).getAbsolutionY() * gridSize);
		g2.drawLine((x + 3) * gridSize, (y + 4) * gridSize, (x + 3) * gridSize, (y + 5) * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString("bit", x * gridSize + gridSize * 12 / 5, (y + inputs.get(0).deltaY) * gridSize);
		g2.drawString("selector", x * gridSize + gridSize * 7 / 5, (y + inputs.get(0).deltaY) * gridSize + gridSize * 7 / 10);
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).paint(g, gridSize);
		}
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString("S", x * gridSize + gridSize * 14 / 5, (y + inputs.get(1).deltaY) * gridSize - gridSize * 6 / 5);
		outputs.get(0).paint(g, gridSize);

		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Bit Selector";
	}

	@Override
	public void eval() {
		outputs.get(0).value = (inputs.get(0).value & ((((1 << (outputs.get(0).bits - 1) - 1) << (outputs.get(0).bits * inputs.get(1).value))))) >> (outputs.get(0).bits * inputs.get(1).value);
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		this.inputs.get(0).bits = (int) properties.get("Data Bits");
		this.outputs.get(0).bits = (int) properties.get("Output Bits");
		if ((int) Math.ceil(Math.log((double) inputs.get(0).bits / (double) outputs.get(0).bits) / Math.log(2)) <= 0) {
			inputs.get(1).bits = 1;
		} else {
			inputs.get(1).bits = (int) Math.ceil(Math.log((double) inputs.get(0).bits / (double) outputs.get(0).bits) / Math.log(2));
		}
	}
}
