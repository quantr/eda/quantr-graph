/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.plexer;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("demultiplexer")
public class Demultiplexer extends Vertex {

	public Demultiplexer(String name) {
		super(name, 2, 4, 6, 6);
		properties.put("Name", name);
		properties.put("Data Bits", 1);
		properties.put("No. of Bits", "2");
		properties.put("Include Input Enable", "no");
		inputs.get(0).setLocation(0, 2);
		inputs.get(1).setLocation(3, 6);
		inputs.get(1).bits = 2;
		outputs.get(0).setLocation(6, 1);
		outputs.get(1).setLocation(6, 2);
		outputs.get(2).setLocation(6, 3);
		outputs.get(3).setLocation(6, 4);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect((x + 1) * gridSize, y * gridSize, 4 * gridSize, (height - 1) * gridSize);
		g2.setColor(Color.black);
		g2.drawRect((x + 1) * gridSize, y * gridSize, 4 * gridSize, (height - 1) * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString("1 to " + Integer.toString((int) Math.pow(2, inputs.get(1).bits)), (x + 2) * gridSize - gridSize * 2 / 5, (y + 1) * gridSize);
		g2.drawString("DE-MUX", (x + 1) * gridSize + gridSize / 5, (y + inputs.size() / 2) * gridSize + gridSize * 7 / 10);
		g2.drawLine(x * gridSize, (inputs.get(0).getAbsolutionY()) * gridSize, (x + 1) * gridSize, (inputs.get(0).getAbsolutionY()) * gridSize);
		inputs.get(0).paint(g, gridSize);
		g2.setColor(Color.black);
		g2.drawLine((x + inputs.get(1).deltaX) * gridSize, (y + height) * gridSize, (x + inputs.get(1).deltaX) * gridSize, (y + height - 1) * gridSize);
		inputs.get(1).paint(g, gridSize);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString("S", x * gridSize + inputs.get(1).deltaX * gridSize - gridSize * 1 / 5, (y + inputs.get(1).deltaY) * gridSize - gridSize * 6 / 5);
		if (inputs.size() == 3) {
			g2.setColor(Color.black);
			g2.drawLine((x + inputs.get(2).deltaX) * gridSize, (y + height) * gridSize, (x + inputs.get(2).deltaX) * gridSize, (y + height - 1) * gridSize);
			g2.setColor(Color.gray);
			g2.drawString("E", x * gridSize + inputs.get(2).deltaX * gridSize - gridSize * 1 / 5, (y + inputs.get(2).deltaY) * gridSize - gridSize * 6 / 5);
			inputs.get(2).paint(g, gridSize);
		}
		for (int i = 0; i < outputs.size(); i++) {
			g2.setColor(Color.black);
			g2.drawLine((x + 6) * gridSize, (y + 1 + i) * gridSize, (x + 5) * gridSize, (y + 1 + i) * gridSize);
			outputs.get(i).paint(g, gridSize);
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);

		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Demultiplexer";
	}

	@Override
	public void eval() {
		if ((inputs.size() == 3 && inputs.get(2).value == 1) || inputs.size() == 2) {
			for (int i = 0; i < outputs.size(); i++) {
				if (i != (int) inputs.get(1).value) {
					outputs.get(i).value = inputs.get(0).value;
				} else {
					outputs.get(i).value = 0;
				}
			}
			super.eval();
		} else {
			for (int i = 0; i < outputs.size(); i++) {
				outputs.get(i).value = 0;
			}
			super.eval();
		}
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		int data = (int) properties.get("Data Bits");
		inputs.get(0).bits = data;
		for (int i = 0; i < this.outputs.size(); i++) {
			outputs.get(i).bits = data;
		}
		int num = Integer.parseInt((String) properties.get("No. of Bits"));
		if (this.outputs.size() != (int) Math.pow(2, num)) {
			this.outputs.get(this.outputs.size() - 1).bits = num;
			this.height = (int) Math.pow(2, num) + 2;
			if (this.outputs.size() > (int) Math.pow(2, num)) {
				for (int i = this.outputs.size() - 1; i > (int) Math.pow(2, num) - 1; i--) {
					outputs.get(i).edges.clear();
					outputs.remove(i);
				}
			} else if (this.outputs.size() < (int) Math.pow(2, num)) {
				for (int i = this.outputs.size(); i <= (int) Math.pow(2, num); i++) {
					outputs.add(i, new Output(this, "Output " + i));
					outputs.get(i).setLocation(6, i + 1);
					outputs.get(i).bits = data;
				}
			}
			inputs.get(0).setLocation(0, (this.outputs.size() + 1) / 2);
			inputs.get(1).setLocation(3, this.height);
			inputs.get(1).bits = num;
		}
		if (properties.get("Include Input Enable").equals("yes")) {
			if (inputs.size() == 2) {
				inputs.add(new Input(this, "Enable"));
			}
			inputs.get(2).setLocation(2, this.height);
			inputs.get(1).setLocation(4, this.height);
		} else {
			if (inputs.size() == 3) {
				inputs.remove(2);
			}
			inputs.get(1).setLocation(3, this.height);
		}
	}
}
