/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.plexer;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("encoder")
public class Encoder extends Vertex {

	public int noOfBits = 1;
	public boolean inputEnableState = true;
	public boolean outputEnableState = true;

	public Encoder(String name) {
		super(name, 3, 2, 6, 5);
		properties.put("Name", name);
		properties.put("No. of Bits", "1");
		properties.put("Include Input Enable", "yes");
		properties.put("Include Output Enable", "yes");
		this.outputs.get(0).setLocation(6, 2);
		this.outputs.get(1).setLocation(3, 0); // enable output
		this.inputs.get(0).setLocation(0, 2);
		this.inputs.get(1).setLocation(0, 3);
		this.inputs.get(2).setLocation(3, 5); // enable input
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect((x + 1) * gridSize, (y + (outputEnableState ? 1 : 0)) * gridSize, 4 * gridSize, (inputs.size() - (inputEnableState ? 1 : 0) + 1) * gridSize);
		g2.setColor(Color.black);
		g2.drawRect((x + 1) * gridSize, (y + (outputEnableState ? 1 : 0)) * gridSize, 4 * gridSize, (inputs.size() - (inputEnableState ? 1 : 0) + 1) * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString(Integer.toString((int) Math.pow(2, this.noOfBits)) + " × " + Integer.toString(this.outputs.get(0).bits), (x + 2) * gridSize, (y + outputs.get(0).deltaY) * gridSize + gridSize * 2 / 5);
		g2.drawString("Encoder", (x + 2) * gridSize - gridSize * 3 / 5, (y + outputs.get(0).deltaY + 1) * gridSize + gridSize / 10);
		g2.drawLine((x + 5) * gridSize, (y + outputs.get(0).deltaY) * gridSize, (x + 6) * gridSize, (y + outputs.get(0).deltaY) * gridSize);
		g2.setColor(Color.gray);
		g2.drawString("0", (x + 1) * gridSize + gridSize * 2 / 5, (y + 1 + (outputEnableState ? 1 : 0)) * gridSize);
		outputs.get(0).paint(g, gridSize);

		for (int i = 0; i < (int) Math.pow(2, this.noOfBits); i++) {
			g2.setColor(Color.black);
			g2.drawLine(x * gridSize, (y + i + 1 + (this.outputEnableState ? 1 : 0)) * gridSize, (x + 1) * gridSize, (y + i + 1 + (this.outputEnableState ? 1 : 0)) * gridSize);
			inputs.get(i).paint(g, gridSize);
		}

		if (inputEnableState) {
			g2.setColor(Color.black);
			g2.drawLine((x + 3) * gridSize, (y + (this.inputs.size() + this.outputs.size())) * gridSize, (x + 3) * gridSize, (y + this.inputs.size() + this.outputs.size() - 1) * gridSize);
			g2.setColor(Color.gray);
			g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 5));
			g2.drawString("E", (x + 3) * gridSize - gridSize / 5, (y + this.inputs.size() + this.outputs.size() - 1) * gridSize - gridSize * 3 / 10);
			inputs.get(inputs.size() - 1).paint(g, gridSize);
		}

		if (outputEnableState) {
			if (outputs.size() == 2) {
				g2.setColor(Color.black);
				g2.drawLine((x + 3) * gridSize, y * gridSize, (x + 3) * gridSize, (y + 1) * gridSize);
				g2.setColor(Color.gray);
				g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 5));
				g2.drawString("E", (x + 3) * gridSize - gridSize / 5, (y + 2) * gridSize - gridSize / 5);
				outputs.get(1).paint(g, gridSize);
			}
		}
		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);
		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Encoder";
	}

	@Override
	public void eval() {
		if (inputEnableState) {
			if (inputs.get(inputs.size() - 1).value == 1) {
				if (outputs.size() == 2) {
					outputs.get(1).value = 1;
				}
				boolean done = false;
				for (int i = inputs.size() - 2; i >= 0; i--) {
					if (inputs.get(i).value == 1) {
						outputs.get(0).value = i;
						done = true;
						break;
					}
				}
				if (!done) {
					outputs.get(0).value = 0;
				}
			} else {
				if (outputs.size() == 2) {
					outputs.get(1).value = 0;
				}
				for (int i = inputs.size() - 1; i >= 0; i--) {
					outputs.get(0).value = 0;
				}
			}
		} else {
			if (outputs.size() == 2) {
				outputs.get(1).value = 1;
			}
			boolean done = false;
			for (int i = inputs.size() - 1; i >= 0; i--) {
				if (inputs.get(i).value == 1) {
					outputs.get(0).value = i;
					done = true;
					break;
				}
			}
			if (!done) {
				outputs.get(0).value = 0;
			}
		}
		for (Port p : this.portConnectedTo(outputs.get(0))) {
			if (p.bits == this.outputs.get(0).bits) {
				p.value = 0;
			}
		}
//		if (!outputEnableState && !inputEnableState) {
		for (int i = 0; i < inputs.size(); i++) {
			this.inputs.get(i).value = this.outputs.get(0).value == i ? 1 : 0;
		}
		super.eval();
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		this.noOfBits = Integer.parseInt((String) properties.get("No. of Bits"));
		this.inputEnableState = ((String) properties.get("Include Input Enable")).equals("yes");
		this.outputEnableState = ((String) properties.get("Include Output Enable")).equals("yes");

		if (this.outputEnableState) {
			if (this.outputs.size() == 1) {
				this.outputs.add(new Output(this, "Output Enable"));
				this.outputs.get(1).setLocation(3, 0);
			}
		} else {
			if (this.outputs.size() > 1) {
				outputs.get(this.outputs.size() - 1).edges.clear();
				this.outputs.remove(this.outputs.size() - 1);
			}
		}
		if (this.inputEnableState) {
			if (this.inputs.size() == (int) Math.pow(2, noOfBits)) {
				this.inputs.add(new Input(this, "Input Enable"));
				this.inputs.get(inputs.size() - 1).setLocation(3, inputs.size() + outputs.size());
			}
		} else if (this.inputs.size() > (int) Math.pow(2, noOfBits)) {
			inputs.get(this.inputs.size() - 1).edges.clear();
			this.inputs.remove(this.inputs.size() - 1);
		}

		this.outputs.get(0).bits = this.noOfBits;

		if (this.inputs.size() != (int) Math.pow(2, this.noOfBits)) {
			if (this.inputs.size() > (int) Math.pow(2, this.noOfBits)) {
				for (int i = this.inputs.size() - 1 - (this.inputEnableState ? 1 : 0); i >= (int) Math.pow(2, this.noOfBits); i--) {
					inputs.get(i).edges.clear();
					inputs.remove(i);
				}
			} else if (this.inputs.size() < (int) Math.pow(2, this.noOfBits)) {
				for (int i = this.inputs.size() - (this.inputEnableState ? 1 : 0); i < (int) Math.pow(2, this.noOfBits); i++) {
					inputs.add(i, new Input(this, "Input " + i));
					inputs.get(i).setLocation(0, i + 1 + (this.outputEnableState ? 1 : 0));
				}
			}
		}
		System.out.println(this.inputs.size());
		outputs.get(0).setLocation(6, (this.inputs.size() / 2) + outputs.size() - 1);
		for (int i = 0; i < inputs.size() - (this.inputEnableState ? 1 : 0); i++) {
			inputs.get(i).setLocation(0, i + 1 + (this.outputEnableState ? 1 : 0));
		}
		if (inputEnableState) {
			this.inputs.get(inputs.size() - 1).setLocation(3, inputs.size() + outputs.size());
		}
		this.height = this.inputs.size() + outputs.size();
	}
}
