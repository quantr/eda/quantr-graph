/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.data.plexer;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author michelle
 */
@XStreamAlias("decoder")
public class Decoder extends Vertex {

	public int noOfBits = 1;
	public String inputEnableState = "yes";

	public Decoder(String name) {
		super(name, 2, 2, 6, 4);
		properties.put("Name", name);
		properties.put("No. of Bits", "1");
		properties.put("Include Input Enable", inputEnableState);
		this.inputs.get(0).setLocation(0, 1);
		this.inputs.get(1).setLocation(3, 4);
		this.outputs.get(0).setLocation(6, 1);
		this.outputs.get(1).setLocation(6, 2);
	}

	public void paint(Graphics g) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect((x + 1) * gridSize, y * gridSize, 4 * gridSize, (this.outputs.size() + 1) * gridSize);
		g2.setColor(Color.black);
		g2.drawRect((x + 1) * gridSize, y * gridSize, 4 * gridSize, (this.outputs.size() + 1) * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 0.8f)));
		g2.drawString(Integer.toString(this.inputs.get(0).bits) + " × " + Integer.toString(this.outputs.size()), (x + 2) * gridSize, (y + inputs.get(0).deltaY - 1) * gridSize + (int) (gridSize * 0.8f));
		g2.drawString("Decoder", (x + 2) * gridSize - gridSize * 3 / 5, (y + inputs.get(0).deltaY) * gridSize + gridSize * 7 / 10);
		g2.drawLine(x * gridSize, (y + inputs.get(0).deltaY) * gridSize, (x + 1) * gridSize, (y + inputs.get(0).deltaY) * gridSize);
		g2.setColor(Color.gray);
		g2.drawString("0", (x + 4) * gridSize + gridSize * 2 / 5, (y + 1) * gridSize);
		inputs.get(0).paint(g, gridSize);

		if (inputEnableState.equals("yes")) {
			if (inputs.size() == 2) {
				g2.setColor(Color.black);
				g2.drawLine((x + 3) * gridSize, (y + (this.outputs.size() + 1)) * gridSize, (x + 3) * gridSize, (y + (this.outputs.size() + 1) + 1) * gridSize);
				g2.setColor(Color.gray);
				g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 5));
				g2.drawString("E", (x + 3) * gridSize - gridSize / 5, (y + (this.outputs.size() + 1)) * gridSize - gridSize / 5);
				inputs.get(1).paint(g, gridSize);
			}
		}
		for (int i = 0; i < outputs.size(); i++) {
			g2.setColor(Color.black);
			g2.drawLine((x + 5) * gridSize, (y + 1 + i) * gridSize, (x + 6) * gridSize, (y + 1 + i) * gridSize);
			outputs.get(i).paint(g, gridSize);
		}

		g2.setColor(Color.darkGray);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString((String) properties.get("Label"), (x + (this.width / 2) - 1) * gridSize - gridSize / 5, (y - 1) * gridSize + gridSize * 2 / 10);

		super.paint(g);
	}

	@Override
	public String getTypeName() {
		return "Decoder";
	}

	@Override
	public void eval() {
		if (inputEnableState.equals("no")) {
			if (inputs.size() != 1) {
				this.updateProperty();
				return;
			} else {
				for (int i = 0; i < outputs.size(); i++) {
					this.outputs.get(i).value = (this.inputs.get(0).value == i ? 1 : 0);
					for (Port p : this.portConnectedTo(outputs.get(i))) {
						if (p instanceof Input) {
							if (p.bits == this.outputs.get(i).bits) {
								p.value = this.outputs.get(i).value;
							}
						}
					}
				}
			}
		} else {
			if (inputs.size() != 2) {
				this.updateProperty();
				return;
			} else {
				if (this.inputs.get(1).value == 1) {
					for (int i = 0; i < outputs.size(); i++) {
						this.outputs.get(i).value = (this.inputs.get(0).value == i ? 1 : 0);
						for (Port p : this.portConnectedTo(outputs.get(i))) {
							if (p instanceof Input) {
								if (p.bits == this.outputs.get(i).bits) {
									p.value = this.outputs.get(i).value;
								}
							}
						}
					}
				} else {
					for (int i = 0; i < outputs.size(); i++) {
						this.outputs.get(i).value = 0;
						for (Port p : this.portConnectedTo(outputs.get(i))) {
							if (p instanceof Input) {
								if (p.bits == this.outputs.get(i).bits) {
									p.value = this.outputs.get(i).value;
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void updateProperty() {
		this.name = (String) properties.get("Name");
		this.noOfBits = Integer.parseInt((String) properties.get("No. of Bits"));
		this.inputEnableState = (String) properties.get("Include Input Enable");

		if (this.inputEnableState.equals("yes")) {
			if (this.inputs.size() == 1) {
				this.inputs.add(new Input(this, "Input " + "2"));
				this.inputs.get(1).setLocation(3, 7);
				System.out.println();
			}
		} else {
			if (this.inputs.size() > 1) {
				inputs.get(this.inputs.size() - 1).edges.clear();
				this.inputs.remove(this.inputs.size() - 1);
			}
		}
		this.inputs.get(0).bits = this.noOfBits;

		if (this.outputs.size() != (int) Math.pow(2, this.noOfBits)) {
			if (this.outputs.size() > (int) Math.pow(2, this.noOfBits)) {
				for (int i = this.outputs.size() - 1; i >= (int) Math.pow(2, this.noOfBits); i--) {
					outputs.get(i).edges.clear();
					outputs.remove(i);
				}
			} else if (this.outputs.size() < (int) Math.pow(2, this.noOfBits)) {
				for (int i = this.outputs.size(); i < (int) Math.pow(2, this.noOfBits); i++) {
					outputs.add(new Output(this, "Output " + i));
				}
			}
			inputs.get(0).setLocation(0, this.outputs.size() / 2);
		}
		if (inputEnableState.equals("yes")) {
			this.inputs.get(1).setLocation(3, (this.outputs.size() + 1) + 1);
		}
		for (int i = 0; i < this.outputs.size(); i++) {
			this.outputs.get(i).setLocation(6, i + 1);

		}
		this.height = this.outputs.size() + 2;

	}
}
