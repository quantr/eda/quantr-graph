/*
 * Copyright 2024 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import hk.quantr.logic.data.gate.Vertex;
import java.util.HashMap;

/**
 *
 * @author ken
 */
public class BDDBuilder {

	public static HashMap<Vertex, BDD> variableMap = new HashMap();
	public static int numOfVar = 0;
	private static BDDFactory factory;

	public static BDD getVar(Vertex v) {
		if (variableMap.get(v) == null) {
			BDD a = factory.ithVar(numOfVar); // Variable a
			numOfVar++;
			variableMap.put(v, a);
			return a;
		} else {
			return variableMap.get(v);
		}
	}

	public static void init() {
		numOfVar = 0;
		int numVariables = 100; // The number of variables you plan to use
		int cacheSize = 10000;  // The cache size for the BDD operations
		factory = BDDFactory.init("javabdd", numVariables, cacheSize);
		factory.setVarNum(numVariables);
	}

	public static void reorder() {
	}
}
