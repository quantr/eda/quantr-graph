/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.engine;

import hk.quantr.logic.QuantrGraphCanvas;
import hk.quantr.logic.data.basic.Led;
import hk.quantr.logic.data.basic.RGBLed;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.data.gate.Vertex;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ken
 */
public class Simulator implements Runnable {

	public QuantrGraphCanvas canvas;
//	SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.S");
	ArrayList<ArrayList<Vertex>> temp = null;
	ArrayList<Vertex> simulatingLeds;
//	int x = 0;
	VCD vcd;
	public boolean stopped = false;
//	FileWriter fw;

	public Simulator(QuantrGraphCanvas canvas) {
		this.canvas = canvas;
		this.initialize();
	}

	public final void initialize() {
		long duation;
		long time = System.nanoTime();
		canvas.getGrid().reconnectEdge(canvas.module);
		temp = canvas.calculateLevel();
		for (int i = 0; i < temp.size(); i++) {
			for (Vertex v : temp.get(i)) {
				v.simulateInit();
			}
		}
		canvas.getGrid().reconnectEdge(canvas.module);
		if (canvas.maxLevel != -1 && canvas.simulating) {
			simulatingLeds = new ArrayList();
			for (int i = 0; i <= canvas.maxLevel; i++) {
				for (Vertex v : temp.get(i)) {
					if (v instanceof Led) {
						simulatingLeds.add(v);
					}
					if (v instanceof RGBLed) {
						simulatingLeds.add(v);
					}

				}
			}
		}

		for (Vertex v : canvas.module.vertices) {
			if (v instanceof VCD) {
				vcd = (VCD) v;
				for (int i = 0; i < vcd.inputs.size(); i++) {
					if (!vcd.wires.contains(vcd.portConnectedTo(vcd.inputs.get(i)).get(0))) {
						System.out.println("ref: " + vcd.portConnectedTo(vcd.inputs.get(i)).get(0).refFormatter(vcd.refCnt));
						vcd.portConnectedTo(vcd.inputs.get(i)).get(0).ref = "";
						vcd.portConnectedTo(vcd.inputs.get(i)).get(0).refFormatter(vcd.refCnt++);
						vcd.wires.add(vcd.portConnectedTo(vcd.inputs.get(i)).get(0));
						vcd.wires.get(i).name = vcd.inputs.get(i).name;
					}
				}
			}
		}
		for (Vertex v : canvas.module.vertices) {
			if (v instanceof VCD) {
				if (!((VCD) v).defined) {
					try {
						FileWriter fw = new FileWriter(canvas.quantrGraphPanel.tempVCDFile);
						fw.write("");
						fw.append("$timescale " + ((VCD) v).timeUnit + " $end\n");
						fw.append("$scope module TOP $end\n" + ((VCD) v).initVCDFile());
						fw.append("$upscope $end\n$enddefinitions $end\n\n\n");
						fw.close();
					} catch (IOException ex) {
						Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
					}
					((VCD) v).defined = true;
				}
			}
		}
		duation = System.nanoTime() - time;
		System.out.println("duration : " + duation + " ns");
		canvas.quantrGraphPanel.getStatusLabel1().setText("Simulation rate: -- cycles/s");
	}

	public void dump() {
		if (vcd != null) {
			synchronized (vcd) {
				try {
					FileWriter fw = new FileWriter(canvas.quantrGraphPanel.tempVCDFile, true);
					fw.append(vcd.getDataToDump());
					fw.close();
					System.out.println("dumped");
				} catch (IOException ex) {
					Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	@Override
	public void run() {
		long time;
		double samplingRate = 100;
		while (canvas.simulating) {
			time = System.nanoTime();
			for (int count = 0; count < samplingRate && !stopped; count++) {
				for (int i = 0; i <= canvas.maxLevel; i++) {
					for (Vertex v : temp.get(i)) {
//						eventTrigger(v);
						v.eventTriggerEval();
//						v.eval();
					}
				}
				for (Vertex l : simulatingLeds) {
					if (l instanceof Led) {
						((Led) l).update();
					} else if (l instanceof RGBLed) {
						((RGBLed) l).update();
					}
				}
			}
			System.out.println(System.nanoTime() - time);
			time = (System.nanoTime() - time) / (int) samplingRate;
			samplingRate = 1.0 / time * 1e9;
			canvas.quantrGraphPanel.getStatusLabel1().setText("Simulation rate: " + String.format("%.2f", 1.0 / time * 1e9) + " cycles/s");
		}
	}
}
