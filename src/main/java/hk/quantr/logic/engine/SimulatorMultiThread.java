/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.engine;

import hk.quantr.logic.QuantrGraphCanvas;
import hk.quantr.logic.data.basic.Clock;
import hk.quantr.logic.data.basic.Led;
import hk.quantr.logic.data.basic.RGBLed;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ken
 */
public class SimulatorMultiThread implements Runnable {

	public QuantrGraphCanvas canvas;
//	SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.S");
	ArrayList<Vertex> temp = null;
	ArrayList<Vertex> simulatingLeds;
//	int x = 0;
	VCD vcd;
//	FileWriter fw;
//	ArrayList<SimulationEvent> timeLine = new ArrayList();
	int sizeOfTimeLine = 200000;
	SimulationEvent[] timeLine = new SimulationEvent[sizeOfTimeLine];
	int[] threadLocalTime = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int numberOfThread = 0;
	int dataIndex = 0;
	long totalRunTime = 0;
	boolean timeFlag = false;

	public SimulatorMultiThread(QuantrGraphCanvas canvas) {
		this.canvas = canvas;
	}

//	public void printX() {
//		System.out.println(x);
//		x = 0;
//	}
	public final void initialize() {
		long duration, time;
		temp = canvas.calculateLevel(canvas.calculateLevel());
		dataIndex = 0;
		totalRunTime = 0;
		for (Vertex v : temp) {
			v.simulateInit();
			for (Output o : v.outputs) {
				if (!v.portConnectedTo(o).isEmpty()) {
					for (Port p : v.portConnectedTo(o)) {
						if (p instanceof Input) {
							((Input) p).dataPortIndex = dataIndex;
							p.parent.name += p + " index = " + ((Input) p).dataPortIndex;
						}
					}
					v.name += " index = " + dataIndex;
					o.dataIndex = dataIndex;
					dataIndex++;
				}
			}
			time = System.nanoTime();
			for (int c = 0; c < 1000; c++) {
				v.eval();
			}
			v.simTime = System.nanoTime() - time;
			v.simTime /= 1000;
			totalRunTime += v.simTime;
		}
		if (canvas.maxLevel != -1 && canvas.simulating) {
			simulatingLeds = new ArrayList();
			for (Vertex v : temp) {
				if (v instanceof Led) {
					simulatingLeds.add(v);
				}
				if (v instanceof RGBLed) {
					simulatingLeds.add(v);
				}
			}
		}

		int lvl = -1;
		while (++lvl <= canvas.maxLevel) {
			for (Vertex v : canvas.module.vertices) {
				if (v instanceof VCD && v.level == lvl) {
					vcd = (VCD) v;
					for (int i = 0; i < vcd.inputs.size(); i++) {
						if (!vcd.wires.contains(vcd.portConnectedTo(vcd.inputs.get(i)).get(0))) {
							System.out.println("ref: " + vcd.portConnectedTo(vcd.inputs.get(i)).get(0).refFormatter(vcd.refCnt));
							vcd.portConnectedTo(vcd.inputs.get(i)).get(0).ref = "";
							vcd.portConnectedTo(vcd.inputs.get(i)).get(0).refFormatter(vcd.refCnt++);
							vcd.wires.add(vcd.portConnectedTo(vcd.inputs.get(i)).get(0));
							vcd.wires.get(i).name = vcd.inputs.get(i).name;
						}
//						if (vcd.portConnectedTo(vcd.inputs.get(i)).get(0).parent instanceof VCD) {
//						} else {
//						}
					}
				}
			}
		}
		for (Vertex v : canvas.module.vertices) {
			if (v instanceof VCD && v.level == canvas.maxLevel) {
				if (!((VCD) v).defined) {
					try {
						FileWriter fw = new FileWriter(canvas.quantrGraphPanel.tempVCDFile);
						fw.write("");
						fw.append("$timescale " + ((VCD) v).timeUnit + " $end\n");
						fw.append("$scope module TOP $end\n" + ((VCD) v).initVCDFile());
						fw.append("$upscope $end\n$enddefinitions $end\n\n\n");
						fw.close();
					} catch (IOException ex) {
						Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
					}
					((VCD) v).defined = true;
				}
			}
		}
		for (int i = 0; i < timeLine.length; i++) {
			timeLine[i] = new SimulationEvent(canvas.maxLevel, this.dataIndex);
		}
		for (int cc = 0; cc < 5; cc++) {
			long threadTime = 0;
			ArrayList<Vertex> threadVertex = new ArrayList();
			if (!temp.isEmpty()) {
//				System.out.println("not empty");
				while (threadTime < totalRunTime / 5 && !temp.isEmpty() && temp.get(0).level >= 0) {
					threadTime += temp.get(0).simTime;
					threadVertex.add(temp.get(0));
					temp.remove(0);
				}
				int c = cc;
				threadLocalTime[c] = 0;
				numberOfThread++;
				new Thread(new Runnable() {
					@Override
					public void run() {
						int timeIndex = c;
						System.out.println(timeIndex);
						boolean move = true;
						ArrayList<Vertex> localVertex = threadVertex;
						while (canvas.simulating) {

//							System.out.println("simlating + Time = " + localTime + "  " + localVertex);
//							try {
//								Thread.sleep(100);
//							} catch (InterruptedException ex) {
//								Logger.getLogger(SimulatorMultiThread.class.getName()).log(Level.SEVERE, null, ex);
//							}
							if (!timeFlag && threadLocalTime[timeIndex] < sizeOfTimeLine / 2) {
								move = true;
								for (Vertex v : localVertex) {
									move = move && receiveData(v, threadLocalTime[timeIndex]);
								}
								if (move) {
									threadLocalTime[timeIndex]++;
								}
//								System.out.println("first " + move);
							} else if (timeFlag && threadLocalTime[timeIndex] >= sizeOfTimeLine / 2 && threadLocalTime[timeIndex] < sizeOfTimeLine) {
								move = true;
								for (Vertex v : localVertex) {
									move = move && receiveData(v, threadLocalTime[timeIndex]);

								}
//								System.out.println(move);
								if (move) {
									threadLocalTime[timeIndex]++;
								}
							}
							if (threadLocalTime[timeIndex] == sizeOfTimeLine && !timeFlag) {
								threadLocalTime[timeIndex] = 0;
							}
						}
					}
				}).start();
			}
		}
	}

	public void dump() {
		if (vcd != null) {
			synchronized (vcd) {
				try {
					FileWriter fw = new FileWriter(canvas.quantrGraphPanel.tempVCDFile, true);
					fw.append(vcd.getDataToDump());
					fw.close();
					System.out.println("dumped");
				} catch (IOException ex) {
					Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	@Override
	public void run() {
		this.initialize();
		int count = 0;
		long time = System.nanoTime();
		System.out.println("started");
		System.out.println(numberOfThread);
		while (canvas.simulating) {
			if (!timeFlag) {
				if (checkLocalTime(sizeOfTimeLine / 2, 1) && checkLocalTime(sizeOfTimeLine, 2)) {
					timeFlag = true;
					for (int i = 0; i < sizeOfTimeLine / 2; i++) {
						timeLine[i].portEvents = new PortEvent[dataIndex];
					}
				}
			} else {
				if (checkLocalTime(sizeOfTimeLine, 0)) {
					timeFlag = false;
					for (int i = sizeOfTimeLine / 2; i < sizeOfTimeLine; i++) {
						timeLine[i].portEvents = new PortEvent[dataIndex];
					}
					count++;
				}
			}
//			System.out.println();
			if (count >= 50) {
				System.out.println(System.nanoTime() - time);
				time /= (sizeOfTimeLine * 50);
				count = 0;
				time = System.nanoTime();
			}
//			try {
//				Thread.sleep(10);
//			} catch (InterruptedException ex) {
//				Logger.getLogger(SimulatorMultiThread.class.getName()).log(Level.SEVERE, null, ex);
//			}
		}
	}

	public boolean checkLocalTime(int time, int check) {
		if (check == 0) {
			for (int i = 0; i < numberOfThread; i++) {
				if (threadLocalTime[i] < time) {
					return false;
				}
			}
			return true;
		} else if (check == 1) {
			for (int i = 0; i < numberOfThread; i++) {
				if (threadLocalTime[i] < time) {
					return false;
				}
			}
			return true;
		} else {
			for (int i = 0; i < numberOfThread; i++) {
				if (threadLocalTime[i] == time) {
					return false;
				}

			}
			return true;
		}
	}

	public boolean receiveData(Vertex v, int time) {
//		if (time == 20) {
//			System.out.println("why time = 20,");
//			System.out.println(timeLine.length +" "+ timeLine[time]);
//		}
		if (time == sizeOfTimeLine) {
			return true;
		}
		if (v.level > 0) {
			boolean temp = true;
			for (Input input : v.inputs) {
				if (input.dataPortIndex >= 0) {
					if (input.state.equals("on") || input.state.equals("off")) {
//						System.out.println(v + " " + input.dataPortIndex + " " + timeLine.get(time).portEvents[input.dataPortIndex]);
						if (timeLine[time].portEvents[input.dataPortIndex] != null) {
							if (input.bits == timeLine[time].portEvents[input.dataPortIndex].port.bits) {
								input.value = timeLine[time].portEvents[input.dataPortIndex].value;
								input.state = timeLine[time].portEvents[input.dataPortIndex].state;
//								input.status = timeLine[time].portEvents[input.dataPortIndex].status;
							}
//							System.out.println("receive" + input + input.value);
						} else {
							temp = false;
						}
					}
				}
			}
			if (temp) {
				v.eval2();
				retrieveData(v, time);
			}
			return temp;
		} else if (v.level == 0) {
//				System.out.println("level is 0");
			v.eval2();
//				System.out.println("eval");
			retrieveData(v, time);
			return true;
		}
		return false;
	}

	public void retrieveData(Vertex v, int time) {
		if (v instanceof Clock) {
			for (Output output : v.outputs) {
//			System.out.println(output + " " + output.dataIndex);
				if (output.dataIndex >= 0) {
					if (timeLine[time].portEvents[output.dataIndex] == null) {
						timeLine[time].portEvents[output.dataIndex] = new PortEvent(output, output.value, output.state);
					}
				}
			}
		} else {
			for (Output output : v.outputs) {
//			System.out.println(output + " " + output.dataIndex);
				if (output.dataIndex >= 0) {
					if (timeLine[time].portEvents[output.dataIndex] == null) {
						timeLine[time].portEvents[output.dataIndex] = new PortEvent(output, output.value, output.state);
					}
				}
			}
		}
	}
}
