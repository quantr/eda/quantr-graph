///*
// * Copyright 2023 ken.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package hk.quantr.logic.engine;
//
//import hk.quantr.logic.QuantrGraphCanvas;
//import hk.quantr.logic.data.basic.Led;
//import hk.quantr.logic.data.basic.RGBLed;
//import hk.quantr.logic.data.gate.Vertex;
//import java.io.IOException;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.objectweb.asm.ClassReader;
//import org.objectweb.asm.ClassVisitor;
//import org.objectweb.asm.ClassWriter;
//import org.objectweb.asm.MethodVisitor;
//import org.objectweb.asm.Opcodes;
//import static org.objectweb.asm.Opcodes.ASM7;
//
///**
// *
// * @author ken
// */
//class OwnClassLoader extends ClassLoader {
//
//	public Class<?> defineClass(String name, byte[] b) {
//		return defineClass(name, b, 0, b.length);
//	}
//}
//
//public class Emulator implements Runnable {
//
//	QuantrGraphCanvas canvas;
//	SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.S");
//	ArrayList<ArrayList<Vertex>> temp = new ArrayList<ArrayList<Vertex>>();
//	ArrayList<Vertex> simulatingLeds;
//	ArrayList<Method> method = new ArrayList();
//	ArrayList<Vertex> methodedVertex = new ArrayList();
//	Method simulate;
//	Object s;
//
//	private Class<?> createClass(byte[] b) {
//		return new OwnClassLoader().defineClass("hk.quantr.logic.engine.Simulator", b);
//	}
//
//	public Emulator(QuantrGraphCanvas canvas) {
//		this.canvas = canvas;
//	}
//
//	public void initialize() throws IOException, InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {
//		ClassReader cr = new ClassReader("hk.quantr.logic.engine.Simulator");
//		ClassWriter cw = new ClassWriter(0);
//		ClassVisitor cv = new ClassVisitor(ASM7, cw) {
//			@Override
//			public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
//				if ("start".equals(name)) {
//					return null;
//				}
//				return super.visitMethod(access, name, desc, signature, exceptions);
//			}
//
//			@Override
//			public void visitEnd() {
//				MethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC, "start", "(II)I", null, null);
////				0: aload_0
////       1: getfield      #28                 // Field canvas:Lhk/quantr/logic/QuantrGraphCanvas;
////       4: getfield      #85                 // Field hk/quantr/logic/QuantrGraphCanvas.simulating:Z
////       7: ifeq          195
////      10: aload_0
////      11: dup
////      12: getfield      #24                 // Field x:I
////      15: iconst_1
////      16: iadd
////      17: putfield      #24                 // Field x:I
////      20: aload_0
////      21: getfield      #20                 // Field temp:Ljava/util/ArrayList;
////      24: iconst_0
////      25: invokevirtual #62                 // Method java/util/ArrayList.get:(I)Ljava/lang/Object;
////      28: checkcast     #54                 // class java/util/ArrayList
////      31: invokevirtual #66                 // Method java/util/ArrayList.iterator:()Ljava/util/Iterator;
////      34: astore_1
////      35: aload_1
////      36: invokeinterface #70,  1           // InterfaceMethod java/util/Iterator.hasNext:()Z
////      41: ifeq          71
////      44: aload_1
////      45: invokeinterface #76,  1           // InterfaceMethod java/util/Iterator.next:()Ljava/lang/Object;
////      50: checkcast     #80                 // class hk/quantr/logic/data/gate/Vertex
////      53: astore_2
////      54: aload_2
////      55: getfield      #101                // Field hk/quantr/logic/data/gate/Vertex.inputs:Ljava/util/ArrayList;
////      58: invokevirtual #104                // Method java/util/ArrayList.isEmpty:()Z
////      61: ifeq          68
////      64: aload_2
////      65: invokevirtual #107                // Method hk/quantr/logic/data/gate/Vertex.eval:()V
////      68: goto          35
////      71: iconst_1
////      72: istore_1
////      73: iload_1
////      74: aload_0
////      75: getfield      #28                 // Field canvas:Lhk/quantr/logic/QuantrGraphCanvas;
////      78: getfield      #59                 // Field hk/quantr/logic/QuantrGraphCanvas.maxLevel:I
////      81: if_icmpgt     131
////      84: aload_0
////      85: getfield      #20                 // Field temp:Ljava/util/ArrayList;
////      88: iload_1
////      89: invokevirtual #62                 // Method java/util/ArrayList.get:(I)Ljava/lang/Object;
////      92: checkcast     #54                 // class java/util/ArrayList
////      95: invokevirtual #66                 // Method java/util/ArrayList.iterator:()Ljava/util/Iterator;
////      98: astore_2
////      99: aload_2
////     100: invokeinterface #70,  1           // InterfaceMethod java/util/Iterator.hasNext:()Z
////     105: ifeq          125
////     108: aload_2
////     109: invokeinterface #76,  1           // InterfaceMethod java/util/Iterator.next:()Ljava/lang/Object;
////     114: checkcast     #80                 // class hk/quantr/logic/data/gate/Vertex
////     117: astore_3
////     118: aload_3
////     119: invokevirtual #107                // Method hk/quantr/logic/data/gate/Vertex.eval:()V
////     122: goto          99
////     125: iinc          1, 1
////     128: goto          73
////     131: aload_0
////     132: getfield      #90                 // Field simulatingLeds:Ljava/util/ArrayList;
////     135: invokevirtual #66                 // Method java/util/ArrayList.iterator:()Ljava/util/Iterator;
////     138: astore_1
////     139: aload_1
////     140: invokeinterface #70,  1           // InterfaceMethod java/util/Iterator.hasNext:()Z
////     145: ifeq          192
////     148: aload_1
////     149: invokeinterface #76,  1           // InterfaceMethod java/util/Iterator.next:()Ljava/lang/Object;
////     154: checkcast     #80                 // class hk/quantr/logic/data/gate/Vertex
////     157: astore_2
////     158: aload_2
////     159: instanceof    #93                 // class hk/quantr/logic/data/basic/Led
////     162: ifeq          175
////     165: aload_2
////     166: checkcast     #93                 // class hk/quantr/logic/data/basic/Led
////     169: invokevirtual #110                // Method hk/quantr/logic/data/basic/Led.update:()V
////     172: goto          189
////     175: aload_2
////     176: instanceof    #99                 // class hk/quantr/logic/data/basic/RGBLed
////     179: ifeq          189
////     182: aload_2
////     183: checkcast     #99                 // class hk/quantr/logic/data/basic/RGBLed
////     186: invokevirtual #113                // Method hk/quantr/logic/data/basic/RGBLed.update:()V
////     189: goto          139
////     192: goto          0
////     195: return
//				mv.visitEnd();
//				mv.visitMaxs(4, 4);
//			}
//		};
//		cr.accept(cv, 0);
//		byte bytes[] = cw.toByteArray();
//		Class<Simulator> c = (Class<Simulator>) new OwnClassLoader().defineClass("hk.quantr.logic.engine.Simulator", bytes);
//		s = c.getDeclaredConstructor().newInstance();
////		((Simulator) s).canvas = this.canvas;
//		s.getClass().getMethod("setCanvas", QuantrGraphCanvas.class).invoke(s, this.canvas);
//		simulate = s.getClass().getMethod("start");
//	}
//
//	public void start() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, InstantiationException, NoSuchMethodException {
//		this.initialize();
//		long time = System.nanoTime();
////		while (canvas.simulating) {
////			simulate.invoke(s);
////		}
////		}
//	}
//
//	@Override
//	public void run() {
//		try {
//			this.start();
//		} catch (IllegalAccessException ex) {
//			Logger.getLogger(Emulator.class
//					.getName()).log(Level.SEVERE, null, ex);
//
//		} catch (IllegalArgumentException ex) {
//			Logger.getLogger(Emulator.class
//					.getName()).log(Level.SEVERE, null, ex);
//
//		} catch (InvocationTargetException ex) {
//			Logger.getLogger(Emulator.class
//					.getName()).log(Level.SEVERE, null, ex);
//
//		} catch (IOException ex) {
//			Logger.getLogger(Emulator.class
//					.getName()).log(Level.SEVERE, null, ex);
//
//		} catch (InstantiationException ex) {
//			Logger.getLogger(Emulator.class
//					.getName()).log(Level.SEVERE, null, ex);
//
//		} catch (NoSuchMethodException ex) {
//			Logger.getLogger(Emulator.class
//					.getName()).log(Level.SEVERE, null, ex);
//		}
//	}
//}
