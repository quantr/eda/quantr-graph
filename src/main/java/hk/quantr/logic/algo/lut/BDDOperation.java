/*
 * Copyright 2024 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.algo.lut;

import com.github.javabdd.BDD;
import hk.quantr.logic.data.basic.OutputPin;
import hk.quantr.logic.data.gate.AndGate;
import hk.quantr.logic.data.gate.NandGate;
import hk.quantr.logic.data.gate.NorGate;
import hk.quantr.logic.data.gate.NotGate;
import hk.quantr.logic.data.gate.OrGate;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.logic.data.gate.XnorGate;
import hk.quantr.logic.data.gate.XorGate;
import hk.quantr.lutlibrary.structure.BDDStruct;
import java.util.ArrayList;

/**
 *
 * @author michelle
 */
public class BDDOperation {

	private BDDStruct<Vertex> bddStruct;
	private BDD result;

	public BDDOperation(Vertex v) {
		bddStruct = new BDDStruct(100, 10000);
		if (v.inputs.isEmpty()) {
			bddStruct.addVar(v);
			result = bddStruct.variableMap.get(v);
		} else {
			for (Port p : v.portConnectedTo(v.inputs.get(0))) {
				if (p instanceof Output) {
					result = buildBDD(p.parent);
					break;
				}
			}
		}
	}

	public BDD buildBDD(Vertex v) {
		BDD result = null;
		if (v.inputs.isEmpty()) {
			bddStruct.addVar(v);
			return bddStruct.variableMap.get(v);
		} else {
			if (bddStruct.variableMap.containsKey(v)) {
				return bddStruct.variableMap.get(v);
			} else {
				ArrayList<Vertex> childrenVertex = new ArrayList();
				for (int i = 0; i < v.inputs.size(); i++) {
					for (Port p : v.portConnectedTo(v.inputs.get(i))) {
						if (p instanceof Output) {
							childrenVertex.add(p.parent);
						}
					}
				}

				if (childrenVertex.isEmpty()) {
//					throw new Exception("Empty Input");
				} else {
					for (Vertex child : childrenVertex) {
						bddStruct.addSubBdd(child, buildBDD(child)); // confirm map contains child vertex
						if (result == null) {
							if (v instanceof NotGate) {
								result = bddStruct.variableMap.get(child);
								result.not();
							} else {
								result = buildBDD(child);
							}
						} else if (v instanceof AndGate || v instanceof NandGate) {
							result.andWith(buildBDD(child));
						} else if (v instanceof OrGate || v instanceof NorGate) {
							result.orWith(buildBDD(child));
						} else if (v instanceof XorGate || v instanceof XnorGate) {
							result.xorWith(buildBDD(child));
						} else if (v instanceof OutputPin) {
							result = buildBDD(child);
						}
					}

					if (v instanceof NandGate || v instanceof NorGate || v instanceof XnorGate) {
						result.not();
					}
				}
			}
		}
		bddStruct.addSubBdd(v, result);
		System.out.println(v);
		result.printDot();
		return result;
	}

	public BDDStruct<Vertex> getBddStruct() {
		return bddStruct;
	}

	public BDD getResultBDD() {
		return result;
	}
}
/*
@Override

	@Override
	public BDD buildBDD() {
		BDD result = null;
		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output) {
				result = p.parent.buildBDD();
				break;
			}
		}
		return result;
	}

	@Override
	public BDD buildBDD() {
		return BDDBuilder.getVar(this);
	}

	public BDD buildBDD() {
		BDD result = null;
		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output) {
				result = p.parent.buildBDD();
				break;
			}
		}
		return result.not();
	}


	@Override
	public BDD buildBDD() {
		BDD result = null;
		for (Port p : this.portConnectedTo(this.inputs.get(0))) {
			if (p instanceof Output) {
				result = p.parent.buildBDD();
				break;
			}
		}

		for (int i = 1; i < this.inputs.size(); i++) {
			for (Port p : this.portConnectedTo(this.inputs.get(i))) {
				if (p instanceof Output) {
					if (result != null) {
						result.orWith(p.parent.buildBDD());
					}
				}
			}
		}
		return result;
	}
 */
