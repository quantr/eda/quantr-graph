///*
// * Copyright 2023 Peter <peter@quantr.hk>.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package hk.quantr.logic.algo.lee;
//
//import hk.quantr.logic.data.basic.Clock;
//import hk.quantr.logic.data.basic.Tunnel;
//import hk.quantr.logic.data.gate.AutoEdge;
//import hk.quantr.logic.data.gate.Output;
//import hk.quantr.logic.data.gate.Port;
//import hk.quantr.routingalgo.lee.Point;
//import java.awt.Color;
//import java.awt.Font;
//import java.awt.Graphics;
//import java.util.ArrayList;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class Path extends hk.quantr.routingalgo.lee.Path {
//
//	public AutoEdge autoEdge;
//	
//	public Path(Point start, Point end, ArrayList<Point> corners) {
//		super(start, end, corners);
//	}
//	
//	public void paint(Graphics g) {
//		if (this.autoEdge == null) {
//			return;
//		}
//		int count = 0;
//		int bit = -1;
//		Port tempOutput = new Port(null, null);
//		for (Port p : this.autoEdge.findConnectedPorts()) {
//			if (bit == -1) {
//				bit = p.bits;
//			} else {
//				if (bit != p.bits) {
//					count = -1;
//					g.setColor(Color.red);
//					for (Port port : this.autoEdge.findConnectedPorts()) {
//						g.setFont(new Font("arial", Font.BOLD, 20));
//						g.drawString(Integer.toString(port.bits) + " bits", port.getAbsolutionX() * this.autoEdge.gridSize, port.getAbsolutionY() * this.autoEdge.gridSize);
//					}
//					g.setColor(Color.orange);
//					break;
//				}
//			}
//			if (p instanceof Output || (p.parent instanceof Tunnel && ((Tunnel) p.parent).hasTunnelOutput)) {
//				count++;
//				tempOutput = p;
//			}
//		}
//		if (this.autoEdge.isSelected) {
//			g.setColor(Color.blue);
//		} else if (count >= 2) {
//			g.setColor(Color.red);  //when the edge is connected to more than 2 outputs
//		} else if (count == 1 &&  (!(tempOutput.parent instanceof Clock) && tempOutput.value > 0)) {
//			g.setColor(Color.green); //has signal
//		} else if (count >= 0) {
//			g.setColor(Color.gray); // no signal
//		}
//		if (!this.getCorners().isEmpty()) {
//			g.drawLine(this.getEnd().x * this.autoEdge.gridSize, this.getEnd().y * this.autoEdge.gridSize, this.getCorners().get(this.getCorners().size() - 1).x * this.autoEdge.gridSize, this.getCorners().get(this.getCorners().size() - 1).y * this.autoEdge.gridSize);
//			for (int i = this.getCorners().size() - 2; i >= 0; i--) {
//				g.drawLine(this.getCorners().get(i).x * this.autoEdge.gridSize, this.getCorners().get(i).y * this.autoEdge.gridSize, this.getCorners().get(i + 1).x * this.autoEdge.gridSize, this.getCorners().get(i + 1).y * this.autoEdge.gridSize);
//			}
//			g.drawLine(this.getCorners().get(0).x * this.autoEdge.gridSize, this.getCorners().get(0).y * this.autoEdge.gridSize, this.getStart().x * this.autoEdge.gridSize, this.getStart().y * this.autoEdge.gridSize);
//		} else {
//			g.drawLine(this.getEnd().x * this.autoEdge.gridSize, this.getEnd().y * this.autoEdge.gridSize, this.getStart().x * this.autoEdge.gridSize, this.getStart().y * this.autoEdge.gridSize);
//		}
//
//	}
//}
