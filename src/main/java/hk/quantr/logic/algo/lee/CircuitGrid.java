/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.algo.lee;

import hk.quantr.logic.QuantrGraphCanvas;
import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Point;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.routingalgo.lee.Path;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author ken
 */
public class CircuitGrid extends hk.quantr.routingalgo.lee.VerifyGrid {

	QuantrGraphCanvas canvas;
	public Port[][] ports;
	public ArrayList<Port> connectedPorts = new ArrayList();
	public boolean shouldRun = true;
	private int identity;
	private ArrayList<Integer> autoWiresProgress = new ArrayList();
//	public CircuitGrid(int size) {
//		super(size);
//		initGrid();
//	}

	public CircuitGrid(int size, QuantrGraphCanvas canvas) {
		super(size);
		this.canvas = canvas;
		initGrid();
	}

	public void initGrid() {
		ports = new Port[size][size];
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				ports[x][y] = new Port(("Circuit port" + "(" + x + "," + y + ")"), x, y);
				nodeLoc[x][y].applied = false;
				wireCol[x][y] = false;
				wireRow[x][y] = false;
			}
		}
	}

	public void clean() {
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				nodeLoc[x][y].applied = false;
				ports[x][y].edges.clear();
				ports[x][y].vertexPort = null;
				wireCol[x][y] = false;
				wireRow[x][y] = false;
			}
		}
	}

	public void paint(Graphics g) {
//		for (int x = 0; x < size; x++) {
//			for (int y = 0; y < size; y++) {
//				if (wireCol[x][y]) {
//					g.setColor(Color.black);
////					if (findEdge(x, y, false) != null) {
////						findEdge(x, y, false).paint(g);
////					}
//					g.drawLine(x * canvas.gridSize, (y + 1) * canvas.gridSize, x * canvas.gridSize, y * canvas.gridSize);
//				}
//				if (wireRow[x][y]) {
//					g.setColor(Color.black);
////					if (findEdge(x, y, true) != null) {
////						findEdge(x, y, true).paint(g);
////					}
//					g.drawLine(x * canvas.gridSize, y * canvas.gridSize, x * canvas.gridSize + canvas.gridSize, y * canvas.gridSize);
//				}
//				g.setColor(Color.blue);
//				if (nodeLoc[x][y].applied) {
//					g.fillOval(x * canvas.gridSize, y * canvas.gridSize, 5, 5);
//				}
//				if (ports[x][y].getConnectionState()) {
//					ports[x][y].paint(g, canvas.gridSize);
//				}
//			}
//		}
	}

	public void calculateEdges(Port p) {
		chopWire(p.x, p.y, true, p.getConnectionState());
		chopWire(p.x, p.y, false, p.getConnectionState());
	}

	public boolean analyzePort(int portX, int portY, boolean add) { // true for connection at the start and end of wire
		if (add) {
			if (portX == 0 && portY == 0) {
				return false;
			} else if (portX == 0) {  // handle port on the boundary
				if (wireRow[portX][portY] && wireCol[portX][portY] && wireCol[portX][portY - 1]) { // -| Type wire
					if (!ports[portX][portY].getConnectionState()) {
						ports[portX][portY].connect();
						chopWire(portX, portY, false, true);
						return true;
					}
				}
			} else if (portY == 0) {
				if (wireCol[portX][portY] && wireRow[portX][portY] && wireRow[portX - 1][portY]) { // T Type wire
					if (!ports[portX][portY].getConnectionState()) {
						ports[portX][portY].connect();
						chopWire(portX, portY, true, true);
						return true;
					}
				}
			} else if ((wireRow[portX][portY] ^ wireRow[portX - 1][portY]) && wireCol[portX][portY] && wireCol[portX][portY - 1]) { // -| Type wire
				ports[portX][portY].connect();
				calculateEdges(ports[portX][portY]);
				return true;
			} else if ((wireCol[portX][portY] ^ wireCol[portX][portY - 1]) && wireRow[portX][portY] && wireRow[portX - 1][portY]) { // T Type wire
				ports[portX][portY].connect();
				calculateEdges(ports[portX][portY]);
				return true;
			}
		} else {
			if (portX == 0 && portY == 0) {
				return false;
			} else if (portX == 0) {
				if (!(wireCol[portX][portY] && wireCol[portX][portY - 1] && wireRow[portX][portY])) {
					ports[portX][portY].disconnect();
					calculateEdges(ports[portX][portY]);
				}
			} else if (portY == 0) {
				if (!(wireCol[portX][portY] && wireRow[portX][portY] && wireRow[portX - 1][portY])) {
					ports[portX][portY].disconnect();
					calculateEdges(ports[portX][portY]);
				}
			} else if ((wireCol[portX][portY] ? 1 : 0) + (wireCol[portX][portY - 1] ? 1 : 0) + (wireRow[portX][portY] ? 1 : 0) + (wireRow[portX - 1][portY] ? 1 : 0) <= 2) {
				ports[portX][portY].disconnect();
				calculateEdges(ports[portX][portY]);
			}
		}
		return false;
	}

	public boolean analyzeEndPort(int portX, int portY, boolean add) { // true for connection at the start and end of wire
		if (add) {
			if (portX == 0 && portY == 0) {
				return false;
			} else if (portX == 0) {  // handle port on the boundary
				if (wireRow[portX][portY] && wireCol[portX][portY] && wireCol[portX][portY - 1]) { // -| Type wire
					if (!ports[portX][portY].getConnectionState()) {
						ports[portX][portY].connect();
						chopWire(portX, portY, false, true);
						return true;
					}
				}
			} else if (portY == 0) {
				if (wireCol[portX][portY] && wireRow[portX][portY] && wireRow[portX - 1][portY]) { // T Type wire
					if (!ports[portX][portY].getConnectionState()) {
						ports[portX][portY].connect();
						chopWire(portX, portY, true, true);
						return true;
					}
				}
			} else if ((wireRow[portX][portY] ^ wireRow[portX - 1][portY]) && wireCol[portX][portY] && wireCol[portX][portY - 1]) { // -| Type wire
				if (!ports[portX][portY].getConnectionState()) {
					ports[portX][portY].connect();
					chopWire(portX, portY, false, true);
					return true;
				}
			} else if ((wireCol[portX][portY] ^ wireCol[portX][portY - 1]) && wireRow[portX][portY] && wireRow[portX - 1][portY]) { // T Type wire
				if (!ports[portX][portY].getConnectionState()) {
					ports[portX][portY].connect();
					chopWire(portX, portY, true, true);
					return true;
				}
			}
		} else {
			if (portX == 0 && portY == 0) {
				return false;
			} else if (portX == 0) {
				if (!(wireCol[portX][portY] && wireCol[portX][portY - 1] && wireRow[portX][portY])) {
					ports[portX][portY].disconnect();
					calculateEdges(ports[portX][portY]);
				}
			} else if (portY == 0) {
				if (!(wireCol[portX][portY] && wireRow[portX][portY] && wireRow[portX - 1][portY])) {
					ports[portX][portY].disconnect();
					calculateEdges(ports[portX][portY]);
				}
			} else if ((wireCol[portX][portY] ? 1 : 0) + (wireCol[portX][portY - 1] ? 1 : 0) + (wireRow[portX][portY] ? 1 : 0) + (wireRow[portX - 1][portY] ? 1 : 0) <= 2) {
				ports[portX][portY].disconnect();
			}
		}
//		calculateEdges(ports[portX][portY]);
		return false;
	}

	public void addEdge(Edge e) {
		addEdge(e, -1);
	}

	public void addEdge(Edge e, int identity) {
		this.identity = identity;
		if (e != null) {
			if (e.start.x >= 0 || e.start.y >= 0 || e.end.x >= 0 || e.end.y >= 0) {
				if (e.startPort != null && e.endPort != null) {
//					System.out.println("configLine");
					this.configLine(e.start, e.end, true);
				}
			}
		}
	}

	public void insertEdgeToCanvas(Edge e) { // add e to edges of ports, connect gridPort and vertex Port
		if (e != null) {
			e.setID(this.identity);
			if (!(e.start.x == e.end.x && e.start.y == e.end.y)) {
				if (e.startPort == null) {
					e.startPort = this.findPort(e.start.x, e.start.y);
//					System.out.println(e.startPort);
				}
				if (e.endPort == null) {
					e.endPort = this.findPort(e.end.x, e.end.y);
//					System.out.println(e.endPort);
				}
				for (int i = 0; i < canvas.module.edges.size(); i++) {
					if (e.startPort == canvas.module.edges.get(i).startPort && e.endPort == canvas.module.edges.get(i).endPort) {
						return;
					}
				}
				canvas.module.edges.add(e);

				e.startPort.edges.add(e);
				e.startPort.vertexPort = canvas.findPort(e.start.x * canvas.gridSize, e.start.y * canvas.gridSize);
				if (e.startPort.vertexPort != null) {
					e.startPort.vertexPort.edges.add(e);
					e.startPort.vertexPort.vertexPort = e.startPort;
				}

				e.endPort.edges.add(e);
				e.endPort.vertexPort = canvas.findPort(e.end.x * canvas.gridSize, e.end.y * canvas.gridSize);
				if (e.endPort.vertexPort != null) {
					e.endPort.vertexPort.edges.add(e);
					e.endPort.vertexPort.vertexPort = e.endPort;
				}
			}
			e.name += e.startPort.vertexPort + " " + e.endPort.vertexPort;
		}
	}

	public void removeEdge(Edge e, boolean fromCanvas) {// remove e to edges of ports, connect gridPort and vertex Port
		if (e != null) {
			if (e.start.x >= 0 && e.start.y >= 0 && e.end.x >= 0 && e.end.y >= 0) {
				if (canvas.module.edges.contains(e)) {
					canvas.module.edges.remove(e);
					e.startPort.edges.remove(e);
					if (e.startPort.vertexPort != null) {
						e.startPort.vertexPort.edges.remove(e);
					}
					e.endPort.edges.remove(e);
					if (e.endPort.vertexPort != null) {
						e.endPort.vertexPort.edges.remove(e);
					}
				}
				if (fromCanvas) {
					this.configLine(e.start, e.end, false);
				}
			}
		}
	}

	public void recalculateLines(hk.quantr.logic.data.gate.Module module) {
		for (int x = 0; x < this.nodeLoc.length; x++) {
			for (int y = 0; y < this.nodeLoc[0].length; y++) {
				this.nodeLoc[x][y].applied = false;
				this.wireCol[x][y] = false;
				this.wireRow[x][y] = false;
//				this.ports[x][y].vertexPort = null;
			}
		}
		for (Vertex v : module.vertices) {
			for (int x = v.x; x <= v.x + v.width && x < this.nodeLoc.length; x++) {
				for (int y = v.y; y <= v.y + v.height && y < this.nodeLoc[0].length; y++) {
					if (x >= 0 && y >= 0) {
						this.nodeLoc[x][y].applied = true;
					}
				}
			}
		}
		for (Edge e : module.edges) {
			this.applyLine(new hk.quantr.routingalgo.lee.Point(e.start.x, e.start.y), new hk.quantr.routingalgo.lee.Point(e.end.x, e.end.y));
		}
//		reconnectEdge(module);
	}

	public void reconnectEdge(hk.quantr.logic.data.gate.Module module) {
		if (!shouldRun) {
			return;
		}
//		if (1 == 1) {
//			return;
//		}
		for (int x = 0; x < this.nodeLoc.length; x++) {
			for (int y = 0; y < this.nodeLoc[0].length; y++) {
				this.nodeLoc[x][y].applied = false;
				this.wireCol[x][y] = false;
				this.wireRow[x][y] = false;
				this.ports[x][y].vertexPort = null;
				this.ports[x][y].edges.clear();
				this.analyzeEndPort(x, y, false);
			}
		}
		ArrayList<Edge> tempEdges = (ArrayList<Edge>) module.edges.clone();
		module.edges.clear();
		for (Edge e : tempEdges) {
			this.addEdge(e, e.getID());
		}
		for (Vertex v : module.vertices) {
			for (int x = v.x; x <= v.x + v.width && x < this.nodeLoc.length; x++) {
				for (int y = v.y; y <= v.y + v.height && y < this.nodeLoc[0].length; y++) {
					if (x >= 0 && y >= 0) {
						this.nodeLoc[x][y].applied = true;
					}
				}
			}

			for (Input input : v.inputs) {
				input.vertexPort = null;
				Port gridPort = this.findPort(input.getAbsolutionX(), input.getAbsolutionY());
				if (gridPort != null) {
					gridPort.vertexPort = input;
					input.vertexPort = gridPort;
					input.edges = input.vertexPort.edges;
					this.chopWire(gridPort.x, gridPort.y, true, true);
					this.chopWire(gridPort.x, gridPort.y, false, true);
					this.analyzeEndPort(input.getAbsolutionX(), input.getAbsolutionX(), true);
				}
//				System.out.println(input.vertexPort);
			}
			for (Output output : v.outputs) {
				output.vertexPort = null;
				Port gridPort = this.findPort(output.getAbsolutionX(), output.getAbsolutionY());
				if (gridPort != null) {
					gridPort.vertexPort = output;
					output.vertexPort = gridPort;
					gridPort.vertexPort = output;
					output.edges = output.vertexPort.edges;
					this.chopWire(gridPort.x, gridPort.y, true, true);
					this.chopWire(gridPort.x, gridPort.y, false, true);
					this.analyzeEndPort(output.getAbsolutionX(), output.getAbsolutionX(), true);
				}
//				System.out.println(output.vertexPort);
			}
			// avoid overlapping corner wire |_ when finding autoPath 
			for (int x = 0; x < this.nodeLoc.length; x++) {
				for (int y = 0; y < this.nodeLoc[0].length; y++) {
					int countNeighborWire = 0;
					for (int i = 0; i < 4; i++) {
						if ((i == 0 && 0 <= y + direction[i][1] && y + direction[i][1] < this.size && wireColIfApplied(x, y + direction[i][1]))
								|| (i == 1 && wireColIfApplied(x, y))
								|| (i == 2 && 0 <= x + direction[i][0] && y + x + direction[i][0] < this.size && wireRowIfApplied(x + direction[i][0], y))
								|| (i == 3 && wireRowIfApplied(x, y))) {
							countNeighborWire += (1 << i);
						}
					}
					// _ _ _ _ : (x,y) left - right - down - up3
					// _ _ _ _ e.g. 0010: no corner wire, 0011: straight wire, 0101: corner wire
					if (countNeighborWire == 9 || countNeighborWire == 5 || countNeighborWire == 6 || countNeighborWire == 10) {
						this.nodeLoc[x][y].applied = true;
					}
				}
			}
		}
	}

	public void portProtection(Port ignorePort1, Port ignorePort2) {
//		start and end ports no need to be protected, otherwise they are blocked
		for (Vertex v : this.canvas.module.vertices) {
			for (Input i : v.inputs) {
				if (i.edges.isEmpty() && i != ignorePort1 && i != ignorePort2) {
					int x = i.getAbsolutionX();
					int y = i.getAbsolutionY();
					if (y > 0) {
						this.nodeLoc[x][y - 1].applied = true;
					}
					if (y + 1 < 300) {
						this.nodeLoc[x][y + 1].applied = true;
					}
					if (x > 0) {
						this.nodeLoc[x - 1][y].applied = true;
					}
					if (x + 1 < 300) {
						this.nodeLoc[x + 1][y].applied = true;
					}

				}
//				else {
//					System.out.println(i + " ::: " + ignorePort1 + " ::: " + (i == ignorePort1) + " ::: " + ignorePort2 + " ::: " + (i == ignorePort2));
//				}
			}
			for (Output o : v.outputs) {
				if (o.edges.isEmpty() && o != ignorePort1 && o != ignorePort2) {
					int x = o.getAbsolutionX();
					int y = o.getAbsolutionY();
					if (y > 0) {
						this.nodeLoc[x][y - 1].applied = true;
					}
					if (y + 1 < 300) {
						this.nodeLoc[x][y + 1].applied = true;
					}
					if (x > 0) {
						this.nodeLoc[x - 1][y].applied = true;
					}
					if (x + 1 < 300) {
						this.nodeLoc[x + 1][y].applied = true;
					}

				}
//				else {
//					System.out.println(o + " ::: " + ignorePort1 + " ::: " + (o == ignorePort1) + " ::: " + ignorePort2 + " ::: " + (o == ignorePort2));
//				}
			}
		}
	}

	public void configLine(Point start, Point end, boolean add) { // mark the wire on grid be applied
		Point tempPoint;
		boolean extended = false;
		ArrayList<Edge> overLapEdges = new ArrayList();
		if (start.y == end.y) { // if horizontal 
			if (start.x < end.x) {
				tempPoint = start;
				for (int x = start.x; x < end.x; x++) {
					extended = connectWire(x, end, add, extended, overLapEdges, tempPoint); // marking from left end point of wire to the right 
				}
//				if (!overLapEdges.contains(findEdge(end.x, end.y, true))) { // if no tangent wire connected, find the overlapping edge
//					if (findEdge(end.x, end.y, true) != null) {
//						overLapEdges.add(findEdge(end.x, end.y, true));
//					}
//				}
				if (extended) { // if the new wire be applied on the grid 
					extendWire(overLapEdges, tempPoint, end); // handle the extended wire
				}
				analyzeEndPort(end.x, end.y, add);// analyze the port and handle the port if wires are connected on the port
			} else if (start.x > end.x) {
				tempPoint = end;
				for (int x = end.x; x < start.x; x++) {
					extended = connectWire(x, start, add, extended, overLapEdges, tempPoint);
//					extended = configNode(tempPoint, extended, overLapEdges, x, end.y, add, true);
				}
//				if (!overLapEdges.contains(findEdge(start.x, start.y, true))) { // if no tangent wire connected, find the overlapping edge
//					if (findEdge(start.x, start.y, true) != null) {
//						overLapEdges.add(findEdge(start.x, start.y, true));
//					}
//				}
				if (extended) { // if have new wire 
					extendWire(overLapEdges, tempPoint, start);
				}
				analyzeEndPort(start.x, start.y, add);
			}
		} else if (end.x == start.x) {
			if (start.y < end.y) {
				tempPoint = start;
				for (int y = start.y; y < end.y; y++) {
					extended = connectWire2(y, end, add, extended, overLapEdges, tempPoint);
				}
//				if (!overLapEdges.contains(findEdge(end.x, end.y, false))) { // if no tangent wire connected, find the overlapping edge
//					if (findEdge(end.x, end.y, false) != null) {
//						overLapEdges.add(findEdge(end.x, end.y, false));
//					}
//				}
				if (extended) { // if have new wire 
					extendWire2(overLapEdges, tempPoint, end);
				}
				analyzeEndPort(end.x, end.y, add);
			} else if (start.y > end.y) {
				tempPoint = end;
				for (int y = end.y; y < start.y; y++) {
					extended = connectWire2(y, start, add, extended, overLapEdges, tempPoint);
				}
//				if (!overLapEdges.contains(findEdge(start.x, start.y, false))) { // if no tangent wire connected, find the overlapping edge
//					if (findEdge(start.x, start.y, false) != null) {
//						overLapEdges.add(findEdge(start.x, start.y, false));
//					}
//				}
				if (extended) { // if have new wire 
					extendWire2(overLapEdges, tempPoint, start);
				}
				analyzeEndPort(start.x, start.y, add);
			}
		}
	}

	public void extendWire(ArrayList<Edge> overLapEdges, Point tempPoint, Point end) { // for horizontal lines (wireRow)
		if (overLapEdges.isEmpty()) {
			Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(end.x, end.y)); // no overlapped edge, create edge from tempPoint to endPoint directly
			tempPoint.x = end.x;
			tempPoint.y = end.y;
			this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
			this.nodeLoc[end.x][end.y].applied = true;
			insertEdgeToCanvas(e);
		} else {
			Point tempEnd = end; // have overlapped edges, calculating new edge to cover old overlapped edges
			for (Edge e : overLapEdges) {
				if (tempPoint.x > e.start.x) {
					tempPoint.x = e.start.x;
					tempPoint.y = e.start.y;
				}
				if (tempEnd.x < e.end.x) {
					tempEnd = e.end;
				}
				this.removeEdge(e, false); //no need to erase on grid
			}
			Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(tempEnd.x, tempEnd.y));
			tempPoint.x = end.x;
			tempPoint.y = end.y;
			this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
			this.nodeLoc[end.x][end.y].applied = true;
			insertEdgeToCanvas(e);
		}
	}

	public void extendWire2(ArrayList<Edge> overLapEdges, Point tempPoint, Point end) { // for vertical lines (wireCol)
		if (overLapEdges.isEmpty()) {
			Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(end.x, end.y));
			tempPoint.x = end.x;
			tempPoint.y = end.y;
			this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
			this.nodeLoc[end.x][end.y].applied = true;
			insertEdgeToCanvas(e);
		} else {
			Point tempEnd = end;
			for (Edge e : overLapEdges) {
				if (tempPoint.y > e.start.y) {
					tempPoint.x = e.start.x;
					tempPoint.y = e.start.y;
				}
				if (tempEnd.y < e.end.y) {
					tempEnd = e.end;
				}
				this.removeEdge(e, false); //no need to erase on grid
			}
			Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(tempEnd.x, tempEnd.y));
			tempPoint.x = end.x;
			tempPoint.y = end.y;
			this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
			this.nodeLoc[end.x][end.y].applied = true;
			insertEdgeToCanvas(e);
		}
	}

	public boolean connectWire(int x, Point end, boolean add, boolean ex, ArrayList<Edge> overLapEdges, Point tempPoint) { // for horizontal lines (wireRow)
		boolean extended = ex;
		boolean thisExtended = false;
		if (this.wireRow[x][end.y] ^ add) { // if originally not applied and it will be applied then apply it,
			this.wireRow[x][end.y] = add; // if originally applied and add == false, then unApply it
			extended = add;
			thisExtended = add;
		} else if (!overLapEdges.contains(findEdge(x, end.y, true))) { // if no wire apply toggling, then find the overlapping edge
			if (findEdge(x, end.y, true) != null) {
				overLapEdges.add(findEdge(x, end.y, true));
			}
			if (analyzePort(x, end.y, add)) {
				overLapEdges.clear();
			}
		}
		if (add) { // if connected to other wire
			if ((analyzePort(x, end.y, add) && extended)) {
				if (overLapEdges.isEmpty()) {
					Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(x, end.y));
					tempPoint.x = x;
					tempPoint.y = end.y;
					this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
					this.nodeLoc[x][end.y].applied = true;
					insertEdgeToCanvas(e);
//					extended = false;
				} else {
					for (int i = 0; i < overLapEdges.size(); i++) {
						Edge e = overLapEdges.get(i);
						if (tempPoint.x > e.start.x) {
							tempPoint.x = e.start.x;
							tempPoint.y = e.start.y;
						}
						this.removeEdge(e, false); //no need to erase on grid
					}
					Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(x, end.y));
					tempPoint.x = x;
					tempPoint.y = end.y;
					this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
					this.nodeLoc[x][end.y].applied = true;
					insertEdgeToCanvas(e);
//					extended = false;
					overLapEdges.clear();
				}
			} else if (analyzePort(x, end.y, add)) {
				tempPoint.x = x;
				tempPoint.y = end.y;
			} else if (extended && findPort(x, end.y).getConnectionState()) { //handle wire from startPoint to a port which connected with 3 wires alr
				Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(x, end.y));
				tempPoint.x = x;
				tempPoint.y = end.y;
				this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
				this.nodeLoc[x][end.y].applied = true;
				insertEdgeToCanvas(e);
				extended = thisExtended;
//				System.out.println(tempPoint.x + "," + tempPoint.y);
			}
		}
		return extended;
	}

	public boolean connectWire2(int y, Point end, boolean add, boolean ex, ArrayList<Edge> overLapEdges, Point tempPoint) { // for vertical lines (wireCol)
		boolean extended = ex;
		boolean thisExtended = false;
		if (this.wireCol[end.x][y] ^ add) {
			this.wireCol[end.x][y] = add;
			extended = add;
			thisExtended = add;
		} else if (!overLapEdges.contains(findEdge(end.x, y, false))) {
			if (findEdge(end.x, y, false) != null) {
				overLapEdges.add(findEdge(end.x, y, false));
			}
			if (analyzePort(end.x, y, add)) {
				overLapEdges.clear();
			}
		}
		if (add) { // if connected to other wire
			if ((analyzePort(end.x, y, add) && extended)) {
				if (overLapEdges.isEmpty()) {
					Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(end.x, y));
					tempPoint.x = end.x;
					tempPoint.y = y;
					this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
					this.nodeLoc[end.x][y].applied = true;
					insertEdgeToCanvas(e);
//					extended = false;
				} else {
					for (int i = 0; i < overLapEdges.size(); i++) {
						Edge e = overLapEdges.get(i);
						if (tempPoint.x > e.start.x) {
							tempPoint.x = e.start.x;
							tempPoint.y = e.start.y;
						}
						this.removeEdge(e, false); //no need to erase on grid
					}
					Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(end.x, y));
					tempPoint.x = end.x;
					tempPoint.y = y;
					this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
					this.nodeLoc[end.x][y].applied = true;
					insertEdgeToCanvas(e);
//					extended = false;
					overLapEdges.clear();
				}
			} else if (analyzePort(end.x, y, add)) {
				tempPoint.x = end.x;
				tempPoint.y = y;
			} else if (extended && findPort(end.x, y).getConnectionState()) {
				Edge e = new Edge("wire", findPort(tempPoint.x, tempPoint.y), findPort(end.x, y));
				tempPoint.x = end.x;
				tempPoint.y = y;
				this.nodeLoc[tempPoint.x][tempPoint.y].applied = true;
				this.nodeLoc[end.x][y].applied = true;
				insertEdgeToCanvas(e);
				extended = thisExtended;
			}
		}
		return extended;
	}

	public void chopWire(int x, int y, boolean isHorizontal, boolean isChopping) {
		if (isChopping) {
			if (isHorizontal) {
				ArrayList<Edge> es = findEdges(x, y, true);
				if (wireRow[x][y] && wireRow[x - 1][y] && es.size() == 1) {
					Edge newEdge1 = new Edge("wire", es.get(0).startPort, findPort(x, y));
					Edge newEdge2 = new Edge("wire", findPort(x, y), es.get(0).endPort);
					this.removeEdge(es.get(0), false); //no need to erase on grid
					insertEdgeToCanvas(newEdge1);
					insertEdgeToCanvas(newEdge2);
				}
			} else {
				ArrayList<Edge> es = findEdges(x, y, false);
				if (wireCol[x][y] && wireCol[x][y - 1] && es.size() == 1) {
					Edge newEdge1 = new Edge("wire", es.get(0).startPort, findPort(x, y));
					Edge newEdge2 = new Edge("wire", findPort(x, y), es.get(0).endPort);
					this.removeEdge(es.get(0), false); //no need to erase on grid
					insertEdgeToCanvas(newEdge1);
					insertEdgeToCanvas(newEdge2);
				}
			}
		} else {

		}
	}

	public Edge findEdge(int x, int y, boolean isHorizontal) {
		for (Edge e : canvas.module.edges) {
			if (e.isHorizontal() == isHorizontal) {
				if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
						|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
					return e;
				}
			}
		}
		return null;
	}

	public ArrayList<Edge> findEdges(int x, int y, boolean isHorizontal) {
		ArrayList<Edge> es = new ArrayList();
		for (Edge e : canvas.module.edges) {
			if (e.isHorizontal() == isHorizontal) {
				if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
						|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
					if (!es.contains(e)) {
						es.add(e);
					}
				}
			}
		}
		return es;
	}

	public ArrayList<Edge> findEdges(int x, int y) {
		ArrayList<Edge> es = new ArrayList();
		for (Edge e : canvas.module.edges) {
			if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
					|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
				if (!es.contains(e)) {
					es.add(e);
				}
			}
		}
		return es;
	}

	public Edge findEdge(int x, int y) {
		for (Edge e : canvas.module.edges) {
			if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
					|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
				return e;
			}
		}
		return null;
	}

	public Port findPort(int x, int y) {
		if (x >= 0 && y >= 0) {
			return ports[x][y];
		}
		return null;
	}

	public void autoEdgeSync(Port p1, Port p2) {
		reconnectEdge(canvas.module);
		if (p1.vertexPort == null || p2.vertexPort == null) {
			return;
		}
		if (p1.parent.portConnectedTo(p1).contains(p2)) {
			return;
		}
		System.out.println("loading Connectable Port");
		ArrayList<Port> portList1 = new ArrayList();
		portList1.add(p1);
		ArrayList<Port> portList2 = new ArrayList();
		portList2.add(p2);
		for (Edge e : p1.vertexPort.edges) {   // searching connectable port from port1 for auto edge
			if (!portList1.contains(e.startPort)) {
				if (portList1.add(e.startPort));
			}

			if (!portList1.contains(e.endPort)) {
				if (portList1.add(e.endPort));
			}
			ArrayList<Port> ps = scanConnectablePortOnEdge((Edge) e);
			for (int i = 0; i < ps.size(); i++) {
				if (!portList1.contains(ps.get(i))) {
					portList1.add(ps.get(i));
				}
			}
			if (e instanceof Edge) {
				for (Edge e1 : ((Edge) e).findConnectedEdges()) { // search all connectable port
					if (!portList1.contains(e1.startPort)) {
						if (portList1.add(e1.startPort));
					}
					if (!portList1.contains(e1.endPort)) {
						if (portList1.add(e1.endPort));
					}
					ps = scanConnectablePortOnEdge((Edge) e1);
					for (int i = 0; i < ps.size(); i++) {
						if (!portList1.contains(ps.get(i))) {
							portList1.add(ps.get(i));
						}
					}
				}
			}
		}
		for (Edge e : p2.vertexPort.edges) {   // searching connectable port from port2 for auto edge 
			if (!portList2.contains(e.startPort)) {
				if (portList2.add(e.startPort));
			}
			if (!portList2.contains(e.endPort)) {
				if (portList2.add(e.endPort));
			}
			ArrayList<Port> ps = scanConnectablePortOnEdge((Edge) e);
			for (int i = 0; i < ps.size(); i++) {
				if (!portList2.contains(ps.get(i))) {
					portList2.add(ps.get(i));
				}
			}
			if (e instanceof Edge) {
				for (Edge e1 : ((Edge) e).findConnectedEdges()) {
					if (!portList2.contains(e1.startPort)) {
						if (portList2.add(e1.startPort));
					}
					if (!portList2.contains(e1.endPort)) {
						if (portList2.add(e1.endPort));
					}
					ps = scanConnectablePortOnEdge((Edge) e1);
					for (int i = 0; i < ps.size(); i++) {
						if (!portList2.contains(ps.get(i))) {
							portList2.add(ps.get(i));
						}
					}
				}
			}
		}//p2.getAbsolutionX(),p2.getAbsolutionY()getDistanceAutoEdge
		System.out.println("sorting connectable port");
		portList1.sort(Comparator.comparing(p -> p.getDistanceAutoEdge(p2.getAbsolutionX(), p2.getAbsolutionY())));
		Port nearestPort = portList1.get(0);
		portList2.sort(Comparator.comparing(p -> p.getDistanceAutoEdge(nearestPort.getAbsolutionX(), nearestPort.getAbsolutionY())));
		for (int i = 0; i < portList1.size(); i++) { // loop found ports and find the path
			for (int j = 0; j < portList2.size(); j++) {
				System.out.println("finding path");
				hk.quantr.routingalgo.lee.Path p = findPath(new hk.quantr.routingalgo.lee.Point(portList1.get(i).getAbsolutionX(), portList1.get(i).getAbsolutionY()),
						new hk.quantr.routingalgo.lee.Point(portList2.get(j).getAbsolutionX(), portList2.get(j).getAbsolutionY()), false);
				if (p != null) {
//					System.out.println(p);
					if (p.corners.isEmpty()) {
						Edge ghost = new Edge("ghost", findPort(p.end.x, p.end.y), findPort(p.start.x, p.start.y));
						System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
						addEdge(ghost);
					} else {
						Edge ghost = new Edge("ghost", findPort(p.start.x, p.start.y), findPort(p.corners.get(0).x, p.corners.get(0).y));
						System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
						addEdge(ghost);
						for (int c = 0; c < p.corners.size() - 1; c++) {
							ghost = new Edge("ghost", findPort(p.corners.get(c).x, p.corners.get(c).y), findPort(p.corners.get(c + 1).x, p.corners.get(c + 1).y));
							System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
							addEdge(ghost);
						}
						ghost = new Edge("ghost", findPort(p.corners.get(p.corners.size() - 1).x, p.corners.get(p.corners.size() - 1).y), findPort(p.end.x, p.end.y));
						System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
						addEdge(ghost);
					}
					return;
				}
			}
		}
	}

	public void autoEdge(Port p1, Port p2) {
//		if (!(p1 instanceof Input || p1 instanceof Output)) {
//			if (!(p2 instanceof Input || p2 instanceof Output)) { // if p1 or p2 is not port from vertex, return
//				return;
//			}
//		}
		reconnectEdge(canvas.module);
		if (p1.vertexPort == null || p2.vertexPort == null) {
			return;
		}
		shouldRun = false;
		new Thread(new Runnable() {
			@Override
			public void run() {
				ArrayList<Port> portList1 = new ArrayList();
				portList1.add(p1);
				ArrayList<Port> portList2 = new ArrayList();
				portList2.add(p2);
				for (Edge e : p1.vertexPort.edges) {   // searching connectable port from port1 for auto edge
					if (!portList1.contains(e.startPort)) {
						if (portList1.add(e.startPort));
					}

					if (!portList1.contains(e.endPort)) {
						if (portList1.add(e.endPort));
					}
					ArrayList<Port> ps = scanConnectablePortOnEdge((Edge) e);
					for (int i = 0; i < ps.size(); i++) {
						if (!portList1.contains(ps.get(i))) {
							portList1.add(ps.get(i));
						}
					}
					if (e instanceof Edge) {
						for (Edge e1 : ((Edge) e).findConnectedEdges()) { // search all connectable port
//							System.out.println(e1.toString());
							if (!portList1.contains(e1.startPort)) {
								if (portList1.add(e1.startPort));
							}
							if (!portList1.contains(e1.endPort)) {
								if (portList1.add(e1.endPort));
							}
							ps = scanConnectablePortOnEdge((Edge) e1);
							for (int i = 0; i < ps.size(); i++) {
								if (!portList1.contains(ps.get(i))) {
									portList1.add(ps.get(i));
								}
							}
						}
					}
				}
				for (Edge e : p2.vertexPort.edges) {   // searching connectable port from port2 for auto edge 
					if (!portList2.contains(e.startPort)) {
						if (portList2.add(e.startPort));
					}
					if (!portList2.contains(e.endPort)) {
						if (portList2.add(e.endPort));
					}
					ArrayList<Port> ps = scanConnectablePortOnEdge((Edge) e);
					for (int i = 0; i < ps.size(); i++) {
						if (!portList2.contains(ps.get(i))) {
							portList2.add(ps.get(i));
						}
					}
					if (e instanceof Edge) {
						for (Edge e1 : ((Edge) e).findConnectedEdges()) {
//							System.out.println(e1.toString());
							if (!portList2.contains(e1.startPort)) {
								if (portList2.add(e1.startPort));
							}
							if (!portList2.contains(e1.endPort)) {
								if (portList2.add(e1.endPort));
							}
							ps = scanConnectablePortOnEdge((Edge) e1);
							for (int i = 0; i < ps.size(); i++) {
								if (!portList2.contains(ps.get(i))) {
									portList2.add(ps.get(i));
								}
							}
						}
					}
				}//p2.getAbsolutionX(),p2.getAbsolutionY()getDistanceAutoEdge
				portList1.sort(Comparator.comparing(p -> p.getDistanceAutoEdge(p2.getAbsolutionX(), p2.getAbsolutionY())));
				Port nearestPort = portList1.get(0);
				portList2.sort(Comparator.comparing(p -> p.getDistanceAutoEdge(nearestPort.getAbsolutionX(), nearestPort.getAbsolutionY())));
//				System.out.println(portList1);
//				System.out.println(portList2);
				for (int i = 0; i < portList1.size(); i++) { // loop found ports and find the path
					for (int j = 0; j < portList2.size(); j++) {
						hk.quantr.routingalgo.lee.Path p = findPath(new hk.quantr.routingalgo.lee.Point(portList1.get(i).getAbsolutionX(), portList1.get(i).getAbsolutionY()),
								new hk.quantr.routingalgo.lee.Point(portList2.get(j).getAbsolutionX(), portList2.get(j).getAbsolutionY()), false);
						if (p != null) {
//							System.out.println(p);
							if (p.corners.isEmpty()) {
								Edge ghost = new Edge("ghost", findPort(p.end.x, p.end.y), findPort(p.start.x, p.start.y));
//								System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
								addEdge(ghost);
							} else {
								Edge ghost = new Edge("ghost", findPort(p.start.x, p.start.y), findPort(p.corners.get(0).x, p.corners.get(0).y));
//								System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
								addEdge(ghost);
								for (int c = 0; c < p.corners.size() - 1; c++) {
									ghost = new Edge("ghost", findPort(p.corners.get(c).x, p.corners.get(c).y), findPort(p.corners.get(c + 1).x, p.corners.get(c + 1).y));
//									System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
									addEdge(ghost);
								}
								ghost = new Edge("ghost", findPort(p.corners.get(p.corners.size() - 1).x, p.corners.get(p.corners.size() - 1).y), findPort(p.end.x, p.end.y));
//								System.out.println(portList1.get(i).name + "  " + portList2.get(j).name + "  " + ghost);
								addEdge(ghost);
							}
							shouldRun = true;
							return;
						}
					}
				}
				shouldRun = true;
			}
		}).start();
	}

	public boolean circuitRouting() {

		return false;
	}

//	public hk.quantr.routingalgo.lee.Point[] nearestPoint(Port p1, Port p2) {
	public ArrayList<Port>[] nearestPoint(Port p1, Port p2) {
		ArrayList<Port> portList1 = new ArrayList();
		portList1.add(p1);
		ArrayList<Port> portList2 = new ArrayList();
		portList2.add(p2);
		for (Edge e : p1.vertexPort.edges) {   // searching connectable port from port1 for auto edge
			if (!portList1.contains(e.startPort)) {
				if (portList1.add(e.startPort));
			}

			if (!portList1.contains(e.endPort)) {
				if (portList1.add(e.endPort));
			}
			ArrayList<Port> ps = scanConnectablePortOnEdge((Edge) e);
			for (int i = 0; i < ps.size(); i++) {
				if (!portList1.contains(ps.get(i))) {
					portList1.add(ps.get(i));
				}
			}
			if (e instanceof Edge) {
				for (Edge e1 : ((Edge) e).findConnectedEdges()) { // search all connectable port
//							System.out.println(e1.toString());
					if (!portList1.contains(e1.startPort)) {
						if (portList1.add(e1.startPort));
					}
					if (!portList1.contains(e1.endPort)) {
						if (portList1.add(e1.endPort));
					}
					ps = scanConnectablePortOnEdge((Edge) e1);
					for (int i = 0; i < ps.size(); i++) {
						if (!portList1.contains(ps.get(i))) {
							portList1.add(ps.get(i));
						}
					}
				}
			}
		}
		for (Edge e : p2.vertexPort.edges) {   // searching connectable port from port2 for auto edge 
			if (!portList2.contains(e.startPort)) {
				if (portList2.add(e.startPort));
			}
			if (!portList2.contains(e.endPort)) {
				if (portList2.add(e.endPort));
			}
			ArrayList<Port> ps = scanConnectablePortOnEdge((Edge) e);
			for (int i = 0; i < ps.size(); i++) {
				if (!portList2.contains(ps.get(i))) {
					portList2.add(ps.get(i));
				}
			}
			if (e instanceof Edge) {
				for (Edge e1 : ((Edge) e).findConnectedEdges()) {
//							System.out.println(e1.toString());
					if (!portList2.contains(e1.startPort)) {
						if (portList2.add(e1.startPort));
					}
					if (!portList2.contains(e1.endPort)) {
						if (portList2.add(e1.endPort));
					}
					ps = scanConnectablePortOnEdge((Edge) e1);
					for (int i = 0; i < ps.size(); i++) {
						if (!portList2.contains(ps.get(i))) {
							portList2.add(ps.get(i));
						}
					}
				}
			}
		}//p2.getAbsolutionX(),p2.getAbsolutionY()getDistanceAutoEdge
		portList1.sort(Comparator.comparing(p -> p.getDistanceAutoEdge(p2.getAbsolutionX(), p2.getAbsolutionY())));
		Port nearestPort = portList1.get(0);
		portList2.sort(Comparator.comparing(p -> p.getDistanceAutoEdge(nearestPort.getAbsolutionX(), nearestPort.getAbsolutionY())));
		return new ArrayList[]{portList1, portList2};
//		return new hk.quantr.routingalgo.lee.Point[]{new hk.quantr.routingalgo.lee.Point(nearestPort.getAbsolutionX(), nearestPort.getAbsolutionY()),
//			new hk.quantr.routingalgo.lee.Point(portList2.get(0).getAbsolutionX(), portList2.get(0).getAbsolutionY())};
	}

	public boolean circuitRouting(HashMap<Port, ArrayList<Port>> portMap) throws Exception {
		ArrayList<Edge> pathToConnect = new ArrayList();
//		pathToConnect.removeIf(filter);
		for (int i = 0; i < portMap.keySet().toArray().length; i++) {
			for (int j = 0; j < portMap.get(portMap.keySet().toArray()[i]).size(); j++) {
				Port p1 = (Port) portMap.keySet().toArray()[i];
				Port p2 = (Port) portMap.get(portMap.keySet().toArray()[i]).get(j);
				Edge e = new Edge("auto", p1, p2);
				boolean add = true;
				for (int k = 0; k < pathToConnect.size(); k++) {
					if (pathToConnect.get(k).equals(e)) {
						add = false;
						break;
					}
				}
				if (add) {
					pathToConnect.add(e);
				}
			}
		} // filter the repeated port map
//		System.out.println(pathToConnect.size());
//		for (int k = 0; k < pathToConnect.size(); k++) {
//			System.out.println(k + ": " + pathToConnect.get(k).startPort + " ::: " + pathToConnect.get(k).endPort);
//		}

		HashMap<Edge, ArrayList<Path>> pathsMap = new HashMap();
		boolean routingCompleted = false;
		if (!pathToConnect.isEmpty()) {   // find paths of first pair of ports
//			ArrayList<Port>[] portLists = nearestPoint(pathToConnect.get(0).startPort, pathToConnect.get(0).endPort);
//			hk.quantr.routingalgo.lee.Point point1 = nearestPoint(pathToConnect.get(0).startPort, pathToConnect.get(0).endPort)[0];
//			hk.quantr.routingalgo.lee.Point point2 = nearestPoint(pathToConnect.get(0).startPort, pathToConnect.get(0).endPort)[1];
//			hk.quantr.routingalgo.lee.Point point1 = new hk.quantr.routingalgo.lee.Point(portLists[0].get(0).getAbsolutionX(), portLists[0].get(0).getAbsolutionY());
//			hk.quantr.routingalgo.lee.Point point2 = new hk.quantr.routingalgo.lee.Point(portLists[1].get(0).getAbsolutionX(), portLists[1].get(0).getAbsolutionY());
//			ArrayList paths = this.findPaths(point1, point2);
//			pathsMap.put(pathToConnect.get(0), paths);
//			for (int i = 0; i < paths.size(); i++) {
//
//			}

			ArrayList<Path> appliedPath = new ArrayList();
			routingCompleted = loopPath(pathToConnect, 0, appliedPath);
			this.recalculateLines(this.canvas.module);
//			System.out.println(appliedPath.size());
			for (int i = 0; i < appliedPath.size(); i++) {
				Path p = appliedPath.get(i);
				if (p != null) {
//							System.out.println(p);
					if (p.corners.isEmpty()) {
						Edge ghost = new Edge("ghost", findPort(p.end.x, p.end.y), findPort(p.start.x, p.start.y));
						addEdge(ghost);
					} else {
						Edge ghost = new Edge("ghost", findPort(p.start.x, p.start.y), findPort(p.corners.get(0).x, p.corners.get(0).y));
						addEdge(ghost);
						for (int c = 0; c < p.corners.size() - 1; c++) {
							ghost = new Edge("ghost", findPort(p.corners.get(c).x, p.corners.get(c).y), findPort(p.corners.get(c + 1).x, p.corners.get(c + 1).y));
							addEdge(ghost);
						}
						ghost = new Edge("ghost", findPort(p.corners.get(p.corners.size() - 1).x, p.corners.get(p.corners.size() - 1).y), findPort(p.end.x, p.end.y));
						addEdge(ghost);
					}
//					shouldRun = true;
				}
			}
		}

//		for (int i = 1; i < pathToConnect.size(); i++) {
//			hk.quantr.routingalgo.lee.Point point1 = nearestPoint(pathToConnect.get(i).startPort, pathToConnect.get(i).endPort)[0];
//			hk.quantr.routingalgo.lee.Point point2 = nearestPoint(pathToConnect.get(i).startPort, pathToConnect.get(i).endPort)[1];
//			ArrayList paths = this.findPaths(point1, point2);
//			if (paths != null) {
//				pathsMap.put(pathToConnect.get(i), paths);
//			} else {
//
//			}
//		}
		return routingCompleted;
	}

	public boolean loopPath(ArrayList<Edge> pathToConnect, int i, ArrayList<Path> appliedPath) throws Exception {
//		System.out.println("looping " + i + " : " + pathToConnect.get(i).startPort.name + " ::: " + pathToConnect.get(i).endPort.name + '\n');
		this.canvas.module.edges.clear();
		this.reconnectEdge(this.canvas.module);
//		for (int k = 0; k < appliedPath.size(); k++) {
//			this.applyPath(appliedPath.get(k));
//		}

		for (int k = 0; k < appliedPath.size(); k++) { // reload the previous choosen path
			Path p = appliedPath.get(k);
			if (p != null) {
				applyGhostPath(p);
			}
		}
		this.reconnectEdge(this.canvas.module);
		// done reload

		if (pathToConnect.get(i).startPort.parent.portConnectedTo(pathToConnect.get(i).startPort).contains(pathToConnect.get(i).endPort)) {
			// the path is duplicated
			if (i != pathToConnect.size() - 1) {
				return loopPath(pathToConnect, i + 1, appliedPath);
			}
			return true;
		}

		// find nearest points to connect two port to reduce cost. 
		ArrayList<Port>[] portLists = nearestPoint(pathToConnect.get(i).startPort, pathToConnect.get(i).endPort);

		// find paths in this loop
		ArrayList<Path> paths = null;
		for (int z = 0; z < portLists[0].size(); z++) {
			for (int a = 0; a < portLists[1].size(); a++) {
				hk.quantr.routingalgo.lee.Point point1 = new hk.quantr.routingalgo.lee.Point(portLists[0].get(z).getAbsolutionX(), portLists[0].get(z).getAbsolutionY());
				hk.quantr.routingalgo.lee.Point point2 = new hk.quantr.routingalgo.lee.Point(portLists[1].get(a).getAbsolutionX(), portLists[1].get(a).getAbsolutionY());
				this.reconnectEdge(this.canvas.module);
				this.portProtection(portLists[0].get(z), portLists[1].get(a));
				paths = this.findPaths(point1, point2);
				if (paths != null) { // if path is found then break
					break;
				}
				// otherwise keep looping
			}
			if (paths != null) {// if path is found then break
				break;
			}
			// otherwise keep looping
		}

		if (paths == null) { // if no path is found, return to previous loopPath and keep looping
//			System.out.println("return since blocked " + i);
//			return false;
			throw new Exception(); // if exception is thrown, there is a bug
		}
//		ArrayList<Path> paths = this.findPaths(point1, point2);
		ArrayList<Path> tempPaths = null;
		boolean notBlocking = true;
		int c = 0;
		if (i < pathToConnect.size() - 1) {
			for (Path p : paths) {

				//load new path 
				appliedPath.add(p);
				this.canvas.module.edges.clear();
				this.reconnectEdge(this.canvas.module);
				for (int k = 0; k < appliedPath.size(); k++) { // reload the previous choosen path
					Path temp = appliedPath.get(k);
					if (temp != null) {
						applyGhostPath(temp);
					}
				}
				this.reconnectEdge(this.canvas.module);
				//End load new path

				// checking if new path blocked any port
				for (c = i + 1; c < pathToConnect.size(); c++) {
					tempPaths = null;
					if (pathToConnect.get(c).startPort.parent.portConnectedTo(pathToConnect.get(c).startPort).contains(pathToConnect.get(c).endPort)) {
						// the path is duplicated
//						tempPaths = new ArrayList();
						continue;
					}
					ArrayList<Port>[] tempPortLists = nearestPoint(pathToConnect.get(c).startPort, pathToConnect.get(c).endPort);
					for (int z = 0; z < tempPortLists[0].size(); z++) {
						for (int a = 0; a < tempPortLists[1].size(); a++) {
							hk.quantr.routingalgo.lee.Point point1 = new hk.quantr.routingalgo.lee.Point(tempPortLists[0].get(z).getAbsolutionX(), tempPortLists[0].get(z).getAbsolutionY());
							hk.quantr.routingalgo.lee.Point point2 = new hk.quantr.routingalgo.lee.Point(tempPortLists[1].get(a).getAbsolutionX(), tempPortLists[1].get(a).getAbsolutionY());
//							tempPaths = this.findPaths(point1, point2);
							notBlocking = isReachable(point1, point2);
							if (notBlocking) {
								break;
							}
						}
						if (notBlocking) {
							break;
						}
					}
					if (!notBlocking) {
						appliedPath.remove(p);
						break;
					}
				}

				// not blocking other port
				if (notBlocking) {
					appliedPath.remove(p);
					break;
				} else {
					appliedPath.remove(p);
				}
			}
			if (!notBlocking) {
//				System.out.println(pathToConnect.get(c).startPort + " ::: " + pathToConnect.get(c).endPort);
//				System.out.println(pathToConnect.get(c).startPort.parent.portConnectedTo(pathToConnect.get(c).startPort));
//				System.out.println(pathToConnect.get(c).startPort.parent.portConnectedTo(pathToConnect.get(c).startPort).contains(pathToConnect.get(c - 1).endPort));
//				System.out.println("return since blocked " + c);
//				System.out.println(paths);
//				throw new Exception();
				return false;
			}
		}

		boolean finished = false;
//		if (paths == null) {
//			System.out.println(paths);
//			return false;
//		} else {
//		System.out.println("size" + paths.size());
		for (int k = 0; k < paths.size(); k++) {
			if (i != pathToConnect.size() - 1) {
				appliedPath.add(paths.get(k));
				finished = loopPath(pathToConnect, i + 1, appliedPath);
//				System.out.println(k + "/" + paths.size());
				if (finished) {
					return true;
				} else {
					appliedPath.remove(paths.get(k));
				}
			} else {
				appliedPath.add(paths.get(k));
				return true;
			}
		}
//		}
		return finished;
	}

	public ArrayList<Port> scanConnectablePortOnEdge(Edge e) {
		ArrayList<Port> connectablePorts = new ArrayList();
		if (e.isHorizontal()) {
			for (int i = Math.max(e.start.x, e.end.x) - Math.abs(e.start.x - e.end.x); i < Math.max(e.start.x, e.end.x); i++) {
				if (this.findPort(i, e.start.y) != null) {
					if (this.findEdges(i, e.start.y, false).isEmpty()) {
						if (!connectablePorts.contains(this.findPort(i, e.start.y))) {
							connectablePorts.add(this.findPort(i, e.start.y));
						}
					}
				}
			}
		} else {
			for (int i = Math.max(e.start.y, e.end.y) - Math.abs(e.start.y - e.end.y); i < Math.max(e.start.y, e.end.y); i++) {
				if (this.findPort(e.start.x, i) != null) {
					if (this.findEdges(e.start.x, i, true).isEmpty()) {
						if (!connectablePorts.contains(this.findPort(e.start.x, i))) {
							connectablePorts.add(this.findPort(e.start.x, i));
						}
					}
				}
			}
		}
//		System.out.println(connectablePorts);
		return connectablePorts;
	}

	public void applyGhostPath(Path p) {
		if (p.corners.isEmpty()) {
			Edge ghost = new Edge("ghost", findPort(p.end.x, p.end.y), findPort(p.start.x, p.start.y));
			addEdge(ghost);
		} else {
			Edge ghost = new Edge("ghost", findPort(p.start.x, p.start.y), findPort(p.corners.get(0).x, p.corners.get(0).y));
			addEdge(ghost);
			for (int c = 0; c < p.corners.size() - 1; c++) {
				ghost = new Edge("ghost", findPort(p.corners.get(c).x, p.corners.get(c).y), findPort(p.corners.get(c + 1).x, p.corners.get(c + 1).y));
				addEdge(ghost);
			}
			ghost = new Edge("ghost", findPort(p.corners.get(p.corners.size() - 1).x, p.corners.get(p.corners.size() - 1).y), findPort(p.end.x, p.end.y));
			addEdge(ghost);
		}
	}
}
