/*
 * Copyright 2023 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.algo.lee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class LeeAlgorithm {

	public int matrix[][];
	public boolean matrixVisited[][];
	private ArrayList<Node> nodeList = new ArrayList<Node>();
	private final int MAXITERATIONS = 1000000;
	public static final int OBSTACLE = -1;

	public LeeAlgorithm(int matrixWidth, int matrixHeight) {
		matrix = new int[matrixWidth][matrixHeight];
		matrixVisited = new boolean[matrixWidth][matrixHeight];
	}

	public ArrayList<Node> findPath(Node start, Node goal) {
		if (nodeList.isEmpty()) {
			nodeList.add(start);
			matrixVisited[start.getX()][start.getY()] = true;
		}

		for (int i = 1; i < MAXITERATIONS; i++) {
			nodeList = markNeighbors(nodeList, i);

			if (matrix[goal.getX()][goal.getY()] != 0) {
				System.out.println("Path exists");
				break;
			}

			if (i == MAXITERATIONS - 1) {
				System.out.println("No Path exists");
				return null;
			}
		}

		ArrayList<Node> pathList = backtraceFromGoal(goal, start);
		return pathList;
	}

	private ArrayList<Node> markNeighbors(ArrayList<Node> neighborList, int iteration) {
		ArrayList<Node> neighbors = new ArrayList<Node>();

		try {
			for (Node node : neighborList) {
				if (node.getY() + 1 < matrix.length && matrixVisited[node.getX()][node.getY() + 1] == false) {
					Node node1 = new Node(node.getX(), node.getY() + 1);
					neighbors.add(node1);
					matrix[node.getX()][node.getY() + 1] = iteration;
					matrixVisited[node.getX()][node.getY() + 1] = true;
				}

				if (node.getY() >= 1 && matrixVisited[node.getX()][node.getY() - 1] == false) {
					Node node2 = new Node(node.getX(), node.getY() - 1);
					neighbors.add(node2);
					matrix[node.getX()][node.getY() - 1] = iteration;
					matrixVisited[node.getX()][node.getY() - 1] = true;
				}

				if (node.getX() + 1 < matrix.length && matrixVisited[node.getX() + 1][node.getY()] == false) {
					Node node3 = new Node(node.getX() + 1, node.getY());
					neighbors.add(node3);
					matrix[node.getX() + 1][node.getY()] = iteration;
					matrixVisited[node.getX() + 1][node.getY()] = true;
				}

				if (node.getX() >= 1 && matrixVisited[node.getX() - 1][node.getY()] == false) {
					Node node4 = new Node(node.getX() - 1, node.getY());
					neighbors.add(node4);
					matrix[node.getX() - 1][node.getY()] = iteration;
					matrixVisited[node.getX() - 1][node.getY()] = true;
				}
			}
		} catch (Exception ex) {

		}
		return neighbors;
	}

	/*
 Second step

  from goal Node go to next node that has a lower mark than the current node
  add this node to path until start Node is reached
	 */
	private ArrayList<Node> backtraceFromGoal(Node fromGoal, Node toStart) {

		ArrayList<Node> pathList = new ArrayList<Node>();

		pathList.add(fromGoal);
		Node currentNode = null;

		while (!pathList.get(pathList.size() - 1).equals(toStart)) {
			currentNode = pathList.get(pathList.size() - 1);
			Node n = getNeighbor(currentNode);
			if (n == null) {
				break;
			}
			pathList.add(n);
			n = currentNode;
		}
		return pathList;
	}

	/*
  get Neighbor of node with smallest matrix value, todo shuffle
	 */
	private Node getNeighbor(Node node) {

		ArrayList<Node> possibleNeighbors = new ArrayList<Node>();

		if (node.getY() + 1 < matrix.length && matrixVisited[node.getX()][node.getY() + 1] == true
				&& matrix[node.getX()][node.getY() + 1] != OBSTACLE) {

			Node n = new Node(node.getX(), node.getY() + 1, matrix[node.getX()][node.getY() + 1]);
			possibleNeighbors.add(n);
		}

		if (node.getY() >= 1 && matrixVisited[node.getX()][node.getY() - 1] == true
				&& matrix[node.getX()][node.getY() - 1] != OBSTACLE) {

			Node n = new Node(node.getX(), node.getY() - 1, matrix[node.getX()][node.getY() - 1]);
			possibleNeighbors.add(n);
		}

		if (node.getX() + 1 < matrix.length && matrixVisited[node.getX() + 1][node.getY()] == true
				&& matrix[node.getX() + 1][node.getY()] != OBSTACLE) {

			Node n = new Node(node.getX() + 1, node.getY(), matrix[node.getX() + 1][node.getY()]);
			possibleNeighbors.add(n);
		}

		if (node.getX() >= 1 && matrixVisited[node.getX() - 1][node.getY()] == true
				&& matrix[node.getX() - 1][node.getY()] != OBSTACLE) {

			Node n = new Node(node.getX() - 1, node.getY(), matrix[node.getX() - 1][node.getY()]);
			possibleNeighbors.add(n);
		}

		Collections.sort(possibleNeighbors, new Comparator<Node>() {
			@Override
			public int compare(Node first, Node second) {
				return first.getValue() - second.getValue();
			}
		});

		if (possibleNeighbors.size() > 0) {
			Node n = possibleNeighbors.remove(0);
			return n;
		} else {
			return null;
		}
	}

	private void printSolution(ArrayList<Node> output) {

		System.out.println("Shortest Path:");
		for (Node n : output) {
			int x = n.getX();
			int y = n.getY();
			System.out.println(n.toString());
			matrix[x][y] = 0;
		}

		System.out.println("");

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {

				if (matrix[i][j] != 0 && matrix[i][j] != OBSTACLE) {
					matrix[i][j] = 1;
				}

				if (matrixVisited[i][j] == false) {
					matrix[i][j] = 1;
				}

				if (matrix[i][j] == OBSTACLE) {
					System.out.print("O ");
				} else {

					System.out.print(matrix[i][j] + " ");
				}
			}
			System.out.println("");
		}
	}

}
