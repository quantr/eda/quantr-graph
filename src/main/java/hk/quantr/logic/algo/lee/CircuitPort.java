/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.algo.lee;

import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ken
 */
public class CircuitPort extends Port {

	private boolean connected = false;
	private Edge[] adjcentEdge;

	public CircuitPort(Vertex v, String name) {
		super(v, name);
		adjcentEdge = new Edge[4];
	}

	public CircuitPort(String name, int x, int y) {
		super(name);
		this.x = x;
		this.y = y;
		adjcentEdge = new Edge[4];
	}

	public void connect() {
		connected = true;
	}

	public void disconnect() {
		connected = false;
	}

	public void addEdge(Edge e, int i) {

	}

	public void paint(Graphics g, int gridSize) {
		g.setColor(Color.blue);
		int ovalSize = gridSize / 10 * 4 * (connected ? 4 : 1);
		g.fillOval(getAbsolutionX() * gridSize - ovalSize / 2, getAbsolutionY() * gridSize - ovalSize / 2, ovalSize, ovalSize);
	}
}
