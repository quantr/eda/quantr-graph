/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;
import javax.swing.JPanel;
import org.jdesktop.core.animation.timing.Animator;
import org.jdesktop.core.animation.timing.TimingSource;
import org.jdesktop.core.animation.timing.TimingTarget;
import org.jdesktop.core.animation.timing.TimingTargetAdapter;
import org.jdesktop.swing.animation.timing.sources.SwingTimerTimingSource;
import hk.quantr.logic.data.gate.OrGate;
import hk.quantr.logic.data.gate.AndGate;
import hk.quantr.logic.data.gate.NandGate;
import hk.quantr.logic.data.gate.NotGate;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.logic.data.gate.ModuleComponent;
import hk.quantr.logic.tree.ToolbarTreeNode;
import hk.quantr.javalib.CommonLib;
import static hk.quantr.logic.LogicGateDrawer.init;
import hk.quantr.logic.algo.lee.CircuitGrid;
import hk.quantr.logic.arduino.ArduinoHeltecWifiOled;
import hk.quantr.logic.arduino.ArduinoSerial;
import hk.quantr.logic.arduino.ArduinoSerialOled;
import hk.quantr.logic.arduino.ArduinoWifi;
import hk.quantr.logic.arduino.ArduinoWifiOled;
import hk.quantr.logic.data.arithmetic.Adder;
import hk.quantr.logic.data.arithmetic.Shifter;
import hk.quantr.logic.data.arithmetic.Divider;
import hk.quantr.logic.data.arithmetic.Multiplier;
import hk.quantr.logic.data.arithmetic.Negator;
import hk.quantr.logic.data.arithmetic.Subtractor;
import hk.quantr.logic.data.basic.BitExtender;
import hk.quantr.logic.data.basic.Button;
import hk.quantr.logic.data.basic.Clock;
import hk.quantr.logic.data.basic.Combiner;
import hk.quantr.logic.data.basic.Constant;
import hk.quantr.logic.data.basic.DipSwitch;
import hk.quantr.logic.data.basic.Led;
import hk.quantr.logic.data.basic.LedBar;
import hk.quantr.logic.data.basic.OutputPin;
import hk.quantr.logic.data.basic.Pin;
import hk.quantr.logic.data.basic.Probe;
import hk.quantr.logic.data.basic.RGBLed;
import hk.quantr.logic.data.basic.Random;
import hk.quantr.logic.data.basic.Splitter;
import hk.quantr.logic.data.basic.Switch;
import hk.quantr.logic.data.basic.Transistor;
import hk.quantr.logic.data.basic.Tunnel;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.data.flipflop.DFlipflop;
import hk.quantr.logic.data.flipflop.DLatch;
import hk.quantr.logic.data.flipflop.JKFlipflop;
import hk.quantr.logic.data.flipflop.Ram;
import hk.quantr.logic.data.flipflop.SRFlipflop;
import hk.quantr.logic.data.flipflop.TFlipflop;
import hk.quantr.logic.data.gate.Buffer;
import hk.quantr.logic.data.Data;
import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.EvenParity;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.NorGate;
import hk.quantr.logic.data.gate.OddParity;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.TriStateBuffer;
import hk.quantr.logic.data.gate.TriStateNotBuffer;
import hk.quantr.logic.data.gate.XnorGate;
import hk.quantr.logic.data.gate.XorGate;
import hk.quantr.logic.data.gate.Module;
import hk.quantr.logic.data.gate.Point;
import hk.quantr.logic.data.graphics.ImageComponent;
import hk.quantr.logic.data.output.EightSegmentDisplay;
import hk.quantr.logic.data.output.HexDisplay;
import hk.quantr.logic.data.output.TTY;
import hk.quantr.logic.data.plexer.BitSelector;
import hk.quantr.logic.data.plexer.Decoder;
import hk.quantr.logic.data.plexer.Demultiplexer;
import hk.quantr.logic.data.plexer.Encoder;
import hk.quantr.logic.data.plexer.Multiplexer;
import hk.quantr.logic.tree.ProjectTreeNode;
import hk.quantr.routingalgo.lee.VerifyGrid;
import java.awt.BasicStroke;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class QuantrGraphCanvas extends JPanel {

	public Module module = new Module("Main");
	private ArrayList<String> dataHistory = new ArrayList();
	private int dataIndex = 0;
	boolean showGrid;
	public int gridSize = 10;
	private int mouseMoveX;
	private int mouseMoveY;
//	private Vertex dragVertex;
//	private int dragVertexDeltaX;
//	private int dragVertexDeltaY;
	public boolean buttonState;
//	public boolean drawingWire;
//	public boolean autoRoutingWire;
	public int wireStartX = -1;
	public int wireStartY = -1;
	private Vertex autoStartVertex;
	private Vertex autoEndVertex;
	public Port startPort;
	public Port endPort;
	public Port manualStartPort;
	boolean wiring = false;
//	boolean reRoute = false;
	private CircuitGrid grid = new CircuitGrid(300, this);
	private CircuitGrid ghostGrid = new CircuitGrid(300, this);
	private VerifyGrid grid1 = new VerifyGrid(300);
	public ArrayList<Edge> ghostEdges = new ArrayList();
	private ArrayList<int[]> ghostPath = new ArrayList();
	private int mouseDragPathFindingSerialNum = 0;
	public int dragEdgeID = 1;
//	SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");
	public QuantrGraphPanel quantrGraphPanel;
//	private boolean shouldRun = true;
	public Vertex selectedVertex;
	public Set<Vertex> selectedVertices = new HashSet();
	public Set<Vertex> copiedVertices = new HashSet();
	public ArrayList<Edge> copiedEdges = new ArrayList();
	public int pastingID = -1;
	public Edge selectedEdge;
	public ArrayList<Edge> selectedEdges = new ArrayList();
	public WireDirection wireDirection;
	int dragStartX;
	int dragStartY;
//	boolean isRecorded = false;
	Animator animator;
	double fraction1;
	boolean isDraggingSelection = false;
	XStream serializer = new XStream();
	XStream deserializer = new XStream(new StaxDriver());
	boolean rightClickWiring = false;
	ImageIcon rmitLogo = new ImageIcon(getClass().getResource("/hk/quantr/logic/rmit/rmit_logo.png"));
	ImageIcon rmit_logo2560_2560 = new ImageIcon(getClass().getResource("/hk/quantr/logic/rmit/rmit_logo2560_2560.png"));
//	BufferedImage rmitLogoB = CommonLib.toBufferedImage(rmit_logo2560_2560.getImage());
//	BufferedImage rmitLogoSmall = CommonLib.toBufferedImage(rmitLogo.getImage());
	BufferedImage qfLogo = CommonLib.toBufferedImage(new ImageIcon(getClass().getResource("/hk/quantr/logic/qf/Quantr Foundation Logo 2.png")));
//	BufferedImage cityuEE = CommonLib.toBufferedImage(new ImageIcon(getClass().getResource("/hk/quantr/logic/compeition/cityuEE.png")));
//	BufferedImage fuck = CommonLib.toBufferedImage(new ImageIcon(getClass().getResource("/hk/quantr/logic/fuck.png")));
	boolean contToAdd = true;
	boolean cursorInCanvas = false;
	int minX = 2000;
	int maxX = 0;
	int minY = 2000;
	int maxY = 0;
	public BufferedImage image;
	private boolean pathFinding = false;
	final TimingTarget myTimingTarget = new TimingTargetAdapter() {
//		@Override
//		public void repeat(Animator source) {
//			System.out.println("repeat " + source);
//		}
		@Override
		public void timingEvent(Animator source, double fraction) {
			fraction1 = fraction;
			repaint();
//			System.out.println(source);
//			System.out.println(fraction);
		}
	};
	public boolean simulating;
	public int maxLevel = -1;
//	public ArrayList<ArrayList<Vertex>> levelledVertices = new ArrayList();
//	public ArrayList<Vertex> temp = new ArrayList();

	private void initXML() {
		serializer.autodetectAnnotations(true);
		deserializer.autodetectAnnotations(true);
		deserializer.addPermission(PrimitiveTypePermission.PRIMITIVES);
		deserializer.processAnnotations(new Class[]{Adder.class, Divider.class, Multiplier.class, Negator.class, Shifter.class, Subtractor.class, BitExtender.class, Button.class,
			Clock.class, Combiner.class, Constant.class, DipSwitch.class, Led.class, LedBar.class, Pin.class, Probe.class, RGBLed.class, Random.class,
			Splitter.class, Switch.class, Tunnel.class, VCD.class, DFlipflop.class, DLatch.class, JKFlipflop.class, Ram.class, SRFlipflop.class, TFlipflop.class, AndGate.class,
			Edge.class, Buffer.class, Data.class, Edge.class, EvenParity.class, Input.class, Module.class, NandGate.class, NorGate.class, NotGate.class,
			OddParity.class, OrGate.class, Output.class, Point.class, Port.class, TriStateBuffer.class, TriStateNotBuffer.class, Vertex.class, XnorGate.class, XorGate.class,
			EightSegmentDisplay.class, HexDisplay.class, TTY.class, BitSelector.class, Decoder.class, Demultiplexer.class, Encoder.class, Multiplexer.class, ImageComponent.class});
		deserializer.allowTypesByWildcard(new String[]{"hk.quantr.logic.**"});
		deserializer.ignoreUnknownElements();
	}

	public enum WireDirection {
		horizontal,
		vertical
	}

	public QuantrGraphCanvas() {

	}

	public QuantrGraphCanvas(QuantrGraphPanel quantrGraphPanel, Module loadModule) {
//		System.out.println("new Canvas");
		setSize(500, 500);
		this.quantrGraphPanel = quantrGraphPanel;
		this.loadModuleToCanvas(loadModule);
//		try {
//			rmitLogoSmall = CommonLib.resizeImage(rmitLogo, 100, 40, BufferedImage.TYPE_INT_ARGB);
//		} catch (IOException ex) {
//			Logger.getLogger(QuantrGraphCanvas.class.getName()).log(Level.SEVERE, null, ex);
//		}
		TimingSource ts = new SwingTimerTimingSource();
		ts.init();
		Animator.setDefaultTimingSource(ts);
		animator = new Animator.Builder().setDuration(5000, TimeUnit.MILLISECONDS).setRepeatCount(Animator.INFINITE).setRepeatBehavior(Animator.RepeatBehavior.LOOP).setStartDirection(Animator.Direction.FORWARD).addTarget(myTimingTarget).build();
		animator.start();
//		undo
		initXML();
		dataHistory.add(serializer.toXML(loadModule));

		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, InputEvent.SHIFT_MASK), "Delete");
		this.getActionMap().put("Delete", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel.clickDelete();
			}
		});
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "AskDelete");
		this.getActionMap().put("AskDelete", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				int option = JOptionPane.showConfirmDialog(null, "Confirm to delete?", "Delete", JOptionPane.YES_NO_OPTION);
				if (option != JOptionPane.YES_OPTION) {
					return;
				}
				quantrGraphPanel.clickDelete();
			}
		});
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK), "All");
		this.getActionMap().put("All", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				selectAll();
			}
		});

		addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				if (evt.isAltDown() && evt.getKeyCode() == KeyEvent.VK_UP) { //alt + up
					for (Vertex v : selectedVertices) {
						v.properties.put("Orientation", "north");
						v.updateProperty();
					}
				} else if (evt.isAltDown() && evt.getKeyCode() == KeyEvent.VK_RIGHT) { //alt + right
					for (Vertex v : selectedVertices) {
						v.properties.put("Orientation", "east");
						v.updateProperty();
					}
				} else if (evt.isAltDown() && evt.getKeyCode() == KeyEvent.VK_DOWN) { // alt + down
					for (Vertex v : selectedVertices) {
						v.properties.put("Orientation", "south");
						v.updateProperty();
					}
				} else if (evt.isAltDown() && evt.getKeyCode() == KeyEvent.VK_LEFT) { // alt + left
					for (Vertex v : selectedVertices) {
						v.properties.put("Orientation", "west");
						v.updateProperty();
					}
				}
			}

			public void keyReleased(java.awt.event.KeyEvent evt) {
				if (contToAdd && evt.getKeyCode() == KeyEvent.VK_SHIFT) {
					quantrGraphPanel.toolbarTree.setSelectionPath(null);
					contToAdd = false;
				}
			}
		});

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
//				System.out.println(grid.nodeLoc[evt.getX() / gridSize][evt.getY() / gridSize].applied);
				if (evt.getButton() == MouseEvent.BUTTON1 || evt.getButton() == MouseEvent.BUTTON2) {
					if (!simulating) {
						selectedVertex = findVertex(evt.getX(), evt.getY(), true);
						selectedEdge = findEdge(evt.getX(), evt.getY());
						if (!evt.isControlDown()) {
							clearVertexSelection();
						}
						if (selectedVertex != null) {
							selectedVertex.isSelected = true;
							selectedVertices.add(selectedVertex);
							if (evt.getButton() == MouseEvent.BUTTON2) {
								quantrGraphPanel.clickDelete();
							} else if (selectedVertex instanceof Pin) {
								((Pin) selectedVertex).clicked(evt.getX());
								selectedVertex.properties.put("Value", selectedVertex.outputs.get(0).value);
								quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
							} else if (selectedVertex instanceof ImageComponent && evt.getClickCount() == 2) {
								((ImageComponent) selectedVertex).upload();
							}
						} else if (selectedEdge != null) {
							selectedEdge.isSelected = true;
							selectedEdges.add(selectedEdge);
							if (evt.getButton() == MouseEvent.BUTTON2) {
								quantrGraphPanel.clickDelete();
							}
						} else {
							clearVertexSelection();
						}
						repaint();
					} else {
						if (!evt.isControlDown()) {
							clearVertexSelection();
						}
						selectedVertex = findVertex(evt.getX(), evt.getY(), true);
						if (selectedVertex != null) {
							if (selectedVertex instanceof Pin) {
								((Pin) selectedVertex).clicked(evt.getX());
								selectedVertex.properties.put("Value", selectedVertex.outputs.get(0).value);
								quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
							}
						}
						repaint();
					}

					selectedVertex = findVertex(evt.getX(), evt.getY(), true);
					if (selectedVertex != null) {
						if (selectedVertex instanceof Ram) {
							((Ram) selectedVertex).pageClicked(evt.getX(), evt.getY());
							quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
						} else if (selectedVertex instanceof DipSwitch) {
							((DipSwitch) selectedVertex).dipSwitchClicked(evt.getX(), evt.getY());
							quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
						} else if (selectedVertex instanceof Switch) {
							((Switch) selectedVertex).switchClicked(evt.getX(), evt.getY());
							quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
						}
					}
					repaint();
				}
				// undo				
//				recordAction();
			}

			@Override
			public void mousePressed(MouseEvent evt) {
				if (!simulating) {
					if (quantrGraphPanel.wireToggleButton.isSelected()
							|| (evt.getButton() == MouseEvent.BUTTON3 && !isDraggingSelection && !(findVertex(evt.getX(), evt.getY()) != null && findPort(evt.getX(), evt.getY()) == null))) {
						clearVertexSelection();
						if (wiring = !wiring) {
							rightClickWiring = true;
							wireStartX = getRoundedCoord(evt.getX(), true);
							wireStartY = getRoundedCoord(evt.getY(), true);
						}
					} else if (quantrGraphPanel.autoToggleButton.isSelected()) {
						clearVertexSelection();
						Port port = findPort(evt.getX(), evt.getY());
						if (port != null) {
							if (wiring = !wiring) {
								autoStartVertex = findVertex(evt.getX(), evt.getY());
								startPort = port;
							}
						} else {
							wiring = false;
						}
					} else {
						// select vertex and edge
						Vertex vertex = findVertex(evt.getX(), evt.getY(), true);
						ArrayList<Edge> edges = findEdges(evt.getX(), evt.getY());
						ArrayList<Edge> pressGhostEdges = findGhostEdges(evt.getX(), evt.getY());
						quantrGraphPanel.propertyTableModel.vertex = vertex;
//						outsidePanel.propertyCellEditor.vertex = v;
						quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
						if (vertex != null && !selectedVertices.contains(vertex)) {
							if (!evt.isControlDown()) {
								System.out.println("cleared");
								clearVertexSelection();
							}
						}
						if (!edges.isEmpty() && !selectedEdges.containsAll(edges)) {
							if (!evt.isControlDown()) {
								System.out.println("clearededge");
								clearVertexSelection();
							}
						}
						if (!pressGhostEdges.isEmpty() && !ghostEdges.containsAll(pressGhostEdges)) {
							if (!evt.isControlDown()) {
								System.out.println("wtf");
								clearVertexSelection();
							}
						}
//						System.out.println("v: " + vertex + " , edges: " + edges.isEmpty());
						if (vertex == null && edges.isEmpty() && pressGhostEdges.isEmpty()) {	// clear if not pressed on any vertex or edge
							System.out.println(pressGhostEdges);
							clearVertexSelection();
						} else if (vertex != null) {
							vertex.isSelected = true;
							selectedVertices.add(vertex);
							repaint();
						}

						for (Vertex _v : selectedVertices) {
							_v.initX = _v.x;
							_v.initY = _v.y;
						}
						dragStartX = getRoundedCoord(evt.getX(), true);
						dragStartY = getRoundedCoord(evt.getY(), true);
					}
				} else {
					Vertex v = findVertex(evt.getX(), evt.getY(), true);
					quantrGraphPanel.propertyTableModel.vertex = v;
//					outsidePanel.propertyCellEditor.vertex = v;
					quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
					if (!evt.isControlDown() || v == null) {
						clearVertexSelection();
					}
				}
				Vertex v = findVertex(evt.getX(), evt.getY(), true);
				if (v instanceof Button) {
					((Button) v).buttonPressed(evt.getX(), evt.getY());
				}
				quantrGraphPanel.propertyTableModel.vertex = v;
				quantrGraphPanel.propertyTableModel.fireTableStructureChanged();
//				undo
//				recordAction();
			}

			@Override
			public void mouseReleased(MouseEvent evt) {
//				System.out.println("module name: " + module.name);
				if (quantrGraphPanel.wireToggleButton.isSelected() || rightClickWiring) {
					if (!wiring) {
						rightClickWiring = false;
						mouseMoveX = getRoundedCoord(evt.getX(), true);
						mouseMoveY = getRoundedCoord(evt.getY(), true);
						if (wireDirection == WireDirection.horizontal) {
							Edge e1 = new Edge("edge " + getModule().edges.size(), grid.findPort(wireStartX / gridSize, wireStartY / gridSize), grid.findPort(mouseMoveX / gridSize, wireStartY / gridSize));
							Edge e2 = new Edge("edge " + getModule().edges.size(), grid.findPort(mouseMoveX / gridSize, wireStartY / gridSize), grid.findPort(mouseMoveX / gridSize, mouseMoveY / gridSize));
							if (e1.start.x > 0 && e1.end.x > 0 && e2.start.x > 0 && e2.end.x > 0 && e1.start.y > 0 && e1.end.y > 0 && e2.start.y > 0 && e2.end.y > 0) {
								grid.addEdge(e1);
								grid.addEdge(e2);
							}
						} else {
							Edge e1 = new Edge("edge " + getModule().edges.size(), grid.findPort(wireStartX / gridSize, wireStartY / gridSize), grid.findPort(wireStartX / gridSize, mouseMoveY / gridSize));
							Edge e2 = new Edge("edge " + getModule().edges.size(), grid.findPort(wireStartX / gridSize, mouseMoveY / gridSize), grid.findPort(mouseMoveX / gridSize, mouseMoveY / gridSize));
							if (e1.start.x > 0 && e1.end.x > 0 && e2.start.x > 0 && e2.end.x > 0 && e1.start.y > 0 && e1.end.y > 0 && e2.start.y > 0 && e2.end.y > 0) {
								grid.addEdge(e1);
								grid.addEdge(e2);
							}
//							Edge e = new Edge("edge " + new java.util.Random().nextInt(100), new ArrayList<Point>() {
//								{
//									add(new Point(wireStartX / gridSize, wireStartY / gridSize));
//									add(new Point(wireStartX / gridSize, mouseMoveY / gridSize));
//									add(new Point(wireStartX / gridSize, mouseMoveY / gridSize));
//									add(new Point(mouseMoveX / gridSize, mouseMoveY / gridSize));
//								}
//							},
//									null, null);
						}
						repaint();
						quantrGraphPanel.refreshTree();
					}
				} else if (quantrGraphPanel.autoToggleButton.isSelected()) {
					if (!wiring) {
						Port port = findPort(evt.getX(), evt.getY());
//						System.out.println("endPort:" + port);
						if (port != null && startPort != null) {
							autoEndVertex = findVertex(evt.getX(), evt.getY());
							endPort = port;
							System.out.printf("startVx:%d\nstartVy:%d\nstartPx:%d\nstartPy:%d\nendVx:%d\nendVy:%d\nendPx:%d\nendPy:%d\n\n",
									autoStartVertex.x, autoStartVertex.y, startPort.deltaX, startPort.deltaY, autoEndVertex.x, autoEndVertex.y, endPort.deltaX, endPort.deltaY);
							grid.autoEdge(startPort, endPort);
//							AutoEdge a = new AutoEdge("AutoEdge {" + startPort.name + "<->" + endPort.name + "}", autoStartVertex, startPort, autoEndVertex, endPort);
//							module.autoEdges.add(a);

//							a.startPort.edges.add(a);
//							a.endPort.edges.add(a);
//							quantrGraphPanel.refreshTree();
//							hk.quantr.routingalgo.lee.Path path = grid.findPath(new hk.quantr.routingalgo.lee.Point(a.startPort.deltaX + a.startVertex.x, a.startPort.deltaY + a.startVertex.y),
//									new hk.quantr.routingalgo.lee.Point(a.endPort.deltaX + a.endVertex.x, a.endPort.deltaY + a.endVertex.y), false);
//							if (path != null) {
//								Path p = new Path(path.start, path.end, path.corners);
//								p.name = "autoPath {" + a.startPort.name + "<->" + "}";
//								p.autoEdge = a;
//								autoPath.add(p);
//							}
//							a.startPort.parent.updateProperty();
//							a.endPort.parent.updateProperty();
						}
					}
				} else {
					for (Vertex v : getModule().vertices) {
						if (v instanceof Button) {
							((Button) v).buttonReleased();
						}
					}
					int t = 0;
					while (pathFinding && t < 100) {
						try {
							Thread.sleep(1);
						} catch (InterruptedException ex) {
							Logger.getLogger(QuantrGraphCanvas.class.getName()).log(Level.SEVERE, null, ex);
						}
						t++;
					}
					if (!selectedVertices.isEmpty() || !selectedEdges.isEmpty()) {
						if (!ghostEdges.isEmpty()) {
							ArrayList<Edge> cloneEdges = (ArrayList<Edge>) selectedEdges.clone();
							selectedEdges.clear();
							for (Edge e : ghostEdges) {
								grid.addEdge(e, e.getID());
							}
						}
					} else if (pastingID == dragEdgeID) {
						for (Edge e : ghostEdges) {
							grid.addEdge(e, e.getID());
						}
					}
					recalculateLines();
//					for (Edge e : module.edges) {
//						if (e.getID() == dragEdgeID) {
//							e.isSelected = true;
//							selectedEdges.add(e);
//						}
//					}
//					dragEdgeID++;
					ghostEdges.clear();
					repaint();
					if (!released(evt) && isDraggingSelection) {
						dragEdgeID++;
//						System.out.println("id++");
						int minX = getRoundedCoord(Math.min(dragStartX, mouseMoveX), false);
						int w = getRoundedCoord(Math.abs(mouseMoveX - dragStartX), false);
						int minY = getRoundedCoord(Math.min(dragStartY, mouseMoveY), false);
						int h = getRoundedCoord(Math.abs(mouseMoveY - dragStartY), false);
						for (Vertex v : getModule().vertices) {
							if (v.x + v.width >= minX && v.x <= minX + w && v.y + v.height >= minY && v.y <= minY + h) {
								v.isSelected = true;
								selectedVertices.add(v);
							}
						}
						Rectangle region = new Rectangle(minX, minY, w, h);
						for (Edge e : getModule().edges) {
							if (new java.awt.geom.Line2D.Double(e.start.x, e.start.y, e.end.x, e.end.y).intersects(region)) {
								e.isSelected = true;
								selectedEdges.add(e);
							}
						}
					}
					for (Edge e : module.edges) {
						if (e.getID() == dragEdgeID) {
							e.isSelected = true;
							selectedEdges.add(e);
						}
					}
//					dragVertex = null;
					quantrGraphPanel.refreshTree();
					isDraggingSelection = false;
					dragStartX = -1;
					dragStartX = -1;
				}
//				undo
				recordAction();
				if (IPHelper.isInHongKong()) {
					Desktop desk = Desktop.getDesktop();
					try {
						desk.browse(new URI(IPHelper.youtube[0]));
					} catch (URISyntaxException ex) {
						Logger.getLogger(QuantrGraphCanvas.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IOException ex) {
						Logger.getLogger(QuantrGraphCanvas.class.getName()).log(Level.SEVERE, null, ex);
					} catch (UnsupportedOperationException ex) {
						try {
							Runtime.getRuntime().exec(new String[]{"xdg-open", IPHelper.youtube[0]});
						} catch (IOException ex1) {
							Logger.getLogger(QuantrGraphCanvas.class.getName()).log(Level.SEVERE, null, ex1);
						}
					}
				}
			}//GEN-LAST:event_thisMouseReleased

			@Override
			public void mouseExited(MouseEvent evt) {
				cursorInCanvas = false;
			}

			@Override
			public void mouseEntered(MouseEvent evt) {
				cursorInCanvas = true;
			}

			private void autoPositionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoPositionButtonActionPerformed
//				System.out.println("called");
				ArrayList<ArrayList<Vertex>> autoVertices = new ArrayList();
				autoVertices = calculateLevel();
				int col = -1;
				int maxWidth = 0;
				int maxHeight = 0;
				int colWidth = 0;
				int colHeight = 4;
				if (autoVertices.size() > maxLevel + 1) {
					col++;
					colWidth += maxWidth + 2;
					maxWidth = 0;
					colHeight = 2;
					for (int i = 0; i < autoVertices.get(autoVertices.size() - 1).size(); i++) {
						if (colHeight >= 60) {
							col++;
							colWidth += maxWidth + 2;
							maxWidth = 0;
							colHeight = 2;
						}
						if (autoVertices.get(autoVertices.size() - 1).get(i).width > maxWidth) {
							maxWidth = autoVertices.get(autoVertices.size() - 1).get(i).width;
						}
						autoVertices.get(autoVertices.size() - 1).get(i).setLocation(colWidth, colHeight);
						colHeight += autoVertices.get(autoVertices.size() - 1).get(i).height + 2;
					}
				}
				for (int i = 0; i < maxLevel + 1; i++) {
					col++;
					colWidth += maxWidth + 4;
					maxWidth = 0;
					colHeight = 4;
					for (int j = 0; j < autoVertices.get(i).size(); j++) {
						if (colHeight >= 60) {
							col++;
							colWidth += maxWidth + 4;
							maxWidth = 0;
							colHeight = 4;
						}
						if (autoVertices.get(i).get(j).width > maxWidth) {
							maxWidth = autoVertices.get(i).get(j).width;
						}
						autoVertices.get(i).get(j).setLocation(colWidth, colHeight);

						System.out.println(autoVertices.get(i).get(j).name + " " + autoVertices.get(i).get(j).x + "--->>" + colWidth + ", " + colHeight);
						colHeight += autoVertices.get(i).get(j).height + 4;
					}

				}
				recalculateLines();
				repaint();

			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent evt) {
				if (quantrGraphPanel.wireToggleButton.isSelected() || rightClickWiring) {
					if (wiring) {
						if (wireDirection == null && (Math.abs(evt.getX() - wireStartX) >= gridSize || Math.abs(evt.getY() - wireStartY) >= gridSize)) {
							if (Math.abs(evt.getX() - wireStartX) >= Math.abs(evt.getY() - wireStartY)) {
								wireDirection = WireDirection.horizontal;
							} else {
								wireDirection = WireDirection.vertical;
							}
						} else if (wireDirection == WireDirection.horizontal) {
							if (wireStartX == mouseMoveX) {
								wireDirection = WireDirection.vertical;
							}
						} else if (wireDirection == WireDirection.vertical) {
							if (wireStartY == mouseMoveY) {
								wireDirection = WireDirection.horizontal;
							}
						}
					} else {
						wireDirection = null;
					}
				}
				mouseMoveX = getRoundedCoord(evt.getX(), true);
				mouseMoveY = getRoundedCoord(evt.getY(), true);
//				System.out.println(mouseMoveX + "," + mouseMoveY);
				repaint();
//				undo
//				recordAction();
			}

			@Override
			public void mouseDragged(MouseEvent evt) {
				mouseMoveX = getRoundedCoord(evt.getX(), true);
				mouseMoveY = getRoundedCoord(evt.getY(), true);
				if (!simulating) {
					Vertex vertex = findVertex(evt.getX(), evt.getY(), false);
//					if (!evt.isControlDown() || selectedVertex == null) {
//						clearVertexSelection();
//					}
//					if (selectedVertex != null && !selectedVertex.isSelected) {
////						clearVertexSelection();
//						selectedVertex.isSelected = true;
//						selectedVertices.add(selectedVertex);
////						repaint();
//					} else 
					if (selectedVertices.isEmpty()
							&& (quantrGraphPanel.toolbarTree.getSelectionPaths() != null ? getVertexToAdd(((ToolbarTreeNode) quantrGraphPanel.toolbarTree.getLastSelectedPathComponent()).data) == null : true)
							&& dragStartX >= 0 && dragStartY >= 0 && !rightClickWiring
							&& selectedEdges.isEmpty() && ghostEdges.isEmpty()) {
						isDraggingSelection = true;
					}
					if (quantrGraphPanel.wireToggleButton.isSelected()) {
						if (wiring) {
							if (wireDirection == null && (Math.abs(evt.getX() - wireStartX) >= gridSize || Math.abs(evt.getY() - wireStartY) >= gridSize)) {
								if (Math.abs(evt.getX() - wireStartX) >= Math.abs(evt.getY() - wireStartY)) {
									wireDirection = WireDirection.horizontal;
								} else {
									wireDirection = WireDirection.vertical;
								}
							} else if (wireDirection == WireDirection.horizontal) {
								if (wireStartX == mouseMoveX) {
									wireDirection = WireDirection.vertical;
								}
							} else if (wireDirection == WireDirection.vertical) {
								if (wireStartY == mouseMoveY) {
									wireDirection = WireDirection.horizontal;
								}
							}
						} else {
							wireDirection = null;
						}
					}
					if (!selectedVertices.isEmpty() || !selectedEdges.isEmpty()) {
//						System.out.println("in");
						dragEdgeID++;
						pathFinding = true;
						ghostEdges.clear();
//						ghostEdges.removeIf(e -> (e.getID() != pastingID));
						ghostPath.clear();
						for (Vertex v : selectedVertices) {
							if (cursorInCanvas) {
								v.setLocation(v.initX + getRoundedCoord(evt.getX() - dragStartX, false), v.initY + getRoundedCoord(evt.getY() - dragStartY, false));
								extendEdge(v);
							}
						}
						ArrayList<Edge> cloneEdge = (ArrayList<Edge>) selectedEdges.clone();
						for (Edge e : selectedEdges) {
							if (cursorInCanvas) {
								removeEdge(e);
							}
						}
//						for (Edge e : copiedEdges) {
//							e.setID(dragEdgeID);
//							if (cursorInCanvas) {
//								Edge ghost = new Edge("ghost", grid.findPort(e.start.x + getRoundedCoord(evt.getX() - dragStartX, false), e.start.y + getRoundedCoord(evt.getY() - dragStartY, false)),
//										grid.findPort(e.end.x + getRoundedCoord(evt.getX() - dragStartX, false), e.end.y + getRoundedCoord(evt.getY() - dragStartY, false)));
//								ghost.setID(dragEdgeID);
//								ghostEdges.add(ghost);
//							}
//						}
						for (Edge e : selectedEdges) {
							e.setID(dragEdgeID);
							if (cursorInCanvas) {
								Edge ghost = new Edge("ghost", grid.findPort(e.start.x + getRoundedCoord(evt.getX() - dragStartX, false), e.start.y + getRoundedCoord(evt.getY() - dragStartY, false)),
										grid.findPort(e.end.x + getRoundedCoord(evt.getX() - dragStartX, false), e.end.y + getRoundedCoord(evt.getY() - dragStartY, false)));
								ghost.setID(dragEdgeID);
								ghostEdges.add(ghost);
								if (!copiedEdges.contains(e)) {
									ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
									extendEdge(e, ghost);
								}
							}
						}
						ghostGrid.recalculateLines(module);
						new Thread(new Runnable() {
							@Override
							public void run() {
								int threadID = dragEdgeID;
								for (int k = 0; k < ghostPath.size(); k++) {
									if (threadID != dragEdgeID) {
										return;
									}
									int[] i = ghostPath.get(k);
									if (i.length == 4) {
										hk.quantr.routingalgo.lee.Path p = ghostGrid.findPath(new hk.quantr.routingalgo.lee.Point(i[0], i[1]),
												new hk.quantr.routingalgo.lee.Point(i[2], i[3]), false);
										if (p != null) {
											if (p.corners.isEmpty()) {
												Edge ghost = new Edge("ghost", grid.findPort(p.end.x, p.end.y), grid.findPort(p.start.x, p.start.y));
												ghostEdges.add(ghost);
												ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
											} else {
												Edge ghost = new Edge("ghost", grid.findPort(p.start.x, p.start.y), grid.findPort(p.corners.get(0).x, p.corners.get(0).y));
												ghostEdges.add(ghost);
												ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
												for (int c = 0; c < p.corners.size() - 1; c++) {
													ghost = new Edge("ghost", grid.findPort(p.corners.get(c).x, p.corners.get(c).y), grid.findPort(p.corners.get(c + 1).x, p.corners.get(c + 1).y));
													ghostEdges.add(ghost);
													ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
												}
												ghost = new Edge("ghost", grid.findPort(p.corners.get(p.corners.size() - 1).x, p.corners.get(p.corners.size() - 1).y), grid.findPort(p.end.x, p.end.y));
												ghostEdges.add(ghost);
												ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
											}
										}
									}
								}
								pathFinding = false;
							}
						}).start();

//						pathFinding = true;
//						ghostEdges.clear();
//						ghostGrid.recalculateLines(module);
//						ghostPath.clear();
//						for (Vertex v : selectedVertices) {
//							if (cursorInCanvas) {
//								v.setLocation(v.initX + getRoundedCoord(evt.getX() - dragStartX, false), v.initY + getRoundedCoord(evt.getY() - dragStartY, false));
//								if (!simulating) {
//									extendEdge(v);
//								}
//							}
//						}
//						new Thread(new Runnable() {
//							@Override
//							public void run() {
//								CircuitGrid threadGrid = new CircuitGrid(300, getCanvas());
//								mouseDragPathFindingSerialNum++;
//								int num = mouseDragPathFindingSerialNum;
//								for (int k = 0; k < ghostPath.size(); k++) {
//									int[] i = ghostPath.get(k);
//									if (i.length == 4) {
//										hk.quantr.routingalgo.lee.Path p = ghostGrid.findPath(new hk.quantr.routingalgo.lee.Point(i[0], i[1]),
//												new hk.quantr.routingalgo.lee.Point(i[2], i[3]), false);
//										if (p != null) {
//											if (p.corners.isEmpty()) {
//												Edge ghost = new Edge("ghost", grid.findPort(p.end.x, p.end.y), grid.findPort(p.start.x, p.start.y));
//												ghostEdges.add(ghost);
//												ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
//											} else {
//												Edge ghost = new Edge("ghost", grid.findPort(p.start.x, p.start.y), grid.findPort(p.corners.get(0).x, p.corners.get(0).y));
//												ghostEdges.add(ghost);
//												ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
//												for (int c = 0; c < p.corners.size() - 1; c++) {
//													ghost = new Edge("ghost", grid.findPort(p.corners.get(c).x, p.corners.get(c).y), grid.findPort(p.corners.get(c + 1).x, p.corners.get(c + 1).y));
//													ghostEdges.add(ghost);
//													ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
//												}
//												ghost = new Edge("ghost", grid.findPort(p.corners.get(p.corners.size() - 1).x, p.corners.get(p.corners.size() - 1).y), grid.findPort(p.end.x, p.end.y));
//												ghostEdges.add(ghost);
//												ghostGrid.applyLine(new hk.quantr.routingalgo.lee.Point(ghost.start.x, ghost.start.y), new hk.quantr.routingalgo.lee.Point(ghost.end.x, ghost.end.y));
//											}
//										}
//									}
//								}
//								if (mouseDragPathFindingSerialNum == num) {
//									
//									pathFinding = false;
//								}
//							}
//						}).start();
					}
					repaint();
				}
//				undo
//				recordAction();
			}
		}
		);
	}

	public void recalculateManualLines() {
		for (int x = 0; x < grid.nodeLoc.length; x++) {
			for (int y = 0; y < grid.nodeLoc[0].length; y++) {
				grid.nodeLoc[x][y].applied = false;
			}
		}
		for (Vertex v : this.module.vertices) {
			for (int x = v.x; x <= v.x + v.width && x < grid.nodeLoc.length; x++) {
				for (int y = v.y; y <= v.y + v.height && y < grid.nodeLoc[0].length; y++) {
					grid.nodeLoc[x][y].applied = true;
				}
			}
		}
//		synchronized (autoPath) {
//			autoPath.clear();
//			for (AutoEdge a : this.module.autoEdges) {
//				Path p = grid.findPath(new hk.quantr.routingalgo.lee.Point(a.startPort.getAbsolutionX(), a.startPort.getAbsolutionY()),
//						new hk.quantr.routingalgo.lee.Point(a.endPort.getAbsolutionX(), a.endPort.getAbsolutionY()), false);
//				if (p != null) {
//					p.name = "autoPath {" + a.startPort.name + "<->" + a.endPort.name + "}";
//					autoPath.add(p);
//				} else {
////									System.out.println("path is null");
//				}
//			}
//		}
		repaint();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(2000, 2000);
	}

	public boolean released(MouseEvent evt) {
		if (quantrGraphPanel.getProjectTree().hasFocus()) {
			if (quantrGraphPanel.getProjectTree().getSelectionPaths() != null && !simulating) {
				ProjectTreeNode node = (ProjectTreeNode) quantrGraphPanel.getProjectTree().getLastSelectedPathComponent();
				if (node.data.toString().equals(this.module.name)) {
					grabFocus();
					return false;
				}
				System.out.println("add " + node.data.toString());
				String name = node.data.getClass().getSimpleName() + " " + this.module.vertices.size();
				for (int i = 0; i < quantrGraphPanel.data.modules.size(); i++) {
					if (quantrGraphPanel.data.modules.get(node.data.toString()) != null) {
						ModuleComponent v = new ModuleComponent(name, quantrGraphPanel.data.modules.get(node.data.toString()));
						v.setLocation(evt.getX() / this.gridSize, evt.getY() / this.gridSize);
						this.module.vertices.add(v);
						this.repaint();
						quantrGraphPanel.refreshTree();
						grabFocus();
						return true;
					}
				}
			}
		} else if (quantrGraphPanel.toolbarTree.hasFocus() || contToAdd) {
			if (quantrGraphPanel.toolbarTree.getSelectionPaths() != null && !simulating) {
				ToolbarTreeNode node = (ToolbarTreeNode) quantrGraphPanel.toolbarTree.getLastSelectedPathComponent();
//			String name = JOptionPane.showInputDialog(this, "Node name?");
//			if (name == null) {
//				return;
//			}

				Vertex v1 = getVertexToAdd(node.data);

				boolean flag = false;
				if (v1 != null) {
					v1.setLocation(getRoundedCoord(evt.getX(), false), getRoundedCoord(evt.getY(), false));
					v1.gridSize = this.gridSize;
					this.module.vertices.add(v1);
					flag = true;
				}
				if (!evt.isShiftDown()) {
					quantrGraphPanel.toolbarTree.setSelectionPath(null);
					contToAdd = false;
				} else {
					contToAdd = true;
				}
				this.recalculateLines();
				this.repaint();
				quantrGraphPanel.refreshTree();
				grabFocus();
				return flag;
			}
		}
		grabFocus();
		return false;
	}

	public Vertex getVertexToAdd(Object data) {
		String name = data.getClass().getSimpleName() + " " + this.module.vertices.size();
		Vertex v1 = null;
		if (data instanceof OrGate) {
			v1 = new OrGate(name);
		} else if (data instanceof AndGate) {
			v1 = new AndGate(name);
		} else if (data instanceof NotGate) {
			v1 = new NotGate(name);
		} else if (data instanceof NorGate) {
			v1 = new NorGate(name);
		} else if (data instanceof NandGate) {
			v1 = new NandGate(name);
		} else if (data instanceof XorGate) {
			v1 = new XorGate(name);
		} else if (data instanceof XnorGate) {
			v1 = new XnorGate(name);
		} else if (data instanceof Pin) {
			v1 = new Pin(name);
		} else if (data instanceof Led) {
			v1 = new Led(name);
		} else if (data instanceof Clock) {
			v1 = new Clock(name);
		} else if (data instanceof DLatch) {
			v1 = new DLatch(name);
		} else if (data instanceof DFlipflop) {
			v1 = new DFlipflop(name);
		} else if (data instanceof JKFlipflop) {
			v1 = new JKFlipflop(name);
		} else if (data instanceof SRFlipflop) {
			v1 = new SRFlipflop(name);
		} else if (data instanceof TFlipflop) {
			v1 = new TFlipflop(name);
		} else if (data instanceof HexDisplay) {
			v1 = new HexDisplay(name);
		} else if (data instanceof EightSegmentDisplay) {
			v1 = new EightSegmentDisplay(name);
		} else if (data instanceof Ram) {
			v1 = new Ram(name);
		} else if (data instanceof Tunnel) {
			v1 = new Tunnel(name);
			v1.updateProperty();
		} else if (data instanceof Combiner) {
			v1 = new Combiner(name);
		} else if (data instanceof Splitter) {
			v1 = new Splitter(name);
		} else if (data instanceof Adder) {
			v1 = new Adder(name);
		} else if (data instanceof Subtractor) {
			v1 = new Subtractor(name);
		} else if (data instanceof Multiplier) {
			v1 = new Multiplier(name);
		} else if (data instanceof Divider) {
			v1 = new Divider(name);
		} else if (data instanceof RGBLed) {
			v1 = new RGBLed(name);
		} else if (data instanceof TTY) {
			v1 = new TTY(name);
		} else if (data instanceof Shifter) {
			v1 = new Shifter(name);
		} else if (data instanceof DipSwitch) {
			v1 = new DipSwitch(name);
		} else if (data instanceof Switch) {
			v1 = new Switch(name);
		} else if (data instanceof Button) {
			v1 = new Button(name);
		} else if (data instanceof LedBar) {
			v1 = new LedBar(name);
		} else if (data instanceof Negator) {
			v1 = new Negator(name);
		} else if (data instanceof Probe) {
			v1 = new Probe(name);
		} else if (data instanceof BitExtender) {
			v1 = new BitExtender(name);
		} else if (data instanceof BitSelector) {
			v1 = new BitSelector(name);
		} else if (data instanceof Demultiplexer) {
			v1 = new Demultiplexer(name);
		} else if (data instanceof Multiplexer) {
			v1 = new Multiplexer(name);
		} else if (data instanceof Encoder) {
			v1 = new Encoder(name);
		} else if (data instanceof Decoder) {
			v1 = new Decoder(name);
		} else if (data instanceof Buffer) {
			v1 = new Buffer(name);
		} else if (data instanceof EvenParity) {
			v1 = new EvenParity(name);
		} else if (data instanceof OddParity) {
			v1 = new OddParity(name);
		} else if (data instanceof VCD) {
			v1 = new VCD(this.module.name);
		} else if (data instanceof Constant) {
			v1 = new Constant(name);
		} else if (data instanceof OutputPin) {
			v1 = new OutputPin(name);
		} else if (data instanceof hk.quantr.logic.data.basic.Random) {
			v1 = new hk.quantr.logic.data.basic.Random(name);
		} else if (data instanceof TriStateBuffer) {
			v1 = new TriStateBuffer(name);
		} else if (data instanceof TriStateNotBuffer) {
			v1 = new TriStateNotBuffer(name);
		} else if (data instanceof ImageComponent) {
			v1 = new ImageComponent(name);
		} else if (data instanceof ArduinoSerial) {
			v1 = new ArduinoSerial(name);
		} else if (data instanceof ArduinoSerialOled) {
			v1 = new ArduinoSerialOled(name);
		} else if (data instanceof ArduinoWifi) {
			v1 = new ArduinoWifi(name);
		} else if (data instanceof ArduinoWifiOled) {
			v1 = new ArduinoWifiOled(name);
		} else if (data instanceof ArduinoHeltecWifiOled) {
			v1 = new ArduinoHeltecWifiOled(name);
		} else if (data instanceof Transistor) {
			v1 = new Transistor(name);
		} else if (data instanceof Loadable) {
			try {
				Loadable l = (Loadable) data;
//					String jarPath = Paths.get(l.jar).toString();
//					JarFile jarFile = new JarFile(jarPath);
//					Enumeration<JarEntry> e = jarFile.entries();

				URL[] urls = {new URL(l.jar)};
				URLClassLoader cl = URLClassLoader.newInstance(urls);
				Class<Vertex> vertexClass = (Class<Vertex>) cl.loadClass(l.className);

				Class<? extends Vertex> sub = vertexClass.asSubclass(Vertex.class);
				Constructor<? extends Vertex> con = sub.getConstructor(String.class);
				v1 = con.newInstance(name);
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException
					| SecurityException | IllegalArgumentException | InvocationTargetException | MalformedURLException ex) {
				Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return v1;
	}

	private Module getModule() {
		return this.module;
	}

	public void recalculateLines() {
		for (Vertex v : this.module.vertices) {
			synchronized (v) {
				if (v instanceof Splitter || v instanceof Switch || v instanceof Probe || v instanceof Random || v instanceof OutputPin) {
					v.updateProperty();
//					v.updateProperty(this);
				}
			}
		}
		if (!this.simulating) {
			grid.reconnectEdge(module);
		}
		for (int x = 0; x < grid1.nodeLoc.length; x++) {
			for (int y = 0; y < grid1.nodeLoc[0].length; y++) {
				grid1.nodeLoc[x][y].applied = false;
			}
		}
		for (Vertex v : this.module.vertices) {
			for (int x = v.x; x <= v.x + v.width && x < grid1.nodeLoc.length; x++) {
				for (int y = v.y; y <= v.y + v.height && y < grid1.nodeLoc[0].length; y++) {
					if (x >= 0 && y >= 0) {
						grid1.nodeLoc[x][y].applied = true;
					}
				}
			}
		}
		repaint();
		if (quantrGraphPanel != null) {
			quantrGraphPanel.refreshTree();
		}
	}

	@Override
	public void paint(Graphics g) {
//		System.out.println("paint start " + sdf.format(new Date()));
		g.setColor(Color.white);
		g.fillRect(0, 0, getWidth(), getHeight());
		if (showGrid) {
			drawGrid(g);
		}
		drawDot(g);
		if (IPHelper.isInHongKong()) {
//			g.drawImage(fuck, 0, 0, getWidth(), getHeight(), null);
			g.setColor(Color.red);
			Font myFont = new Font("Serif", Font.ITALIC | Font.BOLD, 80);
			g.setFont(myFont);
			g.drawString("BLOCKED HONG KONG", 20, 100);
			g.drawString("HONG KONG PEOPLE ARE NOT", 20, 250);
			g.drawString("ALLOW TO USE THIS SOFTWARE", 20, 400);
			g.drawString("香港任何大學絕不容許使用本軟件", 20, 550);
			return;
		}
		if (quantrGraphPanel != null) {
			for (int i = 0; i < this.module.vertices.size(); i++) {
				Vertex v = this.module.vertices.get(i);
				if (v instanceof Led) {
					((Led) v).paint(g);
				} else {
					v.paint(g);
				}
			}
			BasicStroke stroke = new BasicStroke(this.gridSize * 0.2f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			Graphics2D g2 = init(g);
			g2.setStroke(stroke);
			for (int i = 0; i < this.module.edges.size(); i++) {
				this.module.edges.get(i).paint(g, this.gridSize);
			}
			for (Edge e : ghostEdges) {
				g.setColor(Color.gray);
				g.drawLine(e.start.x * gridSize, e.start.y * gridSize, e.end.x * gridSize, e.end.y * gridSize);
			}
			if (isDraggingSelection) {
				g2.setColor(new Color(135, 206, 235, 128));
				g2.fillRect(Math.min(mouseMoveX, dragStartX), Math.min(mouseMoveY, dragStartY), Math.abs(mouseMoveX - dragStartX), Math.abs(mouseMoveY - dragStartY));
				g2.setColor(new Color(135, 206, 235));
				g2.drawRect(Math.min(mouseMoveX, dragStartX), Math.min(mouseMoveY, dragStartY), Math.abs(mouseMoveX - dragStartX), Math.abs(mouseMoveY - dragStartY));
			}
		}

		drawMouseOverOval(g);
		if (wiring || rightClickWiring) {
			if ((quantrGraphPanel.wireToggleButton.isSelected() || rightClickWiring) && wireStartX != -1 && wireStartY != -1) {
				g.setColor(Color.black);
				if (wireDirection == WireDirection.horizontal) {
					g.drawLine(wireStartX, wireStartY, mouseMoveX, wireStartY);
					g.drawLine(mouseMoveX, wireStartY, mouseMoveX, mouseMoveY);
				} else {
					g.drawLine(wireStartX, wireStartY, wireStartX, mouseMoveY);
					g.drawLine(wireStartX, mouseMoveY, mouseMoveX, mouseMoveY);
				}
			}
		}
		grid.paint(g);
		drawRotateImage(g);
//		drawCityuEELogo(g);
	}

	private void drawRotateImage(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		int logoWidth = 50;
		int logoHeight = qfLogo.getHeight() * logoWidth / qfLogo.getWidth();
//		int drawX = getWidth() - 60;
//		int drawY = 20;
		int drawX = 10;
		int drawY = getHeight() - logoHeight - 10;
		AffineTransform backup = g2d.getTransform();
		AffineTransform trans = new AffineTransform();
		trans.rotate((float) Math.toRadians(fraction1 * 360), drawX + (logoWidth / 2), drawY + (logoHeight / 2));
		g2d.transform(trans);
		g2d.drawImage(qfLogo, drawX, drawY, logoWidth, logoHeight, null);
		g2d.setTransform(backup);
	}

	private void drawCityuEELogo(Graphics g) {
//		Graphics2D g2d = (Graphics2D) g;
//		int logoWidth = 250;
//		int logoHeight = cityuEE.getHeight() * logoWidth / cityuEE.getWidth();
//		int drawX = 10;
//		int drawY = 10;
//		g2d.drawImage(cityuEE, drawX, drawY, logoWidth, logoHeight, null);
	}

	public static BufferedImage rotateImage(BufferedImage img, float angle, java.awt.Point drawOffset) {
		int w = img.getWidth();
		int h = img.getHeight();
		AffineTransform tf = AffineTransform.getRotateInstance(angle, w / 2.0, h / 2.0);
		// get coordinates for all corners to determine real image size
		Point2D[] ptSrc = new Point2D[4];
		ptSrc[0] = new java.awt.Point(0, 0);
		ptSrc[1] = new java.awt.Point(w, 0);
		ptSrc[2] = new java.awt.Point(w, h);
		ptSrc[3] = new java.awt.Point(0, h);
		Point2D[] ptTgt = new Point2D[4];
		tf.transform(ptSrc, 0, ptTgt, 0, ptSrc.length);
		Rectangle rc = new Rectangle(0, 0, w, h);
		for (Point2D p : ptTgt) {
			if (p.getX() < rc.x) {
				rc.width += rc.x - p.getX();
				rc.x = (int) p.getX();
			}
			if (p.getY() < rc.y) {
				rc.height += rc.y - p.getY();
				rc.y = (int) p.getY();
			}
			if (p.getX() > rc.x + rc.width) {
				rc.width = (int) (p.getX() - rc.x);
			}
			if (p.getY() > rc.y + rc.height) {
				rc.height = (int) (p.getY() - rc.y);
			}
		}
		BufferedImage imgTgt = new BufferedImage(rc.width, rc.height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = imgTgt.createGraphics();
		// create a NEW rotation transformation around new center
		tf = AffineTransform.getRotateInstance(angle, rc.getWidth() / 2, rc.getHeight() / 2);
		g2d.setTransform(tf);
		g2d.drawImage(img, -rc.x, -rc.y, null);
		g2d.dispose();
		drawOffset.x += rc.x;
		drawOffset.y += rc.y;
		return imgTgt;
	}

	public void clearVertexSelection() {
//		selectedVertex = null;
//		for (Vertex v : module.vertices) {
//			v.isSelected = false;
//		}
//		selectedEdge = null;
//		for (Edge e : module.edges) {
//			e.isSelected = false;
//		}
//		for (AutoEdge a : module.autoEdges) {
//			a.isSelected = false;
//		}
		for (Vertex v : this.module.vertices) {
			v.isSelected = false;
		}
		for (Edge e : this.module.edges) {
			e.isSelected = false;
		}
		selectedVertices.clear();
		selectedEdges.clear();
		this.repaint();
	}

	public void clearEdgeSelection() {
		for (Edge e : this.module.edges) {
			e.isSelected = false;
		}
		selectedEdges.clear();
		this.repaint();
	}

	public void clearVSelection() {
		for (Vertex v : this.module.vertices) {
			v.isSelected = false;
		}
		selectedVertices.clear();
		this.repaint();
	}

	public Vertex findVertex(int x, int y, boolean s) {
		x = getRoundedCoord(x, false);
		y = getRoundedCoord(y, false);
		for (int i = this.module.vertices.size() - 1; i >= 0; i--) {
			Vertex v = this.module.vertices.get(i);
			if (v.x <= x && x <= v.x + v.width && v.y <= y && y <= v.y + v.height) {
				if (s && findPort(x * gridSize, y * gridSize) == null) {
					v.isSelected = true;
				}
				return v;
			}
		}
		return null;
	}

	public Vertex findVertex(int x, int y) {
		x = getRoundedCoord(x, true);
		y = getRoundedCoord(y, true);
		for (int i = this.module.vertices.size() - 1; i >= 0; i--) {
			Vertex v = this.module.vertices.get(i);
			if (v.x * gridSize <= x && x <= v.x * gridSize + v.width * gridSize && v.y * gridSize <= y && y <= v.y * gridSize + v.height * gridSize) {
//				v.isSelected = !v.isSelected;
				return v;
			}
		}
		return null;
	}

	public Edge findEdge(int x, int y) {
		x = getRoundedCoord(x, false);
		y = getRoundedCoord(y, false);
		for (Edge e : this.module.edges) {
			if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
					|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
				return e;
			}
		}
		return null;
	}

	public ArrayList<Edge> findGhostEdges(int x, int y) {
		x = getRoundedCoord(x, false);
		y = getRoundedCoord(y, false);
		ArrayList<Edge> result = new ArrayList();
		for (Edge e : this.ghostEdges) {
			if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
					|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
				if (!result.contains(e)) {
					result.add(e);
				}
			}
		}
		return result;
	}

	public ArrayList<Edge> findEdges(int x, int y) {
		x = getRoundedCoord(x, false);
		y = getRoundedCoord(y, false);
		ArrayList<Edge> result = new ArrayList();
		for (Edge e : this.module.edges) {
			if ((e.start.y == y && (Math.abs(x - e.start.x) <= Math.abs(e.start.x - e.end.x)) && (Math.abs(x - e.end.x) <= Math.abs(e.start.x - e.end.x)))
					|| (e.start.x == x && (Math.abs(y - e.start.y) <= Math.abs(e.start.y - e.end.y)) && (Math.abs(y - e.end.y) <= Math.abs(e.start.y - e.end.y)))) {
				if (!result.contains(e)) {
					result.add(e);
				}
			}
		}
		return result;
	}

	public Port findPort(int x, int y) {
		x = getRoundedCoord(x, true);
		y = getRoundedCoord(y, true);
		for (Vertex v : this.module.vertices) {
			if (v.x * gridSize <= x && x <= v.x * gridSize + v.width * gridSize && v.y * gridSize <= y && y <= v.y * gridSize + v.height * gridSize) {
				for (Input in : v.inputs) {
					if (in.deltaX + v.x == x / gridSize && in.deltaY + v.y == y / gridSize) {
						return in;
					}
				}
				for (Output o : v.outputs) {
					if (o.deltaX + v.x == x / gridSize && o.deltaY + v.y == y / gridSize) {
						return o;
					}
				}
			}
		}
		return null;
	}

	private void drawDot(Graphics g) {
		g.setColor(Setting.getInstance().gridColor == null ? Color.decode("#000000") : Setting.getInstance().gridColor);
		for (int y = 0; y < getHeight(); y += gridSize) {
			for (int x = 0; x < getWidth(); x += gridSize) {
				g.drawLine(x, y, x, y);
			}
		}
	}

	private void drawGrid(Graphics g) {
		g.setColor(Color.decode("#eeeeee"));
		for (int y = 0; y < getHeight(); y += gridSize) {
			g.drawLine(0, y, getWidth(), y);
		}
		for (int x = 0; x < getWidth(); x += gridSize) {
			g.drawLine(x, 0, x, getHeight());
		}
	}
//	private void drawTable(Graphics g, boolean[][] table) {
//		g.setColor(Color.decode("#ff0000"));
//		for (int x = 0; x < getWidth(); x += gridSize) {
//			for (int y = 0; y < getHeight(); y += gridSize) {
//				if (table[x / gridSize][y / gridSize]) {
//					g.fillRect(x, y, gridSize, gridSize);
//				}
//			}
//		}
//	}
//
//	private boolean[][] fillTable(ArrayList<Vertex> vertices) {
//		boolean table[][] = new boolean[getWidth() / gridSize + 1][getHeight() / gridSize + 1];
//		for (Vertex v : vertices) {
//			for (int x = 0; x < v.width; x += gridSize) {
//				for (int y = 0; y < v.height; y += gridSize) {
//					table[(v.x + x) / gridSize][(v.y + y) / gridSize] = true;
//				}
//			}
//		}
//		return table;
//	}

	void setGridSize(int gridSize) {
		this.gridSize = gridSize;
		for (Vertex v : this.module.vertices) {
			v.gridSize = gridSize;
		}
		for (Edge e : this.module.edges) {
			e.gridSize = gridSize;
		}
		this.repaint();
	}

	private void drawMouseOverOval(Graphics g) {
		g.setColor(Color.pink);
		if (cursorInCanvas) {
			if (quantrGraphPanel.toolbarTree.getSelectionPaths() != null) {
				Vertex v = getVertexToAdd(((ToolbarTreeNode) quantrGraphPanel.toolbarTree.getLastSelectedPathComponent()).data);
				if (v != null) {
					v.gridSize = this.gridSize;
					v.setLocation(getRoundedCoord(mouseMoveX, false), getRoundedCoord(mouseMoveY, false));
					v.paint(g);
				} else {
					g.drawOval(getRoundedCoord(mouseMoveX, true) - 5, getRoundedCoord(mouseMoveY, true) - 5, 10, 10);
				}
			} else {
				g.drawOval(getRoundedCoord(mouseMoveX, true) - 5, getRoundedCoord(mouseMoveY, true) - 5, 10, 10);
			}
		}
	}

	private int getRoundedCoord(int x, boolean isReal) {
		return isReal
				? ((x - (x / gridSize) * gridSize > (x / gridSize) * gridSize + gridSize - x)
						? (x / gridSize) * gridSize + gridSize : (x / gridSize) * gridSize)
				: (((x - (x / gridSize) * gridSize > (x / gridSize) * gridSize + gridSize - x)
						? (x / gridSize) * gridSize + gridSize : (x / gridSize) * gridSize) / gridSize);
	}

	void showLevel(boolean b) {
		for (Vertex v : this.module.vertices) {
			v.showLevel = b;
		}
	}

	void loop(HashSet<Port> checkFinish, Port port, int level) {
//		System.out.println(port);
		if (checkFinish.contains(port)) {
			return;
		}
		checkFinish.add(port);

		if (level > port.level) {
			port.level = level;
		} else {
			return;
		}
		if (!port.parent.outputConnectedTo().isEmpty()) {
			for (Port p : port.parent.outputConnectedTo()) {
				if (p instanceof Input) {
					loop(checkFinish, p, level + 1);
				}
			}
		}
//		else {
//			checkFinish.clear();
//		}
	}

	public ArrayList<Vertex> calculateLevel(ArrayList<ArrayList<Vertex>> list) {
		ArrayList<Vertex> temp = new ArrayList();
		for (int i = 0; i <= maxLevel; i++) {
			for (Vertex v : list.get(i)) {
				temp.add(v);
			}
		}
		return temp;
	}

	public ArrayList<ArrayList<Vertex>> calculateLevel() {
		this.maxLevel = -1;
		for (Vertex v : this.module.vertices) {
			for (Input input : v.inputs) {
				input.level = -1;
			}
		}
		for (Vertex v : this.module.vertices) {
			HashSet<Port> checkFinish = new HashSet();
			if (v.inputs.isEmpty() && !v.outputs.isEmpty()) {
				v.outputs.get(0).level = 0;
				if (!v.outputConnectedTo().isEmpty()) {
					for (Port port : v.outputConnectedTo()) {
						loop(checkFinish, port, v.outputs.get(0).level);
					}
				}
			}
		}

		for (Vertex v : this.module.vertices) {
			if (v instanceof Tunnel) {
				if (v.outputConnectedTo().isEmpty()) {
					v.level = -1;
				} else {
					for (Port p : v.outputConnectedTo()) {
						if (p instanceof Output) {
							v.level = p.level + 1;
						}
					}
				}
			} else if (!v.outputConnectedTo().isEmpty()) {
				v.level = v.outputConnectedTo().get(0).level;
			} else {
				if (v.inputs.isEmpty()) {
					v.level = -1;
				} else {
					int tempLevel = -2;
					for (Input input : v.inputs) {
						if (input.level != -1) {
							tempLevel = input.level;
						}
						if (v.level < input.level + 1) {
							v.level = input.level + 1;
						}
					}
					if (tempLevel == -2) {
						v.level = -1;
					}
					for (int i = 0; i < v.outputs.size(); i++) {
						v.outputs.get(i).level = v.level;
					}
				}
			}
		}

		for (Vertex v : this.module.vertices) {
			if (maxLevel < v.level) {
				maxLevel = v.level;
			}
		}
		ArrayList<ArrayList<Vertex>> levelledVertices = new ArrayList();
		for (int i = 0; i <= maxLevel; i++) {
			levelledVertices.add(new ArrayList());
		}
		for (Vertex v : this.module.vertices) {
			if (v.level != -1) {
				levelledVertices.get(v.level).add(v);
			} else if (levelledVertices.size() == this.maxLevel + 1) {
				levelledVertices.add(new ArrayList());
				levelledVertices.get(levelledVertices.size() - 1).add(v);
			} else {
				levelledVertices.get(levelledVertices.size() - 1).add(v);
			}
		}

		return levelledVertices;
	}

	public void removeEdge(Edge e) {
		this.grid.removeEdge(e, true); // erase from grid
//		this.module.edges.remove(e);
//		this.grid.configLine(e.start, e.end, false);
	}

	public void extendEdge(Vertex v) {
		for (int j = 0; j < v.inputs.size(); j++) {
			Input input = v.inputs.get(j);
			createGhost(input);
		}
		for (int j = 0; j < v.outputs.size(); j++) {
			Output output = v.outputs.get(j);
			createGhost(output);
		}
	}

	public void extendEdge(Edge e, Edge ghost) {
		boolean extendOnStart = false;
		for (int i = 0; i < e.startPort.edges.size(); i++) {
			if (e.startPort.edges.get(i) != e) {
				ghostPath.add(new int[]{e.startPort.getAbsolutionX(), e.startPort.getAbsolutionY(), ghost.startPort.getAbsolutionX(), ghost.startPort.getAbsolutionY()});
				extendOnStart = true;
			}
		}
		if (e.startPort.vertexPort != null && !e.startPort.vertexPort.parent.isSelected && !extendOnStart) {
			ghostPath.add(new int[]{e.startPort.getAbsolutionX(), e.startPort.getAbsolutionY(), ghost.startPort.getAbsolutionX(), ghost.startPort.getAbsolutionY()});
		}
		for (int i = 0; i < e.endPort.edges.size(); i++) {
			if (e.endPort.edges.get(i) != e) {
				ghostPath.add(new int[]{e.endPort.getAbsolutionX(), e.endPort.getAbsolutionY(), ghost.endPort.getAbsolutionX(), ghost.endPort.getAbsolutionY()});
				return;
			}
		}
		if (e.endPort.vertexPort != null && !e.endPort.vertexPort.parent.isSelected) {
			ghostPath.add(new int[]{e.endPort.getAbsolutionX(), e.endPort.getAbsolutionY(), ghost.endPort.getAbsolutionX(), ghost.endPort.getAbsolutionY()});
		}
	}

	public void createGhost(Port port) {
		for (Edge e : port.edges) {
			if (!e.isSelected) {
				ghostPath.add(new int[]{port.getAbsolutionX(), port.getAbsolutionY(), port.vertexPort.x, port.vertexPort.y});
				return;
			}
		}
	}

	public void recordAction() {
		if (!simulating) {
			String xml = serializer.toXML(module);
			if (!dataHistory.get(dataIndex).equals(xml)) {
				dataIndex++;
				while (dataHistory.size() > dataIndex) {
					dataHistory.remove(dataHistory.size() - 1);
				}
				dataHistory.add(dataIndex, xml);
				quantrGraphPanel.data.save(quantrGraphPanel.projectFile);
			}
		}
	}

	public void undo() {
		if (dataIndex > 0) {
			this.loadModuleToCanvas((Module) deserializer.fromXML(dataHistory.get(--dataIndex)));
			this.quantrGraphPanel.data.modules.put(module.name, module);
			ghostPath.clear();
			ghostEdges.clear();
			clearVertexSelection();
		}
	}

	public void redo() {
		if (dataHistory.size() > dataIndex + 1) {
			this.loadModuleToCanvas((Module) deserializer.fromXML(dataHistory.get(++dataIndex)));
			this.quantrGraphPanel.data.modules.put(module.name, module);
			ghostPath.clear();
			ghostEdges.clear();
			clearVertexSelection();
		}
	}

	public void reset() {
		for (Vertex v : this.module.vertices) {
			v.reset();
		}
		try {
			quantrGraphPanel.tempVCDFile = java.nio.file.Files.createTempFile("vcdFile", ".vcd").toFile();
			quantrGraphPanel.quantrVCDComponent1.loadVCD(quantrGraphPanel.tempVCDFile);
			quantrGraphPanel.tempVCDFile.deleteOnExit();
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphCanvas.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public CircuitGrid getGrid() {
		return grid;
	}

	private QuantrGraphCanvas getCanvas() {
		return this;
	}

	public void setGrid(CircuitGrid g) {
		this.grid = g;
	}

	public void loadModuleToCanvas(Module module) {
		System.out.println("load");
		this.grid.clean();
		this.ghostEdges.clear();
		this.ghostGrid.clean();
		this.module = module;
		this.module.vertices = module.vertices;
		this.module.name = module.name;
		ArrayList<Edge> tempEdges = (ArrayList<Edge>) this.module.edges.clone();
		for (Vertex v : this.module.vertices) {
			if (v.isSelected) {
				selectedVertices.add(v);
			}
		}
		for (Edge e : this.module.edges) {
			if (e.isSelected) {
				selectedEdges.add(e);
			}
		}
		this.module.edges.clear();
		for (Edge e : tempEdges) {
			Edge e1 = new Edge("wire", grid.findPort(e.start.x, e.start.y), grid.findPort(e.end.x, e.end.y));
			this.grid.addEdge(e1);
		}
		this.grid.reconnectEdge(this.module);
	}

	public void selectAll() {
		for (Vertex v : this.module.vertices) {
			selectedVertices.add(v);
			v.isSelected = true;
		}
		for (Edge e : this.module.edges) {
			selectedEdges.add(e);
			e.isSelected = true;
		}
	}

	public void setImage() {
		for (Vertex v : module.vertices) {
			if (v.x < minX) {
				minX = v.x;
			}
			if (v.x + v.width > maxX) {
				maxX = v.x + v.width;
			}
			if (v.y < minY) {
				minY = v.y;
			}
			if (v.y + v.height > maxY) {
				maxY = v.y + v.height;
			}
		}
		image = new BufferedImage((maxX - minX + 10) * 10, (maxY - minY + 10) * 10, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = image.createGraphics();
		for (Vertex v : this.module.vertices) {
			v.gridSize = 10;
			v.setLocation(v.x - minX + 5, v.y - minY + 5);
			v.isSelected = false;
			v.properties.put("Label", "");
			if (v instanceof Led) {
				((Led) v).paint(g);
			} else {
				v.paint(g);
			}
			v.setLocation(v.x + minX - 5, v.y + minY - 5);
			v.gridSize = this.gridSize;
		}
		for (Edge e : this.module.edges) {
			e.gridSize = 10;
			e.start.x -= minX - 5;
			e.end.x -= minX - 5;
			e.start.y -= minY - 5;
			e.end.y -= minY - 5;
			e.isSelected = false;
			e.paint(g, this.gridSize);
			e.start.x += minX - 5;
			e.end.x += minX - 5;
			e.start.y += minY - 5;
			e.end.y += minY - 5;
			e.gridSize = this.gridSize;
		}
	}
}
