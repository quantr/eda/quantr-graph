/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.arduino;

import static hk.quantr.logic.arduino.ArduinoMethod.*;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michelle
 */
public class ArduinoWifiOled extends Vertex {

	public ArrayList<Port> pin = new ArrayList();

	public ArduinoWifiOled(String name) {
		super(name, 0, 0, 10, 10);
		ArduinoMethod.setLogo();
		properties.remove("Label");
		properties.put("Name", name);
		this.properties.put("Arduino Wifi Oled", "Choose a Model");
		this.properties.put("Address", "0.0.0.0");
		this.properties.put("Orientation", "west");
	}

	@Override
	public void eventTriggerEval() {
		this.eval();
	}

	@Override
	public void eval() {
		String signal = "";
		for (int i = 0; i < inputs.size(); i++) {
			if (!inputs.get(i).name.contains("OLED")) {
				signal += inputs.get(i).name.replaceAll("\\D+", "") + "=";
				signal += (inputs.get(i).value & 1) + ",";
			} else {
				signal += "OLED" + inputs.get(i).bits + "=";
				signal += (inputs.get(i).value & ((2 << inputs.get(i).bits) - 1)) + ",";
			}
		}
		try {
			String feedBack = ArduinoServer.sendSignal(signal);
			int[][] cmd = getSignal(feedBack);
//			System.out.println(feedBack);
			for (int i = 0; i < cmd.length; i++) {
				for (int k = 0; k < this.outputs.size(); k++) {
					if (this.outputs.get(k).name.contains(String.valueOf(cmd[i][0]))) {
						this.outputs.get(k).value = cmd[i][1] & 1;
					}
				}
			}
			super.eval();
//			Thread.sleep(40);
//			System.out.println("");
		} catch (IOException ex) {
			Logger.getLogger(ArduinoWifi.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InterruptedException ex) {
			Logger.getLogger(ArduinoWifi.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void simulateInit() {
		super.simulateInit();

		for (int i = 0; i < inputs.size(); i++) {
			System.out.println("input[" + i + "] = " + inputs.get(i).name);
		}
		for (int i = 0; i < outputs.size(); i++) {
			System.out.println("output[" + i + "] = " + outputs.get(i).name);
		}
	}

	@Override
	public void updateProperty() {
		arduinoUpdatePin(this, "Arduino Wifi Oled");
		arduinoWifiAddress((String) this.properties.get("Address"));
	}

	@Override
	public void paint(Graphics g) {
		arduinoPaint(g, this);
		super.paint(g);
	}

}
