/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.arduino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpTimeoutException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ken
 */
public class ArduinoServer {

	public static String serverUrlString = "http://192.168.0.192/";
	int port = 80;
	Socket socket;
	BufferedReader reader;
	PrintWriter writer;
	InputStream input;
	OutputStream output;

	public ArduinoServer() throws UnknownHostException, IOException, ClassNotFoundException, InterruptedException {
		connectAruinoServer();
	}

	public static String get() throws IOException, InterruptedException, HttpTimeoutException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(ArduinoServer.serverUrlString + "Get-State"))
				.timeout(Duration.of(100, ChronoUnit.MILLIS))
				.build();
		var response
				= client.send(request, HttpResponse.BodyHandlers.ofString());
		return response.body();
	}

	public static String sendSignal(String signal) throws IOException, InterruptedException, HttpTimeoutException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(ArduinoServer.serverUrlString + "," + signal + "Quantr-logic"))
				.timeout(Duration.of(200, ChronoUnit.MILLIS))
				.build();
		var response
				= client.send(request, HttpResponse.BodyHandlers.ofString());
		return response.body();
	}

	public static void setPin(String signal) throws IOException, InterruptedException, HttpTimeoutException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(ArduinoServer.serverUrlString + "," + signal))
				.timeout(Duration.of(200, ChronoUnit.MILLIS))
				.build();
		client.send(request, HttpResponse.BodyHandlers.ofString());
	}

	public boolean receiveData() throws IOException {
		Object line;
		boolean received = false;
//		while ((line = reader.readLine()) != null) {
////			System.out.println(reader.);
//			System.out.println(line);
//			received = true;
//		}
		if (input.available() > 0) {
			System.out.println(Arrays.toString(input.readAllBytes()));
		}
		return received;
	}

	public void sendData() throws IOException {
		byte[] b = "hello world".getBytes();
		output.write(b);
//		writer.println("ready");
//		writer.write("hello");
	}

	public void newReader() throws IOException {
		InputStream input = socket.getInputStream();
		reader = new BufferedReader(new InputStreamReader(input));
//		socket.getInputStream().
	}

	public void connectAruinoServer() {
		URL url;
		try {
			url = new URL(serverUrlString);
		} catch (MalformedURLException ex) {
			ex.printStackTrace();
			return;
		}

		String hostname = url.getHost();
		try {
			socket = new Socket(hostname, port);
			input = socket.getInputStream();
			reader = new BufferedReader(new InputStreamReader(input));
			String line;
			output = socket.getOutputStream();
			writer = new PrintWriter(output);
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (UnknownHostException ex) {

			System.out.println("Server not found: " + ex.getMessage());

		} catch (IOException ex) {

			System.out.println("I/O error: " + ex.getMessage());
		}
	}
}
