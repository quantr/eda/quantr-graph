/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.arduino;

/**
 *
 * @author ken
 */
public class ArduinoPinMap {

	public static final String[] arduinoSerialModel = {"", "Nano", "Other"};
	public static final String[] arduinoSerialOledModel = {"", "Heltec Wifi LoRa 32(V2)", "Other"};
	public static final String[] arduinoWifiModel = {"", "ESP-WROOM-32", "test Model", "Other"};
	public static final String[] arduinoWifiOledModel = {"", "Heltec Wifi LoRa 32(V2)", "Other"};

	public static int[] getPinMap(String boardName) {
		// first element: wanted board width
		//should be in ascending order
		if (boardName.equals("Heltec Wifi LoRa 32(V2)")) { // Heltec ESP32 Dev-Boards
			return new int[]{8, 0, 2, 12, 13, 17, 21, 22, 23, 32, 33, 34, 35, 36, 37, 38, 39};
		} else if (boardName.equals("Nano")) {
			return new int[]{5, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
		} else if (boardName.equals("ESP-WROOM-32")) {
			return new int[]{8, 2, 4, 5, 12, 13, 14, 15, 18, 19, 21, 22, 23, 25, 26, 27, 32, 33, 34, 35, 36, 39};
		} else if (boardName.equals("test Model")) {
			return new int[]{5, 1, 2, 8, 9, 4, 6, 3, 7, 5};
		}
		return new int[]{};
	}

	public static int[] getFixedInputPin(String boardName) {
		if (boardName.equals("Heltec Wifi LoRa 32(V2)")) { // i/o pin name and follow with position, first {no. of Pin each side, fixed board width}
			return new int[]{34, 35, 36, 37, 38, 39};
		} else if (boardName.equals("ESP-WROOM-32")) {
			return new int[]{34, 35, 36, 39};
		} else if (boardName.equals("test Model")) {
			return new int[]{34, 35, 36, 39};
		}
		return new int[]{};
	}
}
