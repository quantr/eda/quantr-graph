/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.arduino;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import hk.quantr.logic.LogicGateDrawer;
import static hk.quantr.logic.arduino.ArduinoMethod.*;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
 *
 * @author michelle
 */
public class ArduinoHeltecWifiOled extends Vertex {

	@XStreamOmitField
	public BufferedImage arduinoLogo;

	public ArduinoHeltecWifiOled(String name) {
		super(name, 0, 0, 10, 10);
		try {
			arduinoLogo = ImageIO.read(getClass().getResource("/hk/quantr/logic/icon/project/arduinoBoardLogo.png"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		properties.remove("Label");
		properties.put("Name", name);
		this.properties.put("Address", "0.0.0.0");
		this.properties.put("Orientation", "west");
	}

	@Override
	public void eventTriggerEval() {
		this.eval();
	}

	@Override
	public void eval() {
	}

	@Override
	public void simulateInit() {
		super.simulateInit();
	}

	@Override
	public void updateProperty() {
//		arduinoUpdatePin(this, "Arduino Wifi Oled");
//		arduinoWifiAddress((String) this.properties.get("Address"));
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize / 5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setFont(new Font("arial", Font.BOLD, this.gridSize - 3));
		g2.setColor(new Color(0, 146, 159));
		g2.fillRect(this.x * this.gridSize, this.y * this.gridSize, this.width * this.gridSize, this.height * this.gridSize);
		int[] orient;
		double rotate = 0;
		if (((String) this.properties.get("Orientation")).equals("east") || ((String) this.properties.get("Orientation")).equals("west")) {
			orient = pinNameLocation[0];
			if (((String) this.properties.get("Orientation")).equals("east")) {
				rotate = 0.5;
			} else {
				rotate = 0;
			}
			g2.setFont(new Font("arial", Font.BOLD, (int) (this.gridSize)));
			g2.setColor(Color.BLACK);
			g2.drawString(this.name, (this.x + this.width) * this.gridSize, this.y * this.gridSize + (this.height * this.gridSize) / 2);
		} else {
			orient = pinNameLocation[1];
			if (((String) this.properties.get("Orientation")).equals("north")) {
				rotate = 0.25;
			} else {
				rotate = 0.75;
			}
			g2.setFont(new Font("arial", Font.BOLD, (int) (this.gridSize)));
			g2.setColor(Color.BLACK);
			g2.drawString(this.name, this.x * this.gridSize, this.y * this.gridSize - this.gridSize);
		}
		if (arduinoLogo != null) {
			int logoWidth = (this.width > this.height ? this.height : this.width);
			int drawX = (this.x + (this.width > this.height ? (this.width - logoWidth) / 2 : 0)) * this.gridSize;
			int drawY = (this.y + (this.height > this.width ? (this.height - logoWidth) / 2 : 0)) * this.gridSize;
			logoWidth *= this.gridSize;
//			int logoHeight = (this.width > this.height ? this.height : this.width) * this.gridSize;
			AffineTransform backup = g2.getTransform();
			AffineTransform trans = new AffineTransform();
			trans.rotate((float) Math.toRadians(rotate * 360), drawX + (logoWidth / 2), drawY + (logoWidth / 2));
			g2.transform(trans);
			g2.drawImage(arduinoLogo, drawX, drawY, logoWidth, logoWidth, null);
			g2.setTransform(backup);
		}
		g2.setFont(new Font("arial", Font.BOLD, this.gridSize - 3));
		for (int i = 0; i < this.inputs.size(); i++) {
			this.inputs.get(i).paint(g, this.gridSize);
			g2.setColor(Color.white);
			String s;
			if (!this.inputs.get(i).name.contains("OLED")) {
				s = this.inputs.get(i).name.replaceAll("\\D+", "");
			} else {
				g2.setColor(Color.cyan);
				s = "OLED";
			}
			int k = s.length();

			if (this.inputs.get(i).deltaY == 0 || this.inputs.get(i).deltaX == 0) {
				g2.drawString(s, (this.inputs.get(i).getAbsolutionX()) * this.gridSize + orient[0] * this.gridSize / 2, (orient[1] + this.inputs.get(i).getAbsolutionY()) * this.gridSize);
			} else {
				g2.drawString(s, (orient[2] + this.inputs.get(i).getAbsolutionX()) * this.gridSize + (orient[2] - orient[2] * (2 - k)) * this.gridSize / 2, (this.inputs.get(i).getAbsolutionY()) * this.gridSize + orient[3] * this.gridSize / 2);
			}
		}
		for (int i = 0; i < this.outputs.size(); i++) {
			this.outputs.get(i).paint(g, this.gridSize);
			g2.setColor(Color.white);
			String s;
			s = this.outputs.get(i).name.replaceAll("\\D+", "");
			int k = s.length();
			if (this.outputs.get(i).deltaY == 0 || this.outputs.get(i).deltaX == 0) {
				g2.drawString(s, (this.outputs.get(i).getAbsolutionX()) * this.gridSize + orient[0] * this.gridSize / 2, (orient[1] + this.outputs.get(i).getAbsolutionY()) * this.gridSize);
			} else {
				g2.drawString(s, (orient[2] + this.outputs.get(i).getAbsolutionX()) * this.gridSize + (orient[2] - orient[2] * (2 - k)) * this.gridSize / 2, (this.outputs.get(i).getAbsolutionY()) * this.gridSize + orient[3] * this.gridSize / 2);
			}
		}
	}

}
