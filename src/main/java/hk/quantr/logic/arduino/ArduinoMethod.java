/*
 * Copyright 2023 ken.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.arduino;

import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author ken
 */
public class ArduinoMethod {

	public static int[][] pinNameLocation = new int[][]{{0, 1, 0, -1}, {1, 0, -1, 0}};
	public static BufferedImage arduinoLogo = null;

	public static void setLogo() {
		if (arduinoLogo == null) {
			try {
				arduinoLogo = ImageIO.read(ArduinoMethod.class.getResource("/hk/quantr/logic/icon/project/arduinoBoardLogo.png"));
			} catch (IOException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public static int[][] getSignal(String input) {
		String[] splitInput = input.split(",");
		String[][] result = new String[splitInput.length][2];
		int index = 0;
		for (String s : splitInput) {
			if (s.contains("=")) {
				String[] splitEquals = s.split("=");
				if (splitEquals.length == 2) {
					result[index][0] = splitEquals[0];
					result[index][1] = splitEquals[1];
					index++;
				}
			}
		}
		int[][] output = new int[index][2];
		for (int i = 0; i < index; i++) {
			if (result[i][0].matches("\\d+") && result[i][1].matches("\\d+")) {
				output[i][0] = Integer.parseInt(result[i][0]);
				output[i][1] = Integer.parseInt(result[i][1]);
			}
		}
		return output;
	}

	public static int getBaurate(String baud) {
		String temp = "";
		for (char c : baud.toCharArray()) {
			if (Character.isDigit(c)) {
				temp += c;
			}
			if (c == ' ') {
				break;
			}
		}
		return Integer.parseInt(temp);
	}

	public static boolean isPin(int[] map, int pin) {
		if (map.length > 0) {
			for (int i = 1; i < map.length; i++) {
				if (map[i] == pin) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isFixedInput(int[] map, int pin) {
		if (map.length > 0) {
			for (int i = 0; i < map.length; i++) {
				if (map[i] == pin) {
					return true;
				}
			}
		}
		return false;
	}

	public static void arduinoUpdatePin(Vertex v, String model) {
		ArrayList<Input> tempInput = (ArrayList<Input>) v.inputs.clone();
		ArrayList<Output> tempOutput = (ArrayList<Output>) v.outputs.clone();
		v.inputs.clear(); // clear pin
		v.outputs.clear();
		int[] pinMap = ArduinoPinMap.getPinMap((String) v.properties.get(model));
		int[] inputPinMap = ArduinoPinMap.getFixedInputPin((String) v.properties.get(model));

		if (!((String) v.properties.get(model)).equals("Choose a Model")) {
			if (pinMap.length > 0 || ((String) v.properties.get(model)).equals("Other")) { // if model exists 

				if (((String) v.properties.get(model)).equals("Other")) {
					if (!v.properties.containsKey("width")) {
						v.properties.put("width", 8);
					}
					if (v.properties.containsKey("Specified Pin ->")) {
						if (!v.properties.containsKey("Pin " + (String) v.properties.get("Specified Pin ->"))) {
							if (NumberUtils.isParsable((String) v.properties.get("Specified Pin ->"))) {
								v.properties.put("Pin " + (String) v.properties.get("Specified Pin ->"), "OUTPUT");
							}
						}
					} else {
						v.properties.put("Specified Pin ->", "");
					}
					ArrayList<Integer> tempPinMap = new ArrayList();
					int width = (int) v.properties.get("width");
					tempPinMap.add(width);
					for (int i = 0; i < v.properties.keySet().toArray().length; i++) {
						if (((String) v.properties.keySet().toArray()[i]).matches("Pin \\d+")) {
							tempPinMap.add(Integer.parseInt(((String) v.properties.keySet().toArray()[i]).replaceAll("\\D+", "")));
						}
					}
					pinMap = tempPinMap.stream().mapToInt(Integer::intValue).toArray();
				} else {
					if (v.properties.containsKey("width")) {
						v.properties.remove("width");
					}
				}
				String orient = (String) v.properties.get("Orientation");
				if (orient.equals("east")) {
					v.height = pinMap[0];
					v.width = pinMap.length;
				} else if (orient.equals("west")) {
					v.height = pinMap[0];
					v.width = pinMap.length;
				} else if (orient.equals("south")) {
					v.height = pinMap.length;
					v.width = pinMap[0];
				} else if (orient.equals("north")) {
					v.height = pinMap.length;
					v.width = pinMap[0];
				}
				if (model.equals("Arduino Wifi Oled") || model.contains("Arduino Serial Oled")) { // OLED
					if (!v.properties.containsKey("OLED Bits")) {
						v.properties.put("OLED Bits", 12);
					}
					int index = -1;
					for (int i = 0; i < tempInput.size(); i++) {
						Input input = tempInput.get(i);
						if (input.name.contains("OLED")) {
							index = i;
							break;
						}
					}
					if (orient.equals("east")) {
						v.width = pinMap.length + pinMap.length % 2;
					} else if (orient.equals("west")) {
						v.width = pinMap.length + pinMap.length % 2;
					} else if (orient.equals("south")) {
						v.height = pinMap.length + pinMap.length % 2;
					} else if (orient.equals("north")) {
						v.height = pinMap.length + pinMap.length % 2;
					}
					if (index == -1) {
						v.inputs.add(new Input(v, "OLED"));
					} else {
						v.inputs.add(tempInput.get(index));
					}
					setPinLocation(v, v.inputs.get(v.inputs.size() - 1), pinMap.length, orient);
					System.out.println(v.inputs.get(v.inputs.size() - 1).deltaX + ", " + v.inputs.get(v.inputs.size() - 1).deltaY);
					if (((int) v.properties.get("OLED Bits")) > 12) {
						v.properties.put("OLED Bits", 12);
						v.inputs.get(v.inputs.size() - 1).bits = 12;
					} else {
						v.inputs.get(v.inputs.size() - 1).bits = (int) v.properties.get("OLED Bits");
					}
				}

				for (int i = 1; i < pinMap.length; i++) {
					if (v.properties.containsKey("Pin " + pinMap[i])) {				// if the pin alr in property table
						if (v.properties.get("Pin " + pinMap[i]).equals("INPUT")) { // then only add back the port
							if (ifOutputPortExist(tempOutput, pinMap[i]) != -1) {
								v.outputs.add(tempOutput.get(ifOutputPortExist(tempOutput, pinMap[i])));
							} else {
								v.outputs.add(new Output(v, "Pin " + pinMap[i]));
							}
							setPinLocation(v, v.outputs.get(v.outputs.size() - 1), i, orient);
						} else if (v.properties.get("Pin " + pinMap[i]).equals("OUTPUT")) {
							if (ifInputPortExist(tempInput, pinMap[i]) != -1) {
								v.inputs.add(tempInput.get(ifInputPortExist(tempInput, pinMap[i])));
							} else {
								v.inputs.add(new Input(v, "Pin " + pinMap[i]));
							}
							setPinLocation(v, v.inputs.get(v.inputs.size() - 1), i, orient);
						}
					} else if (isFixedInput(inputPinMap, pinMap[i])) {				// if the pin is fixed to be input 
						if (v.properties.containsKey("Input Pin " + pinMap[i])) {	// check if the pin exist in property table
						} else {
							v.properties.put("Input Pin " + pinMap[i], "INPUT");	// not exist in property table then add back
						}
						if (ifOutputPortExist(tempOutput, pinMap[i]) != -1) {		// add back fixed input port
							v.outputs.add(tempOutput.get(ifOutputPortExist(tempOutput, pinMap[i])));
						} else {
							v.outputs.add(new Output(v, "Pin " + pinMap[i]));
						}
						setPinLocation(v, v.outputs.get(v.outputs.size() - 1), i, orient);
					} else {
						v.properties.put("Pin " + pinMap[i], "OUTPUT");				// add back property
						if (ifInputPortExist(tempInput, pinMap[i]) != -1) {
							v.inputs.add(tempInput.get(ifInputPortExist(tempInput, pinMap[i])));
						} else {
							v.inputs.add(new Input(v, "Pin " + pinMap[i]));
						}
						setPinLocation(v, v.inputs.get(v.inputs.size() - 1), i, orient);
					}
				}
				Object[] array = v.properties.keySet().toArray();
				String setting = ",";
				for (int i = 0; i < array.length; i++) { // remove wrong Pin from property table
					if (((String) array[i]).contains("Pin") && !((String) array[i]).contains("Specified")) {
						String s = (String) array[i];
						if (isPin(pinMap, Integer.parseInt(s.replaceAll("\\D+", "")))) {
							setting += s.replaceAll("\\D+", "") + "=";
							if (((String) v.properties.get((String) array[i])).equals("OUTPUT")) {
								setting += 3;
							} else {
								setting += 1;
							}
							setting += ",";

							continue;
						}
						v.properties.remove((String) array[i]);
					}
				}
				System.out.println(setting);
				setting += "Setting";
				arduinoSetPin(setting, model, v);
			} else { // if it is "Other"
//				for (int i = 0; i < v.properties.keySet().toArray().length; i++) {
//					if (v.properties.containsKey("Specified Pin ->")) {
//						if (!v.properties.containsKey("Pin " + (String) v.properties.get("Specified Pin ->"))) {
//							if (NumberUtils.isParsable((String) v.properties.get("Specified Pin ->"))) {
//								v.properties.put("Pin " + (String) v.properties.get("Specified Pin ->"), "OUTPUT");
//							}
//						}
//					} else {
//						v.properties.put("Specified Pin ->", "");
//					}
//				}
			}
		}
	}

	public static int ifInputPortExist(ArrayList<Input> arrayList, int num) {
		int index = -1;
		for (int i = 0; i < arrayList.size(); i++) {
			Input input = arrayList.get(i);
			if (input.name.contains(String.valueOf(num))) {
				index = i;
				break;
			}
		}
		return index;
	}

	public static int ifOutputPortExist(ArrayList<Output> arrayList, int num) {
		int index = -1;
		for (int i = 0; i < arrayList.size(); i++) {
			Output output = arrayList.get(i);
			if (output.name.contains(String.valueOf(num))) {
				index = i;
				break;
			}
		}
		return index;
	}

	public static void arduinoSetPin(String setting, String model, Vertex v) {
		if (model.equals("Arduino Wifi") || model.equals("Arduino Wifi Oled")) {
			try {
				ArduinoServer.setPin(setting);
			} catch (IOException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			} catch (InterruptedException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else if (model.equals("Arduino Serial")) {
			arduinoSerialSetPin(setting, (ArduinoSerial) v);
		} else if (model.equals("Arduino Serial Oled")) {
			arduinoSerialOledSetPin(setting, (ArduinoSerialOled) v);
		}
	}

	public static void arduinoSerialSetPin(String setting, ArduinoSerial v) {
		if (v.port != null) {
			v.port.writeBytes(new byte[]{(byte) setting.getBytes().length}, 1);
			v.port.writeBytes(setting.getBytes(), setting.length());
			if (v.port.bytesAvailable() >= 0) {
				byte[] bytes = new byte[v.port.bytesAvailable()];
			}
//			v.port.readBytes(bytes, v.port.bytesAvailable());
		} else {
			try {
				v.connectArduinoSerial();
			} catch (IOException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			} catch (InterruptedException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public static void arduinoSerialOledSetPin(String setting, ArduinoSerialOled v) {
		if (v.port != null) {
			v.port.writeBytes(new byte[]{(byte) setting.getBytes().length}, 1);
			v.port.writeBytes(setting.getBytes(), setting.length());
			if (v.port.bytesAvailable() >= 0) {
				byte[] bytes = new byte[v.port.bytesAvailable()];
			}
//			v.port.readBytes(bytes, v.port.bytesAvailable());
		} else {
			try {
				v.connectArduinoSerial();
			} catch (IOException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			} catch (InterruptedException ex) {
				Logger.getLogger(ArduinoMethod.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public static void arduinoWifiAddress(String address) {
		ArduinoServer.serverUrlString = "http://" + address + "/";
	}

	public static void arduinoPaint(Graphics g, Vertex v) {
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(v.gridSize / 5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setFont(new Font("arial", Font.BOLD, v.gridSize - 3));
		g2.setColor(new Color(0, 146, 159));
		g2.fillRect(v.x * v.gridSize, v.y * v.gridSize, v.width * v.gridSize, v.height * v.gridSize);
		int[] orient;
		double rotate = 0;
		if (((String) v.properties.get("Orientation")).equals("east") || ((String) v.properties.get("Orientation")).equals("west")) {
			orient = pinNameLocation[0];
			if (((String) v.properties.get("Orientation")).equals("east")) {
				rotate = 0.5;
			} else {
				rotate = 0;
			}
			g2.setFont(new Font("arial", Font.BOLD, (int) (v.gridSize)));
			g2.setColor(Color.BLACK);
			g2.drawString(v.name, (v.x + v.width) * v.gridSize, v.y * v.gridSize + (v.height * v.gridSize) / 2);
		} else {
			orient = pinNameLocation[1];
			if (((String) v.properties.get("Orientation")).equals("north")) {
				rotate = 0.25;
			} else {
				rotate = 0.75;
			}
			g2.setFont(new Font("arial", Font.BOLD, (int) (v.gridSize)));
			g2.setColor(Color.BLACK);
			g2.drawString(v.name, v.x * v.gridSize, v.y * v.gridSize - v.gridSize);
		}
		if (arduinoLogo != null) {
			int logoWidth = (v.width > v.height ? v.height : v.width);
			int drawX = (v.x + (v.width > v.height ? (v.width - logoWidth) / 2 : 0)) * v.gridSize;
			int drawY = (v.y + (v.height > v.width ? (v.height - logoWidth) / 2 : 0)) * v.gridSize;
			logoWidth *= v.gridSize;
//			int logoHeight = (v.width > v.height ? v.height : v.width) * v.gridSize;
			AffineTransform backup = g2.getTransform();
			AffineTransform trans = new AffineTransform();
			trans.rotate((float) Math.toRadians(rotate * 360), drawX + (logoWidth / 2), drawY + (logoWidth / 2));
			g2.transform(trans);
			g2.drawImage(arduinoLogo, drawX, drawY, logoWidth, logoWidth, null);
			g2.setTransform(backup);
		}
		g2.setFont(new Font("arial", Font.BOLD, v.gridSize - 3));
		for (int i = 0; i < v.inputs.size(); i++) {
			v.inputs.get(i).paint(g, v.gridSize);
			g2.setColor(Color.white);
			String s;
			if (!v.inputs.get(i).name.contains("OLED")) {
				s = v.inputs.get(i).name.replaceAll("\\D+", "");
			} else {
				g2.setColor(Color.cyan);
				s = "OLED";
			}
			int k = s.length();

			if (v.inputs.get(i).deltaY == 0 || v.inputs.get(i).deltaX == 0) {
				g2.drawString(s, (v.inputs.get(i).getAbsolutionX()) * v.gridSize + orient[0] * v.gridSize / 2, (orient[1] + v.inputs.get(i).getAbsolutionY()) * v.gridSize);
			} else {
				g2.drawString(s, (orient[2] + v.inputs.get(i).getAbsolutionX()) * v.gridSize + (orient[2] - orient[2] * (2 - k)) * v.gridSize / 2, (v.inputs.get(i).getAbsolutionY()) * v.gridSize + orient[3] * v.gridSize / 2);
			}
		}
		for (int i = 0; i < v.outputs.size(); i++) {
			v.outputs.get(i).paint(g, v.gridSize);
			g2.setColor(Color.white);
			String s;
			s = v.outputs.get(i).name.replaceAll("\\D+", "");
			int k = s.length();
			if (v.outputs.get(i).deltaY == 0 || v.outputs.get(i).deltaX == 0) {
				g2.drawString(s, (v.outputs.get(i).getAbsolutionX()) * v.gridSize + orient[0] * v.gridSize / 2, (orient[1] + v.outputs.get(i).getAbsolutionY()) * v.gridSize);
			} else {
				g2.drawString(s, (orient[2] + v.outputs.get(i).getAbsolutionX()) * v.gridSize + (orient[2] - orient[2] * (2 - k)) * v.gridSize / 2, (v.outputs.get(i).getAbsolutionY()) * v.gridSize + orient[3] * v.gridSize / 2);
			}
		}
	}

	public static void setPinLocation(Vertex v, Port p, int i, String orient) {
		i *= 2;
		i -= 1;
		if (orient.equals("west")) {
			if (i > v.width - 1) {
				p.setLocation(i - v.width + (v.width % 2), v.height);
			} else {
				p.setLocation(i, 0);
			}
		} else if (orient.equals("east")) {
			if (i > v.width - 1) {
				p.setLocation(i - v.width + (v.width % 2), 0);
			} else {
				p.setLocation(i, v.height);
			}
		} else if (orient.equals("north")) {
			if (i > v.height - 1) {
				p.setLocation(0, i - v.height + (v.height % 2));
			} else {
				p.setLocation(v.width, i);
			}
		} else if (orient.equals("south")) {
			if (i > v.height - 1) {
				p.setLocation(v.width, v.height * 2 - (v.height % 2) - i);
			} else {
				p.setLocation(0, v.height - i);
			}
		}
	}
}
