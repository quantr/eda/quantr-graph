/*
 * Copyright 2023 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.arduino;

import java.io.*;
import com.fazecast.jSerialComm.SerialPort;
import static hk.quantr.logic.arduino.ArduinoMethod.getBaurate;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Arrays;
//import javax.comm.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michelle
 */
public class ArduinoSerial extends Vertex {

	public SerialPort port = null;

	public ArduinoSerial(String name) {
		super(name, 0, 0, 10, 10);
		this.properties.remove("Label");
		properties.put("Name", name);
		this.properties.put("Arduino Serial", "Choose a model");
		this.properties.put("Orientation", "west");
		this.properties.put("Serial Port", "COM3");
		this.properties.put("Baurate", "115200 baud");
	}

	public void paint(Graphics g) {
		ArduinoMethod.arduinoPaint(g, this);
		super.paint(g);
	}

	@Override
	public void eventTriggerEval() {
		this.eval();
	}

	@Override
	public void eval() {
		try {
			System.out.println(port);
			if (checkIsOpen()) {
				readSerialSignal();
				super.eval();
			} else {
				updateProperty();
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public String getTypeName() {
		return "Arduino Serial";
	}

	@Override
	public void simulateInit() {
		super.simulateInit();
		updateProperty();
	}

	@Override
	public void updateProperty() {
		ArduinoMethod.arduinoUpdatePin(this, "Arduino Serial");
		try {
			connectArduinoSerial();
		} catch (IOException ex) {
			Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InterruptedException ex) {
			Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public void connectArduinoSerial() throws IOException, InterruptedException {
//		SerialPort[] serialPorts = SerialPort.getCommPorts();
//		SerialPort.getCommPort((String) properties.get("Serial Port"));
//		for (SerialPort port : serialPorts) {
//			if (port.toString().equals("USB Serial")) {
//				if (port.openPort()) {
//					System.out.println("opened port");
//					this.port = port;
//					System.out.println("port addr:" + port);
//				}
//				port.setBaudRate(getBaurate((String) properties.get("Baurate")));
//
//			}
//		}
		SerialPort[] serialPorts = SerialPort.getCommPorts();
//		SerialPort.getCommPort((String) properties.get("Serial Port"));
		String s = "connect Arduino"; // 15 bytes
		for (SerialPort port : serialPorts) {
			if (port == null) {
				continue;
			}
			this.port = port;
			System.out.println(port.toString());
		}
		if (port == null) {
			return;
		}
		System.out.println(port.isOpen());
//		if (port.toString().contains("COM7")) {
		port.openPort();
		if (port.isOpen()) {
			port.setBaudRate(getBaurate((String) properties.get("Baurate")));
			System.out.println("opened port");
			port.writeBytes(new byte[]{(byte) s.getBytes().length}, 1);
			port.writeBytes(s.getBytes(), s.length());
			port.getOutputStream().write("hello".getBytes());
			int z = 0;
			while (z < 10) {
				if (port.bytesAvailable() >= 0) {
					byte[] count = new byte[1];
					port.readBytes(count, 1);
					byte[] bytes = new byte[count[0]];
//				Thread.sleep(500);
					int k = 0;
					while (port.bytesAvailable() < count[0] && k < 10) { // waiting full bytes to receive
						Thread.sleep(1);
						k++;
					}

					port.readBytes(bytes, count[0]); // readbytes
//						port.closePort();
					System.out.println(new String(bytes, "UTF-8"));
					if (s.equals(new String(bytes, "UTF-8"))) {
						System.out.println("Connected Arduino");
						return;
					}
				} else {
					System.out.println("cannot connect");
				}
				z++;
				Thread.sleep(1);
			}
		}
	}

	public boolean checkIsOpen() {
		if (port != null) {
			System.out.println(port.getLastErrorCode());
			if (port.getLastErrorCode() != 0) {
				port.closePort();
				port = null;
				try {
					connectArduinoSerial();
				} catch (IOException ex) {
					Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
				} catch (InterruptedException ex) {
					Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
				}
				return false;
			}
			return port.isOpen();
		} else {
			try {
				connectArduinoSerial();
			} catch (IOException ex) {
				Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
			} catch (InterruptedException ex) {
				Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return false;
	}

	public void readSerialSignal() throws UnsupportedEncodingException, InterruptedException {
//		String signal = "";
//		for (int i = 0; i < inputs.size(); i++) {
//			signal += inputs.get(i).name.replaceAll("\\D+", "") + "=";
//			signal += (inputs.get(i).value & 1) + ",";
//		}
//		port.writeBytes(new byte[]{(byte) signal.getBytes().length}, 1);
//		port.writeBytes(signal.getBytes(), signal.length());
//		Thread.sleep(50);
//		while (true) {
//			if (port.bytesAvailable() >= 0) {
//				byte[] count = new byte[1];
//				port.readBytes(count, 1);
//				System.out.println("count: " + count[0]);
//				if (count[0] == 0) {
//					continue;
//				}
//				byte[] bytes = new byte[count[0]];
////				Thread.sleep(500);
//				port.readBytes(bytes, count[0]);
//				int[][] display = ArduinoMethod.getSignal(new String(bytes, "UTF-8"));
//				System.out.println(Arrays.deepToString(display));
//				for (int i = 0; i < display.length; i++) {
//					for (int j = 0; j < this.outputs.size(); j++) {
//						if (this.outputs.get(j).name.contains("Pin " + String.valueOf(display[i][0]))) {
//							this.outputs.get(j).value = display[i][1] & 1;
//						}
//					}
//				}
//				super.eval();
//				return;
//			}
//		}
		String signal = ",";
		for (int i = 0; i < inputs.size(); i++) {
			signal += inputs.get(i).name.replaceAll("\\D+", "") + "=";
			signal += (inputs.get(i).value & 1) + ",";
		}
		signal += "Quantr-Logic";
		System.out.println(signal);
		port.writeBytes(new byte[]{(byte) signal.getBytes().length}, 1);
		port.writeBytes(signal.getBytes(), signal.length());
		System.out.println(signal.getBytes().length);
		System.out.println(signal.length());
		Thread.sleep(20);
		int z = 0;
		while (z < 10) {
			if (port.bytesAvailable() >= 0) {
				byte[] count = new byte[1];
				port.readBytes(count, 1);
				if (count[0] == 0) {
					z++;
					Thread.sleep(10);
					continue;
				}
				byte[] bytes = new byte[count[0]];
//				Thread.sleep(500);
				int k = 0;
				while (port.bytesAvailable() < count[0] && k < 100) { // waiting full bytes to receive
					Thread.sleep(1);
					k++;
				}
				port.readBytes(bytes, count[0]);

				int[][] display = ArduinoMethod.getSignal(new String(bytes, "UTF-8"));
				System.out.println("display" + Arrays.deepToString(display));
				for (int i = 0; i < display.length; i++) {
					for (int j = 0; j < this.outputs.size(); j++) {
						if (this.outputs.get(j).name.contains("Pin " + String.valueOf(display[i][0]))) {
							this.outputs.get(j).value = display[i][1] & 1;
						}
					}
				}
				super.eval();
				return;
			}
		}
	}
}
