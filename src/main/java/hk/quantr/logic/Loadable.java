/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Loadable {

	public String name;
	public String category;
	public String description;
	public String icon;
	public String web;
	public String className;
	public String jar;

	public Loadable(String name, String category, String description, String icon, String web, String className, String jar) {
		this.name = name;
		this.category = category;
		this.description = description;
		this.icon = icon;
		this.web = web;
		this.className = className;
		this.jar = jar;
	}

}
