/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic;

import hk.quantr.logic.data.flipflop.DFlipflop;
import hk.quantr.logic.data.flipflop.DLatch;
import hk.quantr.logic.data.flipflop.JKFlipflop;
import hk.quantr.logic.data.flipflop.SRFlipflop;
import hk.quantr.logic.data.flipflop.TFlipflop;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class LogicGateDrawer {

	public static Graphics2D init(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		return g2;
	}

	public static void drawAndGate(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawPolyline(new int[]{3 * gridSize - gridSize / 10 * 2 + x, gridSize / 10 * 2 + x, gridSize / 10 * 2 + x, 3 * gridSize - gridSize / 10 * 2 + x},
				new int[]{0 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 0 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y,
					4 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 4 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 4);
		g2.drawArc(gridSize - gridSize / 10 * 2 + x, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y,
				4 * gridSize, 4 * gridSize, 90, -180);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, v.y * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 - 2) * gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, (v.y + v.height) * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 + 2) * gridSize);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawOrGate(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.blue);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		if (v.inputs.size() % 2 == 1) {
			g2.drawPolyline(new int[]{x, gridSize / 10 * 10 + x}, new int[]{2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		}
		g2.setColor(Color.black);
		g2.drawArc(x - 1 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize, 4 * gridSize, 90, -180);
		g2.drawArc(x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 12 * gridSize, 9 * gridSize, 90, -56);
		g2.drawArc(x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y - 5 * gridSize, 12 * gridSize, 9 * gridSize, -90, 56);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, v.y * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 - 2) * gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, (v.y + v.height) * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 + 2) * gridSize);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawNandGate(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawPolyline(new int[]{3 * gridSize - gridSize / 10 * 2 + x, gridSize / 10 * 2 + x, gridSize / 10 * 2 + x, 3 * gridSize - gridSize / 10 * 2 + x},
				new int[]{0 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 0 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y,
					4 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 4 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 4);
		g2.drawArc(gridSize - gridSize / 10 * 2 + x, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y,
				4 * gridSize, 4 * gridSize, 90, -180);
		g2.drawOval(5 * gridSize - gridSize / 10 * 2 + x, 2 * gridSize - gridSize / 2 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, gridSize, gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, v.y * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 - 2) * gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, (v.y + v.height) * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 + 2) * gridSize);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);

	}

	public static void drawNorGate(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.blue);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		if (v.inputs.size() % 2 == 1) {
			g2.drawPolyline(new int[]{x, gridSize / 10 * 10 + x}, new int[]{2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		}
		g2.setColor(Color.black);
		g2.drawArc(x - 1 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize, 4 * gridSize, 90, -180);
		g2.drawArc(x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 12 * gridSize, 9 * gridSize, 90, -56);
		g2.drawArc(x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y - 5 * gridSize, 12 * gridSize, 9 * gridSize, -90, 56);
		g2.drawOval(5 * gridSize - gridSize / 10 + x, 2 * gridSize - gridSize / 2 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, gridSize, gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, v.y * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 - 2) * gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, (v.y + v.height) * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 + 2) * gridSize);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawXorGate(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.blue);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		if (v.inputs.size() % 2 == 1) {
			g2.drawPolyline(new int[]{x, gridSize / 10 * 10 + x}, new int[]{2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		}
		g2.setColor(Color.black);
		g2.drawArc(x - 1 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize, 4 * gridSize, 90, -180);
		g2.drawArc(x, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize, 4 * gridSize, 90, -180);
		g2.drawArc(1 * gridSize + x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 12 * gridSize, 9 * gridSize, 90, -56);
		g2.drawArc(1 * gridSize + x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y - 5 * gridSize, 12 * gridSize, 9 * gridSize, -90, 56);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, v.y * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 - 2) * gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, (v.y + v.height) * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 + 2) * gridSize);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawXnorGate(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.blue);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 1 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		g2.drawPolyline(new int[]{x, gridSize / 10 * 8 + x}, new int[]{3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 3 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		if (v.inputs.size() % 2 == 1) {
			g2.drawPolyline(new int[]{x, gridSize / 10 * 10 + x}, new int[]{2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y}, 2);
		}
		g2.setColor(Color.black);
		g2.drawArc(x - 1 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize, 4 * gridSize, 90, -180);
		g2.drawArc(x, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 2 * gridSize, 4 * gridSize, 90, -180);
		g2.drawArc(1 * gridSize + x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, 12 * gridSize, 9 * gridSize, 90, -56);
		g2.drawArc(1 * gridSize + x - 6 * gridSize, (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y - 5 * gridSize, 12 * gridSize, 9 * gridSize, -90, 56);
		g2.drawOval(6 * gridSize - gridSize / 10 + x, 2 * gridSize - gridSize / 2 + (v.inputs.size() > 5 ? v.inputs.size() / 2 - 2 : 0) * gridSize + y, gridSize, gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, v.y * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 - 2) * gridSize);
		g2.drawLine(v.x * gridSize + gridSize / 10 * 2, (v.y + v.height) * gridSize, v.x * gridSize + gridSize / 10 * 2, (v.y + v.height / 2 + 2) * gridSize);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawNotGate(Graphics g, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.blue);
		g2.fillOval(x - gridSize / 10 * 2, 1 * gridSize - gridSize / 10 * 2 + y, gridSize / 10 * 4, gridSize / 10 * 4);
		g2.setColor(Color.black);
		g2.drawPolyline(new int[]{2 * gridSize - gridSize / 10 + x, gridSize / 10 * 2 + x, gridSize / 10 * 2 + x, 2 * gridSize - gridSize / 10 + x},
				new int[]{gridSize + y, y + gridSize / 10 * 2, 2 * gridSize - gridSize / 10 * 2 + y, gridSize + y}, 4);
		g2.drawOval(2 * gridSize - gridSize / 10 + x, gridSize - gridSize / 2 + y, gridSize, gridSize);
		g2.setColor(Color.green);
		g2.fillOval(3 * gridSize - gridSize / 10 + x, gridSize - gridSize / 10 * 2 + y, gridSize / 10 * 4, gridSize / 10 * 4);
	}

	public static void drawDLatch(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x + gridSize, y, 4 * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawLine(x, y + gridSize, x + gridSize, y + gridSize);
		g2.drawLine(x, y + 5 * gridSize, x + gridSize, y + 5 * gridSize);
		g2.drawLine(x + 5 * gridSize, y + gridSize, x + 5 * gridSize + gridSize, y + gridSize);
		g2.drawOval((x + 5 * gridSize), y + (gridSize * 9 / 2), gridSize, gridSize);
//		g2.drawPolyline(new int[]{x + gridSize, x + gridSize * 2, x + gridSize}, new int[]{y + gridSize * 9 / 2, y + gridSize * 5, y + gridSize * 11 / 2}, 3);
		g2.drawRect(x + gridSize, y, (v.width - 2) * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString("D", x + gridSize * 3 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 3 / 2);
		g2.drawString("E", x + gridSize * 3 / 2, y + gridSize * 11 / 2);
		g2.drawString("Q'", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 11 / 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, 10));
		g2.drawString("S", x + gridSize * 13 / 5, y + gridSize * 3 / 2);
		g2.drawString("R", x + gridSize * 13 / 5, y + gridSize * 11 / 2);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		((DLatch) v).outputs.get(1).paint(g, gridSize);
	}

	public static void drawDFlipflop(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x + gridSize, y, 4 * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawLine(x, y + gridSize, x + gridSize, y + gridSize);
		g2.drawLine(x, y + 5 * gridSize, x + gridSize, y + 5 * gridSize);
		g2.drawLine(x + 5 * gridSize, y + gridSize, x + 5 * gridSize + gridSize, y + gridSize);
		g2.drawOval((x + 5 * gridSize), y + (gridSize * 9 / 2), gridSize, gridSize);
		g2.drawPolyline(new int[]{x + gridSize, x + gridSize * 2, x + gridSize}, new int[]{y + gridSize * 9 / 2, y + gridSize * 5, y + gridSize * 11 / 2}, 3);
		g2.drawRect(x + gridSize, y, (v.width - 2) * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString("D", x + gridSize * 3 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q'", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 11 / 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("S", x + gridSize * 13 / 5, y + gridSize * 3 / 2);
		g2.drawString("R", x + gridSize * 13 / 5, y + gridSize * 11 / 2);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		((DFlipflop) v).outputs.get(1).paint(g, gridSize);
	}

	public static void drawTFlipflop(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x + gridSize, y, 4 * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawLine(x, y + gridSize, x + gridSize, y + gridSize);
		g2.drawLine(x, y + 5 * gridSize, x + gridSize, y + 5 * gridSize);
		g2.drawLine(x + 5 * gridSize, y + gridSize, x + 5 * gridSize + gridSize, y + gridSize);
		g2.drawOval((x + 5 * gridSize), y + (gridSize * 9 / 2), gridSize, gridSize);
		g2.drawPolyline(new int[]{x + gridSize, x + gridSize * 2, x + gridSize}, new int[]{y + gridSize * 9 / 2, y + gridSize * 5, y + gridSize * 11 / 2}, 3);
		g2.drawRect(x + gridSize, y, (v.width - 2) * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString("T", x + gridSize * 3 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q'", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 11 / 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("S", x + gridSize * 13 / 5, y + gridSize * 3 / 2);
		g2.drawString("R", x + gridSize * 13 / 5, y + gridSize * 11 / 2);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		((TFlipflop) v).outputs.get(1).paint(g, gridSize);
	}

	public static void drawJKFlipflop(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x + gridSize, y, 4 * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawLine(x, y + gridSize, x + gridSize, y + gridSize);
		g2.drawLine(x, y + 3 * gridSize, x + gridSize, y + 3 * gridSize);
		g2.drawLine(x, y + 5 * gridSize, x + gridSize, y + 5 * gridSize);
		g2.drawLine(x + 5 * gridSize, y + gridSize, x + 5 * gridSize + gridSize, y + gridSize);
		g2.drawOval((x + 5 * gridSize), y + (gridSize * 9 / 2), gridSize, gridSize);
		g2.drawPolyline(new int[]{x + gridSize, x + gridSize * 2, x + gridSize}, new int[]{y + gridSize * 5 / 2, y + gridSize * 3, y + gridSize * 7 / 2}, 3);
		g2.drawRect(x + gridSize, y, (v.width - 2) * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString("J", x + gridSize * 3 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 3 / 2);
		g2.drawString("K", x + gridSize * 3 / 2, y + gridSize * 11 / 2);
		g2.drawString("Q'", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 11 / 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("S", x + gridSize * 13 / 5, y + gridSize * 3 / 2);
		g2.drawString("R", x + gridSize * 13 / 5, y + gridSize * 11 / 2);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		((JKFlipflop) v).outputs.get(1).paint(g, gridSize);
	}

	public static void drawSRFlipflop(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x + gridSize, y, 4 * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawLine(x, y + gridSize, x + gridSize, y + gridSize);
		g2.drawLine(x, y + 3 * gridSize, x + gridSize, y + 3 * gridSize);
		g2.drawLine(x, y + 5 * gridSize, x + gridSize, y + 5 * gridSize);
		g2.drawLine(x + 5 * gridSize, y + gridSize, x + 5 * gridSize + gridSize, y + gridSize);
		g2.drawOval((x + 5 * gridSize), y + (gridSize * 9 / 2), gridSize, gridSize);
		g2.drawPolyline(new int[]{x + gridSize, x + gridSize * 2, x + gridSize}, new int[]{y + gridSize * 5 / 2, y + gridSize * 3, y + gridSize * 7 / 2}, 3);
		g2.drawRect(x + gridSize, y, (v.width - 2) * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, (int) (gridSize * 1.4f)));
		g2.drawString("S", x + gridSize * 3 / 2, y + gridSize * 3 / 2);
		g2.drawString("Q", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 3 / 2);
		g2.drawString("R", x + gridSize * 3 / 2, y + gridSize * 11 / 2);
		g2.drawString("Q'", (x + 3 * gridSize) + gridSize * 1 / 2, y + gridSize * 11 / 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("S", x + gridSize * 13 / 5, y + gridSize * 3 / 2);
		g2.drawString("R", x + gridSize * 13 / 5, y + gridSize * 11 / 2);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		((SRFlipflop) v).outputs.get(1).paint(g, gridSize);
	}

	public static void drawAdder(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.drawLine(x + gridSize, y + gridSize * 2, x + gridSize * 3, y + gridSize * 2);
		g2.drawLine(x + gridSize * 2, y + gridSize, x + gridSize * 2, y + gridSize * 3);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("c in", x + gridSize, y + gridSize);
		g2.drawString("c out", x + gridSize * 2 / 3, y + gridSize * 15 / 4);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		v.outputs.get(1).paint(g, gridSize);
	}

	public static void drawSubtractor(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.drawLine(x + gridSize, y + gridSize * 2, x + gridSize * 3, y + gridSize * 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("b in", x + gridSize, y + gridSize);
		g2.drawString("b out", x + gridSize * 2 / 3, y + gridSize * 15 / 4);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		v.outputs.get(1).paint(g, gridSize);
	}

	public static void drawMultiplier(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.drawLine(x + gridSize, y + gridSize, x + gridSize * 3, y + gridSize * 3);
		g2.drawLine(x + gridSize * 3, y + gridSize, x + gridSize, y + gridSize * 3);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("c in", x + gridSize, y + gridSize);
		g2.drawString("upper", x + gridSize * 2 / 3, y + gridSize * 15 / 4);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		v.outputs.get(1).paint(g, gridSize);
	}

	public static void drawDivider(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.fillOval(x + v.width * gridSize / 2 - gridSize / 4, y + v.height * gridSize * 1 / 4 - gridSize / 4, gridSize / 2, gridSize / 2);
		g2.drawLine(x + gridSize, y + gridSize * 2, x + gridSize * 3, y + gridSize * 2);
		g2.fillOval(x + v.width * gridSize / 2 - gridSize / 4, y + v.height * gridSize * 3 / 4 - gridSize / 4, gridSize / 2, gridSize / 2);
		g2.setColor(Color.gray);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString("div", x + gridSize, y + gridSize);
		g2.drawString("rem", x + gridSize, y + gridSize * 15 / 4);
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
		v.outputs.get(1).paint(g, gridSize);
	}

	public static void drawShifter(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke, String type) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 2));
		switch (type) {
			case "logical left":
				g2.drawString("<<", x + gridSize / 3, y + gridSize * 5 / 2);
				break;
			case "logical right":
				g2.drawString(">>", x + gridSize / 3, y + gridSize * 5 / 2);
				break;
			default:
				g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 2));
				g2.drawString(">>>", x + gridSize / 3, y + gridSize * 5 / 2);
				break;
		}
		for (int z = 0; z < v.inputs.size(); z++) {
			v.inputs.get(z).paint(g, gridSize);
		}
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawNegator(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 2));
		g2.drawString("-x", x + gridSize * 3 / 2, y + gridSize * 5 / 2);
		v.inputs.get(0).paint(g, gridSize);
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawBitExtender(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize));
		g2.drawString(Integer.toString(v.inputs.get(0).bits), x + gridSize * 1 / 2, y + gridSize * 5 / 2);
		g2.drawString(Integer.toString(v.outputs.get(0).bits), x + gridSize * 3, y + gridSize * 5 / 2);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 2));
		g2.drawString("\u2192", x + gridSize * 3 / 2, y + gridSize * 5 / 2);
		v.inputs.get(0).paint(g, gridSize);
		v.outputs.get(0).paint(g, gridSize);
	}

	public static void drawBitSelector(Graphics g, Vertex v, int x, int y, int gridSize, Stroke stroke, int n) {
		Graphics2D g2 = init(g);
		g2.setStroke(stroke);
		g2.setColor(Color.white);
		g2.fillRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setColor(Color.black);
		g2.drawRect(x, y, v.width * gridSize, v.height * gridSize);
		g2.setFont(new Font("arial", Font.BOLD, gridSize * 3 / 2));
		g2.drawString("[" + n + "]", x + gridSize, y + gridSize * 5 / 2);
		v.inputs.get(0).paint(g, gridSize);
		v.inputs.get(1).paint(g, gridSize);
		v.outputs.get(0).paint(g, gridSize);
	}
}
