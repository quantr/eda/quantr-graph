/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class IPHelper {

	static Boolean b = false;

	static final Set<String> whitelist = new HashSet<>(Arrays.asList("223.255.176.242", "123.202.209.162"));

	static final String[] youtube = {"https://www.youtube.com/watch?v=QBmnb-E7pTA"};

	static {
		try {
			whitelist.add(InetAddress.getByName("SmartHome01.asuscomm.com").getHostAddress());
		} catch (UnknownHostException ex) {
//			Logger.getLogger(IPHelper.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static boolean isInHongKong() {
        return false;
//		if (b == null) {
//			try {
//				DatabaseReader dbReader = new DatabaseReader.Builder(IPHelper.class.getResourceAsStream("/hk/quantr/logic/geolite/GeoLite2-City.mmdb")).build();
//
//				String urlString = "http://checkip.amazonaws.com/";
//				URL url = new URL(urlString);
//				BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
//				String ip = br.readLine();
//				br.close();
//				CityResponse response = dbReader.city(InetAddress.getByName(ip));
//				if (response.getCountry().getName().equals("Hong Kong")) {
//					if (whitelist.contains(ip)) {
//						b = false;
//					} else {
//						b = true;
//					}
//				} else {
//					b = false;
//				}
//				return b;
//			} catch (Exception ex) {
//				ex.printStackTrace();
//				return false;
//			}
//		} else {
//			return b;
//		}
	}
}
