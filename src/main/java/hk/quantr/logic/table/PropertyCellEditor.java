/*
 * Copyright 2023 RonaldPark <pkc6krt22@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.table;

import hk.quantr.logic.arduino.ArduinoPinMap;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author RonaldPark <pkc6krt22@gmail.com>
 */
public class PropertyCellEditor extends AbstractCellEditor implements TableCellEditor/*, ActionListener */ {

	private JTextField textField;
	JComboBox<String> comboBox;
	List<String> dropDownTable = Arrays.asList(new String[]{"Radix", "Orientation", "Active High/Low",
		"Shift Type", "No. of Bits", "Include Input Enable", "Include Output Enable", "Trigger",
		"Baurate", "Serial Port", "Border", "Arduino Serial", "Arduino Serial Oled", "Arduino Wifi", "Arduino Wifi Oled", "Icon"});

	@Override
	public Object getCellEditorValue() {
		if (comboBox != null) {
			System.out.println("comboBox.getSelectedItem()="+comboBox.getSelectedItem());
			return comboBox.getSelectedItem();
		} else if (textField != null) {
			return textField.getText();
		}
		return null;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		System.out.println("getTableCellEditorComponent: " + value);
		if (dropDownTable.contains(table.getValueAt(row, 0).toString())) {
			if (table.getValueAt(row, 0).toString().equals("Orientation") && column == 1) {
				comboBox = new JComboBox(new String[]{"north", "east", "south", "west"});
			} else if (table.getValueAt(row, 0).toString().equals("Radix") && column == 1) {
				comboBox = new JComboBox(new String[]{"bin", "dec", "hex"});
			} else if (table.getValueAt(row, 0).toString().equals("Active High/Low") && column == 1) {
				comboBox = new JComboBox(new String[]{"High", "Low"});
			} else if (table.getValueAt(row, 0).toString().equals("Shift Type") && column == 1) {
				comboBox = new JComboBox(new String[]{"logical left", "logical right"/*, "arithmetic right"*/});
			} else if (table.getValueAt(row, 0).toString().equals("No. of Bits") && column == 1) {
				comboBox = new JComboBox(new String[]{"1", "2", "3", "4", "5", "6", "7", "8"});
			} else if (table.getValueAt(row, 0).toString().equals("Include Input Enable") && column == 1) {
				comboBox = new JComboBox(new String[]{"yes", "no"});
			} else if (table.getValueAt(row, 0).toString().equals("Include Output Enable") && column == 1) {
				comboBox = new JComboBox(new String[]{"yes", "no"});
			} else if (table.getValueAt(row, 0).toString().equals("Trigger") && column == 1) {
				comboBox = new JComboBox(new String[]{"positive", "negative", "event"});
			} else if (table.getValueAt(row, 0).toString().equals("Baurate") && column == 1) {
				comboBox = new JComboBox(new String[]{"9600 baud", "115200 baud"});
			} else if (table.getValueAt(row, 0).toString().equals("Serial Port") && column == 1) {
				comboBox = new JComboBox(new String[]{"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7"});
			} else if (table.getValueAt(row, 0).toString().equals("Border") && column == 1) {
				comboBox = new JComboBox(new String[]{"enable", "disable"});
			} else if (table.getValueAt(row, 0).toString().equals("Arduino Serial") && column == 1) {
				comboBox = new JComboBox(ArduinoPinMap.arduinoSerialModel);
			} else if (table.getValueAt(row, 0).toString().equals("Arduino Serial Oled") && column == 1) {
				comboBox = new JComboBox(ArduinoPinMap.arduinoSerialOledModel);
			} else if (table.getValueAt(row, 0).toString().equals("Arduino Wifi") && column == 1) {
				comboBox = new JComboBox(ArduinoPinMap.arduinoWifiModel);
			} else if (table.getValueAt(row, 0).toString().equals("Arduino Wifi Oled") && column == 1) {
				comboBox = new JComboBox(ArduinoPinMap.arduinoWifiOledModel);
			} else if (table.getValueAt(row, 0).toString().equals("Icon") && column == 1) {
				comboBox = new JComboBox(new String[]{"centered name", "module circuit", "custom icon"});
			}
			comboBoxHandler:
			comboBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					stopCellEditing();
				}
			});
			comboBox.setSelectedItem((String) value);
			if (isSelected) {
				comboBox.setBackground(table.getSelectionBackground());
				comboBox.setForeground(table.getSelectionForeground());
			} else {
				comboBox.setBackground(table.getBackground());
				comboBox.setForeground(table.getForeground());
			}
			return comboBox;
		} else if (table.getValueAt(row, 0).toString().contains("Pin ") && column == 1) {
			if (table.getValueAt(row, 0).toString().contains("Input Pin ") || table.getValueAt(row, 0).toString().contains("Specified Pin ->")) {
				comboBox = null;
				if (textField == null) {
					textField = new JTextField();
				} else {
					textField.setText(value.toString());
				}
				return textField;
			} else {
				comboBox = new JComboBox(new String[]{"OUTPUT", "INPUT"});
				comboBox.addItemListener(new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent e) {
						stopCellEditing();
					}
				});
				comboBox.setSelectedItem((String) value);
				if (isSelected) {
					comboBox.setBackground(table.getSelectionBackground());
					comboBox.setForeground(table.getSelectionForeground());
				} else {
					comboBox.setBackground(table.getBackground());
					comboBox.setForeground(table.getForeground());
				}
				return comboBox;
			}
		} else if (column == 1) {
			comboBox = null;
			if (textField == null) {
				textField = new JTextField();
			} else {
				textField.setText(value.toString());
			}
			return textField;
		}
		return new JLabel("<none>");
	}
}
