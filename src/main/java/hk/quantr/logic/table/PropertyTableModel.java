/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.table;

import hk.quantr.logic.data.basic.Pin;
import hk.quantr.logic.data.basic.Probe;
import hk.quantr.logic.data.gate.Vertex;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class PropertyTableModel extends AbstractTableModel {

	public Vertex vertex;

	@Override
	public int getRowCount() {
		if (vertex == null) {
			return 0;
		}
		return vertex.properties.size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (columnIndex == 0) {
			return vertex.properties.keySet().toArray()[rowIndex];
		}
		if (vertex.properties.values().toArray()[rowIndex] instanceof Long) {
			return Long.toUnsignedString((Long) vertex.properties.values().toArray()[rowIndex]);
		}
		return vertex.properties.values().toArray()[rowIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 1) {
			return Object.class;
		}
		return String.class;
	}

	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "Key";
		} else {
			return "Value";
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex != 0;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		System.out.println("setValue(" + rowIndex + ", " + columnIndex + ") = " + aValue);
		if (aValue == null) {
			return;
		}

		if (aValue instanceof String) { //should not delete 
			//Exception in thread "AWT-EventQueue-0" java.lang.NumberFormatException: For input string: ""
			if (aValue.equals("")) {
				return;
			}
		}

		if (vertex instanceof Pin || vertex instanceof Probe) {
		} else {
			if (vertex.properties.values().toArray()[rowIndex] instanceof Integer) {
				if (NumberUtils.isParsable((String) aValue)) {
					try {
						if (((String) vertex.properties.keySet().toArray()[rowIndex]).contains("Bits")) {
							if (Integer.parseInt((String) aValue) > 0 && Integer.parseInt((String) aValue) <= 64) {
								vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], Integer.parseInt((String) aValue));
							}
						} else if (Integer.parseInt((String) aValue) >= 0) {
							vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], Integer.parseInt((String) aValue));
						}
					} catch (NumberFormatException e) {
					}
				}
			} else if (vertex.properties.values().toArray()[rowIndex] instanceof Long) {
				if (NumberUtils.isParsable((String) aValue)) {
					try {
						if (Long.parseLong((String) aValue) >= 0) {
							vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], Long.parseUnsignedLong((String) aValue));
						}
					} catch (NumberFormatException e) {
					}
				}
			} else if (vertex.properties.values().toArray()[rowIndex] instanceof String) {
				if (!Character.isAlphabetic(((String) aValue).charAt(0)) && ((String) vertex.properties.keySet().toArray()[rowIndex]).equals("Name")) {
					return;
				}
				vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], (String) aValue);
			}
			vertex.updateProperty();
			return;
		}
		try {
			if (vertex.properties.keySet().toArray()[rowIndex].equals("Value")) {
				if (!((String) aValue).matches("-?\\d+") || Long.parseUnsignedLong(((String) aValue), 10) >= Math.pow(2, vertex.outputs.get(0).bits)) {
					return;
				}
				vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], Long.parseUnsignedLong(aValue.toString()));
			} else if (((String) vertex.properties.keySet().toArray()[rowIndex]).contains("Bits")) {
				if (!((String) aValue).matches("\\d+") || Integer.parseInt(((String) aValue), 10) > 64) {
					return;
				}
				vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], Integer.parseInt(aValue.toString()));
			} else if (vertex.properties.values().toArray()[rowIndex] instanceof String) {
				if (vertex.properties.keySet().toArray()[rowIndex].equals("Radix") && !((String) aValue).equals("hex") && !((String) aValue).equals("bin") && !((String) aValue).equals("dec")) {
					return;
				}
				vertex.properties.put((String) vertex.properties.keySet().toArray()[rowIndex], (String) aValue);
			}
			vertex.updateProperty();
		} catch (NumberFormatException e) {
		}
	}
}
