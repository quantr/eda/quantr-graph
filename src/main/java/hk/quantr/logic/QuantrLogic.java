package hk.quantr.logic;

import com.formdev.flatlaf.intellijthemes.FlatArcOrangeIJTheme;
import hk.quantr.javalib.CommonLib;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class QuantrLogic extends javax.swing.JFrame {

	/**
	 * Creates new form QuantrGraph
	 */
	public QuantrLogic() {
		System.setProperty("apple.laf.useScreenMenuBar", "true");
		if (Setting.getInstance().windowMaximized) {
			setExtendedState(JFrame.MAXIMIZED_BOTH);

			// need reinit the leftSplitPane since it is vertical
//			this.quantrGraphPanel1.leftSplitPane.setDividerLocation(Setting.getInstance().dividerLocation2);
		}

		initComponents();

		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel"); //$NON-NLS-1$
		getRootPane().getActionMap().put("Cancel", new AbstractAction() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("ESC");
				quantrGraphPanel1.handleESC();
			}
		});

		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, 0), "Auto"); //$NON-NLS-1$
		getRootPane().getActionMap().put("Auto", new AbstractAction() {

			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleEQUALS();
			}
		});

		// Handle Ctrl+C copy event
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK), "Copy");
		getRootPane().getActionMap().put("Copy", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlC();
			}
		});

		// Handle Ctrl+V paste event
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK), "Paste");
		getRootPane().getActionMap().put("Paste", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlV();
			}
		});
// undo
		// Handle Ctrl+Z undo event
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK), "Undo");
		getRootPane().getActionMap().put("Undo", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlZ();
			}
		});

		// Handle Ctrl+Shift+Z redo event
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK), "Redo");
		getRootPane().getActionMap().put("Redo", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlShiftZ();
			}
		});
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK), "New");
		getRootPane().getActionMap().put("New", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlN();
			}
		});

		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK), "Save");
		getRootPane().getActionMap().put("Save", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlS();
			}
		});

		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK), "Open");
		getRootPane().getActionMap().put("Open", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlO();
			}
		});

		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_QUOTE, InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK), "Quit");
		getRootPane().getActionMap().put("Quit", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				if (!quantrGraphPanel1.saved) {
					quantrGraphPanel1.projectFile.delete();
				}
				System.exit(0);
			}
		});

		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK), "Convert");
		getRootPane().getActionMap().put("Convert", new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				quantrGraphPanel1.handleCtrlQ();
			}
		});

		if (CommonLib.getOS() == CommonLib.OSType.mac) {
//			Application application = Application.getApplication();
//			application.setQuitHandler(new QuitHandler() {
//				public void handleQuitRequestWith(QuitEvent event, QuitResponse response) {
//					System.out.println("mac q");
//				}
//			});
		}

		setIconImage(new ImageIcon(getClass().getResource("/hk/quantr/logic/Quantr Logic.png")).getImage());
		if (Setting.getInstance().width == 0 || Setting.getInstance().height == 0) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			setSize(screenSize.width * 2 / 3, screenSize.height * 4 / 5);
		} else {
			setSize(Setting.getInstance().width, Setting.getInstance().height);
		}
		int x = Setting.getInstance().x;
		int y = Setting.getInstance().y;
		if (x <= 0 || y <= 0) {
			this.setLocationRelativeTo(null);
		} else {
			setLocation(x, y);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        quantrGraphPanel1 = new QuantrGraphPanel(this);
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        convertTo2InputMenuItem = new javax.swing.JMenuItem();
        convertToLUTTruthTableMenuItem = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        convertMenuItem = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        aboutUSMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Quantr Logic");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().add(quantrGraphPanel1, java.awt.BorderLayout.CENTER);

        jMenu2.setText("LUT");

        convertTo2InputMenuItem.setText("Convert to 2-input");
        convertTo2InputMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertTo2InputMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(convertTo2InputMenuItem);

        convertToLUTTruthTableMenuItem.setText("Generate LUT/Truth table");
        convertToLUTTruthTableMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertToLUTTruthTableMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(convertToLUTTruthTableMenuItem);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Function");

        convertMenuItem.setText("Convert");
        convertMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(convertMenuItem);

        jMenuBar1.add(jMenu3);

        jMenu1.setText("Help");

        aboutUSMenuItem.setText("About US");
        aboutUSMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutUSMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(aboutUSMenuItem);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(877, 711));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
		Setting.getInstance().width = this.getWidth();
		Setting.getInstance().height = this.getHeight();
		Setting.getInstance().x = this.getLocation().x;
		Setting.getInstance().y = this.getLocation().y;
		Setting.getInstance().dividerLocation1 = this.quantrGraphPanel1.jSplitPane2.getDividerLocation();
		Setting.getInstance().dividerLocation2 = this.quantrGraphPanel1.leftSplitPane.getDividerLocation();
		Setting.getInstance().dividerLocation3 = this.quantrGraphPanel1.mainSplitPane.getDividerLocation();
		Setting.getInstance().windowMaximized = this.getExtendedState() == JFrame.MAXIMIZED_BOTH;
		Setting.getInstance().save();
    }//GEN-LAST:event_formWindowClosing

    private void aboutUSMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutUSMenuItemActionPerformed
		new AboutUsDialog(this, true).setVisible(true);
    }//GEN-LAST:event_aboutUSMenuItemActionPerformed

    private void convertTo2InputMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_convertTo2InputMenuItemActionPerformed
		// TODO add your handling code here:
    }//GEN-LAST:event_convertTo2InputMenuItemActionPerformed

    private void convertToLUTTruthTableMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_convertToLUTTruthTableMenuItemActionPerformed
		// TODO add your handling code here:
    }//GEN-LAST:event_convertToLUTTruthTableMenuItemActionPerformed

    private void convertMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_convertMenuItemActionPerformed
		this.quantrGraphPanel1.convertButton.doClick();
    }//GEN-LAST:event_convertMenuItemActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		System.setProperty("sun.java2d.uiScale", String.valueOf(Setting.getInstance().scale));
//		SwingDPI.applyScalingAutomatically();
//		System.setProperty("sun.java2d.uiScale.enabled", "false");

//		if (2 > 1 /*Toolkit.getDefaultToolkit().getScreenSize().width >= 2560*/) {
//			System.setProperty("sun.java2d.uiScale", "2");
//		}

		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
		/* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			UIManager.setLookAndFeel(new FlatArcOrangeIJTheme());
		} catch (Exception ex) {
			System.err.println("Failed to initialize LaF");
		}
//		try {
//			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//				if ("Nimbus".equals(info.getName())) {
//					javax.swing.UIManager.setLookAndFeel(info.getClassName());
//					break;
//				}
//			}
//		} catch (ClassNotFoundException ex) {
//			java.util.logging.Logger.getLogger(QuantrGraph.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//		} catch (InstantiationException ex) {
//			java.util.logging.Logger.getLogger(QuantrGraph.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//		} catch (IllegalAccessException ex) {
//			java.util.logging.Logger.getLogger(QuantrGraph.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
//			java.util.logging.Logger.getLogger(QuantrGraph.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//		}
		//</editor-fold>

		/* Create and display the form */
		QuantrLogic quantrLogic = new QuantrLogic();
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				quantrLogic.setVisible(true);
			}
		});

		quantrLogic.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (quantrLogic.quantrGraphPanel1.saved) {
					quantrLogic.quantrGraphPanel1.handleCtrlS();
					quantrLogic.dispose();
					System.exit(0);
				} else {
					int choice = JOptionPane.showConfirmDialog(quantrLogic, "Save before closing?", "Confirm Shutdown", JOptionPane.YES_NO_CANCEL_OPTION);
					switch (choice) {
						// Close the application without saving
						case JOptionPane.NO_OPTION:
							quantrLogic.quantrGraphPanel1.projectFile.delete();
							quantrLogic.dispose();
							System.exit(0);
							break;
						// Save changes and then close the application
						case JOptionPane.YES_OPTION:
							quantrLogic.quantrGraphPanel1.handleCtrlS();
							if (quantrLogic.quantrGraphPanel1.saved) {
								quantrLogic.dispose();
								System.exit(0);
								break;
							}
						// Do not close the application
						default:
							quantrLogic.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
					}
				}
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutUSMenuItem;
    private javax.swing.JMenuItem convertMenuItem;
    private javax.swing.JMenuItem convertTo2InputMenuItem;
    private javax.swing.JMenuItem convertToLUTTruthTableMenuItem;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    public hk.quantr.logic.QuantrGraphPanel quantrGraphPanel1;
    // End of variables declaration//GEN-END:variables
}
