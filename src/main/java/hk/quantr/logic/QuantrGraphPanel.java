package hk.quantr.logic;

import com.github.javabdd.BDD;
import hk.quantr.algebralib.MyListener;
import hk.quantr.algebralib.QuantrBooleanAlgebra;
import hk.quantr.algebralib.data.BooleanData;
import hk.quantr.logic.data.Data;
import hk.quantr.logic.data.gate.OrGate;
import hk.quantr.logic.data.gate.AndGate;
import hk.quantr.logic.data.gate.NandGate;
import hk.quantr.logic.data.gate.NotGate;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.logic.table.PropertyTableModel;
import hk.quantr.logic.tree.ComponentTreeCellRenderer;
import hk.quantr.logic.tree.ComponentTreeModel;
import hk.quantr.logic.tree.ToolbarTreeCellRenderer;
import hk.quantr.logic.tree.ToolbarTreeNode;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.swing.advancedswing.jclosabletabbedpane.JClosableTabbedPane;
import hk.quantr.javalib.swing.advancedswing.jprogressbardialog.JProgressBarDialog;
import hk.quantr.logic.algo.lee.CircuitGrid;
import hk.quantr.logic.algo.lut.BDDOperation;
import hk.quantr.logic.arduino.ArduinoHeltecWifiOled;
import hk.quantr.logic.arduino.ArduinoSerial;
import hk.quantr.logic.arduino.ArduinoSerialOled;
import hk.quantr.logic.arduino.ArduinoWifi;
import hk.quantr.logic.arduino.ArduinoWifiOled;
import hk.quantr.logic.data.arithmetic.Adder;
import hk.quantr.logic.data.arithmetic.Shifter;
import hk.quantr.logic.data.arithmetic.Divider;
import hk.quantr.logic.data.arithmetic.Multiplier;
import hk.quantr.logic.data.arithmetic.Negator;
import hk.quantr.logic.data.arithmetic.Subtractor;
import hk.quantr.logic.data.basic.BitExtender;
import hk.quantr.logic.data.basic.BitstreamProgrammer;
import hk.quantr.logic.data.basic.Button;
import hk.quantr.logic.data.basic.Clock;
import hk.quantr.logic.data.basic.Combiner;
import hk.quantr.logic.data.basic.Constant;
import hk.quantr.logic.data.basic.DipSwitch;
import hk.quantr.logic.data.basic.Led;
import hk.quantr.logic.data.basic.LedBar;
import hk.quantr.logic.data.basic.OutputPin;
import hk.quantr.logic.data.basic.Pin;
import hk.quantr.logic.data.basic.Probe;
import hk.quantr.logic.data.basic.RGBLed;
import hk.quantr.logic.data.basic.Splitter;
import hk.quantr.logic.data.basic.Switch;
import hk.quantr.logic.data.basic.Transistor;
import hk.quantr.logic.data.basic.Tunnel;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.data.flipflop.DFlipflop;
import hk.quantr.logic.data.flipflop.DLatch;
import hk.quantr.logic.data.flipflop.JKFlipflop;
import hk.quantr.logic.data.flipflop.Ram;
import hk.quantr.logic.data.flipflop.SRFlipflop;
import hk.quantr.logic.data.flipflop.TFlipflop;
import hk.quantr.logic.data.gate.Buffer;
import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.EvenParity;
import hk.quantr.logic.data.gate.Input;
import hk.quantr.logic.data.gate.NorGate;
import hk.quantr.logic.data.gate.OddParity;
import hk.quantr.logic.data.gate.Output;
import hk.quantr.logic.data.gate.Port;
import hk.quantr.logic.data.gate.TriStateBuffer;
import hk.quantr.logic.data.gate.TriStateNotBuffer;
import hk.quantr.logic.data.gate.XnorGate;
import hk.quantr.logic.data.gate.XorGate;
import hk.quantr.logic.data.gate.Module;
import hk.quantr.logic.data.gate.ModuleComponent;
import hk.quantr.logic.data.graphics.ImageComponent;
import hk.quantr.logic.data.output.EightSegmentDisplay;
import hk.quantr.logic.data.output.HexDisplay;
import hk.quantr.logic.data.output.TTY;
import hk.quantr.logic.data.plexer.BitSelector;
import hk.quantr.logic.data.plexer.Decoder;
import hk.quantr.logic.data.plexer.Demultiplexer;
import hk.quantr.logic.data.plexer.Encoder;
import hk.quantr.logic.data.plexer.Multiplexer;
import hk.quantr.logic.engine.Simulator;
import hk.quantr.logic.engine.SimulatorMultiThread;
import hk.quantr.logic.table.PropertyCellEditor;
import hk.quantr.logic.tree.ProjectTreeCellRenderer;
import hk.quantr.logic.tree.ProjectTreeNode;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class QuantrGraphPanel extends javax.swing.JPanel {

	ToolbarTreeNode toolbarRoot = new ToolbarTreeNode("Component");
	ToolbarTreeCellRenderer toolbarTreeCellRenderer = new ToolbarTreeCellRenderer();
	ProjectTreeNode projectTreeRoot = new ProjectTreeNode("Project");
	ProjectTreeCellRenderer projectTreeCellRenderer = new ProjectTreeCellRenderer();
	ComponentTreeCellRenderer componentCellRenderer = new ComponentTreeCellRenderer();
	PropertyTableModel propertyTableModel = new PropertyTableModel();
//	PropertyTableRenderer propertyTableRenderer = new PropertyTableRenderer();
	PropertyCellEditor propertyCellEditor = new PropertyCellEditor();
	JFrame frame;
	public Simulator simulator;
	public SimulatorMultiThread simulator2;
	public File tempVCDFile;
	public Data data = new Data();
	public File projectFile;
	public String tempFileName;
	public boolean saved = false;
	private QuantrGraphCanvas currentCanvas;
	HashMap<Port, ArrayList<Port>> portConnectionMap;
	ArrayList<ArrayList<Vertex>> booleanAlgebraAutoPositionVertices;
	HashMap<String, Vertex> varsMap = new HashMap();
	int[] levelB2G = new int[5];

	public QuantrGraphPanel() {

	}

	public QuantrGraphPanel(JFrame frame) {
		this.frame = frame;
		initComponents();

		initToolbarTree();
		initToolbar();

//		propertyTable.setDefaultRenderer(Object.class, propertyTableRenderer);
		propertyTable.setDefaultEditor(Object.class, propertyCellEditor);
		JScrollPane tab = new JScrollPane();
		tab.setViewportView(new QuantrGraphCanvas(this, new Module("Main")));
		_addMouseWheelListener(tab);
		mainTabbedPane.addTab("Main", tab);
		mainTabbedPane.setIconAt(0, null); // disable the close button on tab
		mainTabbedPane.setIconAt(1, null); // disable the close button on tab
		mainTabbedPane.setSelectedIndex(1);
		data.modules.put("Main", ((QuantrGraphCanvas) tab.getViewport().getView()).module);
		mainTabbedPane.setSelectedComponent(tab);
		((ComponentTreeModel) componentTree.getModel()).module = ((QuantrGraphCanvas) tab.getViewport().getView()).module;
		currentCanvas = (QuantrGraphCanvas) tab.getViewport().getView();
		statusLabel1.setText("Simulation rate: -- cycles/s");
//		for (int i = 0; i < mainTabbedPane.getTabCount(); i++) {
//			if (!mainTabbedPane.getTitleAt(i).equals("VCD")) {
//				data.modules.
//			}
//		}
//		quantrVCDComponent1.add(new VCDLoadingPanel(), "vcdLoadingPanel");
//		((CardLayout) quantrVCDComponent1.getLayout()).show(quantrVCDComponent1, "vcdLoadingPanel");
		try {
			tempVCDFile = java.nio.file.Files.createTempFile("vcdFile", ".vcd").toFile();
			quantrVCDComponent1.loadVCD(tempVCDFile);
			tempVCDFile.deleteOnExit();
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}

		projectTree.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_DELETE) {
					if (projectTree.hasFocus() && projectTree.getSelectionPaths() != null) {
						if (!((ProjectTreeNode) projectTree.getLastSelectedPathComponent()).data.equals("Main")) {
							if (!evt.isShiftDown()) {
								int option = JOptionPane.showConfirmDialog(null, "Confirm to delete?", "Delete", JOptionPane.YES_NO_OPTION);
								if (option != JOptionPane.YES_OPTION) {
									return;
								}
							}
							projectTreeRoot.remove((ProjectTreeNode) projectTree.getLastSelectedPathComponent());
							((DefaultTreeModel) projectTree.getModel()).reload(projectTreeRoot);
							mainTabbedPane.remove(mainTabbedPane.getSelectedComponent());
						}
					}
				}
			}
		});

		try {
			File folder = new File(System.getProperty("java.io.tmpdir"), "ql_autosave");
			if (!folder.exists()) {
				boolean success = folder.mkdirs();
				if (!success) {
					System.out.println("Autosave Unavailable");
				}
			}
			projectFile = java.nio.file.Files.createTempFile(folder.toPath(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmm")) + "-", ".ql").toFile();
			tempFileName = projectFile.getName();
			System.out.println(tempFileName);
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
//		ImageComponent v = new ImageComponent("test");
//		try {
//			v.setImage((BufferedImage) ImageIO.read(new File("/home/yin/simTest/test.jpeg")));
//		} catch (Exception ex) {
//			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
//		}
//		getCanvas().module.vertices.add(v);
//		getCanvas().repaint();
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        autoEdgePopupMenu = new javax.swing.JPopupMenu();
        deleteAllAutoEdgesMenuItem = new javax.swing.JMenuItem();
        mainSplitPane = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        leftSplitPane = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        propertyTable = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        highDPIJLabel1 = new hk.quantr.javalib.swing.advancedswing.highdpijlabel.HighDPIJLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        toolbarTree = new javax.swing.JTree(toolbarRoot);
        jPanel8 = new javax.swing.JPanel();
        toolbarPanel = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        addModuleButton = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        projectTree = new javax.swing.JTree(projectTreeRoot);
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        componentTree = new javax.swing.JTree();
        jToolBar2 = new javax.swing.JToolBar();
        refreshComponentTreeButton = new javax.swing.JButton();
        mainTabbedPane = new hk.quantr.javalib.swing.advancedswing.jclosabletabbedpane.JClosableTabbedPane();
        vcdPanel = new javax.swing.JPanel();
        quantrVCDComponent1 = new hk.quantr.vcdcomponent.QuantrVCDComponent();
        statusPanel = new javax.swing.JPanel();
        statusLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        newButton = new javax.swing.JButton();
        openButton = new javax.swing.JButton();
        openQuantrButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        saveToQuantrButton = new javax.swing.JButton();
        settingButton = new javax.swing.JButton();
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        zoom100Button = new javax.swing.JButton();
        clearButton = new javax.swing.JButton();
        drawButton = new javax.swing.JButton();
        randomButton = new javax.swing.JButton();
        onOffButton1 = new hk.quantr.javalib.swing.advancedswing.onoffbutton.OnOffButton();
        simulateToggleButton = new javax.swing.JToggleButton();
        resetButton = new javax.swing.JButton();
        wireToggleButton = new javax.swing.JToggleButton();
        autoToggleButton = new javax.swing.JToggleButton();
        deleteButton = new javax.swing.JButton();
        showLevelButton = new javax.swing.JToggleButton();
        autoPositionButton = new javax.swing.JButton();
        testButton = new javax.swing.JButton();
        westButton = new javax.swing.JButton();
        northButton = new javax.swing.JButton();
        eastButton = new javax.swing.JButton();
        southButton = new javax.swing.JButton();
        convertButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        loginToQuantrLabel = new javax.swing.JLabel();
        myProjectLabel = new javax.swing.JLabel();

        deleteAllAutoEdgesMenuItem.setText("Delete All");
        deleteAllAutoEdgesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteAllAutoEdgesMenuItemActionPerformed(evt);
            }
        });
        autoEdgePopupMenu.add(deleteAllAutoEdgesMenuItem);

        setLayout(new java.awt.BorderLayout());

        mainSplitPane.setDividerLocation(Setting.getInstance().dividerLocation3);

        jPanel1.setLayout(new java.awt.BorderLayout());

        leftSplitPane.setDividerLocation(Setting.getInstance().dividerLocation2);
        leftSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jPanel2.setLayout(new java.awt.BorderLayout());

        propertyTable.setModel(propertyTableModel);
        propertyTable.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                propertyTablePropertyChange(evt);
            }
        });
        jScrollPane5.setViewportView(propertyTable);

        jPanel2.add(jScrollPane5, java.awt.BorderLayout.CENTER);

        highDPIJLabel1.padding=10;
        highDPIJLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/rmit/rmit_logo.png"))); // NOI18N
        highDPIJLabel1.setPreferredSize(new java.awt.Dimension(200, 60));
        highDPIJLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                highDPIJLabel1MouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(highDPIJLabel1);

        jPanel2.add(jScrollPane6, java.awt.BorderLayout.SOUTH);

        leftSplitPane.setBottomComponent(jPanel2);

        toolbarTree.setCellRenderer(toolbarTreeCellRenderer);
        toolbarTree.setRowHeight(22);
        toolbarTree.setScrollsOnExpand(false);
        toolbarTree.setShowsRootHandles(true);
        jScrollPane2.setViewportView(toolbarTree);

        jTabbedPane1.addTab("Toolbox", jScrollPane2);

        jPanel8.setLayout(new java.awt.BorderLayout());

        toolbarPanel.setLayout(new javax.swing.BoxLayout(toolbarPanel, javax.swing.BoxLayout.Y_AXIS));
        jPanel8.add(toolbarPanel, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Toolbar", jPanel8);

        jPanel6.setLayout(new java.awt.BorderLayout());

        addModuleButton.setText("+");
        addModuleButton.setToolTipText("Add module");
        addModuleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addModuleButtonActionPerformed(evt);
            }
        });
        jPanel7.add(addModuleButton);

        jPanel6.add(jPanel7, java.awt.BorderLayout.NORTH);

        projectTree.setCellRenderer(projectTreeCellRenderer);
        projectTree.setRowHeight(22);
        projectTree.setScrollsOnExpand(false);
        projectTree.setShowsRootHandles(true);
        projectTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                projectTreeMouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(projectTree);

        jPanel6.add(jScrollPane7, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Project", jPanel6);

        leftSplitPane.setTopComponent(jTabbedPane1);

        jPanel1.add(leftSplitPane, java.awt.BorderLayout.CENTER);

        mainSplitPane.setLeftComponent(jPanel1);

        jSplitPane2.setDividerLocation(Setting.getInstance().dividerLocation1);
        jSplitPane2.setResizeWeight(0.8);

        jPanel3.setLayout(new java.awt.BorderLayout());

        componentTree.setModel(new ComponentTreeModel(null, new DefaultMutableTreeNode("Component")));
        componentTree.setCellRenderer(componentCellRenderer);
        componentTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                componentTreeMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(componentTree);

        jPanel3.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jToolBar2.setRollover(true);

        refreshComponentTreeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arrow_refresh.png"))); // NOI18N
        refreshComponentTreeButton.setText("Refresh");
        refreshComponentTreeButton.setFocusable(false);
        refreshComponentTreeButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        refreshComponentTreeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshComponentTreeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshComponentTreeButtonActionPerformed(evt);
            }
        });
        jToolBar2.add(refreshComponentTreeButton);

        jPanel3.add(jToolBar2, java.awt.BorderLayout.PAGE_START);

        jScrollPane3.setViewportView(jPanel3);

        jSplitPane2.setRightComponent(jScrollPane3);

        mainTabbedPane.setMinimumSize(new java.awt.Dimension(500, 327));
        mainTabbedPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mainTabbedPaneMouseClicked(evt);
            }
        });

        vcdPanel.setLayout(new java.awt.BorderLayout());
        vcdPanel.add(quantrVCDComponent1, java.awt.BorderLayout.CENTER);

        mainTabbedPane.addTab("VCD", vcdPanel);

        jSplitPane2.setLeftComponent(mainTabbedPane);

        mainSplitPane.setRightComponent(jSplitPane2);

        add(mainSplitPane, java.awt.BorderLayout.CENTER);

        statusPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        statusPanel.add(statusLabel1);

        add(statusPanel, java.awt.BorderLayout.SOUTH);

        jPanel4.setLayout(new java.awt.BorderLayout());

        jToolBar1.setRollover(true);

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/script.png"))); // NOI18N
        newButton.setText("New");
        newButton.setFocusable(false);
        newButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(newButton);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folder.png"))); // NOI18N
        openButton.setText("Open");
        openButton.setFocusable(false);
        openButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(openButton);

        openQuantrButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folder.png"))); // NOI18N
        openQuantrButton.setText("Open from Quantr");
        openQuantrButton.setToolTipText("");
        openQuantrButton.setFocusable(false);
        openQuantrButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        openQuantrButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        openQuantrButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openQuantrButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(openQuantrButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/disk.png"))); // NOI18N
        saveButton.setText("Save");
        saveButton.setFocusable(false);
        saveButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveButton);

        saveToQuantrButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/diskQuantr.png"))); // NOI18N
        saveToQuantrButton.setText("Save to Quantr");
        saveToQuantrButton.setFocusable(false);
        saveToQuantrButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        saveToQuantrButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveToQuantrButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveToQuantrButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveToQuantrButton);

        settingButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/cog.png"))); // NOI18N
        settingButton.setText("Setting");
        settingButton.setFocusable(false);
        settingButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        settingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                settingButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(settingButton);

        zoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/zoom_in.png"))); // NOI18N
        zoomInButton.setFocusable(false);
        zoomInButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        zoomInButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomInButton);

        zoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/zoom_out.png"))); // NOI18N
        zoomOutButton.setFocusable(false);
        zoomOutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        zoomOutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoomOutButton);

        zoom100Button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/zoom.png"))); // NOI18N
        zoom100Button.setText("100%");
        zoom100Button.setFocusable(false);
        zoom100Button.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        zoom100Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoom100ButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(zoom100Button);

        clearButton.setText("Clear");
        clearButton.setFocusable(false);
        clearButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        clearButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(clearButton);

        drawButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/paintbrush.png"))); // NOI18N
        drawButton.setText("Draw");
        drawButton.setFocusable(false);
        drawButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        drawButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                drawButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(drawButton);

        randomButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/shape_ungroup.png"))); // NOI18N
        randomButton.setText("Random");
        randomButton.setFocusable(false);
        randomButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        randomButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(randomButton);

        onOffButton1.setText("onOffButton1");
        onOffButton1.setFocusable(false);
        onOffButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        onOffButton1.setMaximumSize(new java.awt.Dimension(53, 15));
        onOffButton1.setMinimumSize(new java.awt.Dimension(53, 26));
        onOffButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        onOffButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onOffButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(onOffButton1);

        simulateToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/control_play.png"))); // NOI18N
        simulateToggleButton.setText("Simulate");
        simulateToggleButton.setFocusable(false);
        simulateToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        simulateToggleButton.setMaximumSize(new java.awt.Dimension(90, 46));
        simulateToggleButton.setMinimumSize(new java.awt.Dimension(64, 26));
        simulateToggleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        simulateToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simulateToggleButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(simulateToggleButton);

        resetButton.setText("Reset");
        resetButton.setFocusable(false);
        resetButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        resetButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        resetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(resetButton);

        wireToggleButton.setText("Wire");
        wireToggleButton.setFocusable(false);
        wireToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        wireToggleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        wireToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wireToggleButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(wireToggleButton);

        autoToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/link.png"))); // NOI18N
        autoToggleButton.setText("Auto");
        autoToggleButton.setFocusable(false);
        autoToggleButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        autoToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoToggleButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(autoToggleButton);

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/cross.png"))); // NOI18N
        deleteButton.setText("Delete");
        deleteButton.setFocusable(false);
        deleteButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(deleteButton);

        showLevelButton.setText("Show Level");
        showLevelButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        showLevelButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        showLevelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showLevelButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(showLevelButton);

        autoPositionButton.setText("Auto Position");
        autoPositionButton.setFocusable(false);
        autoPositionButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        autoPositionButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        autoPositionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoPositionButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(autoPositionButton);

        testButton.setText("Test");
        testButton.setFocusable(false);
        testButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        testButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        testButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(testButton);

        westButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/west.png"))); // NOI18N
        westButton.setFocusable(false);
        westButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        westButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        westButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                westButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(westButton);

        northButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/north.png"))); // NOI18N
        northButton.setFocusable(false);
        northButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        northButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        northButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                northButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(northButton);

        eastButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/east.png"))); // NOI18N
        eastButton.setFocusable(false);
        eastButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        eastButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        eastButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eastButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(eastButton);

        southButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/south.png"))); // NOI18N
        southButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                southButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(southButton);

        convertButton.setText("Convert");
        convertButton.setToolTipText("");
        convertButton.setFocusable(false);
        convertButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        convertButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        convertButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                convertButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(convertButton);

        jPanel4.add(jToolBar1, java.awt.BorderLayout.CENTER);

        loginToQuantrLabel.setText("Login to Quantr");
        loginToQuantrLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginToQuantrLabelMouseClicked(evt);
            }
        });
        jPanel5.add(loginToQuantrLabel);

        myProjectLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                myProjectLabelMouseClicked(evt);
            }
        });
        jPanel5.add(myProjectLabel);

        jPanel4.add(jPanel5, java.awt.BorderLayout.EAST);

        add(jPanel4, java.awt.BorderLayout.NORTH);
    }// </editor-fold>//GEN-END:initComponents

    private void drawButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_drawButtonActionPerformed
		for (int n = 1; n < 64; n++) {
			System.out.print(n + ": ");
			for (int i = -n / 2; i < (n + 1) / 2; i++) {
				System.out.print((i + (i >= 0 && n < 5 && n % 2 == 0 ? 1 : 0)) + ", ");
			}
			System.out.println();
		}
		Data data = new Data();
		NotGate v1 = new NotGate("Not 1");
		v1.setLocation(1, 1);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v1);

		AndGate v2 = new AndGate("and 1");
		v2.setLocation(10, 1);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v2);

		OrGate v3 = new OrGate("Or 1");
		v3.setLocation(19, 1);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v3);

		NandGate v4 = new NandGate("Nand 1");
		v4.setLocation(1, 10);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v4);

		NorGate v5 = new NorGate("Nor 1");
		v5.setLocation(10, 10);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v5);

		XorGate v6 = new XorGate("Xor 1");
		v6.setLocation(19, 10);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v6);

		XnorGate v7 = new XnorGate("Xnor 1");
		v7.setLocation(1, 19);
		data.modules.get(mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex())).vertices.add(v7);
//		Edge edge1 = new Edge(v1.getX(), v1.getY() + 1, v6.getX(), v6.getY() + 1);
//		data.edges.add(edge1);

		/*
		Connection conn = new Connection();
		conn.from = v2;
		conn.to = v4;

		for (Vertex v : data.vertices) {
			for (int x = v.x; x <= v.x + v.width; x++) {
				for (int y = v.y; y <= v.y + v.height; y++) {
					quantrGraphCanvas1.grid.nodeLoc[x][y].applied = true;
				}
			}
		}
		 */
//		quantrGraphCanvas1.data = data;
//		quantrGraphCanvas1.grid.initialize(200);
//		quantrGraphCanvas1.repaint();
		refreshComponentTreeButtonActionPerformed(null);
    }//GEN-LAST:event_drawButtonActionPerformed

    private void onOffButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onOffButton1ActionPerformed
		for (int i = 1; i < mainTabbedPane.getTabCount(); i++) {
			getCanvas(i).showGrid = onOffButton1.isSelected();
		}
		getCanvas().repaint();
    }//GEN-LAST:event_onOffButton1ActionPerformed

    private void zoomInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonActionPerformed
		for (int i = 1; i < mainTabbedPane.getTabCount(); i++) {
			if (getCanvas(i).gridSize < 100) {
				getCanvas(i).setGridSize(getCanvas(i).gridSize + 1);
				getCanvas(i).recalculateLines();
			}
		}
		zoom100Button.setText(getCanvas().gridSize * 10 + "%");
    }//GEN-LAST:event_zoomInButtonActionPerformed

    private void zoomOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonActionPerformed
		for (int i = 1; i < mainTabbedPane.getTabCount(); i++) {
			if (getCanvas(i).gridSize > 5) {
				getCanvas(i).setGridSize(getCanvas(i).gridSize - 1);
				getCanvas(i).recalculateLines();
			}
		}
		zoom100Button.setText(getCanvas().gridSize * 10 + "%");
    }//GEN-LAST:event_zoomOutButtonActionPerformed

	public void refreshTree() {
		refreshComponentTreeButtonActionPerformed(null);
	}

    private void refreshComponentTreeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshComponentTreeButtonActionPerformed
		ComponentTreeModel model = (ComponentTreeModel) componentTree.getModel();
//		System.out.println("all components: " + (((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponents()).toString());
		model.module = getCanvas().module;
		model.refresh();
		model.nodeStructureChanged(model.root);
		CommonLib.expandAll(componentTree, true);
    }//GEN-LAST:event_refreshComponentTreeButtonActionPerformed

    private void componentTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_componentTreeMouseClicked
		if (SwingUtilities.isRightMouseButton(evt)) {
			if (componentTree.getSelectionPath() != null) {
				Object obj = ((DefaultMutableTreeNode) componentTree.getSelectionPath().getLastPathComponent()).getUserObject();
				if (obj instanceof String && (obj.equals("Vertex") || obj.equals("Edge"))) {
					autoEdgePopupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
				}
			}
		} else {
			if (componentTree.getSelectionPath() != null) {
				if (!evt.isControlDown()) {
					getCanvas().clearVertexSelection();
				}
				for (int i = 0; i < componentTree.getSelectionCount(); i++) {
					Object obj = ((DefaultMutableTreeNode) componentTree.getSelectionPaths()[i].getLastPathComponent()).getUserObject();
					if (obj instanceof Vertex) {
						getCanvas().selectedVertex = (Vertex) obj;
						Vertex v = (Vertex) obj;
						v.isSelected = true;
						getCanvas().selectedVertices.add(v);
					} else if (obj instanceof Edge) {
						getCanvas().selectedEdge = (Edge) obj;
						Edge e = (Edge) obj;
						e.isSelected = true;
						getCanvas().selectedEdges.add(e);
					}
				}
				getCanvas().repaint();
			}
		}
    }//GEN-LAST:event_componentTreeMouseClicked

	public JLabel getStatusLabel1() {
		return statusLabel1;
	}

	public void handleESC() {
		getCanvas().clearVertexSelection();
		if (!getCanvas().simulating) {
			wireToggleButton.setSelected(false);
			wireToggleButtonActionPerformed(null);
			autoToggleButton.setSelected(false);
			autoToggleButtonActionPerformed(null);
			toolbarTree.setSelectionPath(null);
		}
	}

	public void handleEQUALS() {
		if (!getCanvas().simulating) {

			for (Vertex v : getCanvas().module.vertices) {
				if (v instanceof Pin) {
					if (!v.outputConnectedTo().isEmpty()) {
						((Pin) v).properties.put("Data Bits", v.outputConnectedTo().get(0).bits);
					}
					((Pin) v).updateProperty();
				}
			}
			getCanvas().recordAction();
		}
	}

	public void handleCtrlC() {
		getCanvas().copiedVertices.clear();
		getCanvas().copiedEdges.clear();
		for (Vertex v : getCanvas().selectedVertices) {
			getCanvas().copiedVertices.add(v);
		}
//		System.out.println(getCanvas().selectedEdges);
		for (Edge e : getCanvas().selectedEdges) {
			getCanvas().copiedEdges.add(e);
		}
	}

	public void handleCtrlV() {
		if (!getCanvas().simulating) {
			if (!getCanvas().copiedVertices.isEmpty()) {
				getCanvas().clearVSelection();
				for (Vertex vertex : getCanvas().copiedVertices) {
					Vertex v = null;
					Class<?> cls = vertex.getClass();
					try {
						try {
							v = (Vertex) cls.getDeclaredConstructor(String.class).newInstance("temp");
						} catch (NoSuchMethodException | SecurityException ex) {
							Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
						}
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
						Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
					}

					try {
						BeanUtils.copyProperties(v, vertex);
					} catch (IllegalAccessException | InvocationTargetException ex) {
						Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
					}
					try {
						v = (Vertex) vertex.clone();
					} catch (CloneNotSupportedException ex) {
						Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
					}
					getCanvas().module.vertices.add(v);
					getCanvas().selectedVertices.add(v);
					v.isSelected = true;
				}
				handleCtrlC();
			}
			if (!getCanvas().copiedEdges.isEmpty()) {
				getCanvas().clearEdgeSelection();
				getCanvas().dragEdgeID++;
				getCanvas().pastingID = getCanvas().dragEdgeID;
				ArrayList<Edge> cloneEdge = (ArrayList<Edge>) getCanvas().copiedEdges.clone();
				getCanvas().copiedEdges.clear();
				for (Edge e : cloneEdge) {
					Edge ghost = new Edge("ghost copy", getCanvas().getGrid().findPort(e.start.x + 1, e.start.y + 1),
							getCanvas().getGrid().findPort(e.end.x + 1, e.end.y + 1));
					ghost.setID(getCanvas().dragEdgeID);
					getCanvas().ghostEdges.add(ghost);
					getCanvas().copiedEdges.add(ghost);
					getCanvas().selectedEdges.add(ghost);
				}
			}
			getCanvas().repaint();
			getCanvas().recordAction();
		}
	}

	public void handleCtrlZ() {
		if (!getCanvas().simulating) {
			getCanvas().undo();
			getCanvas().recalculateLines();
			getCanvas().repaint();
			refreshTree();
		}
	}

	public void handleCtrlShiftZ() {
		if (!getCanvas().simulating) {
			getCanvas().redo();
			getCanvas().recalculateLines();
			getCanvas().repaint();
			refreshTree();
		}
	}

	public void handleCtrlS() {
		saveButtonActionPerformed(null);
	}

	public void handleCtrlO() {
		openButtonActionPerformed(null);
	}

	public void handleCtrlN() {
		newButtonActionPerformed(null);
	}

	public void handleCtrlQ() {
		if (mainTabbedPane.getSelectedIndex() > 0) {
			convertButtonActionPerformed(null);
		}
	}

	public void handleCtrlScroll(int direction) {
		if (direction < 0) {
			zoomInButtonActionPerformed(null);
		} else if (direction > 0) {
			zoomOutButtonActionPerformed(null);
		}
	}

	private void _addMouseWheelListener(JScrollPane tab) {
		tab.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.isControlDown()) {
					tab.setWheelScrollingEnabled(false);
					handleCtrlScroll(e.getWheelRotation());
				} else {
					tab.setWheelScrollingEnabled(true);
				}
			}
		});
	}

    private void wireToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wireToggleButtonActionPerformed
		if (!getCanvas().simulating) {
			autoToggleButton.setSelected(false);
			if (!wireToggleButton.isSelected()) {
				getCanvas().wiring = false;
				getCanvas().wireStartX = -1;
				getCanvas().wireStartY = -1;
			}
		} else {
			wireToggleButton.setSelected(false);
		}
    }//GEN-LAST:event_wireToggleButtonActionPerformed

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
		getCanvas().module.clear();
		getCanvas().setGrid(new CircuitGrid(300, getCanvas()));
		getCanvas().repaint();
		refreshTree();
    }//GEN-LAST:event_clearButtonActionPerformed

    private void zoom100ButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoom100ButtonActionPerformed
		for (int i = 1; i < mainTabbedPane.getTabCount(); i++) {
			getCanvas(i).gridSize = 10;
			getCanvas(i).setGridSize(getCanvas(i).gridSize);
		}
		zoom100Button.setText("100%");
    }//GEN-LAST:event_zoom100ButtonActionPerformed

    private void propertyTablePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_propertyTablePropertyChange
		Vertex v = propertyTableModel.vertex;
		if (v == null) {
			return;
		}
		v.updateProperty(getCanvas());
		getCanvas().recordAction();
		getCanvas().recalculateLines();
		getCanvas().repaint();
		refreshComponentTreeButtonActionPerformed(null);
    }//GEN-LAST:event_propertyTablePropertyChange

    private void autoToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoToggleButtonActionPerformed
		if (!getCanvas().simulating) {
			wireToggleButton.setSelected(false);
			getCanvas().wiring = false;
			if (!autoToggleButton.isSelected()) {
				getCanvas().wiring = false;
				getCanvas().startPort = null;
				getCanvas().endPort = null;
			}
		} else {
			autoToggleButton.setSelected(false);
			getCanvas().startPort = null;
			getCanvas().endPort = null;
		}
    }//GEN-LAST:event_autoToggleButtonActionPerformed

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
		if (JOptionPane.showConfirmDialog(this, "Confirm to create new project?", "message", JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION) {
//			JDialog dialog = new JDialog((Frame) SwingUtilities.getWindowAncestor(this), "Process", false);
//			dialog.setSize(300, 100);
//			JLabel label = new JLabel("Launching New Quantr Logic...");
//			label.setHorizontalAlignment(JLabel.CENTER);
//			dialog.add(label);
//			dialog.setLocationRelativeTo(this);
//			dialog.setVisible(true);

//			Thread processThread = new Thread(() -> {
//				try {
//					File configFile = File.createTempFile("config", ".properties");
//					configFile.deleteOnExit();
//					FileWriter writer = new FileWriter(configFile);
//					writer.write("foo=bar\n");
//					writer.close();
//					List<String> arguments = new ArrayList<>();
//					arguments.add("java");
//					arguments.add("-cp");
//					arguments.add(System.getProperty("java.class.path"));
//					arguments.add("hk.quantr.logic.QuantrLogic");
//					arguments.add(configFile.getAbsolutePath());
//					ProcessBuilder pb = new ProcessBuilder(arguments);
//					pb.inheritIO();
//					Process process = pb.start();
//					Thread.sleep(4000);
//					dialog.dispose();
//				} catch (IOException | InterruptedException ex) {
//					System.err.println("Thread error: " + ex);
//				}
//			});
//			processThread.start();
			QuantrLogic quantrLogic = new QuantrLogic();
			java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					quantrLogic.setVisible(true);
				}
			});

			quantrLogic.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					if (quantrLogic.quantrGraphPanel1.saved) {
						quantrLogic.quantrGraphPanel1.handleCtrlS();
						quantrLogic.dispose();
						System.exit(0);
					} else {
						int choice = JOptionPane.showConfirmDialog(quantrLogic, "Save before closing?", "Confirm Shutdown", JOptionPane.YES_NO_CANCEL_OPTION);
						switch (choice) {
							// Close the application without saving
							case JOptionPane.NO_OPTION:
								quantrLogic.quantrGraphPanel1.projectFile.delete();
								quantrLogic.dispose();
								System.exit(0);
								break;
							// Save changes and then close the application
							case JOptionPane.YES_OPTION:
								quantrLogic.quantrGraphPanel1.handleCtrlS();
								if (quantrLogic.quantrGraphPanel1.saved) {
									quantrLogic.dispose();
									System.exit(0);
									break;
								}
							// Do not close the application
							default:
								quantrLogic.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
						}
					}
				}
			});

		}
    }//GEN-LAST:event_newButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Specify a file to open");
		fileChooser.setFileFilter(new FileNameExtensionFilter("Quantr Logic (.ql)", "ql"));
		fileChooser.setPreferredSize(new Dimension(800, 600));
		if (Setting.getInstance().openProjectPath != null) {
			fileChooser.setCurrentDirectory(new File(Setting.getInstance().openProjectPath));
		}
		int userSelection = fileChooser.showOpenDialog(frame);
		if (userSelection == JFileChooser.APPROVE_OPTION) {
//			projectFile.delete();
			projectFile = fileChooser.getSelectedFile();
			this.saved = true;
			Setting.getInstance().openProjectPath = projectFile.getParentFile().getAbsolutePath();
			Setting.getInstance().save();
			frame.setTitle(projectFile.getAbsolutePath());
			data = Data.load(projectFile);

			initTreeAndCanvas();
		}
    }//GEN-LAST:event_openButtonActionPerformed

	void initTreeAndCanvas() {
		((JScrollPane) mainTabbedPane.getComponentAt(1)).setViewportView(new QuantrGraphCanvas(this, data.modules.get("Main")));
		for (Module m : data.modules.values()) {
			if (!m.name.equals("Main")) {
				JScrollPane temp = new JScrollPane();
				temp.setViewportView(new QuantrGraphCanvas(this, m));
				_addMouseWheelListener(temp);
				mainTabbedPane.addTab(m.name, temp);
				projectTreeRoot.add(new ProjectTreeNode(m.name));
			}
		}
		currentCanvas = (QuantrGraphCanvas) ((JScrollPane) mainTabbedPane.getComponentAt(1)).getViewport().getView();
		((DefaultTreeModel) projectTree.getModel()).reload(projectTreeRoot);
		refreshComponentTreeButtonActionPerformed(null);
		getCanvas().repaint();
	}

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
		if (!saved) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Specify a file to save");
			fileChooser.setFileFilter(new FileNameExtensionFilter("Quantr Logic (.ql)", "ql"));
			fileChooser.setPreferredSize(new Dimension(800, 600));
			if (Setting.getInstance().saveProjectPath != null) {
				fileChooser.setCurrentDirectory(new File(Setting.getInstance().saveProjectPath));
			}
			int userSelection = fileChooser.showSaveDialog(frame);
			if (userSelection == JFileChooser.APPROVE_OPTION) {
				projectFile.delete();
				File file = fileChooser.getSelectedFile();
				if (FilenameUtils.getExtension(file.getName()).equals("ql")) {
					projectFile = fileChooser.getSelectedFile();
				} else {
					projectFile = new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName()) + ".ql");
				}
				Setting.getInstance().saveProjectPath = projectFile.getParentFile().getAbsolutePath();
				Setting.getInstance().save();
				frame.setTitle(projectFile.getAbsolutePath());
				saved = true;
			}
		}
		data.save(projectFile);
    }//GEN-LAST:event_saveButtonActionPerformed

    private void settingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_settingButtonActionPerformed
		SettingDialog d = new SettingDialog(null, true);
		d.setLocationRelativeTo(null);
		d.setVisible(true);
    }//GEN-LAST:event_settingButtonActionPerformed

	public JClosableTabbedPane getMainTabbedPane() {
		return mainTabbedPane;
	}

    private void deleteAllAutoEdgesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteAllAutoEdgesMenuItemActionPerformed
		if (componentTree.getSelectionPath() != null) {
			Object obj = ((DefaultMutableTreeNode) componentTree.getSelectionPath().getLastPathComponent()).getUserObject();
			if (obj instanceof String) {
				if (obj.equals("Vertex")) {
					getCanvas().module.vertices.clear();
					refreshTree();
					getCanvas().repaint();
				} else if (obj.equals("Edge")) {
					getCanvas().module.edges.clear();
					refreshTree();
					getCanvas().repaint();
				}
			}
		}
    }//GEN-LAST:event_deleteAllAutoEdgesMenuItemActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
		if (!getCanvas().simulating && (!getCanvas().selectedVertices.isEmpty() || !getCanvas().selectedEdges.isEmpty())) {
			if (!getCanvas().selectedEdges.isEmpty()) {
				for (Edge e : getCanvas().selectedEdges) {
					if (e instanceof Edge) {
						for (Port p : e.findConnectedPorts()) {
							p.edges.remove(e);
						}
						getCanvas().removeEdge((Edge) e);
						getCanvas().module.edges.remove(e);
					}
				}
				getCanvas().selectedEdges.clear();
			}
			if (!getCanvas().selectedVertices.isEmpty()) {
				for (Vertex v : getCanvas().selectedVertices) {
					getCanvas().module.vertices.remove(v);
					for (Input input : v.inputs) {
						if (input.vertexPort != null && input.vertexPort.vertexPort != null) {
							input.vertexPort.vertexPort = null;
						}
					}
					for (Output output : v.outputs) {
						if (output.vertexPort != null && output.vertexPort.vertexPort != null) {
							output.vertexPort.vertexPort = null;
						}
					}
				}
				getCanvas().selectedVertices.clear();
			}
			getCanvas().recalculateLines();
			getCanvas().repaint();
			refreshTree();
		}
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void showLevelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showLevelButtonActionPerformed
		getCanvas().calculateLevel();
		getCanvas().showLevel(showLevelButton.isSelected());
    }//GEN-LAST:event_showLevelButtonActionPerformed

    private void testButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testButtonActionPerformed
		Simulator s = new Simulator(getCanvas());
		for (int x = 0; x < 100; x++) {
//			s.start();
		}
    }//GEN-LAST:event_testButtonActionPerformed

    private void autoPositionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoPositionButtonActionPerformed
		System.out.println("called");
		ArrayList<ArrayList<Vertex>> autoVertices = new ArrayList();
		HashMap<Port, ArrayList<Port>> portMap = new HashMap();
//		HashMap<Port, Port> portMap = new HashMap();
		if (evt != null) {
			autoVertices = getCanvas().calculateLevel();
			for (int i = 0; i < getCanvas().module.vertices.size(); i++) {
				if (getCanvas().module.vertices.get(i).level == -1) {
					continue;
				}
				for (Input input : getCanvas().module.vertices.get(i).inputs) {
					portMap.put(input, getCanvas().module.vertices.get(i).portConnectedTo(input));
				}
				for (Output output : getCanvas().module.vertices.get(i).outputs) {
					portMap.put(output, getCanvas().module.vertices.get(i).portConnectedTo(output));
				}
			}
			getCanvas().module.edges.clear();
			getCanvas().recalculateLines();
		} else {
			autoVertices = this.booleanAlgebraAutoPositionVertices;
			this.getCanvas().maxLevel = 4;
			Object[] map = this.portConnectionMap.keySet().toArray();
			portMap = this.portConnectionMap;
			for (int i = 0; i < portMap.keySet().toArray().length; i++) {
				System.out.println(portMap.keySet().toArray()[i] + " " + portMap.get((Port) portMap.keySet().toArray()[i]));
			}
//			for (int i = 0; i < map.length; i++) {
//				ArrayList<Port> temp = new ArrayList();
//				temp.add(this.portConnectionMap.get((Port) map[i]));
//				portMap.put((Port) map[i], temp);
//			}
		}
		int col = -1;
		int maxWidth = 0;
		int maxHeight = 0;
		int colWidth = 0;
		int colHeight;
		int distance = 6 + getCanvas().module.vertices.size() / 50; // change this to increase distance between vertex
		int[] vertexWidth = new int[autoVertices.size()];
		System.out.println(autoVertices.size());
		for (int i = 0; i < autoVertices.size(); i++) {
			int max = -1;
			for (Vertex v : autoVertices.get(i)) {
				if (v.width > max) {
					max = v.width;
				}
			}
			if (i > 0) {
				vertexWidth[i] = max + vertexWidth[i - 1] + distance;
				System.out.println(max + "," + vertexWidth[i - 1]);
			} else {
				vertexWidth[i] = max + distance;
			}
		}
		System.out.println("");
//		for (int i = 1; i < vertexWidth.length; i++) {
//			vertexWidth[i] = 
//		}
//		for (int i = autoVertices.size() - 1; i >= 0; i--) {
//			vertexWidth[i] = -1;
//			for (Vertex v : autoVertices.get(i)) {
//				if (v.width > vertexWidth[i]) {
//					vertexWidth[i] = v.width;
//				}
//			}
//			if (i > 0) {
//				vertexWidth[i] = vertexWidth[i] + vertexWidth[i - 1];
//			}
//		}
		for (int i = 0; i < autoVertices.size(); i++) {
			colHeight = distance;
			for (int j = 0; j < autoVertices.get(i).size(); j++) {
				autoVertices.get(i).get(j).setLocation(vertexWidth[i], colHeight);
				System.out.println(vertexWidth[i] + "," + colHeight);
				colHeight += autoVertices.get(i).get(j).height + distance;
			}
		}
		//		if (autoVertices.size() > getCanvas().maxLevel + 1) {
//			col++;
//			colWidth += maxWidth + 2;
//			maxWidth = 0;
//			colHeight = 2;
//			for (int i = 0; i < autoVertices.get(autoVertices.size() - 1).size(); i++) {
////				if (colHeight >= 100) {
////					col++;
////					colWidth += maxWidth + 2;
////					maxWidth = 0;
////					colHeight = 2;
////				}
//				if (autoVertices.get(autoVertices.size() - 1).get(i).width > maxWidth) {
//					maxWidth = autoVertices.get(autoVertices.size() - 1).get(i).width;
//				}
//				autoVertices.get(autoVertices.size() - 1).get(i).setLocation(colWidth, colHeight);
//				colHeight += autoVertices.get(autoVertices.size() - 1).get(i).height + 2;
//			}
//		}
//		for (int i = 0; i < getCanvas().maxLevel + 1; i++) {
//			col++;
//			colWidth += maxWidth + distance;
//			maxWidth = 0;
//			colHeight = distance;
//			for (int j = 0; j < autoVertices.get(i).size(); j++) {
////				if (colHeight >= 60) {
////					col++;
////					colWidth += maxWidth + distance;
////					maxWidth = 0;
////					colHeight = distance;
////				}
//				if (autoVertices.get(i).get(j).width > maxWidth) {
//					maxWidth = autoVertices.get(i).get(j).width;
//				}
//				autoVertices.get(i).get(j).setLocation(colWidth, colHeight);
//
//				System.out.println(autoVertices.get(i).get(j).name + " " + autoVertices.get(i).get(j).x + "--->>" + colWidth + ", " + colHeight);
//				colHeight += autoVertices.get(i).get(j).height + distance;
//			}
//
//		}
//		for (int i = 0; i < portMap.keySet().toArray().length; i++) {
//			for (int j = 0; j < portMap.get(portMap.keySet().toArray()[i]).size(); j++) {
//				System.out.println("loading : i: " + (i + 1) + "/" + portMap.keySet().toArray().length + " j: " + (j + 1) + "/" + portMap.get(portMap.keySet().toArray()[i]).size());
//				getCanvas().getGrid().autoEdgeSync((Port) portMap.keySet().toArray()[i], (Port) portMap.get(portMap.keySet().toArray()[i]).get(j));
////				getCanvas().repaint();
//			}
//		}
		try {
			getCanvas().getGrid().circuitRouting(portMap);
		} catch (Exception ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
		getCanvas().recalculateLines();
		getCanvas().repaint();
    }//GEN-LAST:event_autoPositionButtonActionPerformed

    private void randomButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomButtonActionPerformed
		int noOfGate = Integer.parseInt(JOptionPane.showInputDialog(this, "How many gate?", "Question", JOptionPane.INFORMATION_MESSAGE));
		int width = getCanvas().getWidth() / getCanvas().gridSize;
		int height = getCanvas().getHeight() / getCanvas().gridSize;

		Class v[] = new Class[]{AndGate.class,
			OrGate.class
		};

		try {
			for (int z = 0; z < noOfGate; z++) {
				Class cls = v[ThreadLocalRandom.current().nextInt(v.length)];
				Object clsInstance = (Object) cls.getDeclaredConstructor(String.class
				).newInstance("Node " + z);
				Vertex vertex = (Vertex) clsInstance;
				int x;
				int y;
				do {
					x = ThreadLocalRandom.current().nextInt(width);
					y = ThreadLocalRandom.current().nextInt(height);
				} while (getCanvas().module.isOverlap(x, y, vertex.width, vertex.height));
				vertex.setLocation(x, y);
				getCanvas().module.vertices.add(vertex);

			}
		} catch (Exception ex) {
			Logger.getLogger(QuantrGraphPanel.class
					.getName()).log(Level.SEVERE, null, ex);
		}

		getCanvas().repaint();
		refreshComponentTreeButtonActionPerformed(null);
    }//GEN-LAST:event_randomButtonActionPerformed

    private void simulateToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simulateToggleButtonActionPerformed
		getCanvas().simulating = simulateToggleButton.isSelected();
		if (getCanvas().simulating) {
			autoToggleButton.setSelected(false);
			wireToggleButton.setSelected(false);
			getCanvas().wiring = false;
			simulateToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/control_stop.png")));
			getCanvas().clearVertexSelection();

//		testEngine();
//		Emulator ss = new Emulator(((QuantrGraphCanvas)((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponent(((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponentCount() - 1)));
			simulator = new Simulator(getCanvas());
//			simulator2 = new SimulatorMultiThread(getCanvas());
//		s
//		calculateLevel();
//		s.start(((QuantrGraphCanvas)((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponent(((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponentCount() - 1)));
			getCanvas().calculateLevel();

			if (getCanvas().maxLevel != -1 && getCanvas().simulating) {
				new Thread(simulator).start();
				new Thread(() -> {
					while (getCanvas().simulating) {
						try {
							simulator.dump();
							Thread.sleep(1000);
						} catch (InterruptedException ex) {
							Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
				}).start();
				try {
					quantrVCDComponent1.loadVCD(tempVCDFile);
				} catch (IOException ex) {
					Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		} else {
			simulateToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hk/quantr/logic/icon/control_play.png")));
			simulator.dump();
			simulator.stopped = true;
			getCanvas().repaint();

//			Get boolean expression
//			for (Vertex v : getCanvas().module.vertices) {
//				if (v.level == -1 || !(v instanceof OutputPin)) {
//					continue;
//				}
//				String boolExp = loopChild(v);
//				System.out.println(boolExp);
//			}
			long time = System.nanoTime();
			
//			genDot();

//			System.out.println(System.nanoTime() - time);
//			for (int i = 0; i < BDDBuilder.variableMap.keySet().toArray().length; i++) {
//				System.out.println(((Vertex) BDDBuilder.variableMap.keySet().toArray()[i]).name + ", " + BDDBuilder.variableMap.get(BDDBuilder.variableMap.keySet().toArray()[i]));
//			}
		}
		handleESC();
    }//GEN-LAST:event_simulateToggleButtonActionPerformed
//ArrayList<String> 
	String s = "";

	void genDot() {
		for (Vertex v : getCanvas().module.vertices) {
			if (v.level == -1 || !(v instanceof OutputPin)) {
				continue;
			} else {
				BDDOperation bddOperator = new BDDOperation(v);
				dot(bddOperator.getResultBDD());
			}
		}

	}

	void dot(BDD bdd) {
		PrintStream out = System.out;
//	ArrayList<String> s = new ArrayList();
		s = "";
		s += ("digraph G {") + "\n";
		s += ("0 [shape=box, label=\"0\", style=filled, shape=box, height=0.3, width=0.3];") + "\n";
		s += ("1 [shape=box, label=\"1\", style=filled, shape=box, height=0.3, width=0.3];") + "\n";

//        out.println("digraph G {");
//        out.println("0 [shape=box, label=\"0\", style=filled, shape=box, height=0.3, width=0.3];");
//        out.println("1 [shape=box, label=\"1\", style=filled, shape=box, height=0.3, width=0.3];");
		boolean[] visited = new boolean[bdd.nodeCount() + 2];
		visited[0] = true;
		visited[1] = true;
		HashMap<BDD, Integer> map = new HashMap<>();
		map.put(bdd.getFactory().zero(), 0);
		map.put(bdd.getFactory().one(), 1);
		printdot_rec(bdd, out, 1, visited, map);

		for (Iterator<BDD> i = map.keySet().iterator(); i.hasNext();) {
			BDD b = i.next();
			b.free();
		}
		s += "}";
//		out.println("}");
		try {
			FileUtils.writeStringToFile(new File("./hello.dot"), s, "utf-8");
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	int printdot_rec(BDD bdd, PrintStream out, int current, boolean[] visited, HashMap<BDD, Integer> map) {
		Integer ri = (map.get(bdd));
		if (ri == null) {
			map.put(bdd.id(), ri = ++current);
		}
		int r = ri.intValue();
		if (visited[r]) {
			return current;
		}
		visited[r] = true;

		// TODO: support labelling of vars.
		s += r + " [label=\"" + bdd.var() + "\"];" + "\n";
//		out.println(r + " [label=\"" + this.var() + "\"];");

		BDD l = bdd.low(), h = bdd.high();
		Integer li = map.get(l);
		if (li == null) {
			map.put(l.id(), li = ++current);
		}
		int low = li.intValue();
		Integer hi = map.get(h);
		if (hi == null) {
			map.put(h.id(), hi = ++current);
		}
		int high = hi.intValue();
		s += r + " -> " + low + " [style=dotted];" + "\n";
		s += r + " -> " + high + " [style=filled];" + "\n";
//		out.println(r + " -> " + low + " [style=dotted];");
//		out.println(r + " -> " + high + " [style=filled];");

		current = printdot_rec(l, out, current, visited, map);
		l.free();
		current = printdot_rec(h, out, current, visited, map);
		h.free();
		return current;
	}

	String loopChild(Vertex v) throws Exception {
		String output = "";
		if (v == null) {
			return output;
		} else if (v.hasFeedback()) {
			throw new Exception("Circuit contains Feedback component");
		} else if (v instanceof OutputPin) {
			output += v.name + "=";
			output += loopChild((v.inputs.get(0).getConnectedOutput() != null) ? v.inputs.get(0).getConnectedOutput() : null);
		} else if (v instanceof Pin) {
			output += v.name;
		} else if (v instanceof AndGate) {
			for (int i = 0; i < v.inputs.size(); i++) {
				output += loopChild(v.inputs.get(i).getConnectedOutput());
			}
		} else if (v instanceof NandGate) {
			for (int i = 0; i < v.inputs.size(); i++) {
				output += loopChild(v.inputs.get(i).getConnectedOutput());
			}
			output = "(" + output + ")'";
		} else if (v instanceof OrGate) {
			for (int i = 0; i < v.inputs.size(); i++) {
				int len = output.length();
				output += loopChild(v.inputs.get(i).getConnectedOutput());
				if (i != v.inputs.size() - 1 && len != output.length()) {
					output += "+";
				}
			}
			output = "(" + output + ")";
		} else if (v instanceof NorGate) {
			output += "(";
			for (int i = 0; i < v.inputs.size(); i++) {
				int len = output.length();
				output += loopChild(v.inputs.get(i).getConnectedOutput());
				if (i != v.inputs.size() - 1 && len != output.length()) {
					output += "+";
				}
			}
			output += ")'";
		} else if (v instanceof XorGate) { // only for two input Xorgate now
			for (int i = 0; i < (1 << v.inputs.size()); i++) {
				if (Integer.bitCount(i) % 2 == 1) {
					output += "(";
					for (int j = (1 << (v.inputs.size() - 1)), idx = 0; idx < v.inputs.size(); j >>= 1, idx++) {// idx is index of input of vertex, loop the most left bit
						if ((j & i) != 0) { // e.g. 00110, true if the bit is 1
							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
								output += "(";
							}
							output += loopChild(v.inputs.get(idx).getConnectedOutput());
							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
								output += ")";
							}
						} else {
							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
								output += "(";
							}
							output += loopChild(v.inputs.get(idx).getConnectedOutput());
							if (v.inputs.get(idx).getConnectedOutput() != null) {
								if (!(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
									output += ")";
								}
								output += "'";
							}
						}
					}
					output += ")+";
				}
			}
			if (output.charAt(output.length() - 1) == '+') {
				output = output.substring(0, output.length() - 1);
			}

		} else if (v instanceof XnorGate) {
			for (int i = 0; i < (1 << v.inputs.size()); i++) {
				if (Integer.bitCount(i) % 2 == 1) {
					output += "(";
					for (int j = (1 << (v.inputs.size() - 1)), idx = 0; idx < v.inputs.size(); j >>= 1, idx++) {// idx is index of input of vertex, loop the most left bit
						if ((j & i) != 0) { // e.g. 00110, true if the bit is 1
							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
								output += "(";
							}
							output += loopChild(v.inputs.get(idx).getConnectedOutput());
							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
								output += ")";
							}
						} else {
							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
								output += "(";
							}
							output += loopChild(v.inputs.get(idx).getConnectedOutput());
							if (v.inputs.get(idx).getConnectedOutput() != null) {
								if (!(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
									output += ")";
								}
								output += "'";
							}
						}
					}
					output += ")+";
				}
			}
			if (output.charAt(output.length() - 1) == '+') {
				output = output.substring(0, output.length() - 1);
			}
			output = "(" + output + ")'";
		} else if (v instanceof NotGate) {
			output += loopChild(v.inputs.get(0).getConnectedOutput()) + "'";
		} else if (v instanceof Buffer) {
			output += loopChild(v.inputs.get(0).getConnectedOutput());
		} else if (v instanceof Constant) {
			output += v.outputs.get(0).value;
		}
		return output;
	}

//	BDD buildBDD(Vertex v) throws Exception {
//		String output = "";
//		if (v == null) {
//			return output;
//		} else if (v.hasFeedback()) {
//			throw new Exception("Circuit contains Feedback component");
//		} else if (v instanceof OutputPin) {
//			output += v.name + "=";
//			output += loopChild((v.inputs.get(0).getConnectedOutput() != null) ? v.inputs.get(0).getConnectedOutput() : null);
//		} else if (v instanceof Pin) {
//			output += v.name;
//		} else if (v instanceof AndGate) {
//			for (int i = 0; i < v.inputs.size(); i++) {
//				output += loopChild(v.inputs.get(i).getConnectedOutput());
//			}
//		} else if (v instanceof NandGate) {
//			for (int i = 0; i < v.inputs.size(); i++) {
//				output += loopChild(v.inputs.get(i).getConnectedOutput());
//			}
//			output = "(" + output + ")'";
//		} else if (v instanceof OrGate) {
//			for (int i = 0; i < v.inputs.size(); i++) {
//				int len = output.length();
//				output += loopChild(v.inputs.get(i).getConnectedOutput());
//				if (i != v.inputs.size() - 1 && len != output.length()) {
//					output += "+";
//				}
//			}
//			output = "(" + output + ")";
//		} else if (v instanceof NorGate) {
//			output += "(";
//			for (int i = 0; i < v.inputs.size(); i++) {
//				int len = output.length();
//				output += loopChild(v.inputs.get(i).getConnectedOutput());
//				if (i != v.inputs.size() - 1 && len != output.length()) {
//					output += "+";
//				}
//			}
//			output += ")'";
//		} else if (v instanceof XorGate) { // only for two input Xorgate now
//			for (int i = 0; i < (1 << v.inputs.size()); i++) {
//				if (Integer.bitCount(i) % 2 == 1) {
//					output += "(";
//					for (int j = (1 << (v.inputs.size() - 1)), idx = 0; idx < v.inputs.size(); j >>= 1, idx++) {// idx is index of input of vertex, loop the most left bit
//						if ((j & i) != 0) { // e.g. 00110, true if the bit is 1
//							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//								output += "(";
//							}
//							output += loopChild(v.inputs.get(idx).getConnectedOutput());
//							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//								output += ")";
//							}
//						} else {
//							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//								output += "(";
//							}
//							output += loopChild(v.inputs.get(idx).getConnectedOutput());
//							if (v.inputs.get(idx).getConnectedOutput() != null) {
//								if (!(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//									output += ")";
//								}
//								output += "'";
//							}
//						}
//					}
//					output += ")+";
//				}
//			}
//			if (output.charAt(output.length() - 1) == '+') {
//				output = output.substring(0, output.length() - 1);
//			}
//
//		} else if (v instanceof XnorGate) {
//			for (int i = 0; i < (1 << v.inputs.size()); i++) {
//				if (Integer.bitCount(i) % 2 == 1) {
//					output += "(";
//					for (int j = (1 << (v.inputs.size() - 1)), idx = 0; idx < v.inputs.size(); j >>= 1, idx++) {// idx is index of input of vertex, loop the most left bit
//						if ((j & i) != 0) { // e.g. 00110, true if the bit is 1
//							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//								output += "(";
//							}
//							output += loopChild(v.inputs.get(idx).getConnectedOutput());
//							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//								output += ")";
//							}
//						} else {
//							if (v.inputs.get(idx).getConnectedOutput() != null && !(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//								output += "(";
//							}
//							output += loopChild(v.inputs.get(idx).getConnectedOutput());
//							if (v.inputs.get(idx).getConnectedOutput() != null) {
//								if (!(v.inputs.get(idx).getConnectedOutput().parent instanceof Pin)) { // add symbol ()
//									output += ")";
//								}
//								output += "'";
//							}
//						}
//					}
//					output += ")+";
//				}
//			}
//			if (output.charAt(output.length() - 1) == '+') {
//				output = output.substring(0, output.length() - 1);
//			}
//			output = "(" + output + ")'";
//		} else if (v instanceof NotGate) {
//			output += loopChild(v.inputs.get(0).getConnectedOutput()) + "'";
//		} else if (v instanceof Buffer) {
//			output += loopChild(v.inputs.get(0).getConnectedOutput());
//		} else if (v instanceof Constant) {
//			output += v.outputs.get(0).value;
//		}
//		return output;
//	}
	public String loopChild(Port p) throws Exception { // null port handler
		if (p != null) {
			if (p.parent instanceof DipSwitch) {
				return Long.toString(p.value);
			} else {
				return loopChild(p.parent);
			}
		} else {
			return null;
		}
	}

    private void highDPIJLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_highDPIJLabel1MouseClicked
		try {
			Desktop.getDesktop().browse(new URI("https://www.rmit.edu.au/about/schools-colleges/computing-technologies/our-teaching-areas/computer-science"));
		} catch (IOException | URISyntaxException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_highDPIJLabel1MouseClicked

    private void saveToQuantrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveToQuantrButtonActionPerformed
		try {
			if (data.projectName == null) {
				data.projectName = JOptionPane.showInputDialog(null, "Enter project name", "Question", JOptionPane.PLAIN_MESSAGE);
				if (data.projectName == null) {
					return;
				}
			}
			// check count
				HttpPost post = new HttpPost("https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/api/quantr-logic/getProjectCount.php");
			List<NameValuePair> urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("projectName", data.projectName));
			urlParameters.add(new BasicNameValuePair("ciperText", Setting.getInstance().ciperText));
			post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));

			try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
				int count = Integer.parseInt(EntityUtils.toString(response.getEntity()));
				if (count > 0) {
					int option = JOptionPane.showConfirmDialog(null, "Confirm to overwrite?", "Warning", JOptionPane.YES_NO_OPTION);
					if (option != JOptionPane.YES_OPTION) {
						return;
					}
				}
			} catch (IOException ex) {
				Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
			}
			// end check count

			// save to quantr
			Dimension size = getSize();
			BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = image.createGraphics();
			paint(g);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ImageIO.write(image, "png", byteArrayOutputStream);

			post = new HttpPost("https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/api/quantr-logic/saveProject.php");
			urlParameters = new ArrayList<>();
			urlParameters.add(new BasicNameValuePair("projectName", data.projectName));
			urlParameters.add(new BasicNameValuePair("ciperText", Setting.getInstance().ciperText));
			urlParameters.add(new BasicNameValuePair("xml", data.getXML()));
			urlParameters.add(new BasicNameValuePair("image", Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray())));
			post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));

			try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
				String result = EntityUtils.toString(response.getEntity());
				System.out.println(result);
			} catch (IOException ex) {
				Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
			}
			// end save to quantr
		} catch (UnsupportedEncodingException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_saveToQuantrButtonActionPerformed

    private void loginToQuantrLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginToQuantrLabelMouseClicked
		LoginQuantrDialog d = new LoginQuantrDialog(frame, true);
		d.setLocationRelativeTo(null);
		d.setVisible(true);
		if (d.username != null) {
			Setting.getInstance().ciperText = d.cipherText;
			Setting.getInstance().save();
			loginToQuantrLabel.setText("Logined in as " + d.username);
			myProjectLabel.setText("<html><a href=\"https://www.quantr.foundation/project/?project=Quantr%20Logic\">My Projects</a></html>");
		}
    }//GEN-LAST:event_loginToQuantrLabelMouseClicked

    private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
		simulateToggleButton.setSelected(false);
		getCanvas().simulating = false;
		getCanvas().reset();
		try {
			quantrVCDComponent1.loadVCD(tempVCDFile);
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
		statusLabel1.setText("Simulation rate: -- cycles/s");
		getCanvas().repaint();
    }//GEN-LAST:event_resetButtonActionPerformed

    private void myProjectLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_myProjectLabelMouseClicked
		try {
			Desktop.getDesktop().browse(new URI("https://www.quantr.foundation/project/?project=Quantr%20Logic&tab=My%20Project"));
		} catch (URISyntaxException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(QuantrGraphPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
    }//GEN-LAST:event_myProjectLabelMouseClicked

    private void addModuleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addModuleButtonActionPerformed
		String moduleName = JOptionPane.showInputDialog(null, "Enter the name of the module:", "Add New Module", JOptionPane.PLAIN_MESSAGE);
		for (int i = 1; i < mainTabbedPane.getTabCount(); i++) {
//			System.out.println("tabComponent: " + mainTabbedPane.getTitleAt(i) + " / " + getCanvas(i).module.name);
			if (moduleName == null || getCanvas(i).module.name.equals(moduleName)) {
				return;
			}
		}
		JScrollPane tab = new JScrollPane();
		tab.setViewportView(new QuantrGraphCanvas(this, new Module(moduleName)));
		_addMouseWheelListener(tab);
//		tab.setViewportView(((QuantrGraphCanvas) ((JScrollPane) mainTabbedPane.getComponentAt(0)).getComponent(((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponentCount() - 1)));
		mainTabbedPane.addTab(moduleName, tab);
		this.data.modules.put(moduleName, ((QuantrGraphCanvas) tab.getViewport().getView()).module);
		mainTabbedPane.setSelectedIndex(mainTabbedPane.getTabCount() - 1);
		projectTreeRoot.add(new ProjectTreeNode(moduleName));
		((DefaultTreeModel) projectTree.getModel()).reload(projectTreeRoot);
		currentCanvas = (QuantrGraphCanvas) tab.getViewport().getView();
		getCanvas().recalculateLines();
// to be fixed
    }//GEN-LAST:event_addModuleButtonActionPerformed

    private void mainTabbedPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mainTabbedPaneMouseClicked
//		System.out.println("name: " + ((QuantrGraphCanvas) ((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getViewport().getView()).module.name);
		if (mainTabbedPane.getSelectedComponent() instanceof JScrollPane) {
			currentCanvas = ((QuantrGraphCanvas) ((JScrollPane) mainTabbedPane.getSelectedComponent()).getViewport().getView());
			if (mainTabbedPane.getSelectedComponent() != null && !mainTabbedPane.getTitleAt(mainTabbedPane.getSelectedIndex()).equals("VCD")) {
//			System.out.println("idx: " + mainTabbedPane.getSelectedIndex());
				for (Vertex v : getCanvas().module.vertices) {
					if (v instanceof ModuleComponent) {
						v.updateProperty();
					}
				}
				((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).setViewportView(getCanvas());
				getCanvas().recalculateLines();
				refreshTree();
				handleESC();
			}
		}
    }//GEN-LAST:event_mainTabbedPaneMouseClicked

    private void projectTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_projectTreeMouseClicked
		if (projectTree.getSelectionPaths() != null) {
			ProjectTreeNode node = (ProjectTreeNode) projectTree.getLastSelectedPathComponent();
//			System.out.println("node: " + node.data.toString());
			if (evt.getClickCount() == 2) {
				if (!node.data.toString().equals("Project")) {
					boolean flag = true;
					for (int i = 1; i < mainTabbedPane.getTabCount(); i++) {
						if (getCanvas(i).module.name.equals(node.data.toString())) {
							flag = false;
						}
					}
					if (flag) {
						javax.swing.JScrollPane pane = new javax.swing.JScrollPane();
						pane.setViewportView(new QuantrGraphCanvas(this, data.modules.get(node.data.toString())));
						mainTabbedPane.addTab(node.data.toString(), pane);
					}
					mainTabbedPane.setSelectedIndex(mainTabbedPane.indexOfTab(node.data.toString()));
					mainTabbedPaneMouseClicked(evt);
				}
			}
		}
    }//GEN-LAST:event_projectTreeMouseClicked

    private void northButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_northButtonActionPerformed
		// TODO add your handling code here:
		if (!getCanvas().simulating) {
			for (Vertex v : getCanvas().selectedVertices) {
				v.properties.put("Orientation", "north");
				v.updateProperty();
				System.out.println(v.height);
				System.out.println(v.inputs.get(0).deltaY);
				getCanvas().repaint();
			}
		}
    }//GEN-LAST:event_northButtonActionPerformed

    private void westButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_westButtonActionPerformed
		// TODO add your handling code here:
		if (!getCanvas().simulating) {
			for (Vertex v : getCanvas().selectedVertices) {
				v.properties.put("Orientation", "west");
				v.updateProperty();
				getCanvas().repaint();
			}
		}
    }//GEN-LAST:event_westButtonActionPerformed

    private void eastButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eastButtonActionPerformed
		// TODO add your handling code here:
		if (!getCanvas().simulating) {
			for (Vertex v : getCanvas().selectedVertices) {
				v.properties.put("Orientation", "east");
				v.updateProperty();
				getCanvas().repaint();

			}
		}
    }//GEN-LAST:event_eastButtonActionPerformed

    private void southButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_southButtonActionPerformed
		// TODO add your handling code here:
		if (!getCanvas().simulating) {
			for (Vertex v : getCanvas().selectedVertices) {
				v.properties.put("Orientation", "south");
				v.updateProperty();
				getCanvas().repaint();
			}
		}
    }//GEN-LAST:event_southButtonActionPerformed

    private void convertButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_convertButtonActionPerformed
		String expr = (String) JOptionPane.showInputDialog(
				frame,
				"Enter a boolean algebra expression:",
				"Convert Boolean Algebra Expression",
				JOptionPane.PLAIN_MESSAGE
		);
		if (expr != null) {
			String[] options = {"Convert", "Simplify and Convert"};
			int choice = JOptionPane.showOptionDialog(
					frame,
					"Please choose an action:",
					"Convert Boolean Algebra Expression",
					JOptionPane.DEFAULT_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[1]
			);
			if (choice > -1) {
				try {
					MyListener listener = QuantrBooleanAlgebra.parse(expr);
					if (choice == 1) {
						final JProgressBarDialog d = new JProgressBarDialog(frame, "Espresso Machine", true);
						d.progressBar.setIndeterminate(true);
						d.progressBar.setStringPainted(true);
						d.progressBar.setString("* Smells like a shot of espresso.");

						class MyThread extends Thread {

							public MyListener listener;

							public MyThread(MyListener listener) {
								this.listener = listener;
							}

							public void run() {
								listener = QuantrBooleanAlgebra.eOptimise(listener);
								QuantrBooleanAlgebra.dump(listener.output);
								baToGates(listener.output, expr);
								autoPositionButtonActionPerformed(null);
								autoPositionButtonActionPerformed(new java.awt.event.ActionEvent(this, java.awt.event.ActionEvent.ACTION_PERFORMED, "command", System.currentTimeMillis(), 0));
							}
						}
						d.thread = new MyThread(listener);
						d.setVisible(true);
					} else {
//						listener.output = (hk.quantr.algebralib.data.Output) QuantrBooleanAlgebra.baOptimise(listener.output);
						QuantrBooleanAlgebra.dump(listener.output);
						baToGates(listener.output, expr);
						autoPositionButtonActionPerformed(null);
						autoPositionButtonActionPerformed(new java.awt.event.ActionEvent(this, java.awt.event.ActionEvent.ACTION_PERFORMED, "command", System.currentTimeMillis(), 0));
					}
					this.getCanvas().recordAction();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Input Is Invalid!", "Failed to parse expr.", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
    }//GEN-LAST:event_convertButtonActionPerformed

    private void openQuantrButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openQuantrButtonActionPerformed
		OpenFromQuantrDialog dialog = new OpenFromQuantrDialog(frame, true);
		dialog.setSize(1300, 900);
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
		if (dialog.data != null) {
			frame.setTitle(dialog.data.username + " - " + dialog.data.projectName);
			data = Data.load(dialog.data.json.replaceAll("\\\\\"", "\""));
			initTreeAndCanvas();
		}
    }//GEN-LAST:event_openQuantrButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addModuleButton;
    private javax.swing.JPopupMenu autoEdgePopupMenu;
    private javax.swing.JButton autoPositionButton;
    public javax.swing.JToggleButton autoToggleButton;
    private javax.swing.JButton clearButton;
    private javax.swing.JTree componentTree;
    public javax.swing.JButton convertButton;
    private javax.swing.JMenuItem deleteAllAutoEdgesMenuItem;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton drawButton;
    private javax.swing.JButton eastButton;
    private hk.quantr.javalib.swing.advancedswing.highdpijlabel.HighDPIJLabel highDPIJLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    public javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    public javax.swing.JSplitPane leftSplitPane;
    private javax.swing.JLabel loginToQuantrLabel;
    public javax.swing.JSplitPane mainSplitPane;
    private hk.quantr.javalib.swing.advancedswing.jclosabletabbedpane.JClosableTabbedPane mainTabbedPane;
    private javax.swing.JLabel myProjectLabel;
    private javax.swing.JButton newButton;
    private javax.swing.JButton northButton;
    private hk.quantr.javalib.swing.advancedswing.onoffbutton.OnOffButton onOffButton1;
    private javax.swing.JButton openButton;
    private javax.swing.JButton openQuantrButton;
    private javax.swing.JTree projectTree;
    private javax.swing.JTable propertyTable;
    public hk.quantr.vcdcomponent.QuantrVCDComponent quantrVCDComponent1;
    private javax.swing.JButton randomButton;
    private javax.swing.JButton refreshComponentTreeButton;
    private javax.swing.JButton resetButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JButton saveToQuantrButton;
    private javax.swing.JButton settingButton;
    private javax.swing.JToggleButton showLevelButton;
    private javax.swing.JToggleButton simulateToggleButton;
    private javax.swing.JButton southButton;
    private javax.swing.JLabel statusLabel1;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JButton testButton;
    private javax.swing.JPanel toolbarPanel;
    public javax.swing.JTree toolbarTree;
    private javax.swing.JPanel vcdPanel;
    private javax.swing.JButton westButton;
    public javax.swing.JToggleButton wireToggleButton;
    private javax.swing.JButton zoom100Button;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;
    // End of variables declaration//GEN-END:variables

	private void initToolbarTree() {
		projectTreeRoot.icon = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/project/chip.png"));
//		ProjectTreeNode shit = new ProjectTreeNode("JK FlipFlop");
		projectTreeRoot.add(new ProjectTreeNode("Main"));
		projectTree.expandRow(0);

		HashMap<String, ToolbarTreeNode> folders = new HashMap();
		ToolbarTreeNode basic = new ToolbarTreeNode("Basic");
		folders.put("Basic", basic);
		toolbarRoot.add(basic);
		basic.add(new ToolbarTreeNode(new Pin("Pin")));
		basic.add(new ToolbarTreeNode(new OutputPin("Output Pin")));
		basic.add(new ToolbarTreeNode(new Probe("Probe")));
		basic.add(new ToolbarTreeNode(new Constant("Constant")));
		basic.add(new ToolbarTreeNode(new Led("LED")));
		basic.add(new ToolbarTreeNode(new RGBLed("RGB LED")));
		basic.add(new ToolbarTreeNode(new LedBar("LED Bar")));
		basic.add(new ToolbarTreeNode(new Clock("Clock")));
		basic.add(new ToolbarTreeNode(new Tunnel("Tunnel")));
		basic.add(new ToolbarTreeNode(new Splitter("Splitter")));
		basic.add(new ToolbarTreeNode(new Combiner("Combiner")));
		basic.add(new ToolbarTreeNode(new DipSwitch("Dip Switch")));
		basic.add(new ToolbarTreeNode(new Switch("Switch")));
		basic.add(new ToolbarTreeNode(new Button("Button")));
		basic.add(new ToolbarTreeNode(new BitExtender("Bit Extender")));
		basic.add(new ToolbarTreeNode(new Transistor("Transistor")));
//		basic.add(new ToolbarTreeNode(new TransistorWithState("Transistor+")));
		basic.add(new ToolbarTreeNode(new BitstreamProgrammer("Bitstream")));

		ToolbarTreeNode output = new ToolbarTreeNode("Output");
		folders.put("Output", output);
		toolbarRoot.add(output);
		output.add(new ToolbarTreeNode(new HexDisplay("Hex Display")));
		output.add(new ToolbarTreeNode(new EightSegmentDisplay("8 Segment Display")));
		output.add(new ToolbarTreeNode(new TTY("TTY")));

		ToolbarTreeNode gate = new ToolbarTreeNode("Gate");
		folders.put("Gate", gate);
		toolbarRoot.add(gate);

		gate.add(new ToolbarTreeNode(new AndGate("And 2")));
		gate.add(new ToolbarTreeNode(new NandGate("Nand 2")));
		gate.add(new ToolbarTreeNode(new OrGate("Or 2")));
		gate.add(new ToolbarTreeNode(new NorGate("Nor 2")));
		gate.add(new ToolbarTreeNode(new Buffer("Buffer")));
		gate.add(new ToolbarTreeNode(new NotGate("Not 2")));
		gate.add(new ToolbarTreeNode(new XorGate("Xor 2")));
		gate.add(new ToolbarTreeNode(new XnorGate("Xnor 2")));
		gate.add(new ToolbarTreeNode(new TriStateBuffer("Tri-State Buffer")));
		gate.add(new ToolbarTreeNode(new TriStateNotBuffer("Tri-State Not Buffer")));
		gate.add(new ToolbarTreeNode(new OddParity("Odd Parity")));
		gate.add(new ToolbarTreeNode(new EvenParity("Even Parity")));
		gate.add(new ToolbarTreeNode(new hk.quantr.logic.data.basic.Random("Random")));

		ToolbarTreeNode plexer = new ToolbarTreeNode("Plexer");
		folders.put("Plexer", plexer);
		toolbarRoot.add(plexer);
		plexer.add(new ToolbarTreeNode(new Multiplexer("Multiplexer")));
		plexer.add(new ToolbarTreeNode(new Demultiplexer("Demultiplexer")));
		plexer.add(new ToolbarTreeNode(new Encoder("Encoder")));
		plexer.add(new ToolbarTreeNode(new Decoder("Decoder")));
		plexer.add(new ToolbarTreeNode(new BitSelector("Bit Selector")));

		ToolbarTreeNode arithmetic = new ToolbarTreeNode("Arithmetic");
		folders.put("Arithmetic", arithmetic);
		toolbarRoot.add(arithmetic);
		arithmetic.add(new ToolbarTreeNode(new Adder("Adder")));
		arithmetic.add(new ToolbarTreeNode(new Subtractor("Subtractor")));
		arithmetic.add(new ToolbarTreeNode(new Multiplier("Multiplier")));
		arithmetic.add(new ToolbarTreeNode(new Divider("Divider")));
		arithmetic.add(new ToolbarTreeNode(new Negator("Negator")));
		arithmetic.add(new ToolbarTreeNode(new Shifter("Shifter")));

		ToolbarTreeNode flipFlop = new ToolbarTreeNode("Flip flop");
		folders.put("Flip flop", flipFlop);
		toolbarRoot.add(flipFlop);
		flipFlop.add(new ToolbarTreeNode(new DLatch("D Latch")));
		flipFlop.add(new ToolbarTreeNode(new DFlipflop("D Flipflop")));
		flipFlop.add(new ToolbarTreeNode(new TFlipflop("T Flipflop")));
		flipFlop.add(new ToolbarTreeNode(new JKFlipflop("JK Flip Flop")));
		flipFlop.add(new ToolbarTreeNode(new SRFlipflop("SR Flip Flop")));

		ToolbarTreeNode memory = new ToolbarTreeNode("Memory");
		folders.put("Memory", memory);
		toolbarRoot.add(memory);
		memory.add(new ToolbarTreeNode(new Ram("RAM")));

		ToolbarTreeNode arduino = new ToolbarTreeNode("Arduino");
		folders.put("Arduino", arduino);
		toolbarRoot.add(arduino);
		arduino.add(new ToolbarTreeNode(new ArduinoSerial("Arduino Serial")));
		arduino.add(new ToolbarTreeNode(new ArduinoSerialOled("Arduino Serial Oled")));
		arduino.add(new ToolbarTreeNode(new ArduinoWifi("Arduino Wifi")));
		arduino.add(new ToolbarTreeNode(new ArduinoWifiOled("Arduino Wifi Oled")));
		arduino.add(new ToolbarTreeNode(new ArduinoHeltecWifiOled("Arduino Heltec Wifi Oled")));

		ToolbarTreeNode file = new ToolbarTreeNode("File");
		folders.put("File", file);
		toolbarRoot.add(file);
		file.add(new ToolbarTreeNode(new VCD("VCD")));

		ToolbarTreeNode graphics = new ToolbarTreeNode("Graphics");
		folders.put("Graphics", file);
		toolbarRoot.add(file);
		file.add(new ToolbarTreeNode(new ImageComponent("Image")));

		CommonLib.expandAll(toolbarTree, true);

		try {
			URL url = new URL("https://www.quantr.foundation/wp-json/quantr-foundation-wordpress-widgets/componentLibrary");
			String content = IOUtils.toString(url.openStream(), "UTF-8");
			JSONArray arr = new JSONArray(content);
			for (int i = 0; i < arr.length(); i++) {
				String name = arr.getJSONObject(i).getString("name");
				String icon = arr.getJSONObject(i).getString("icon");
				String category = arr.getJSONObject(i).getString("category");
				String description = arr.getJSONObject(i).getString("icon");
				String className = arr.getJSONObject(i).getString("class");
				String web = arr.getJSONObject(i).getString("web");
				String jar = arr.getJSONObject(i).getString("jar");

				ToolbarTreeNode folder = folders.get(category);
				if (folder == null) {
					folder = new ToolbarTreeNode(category);
				}
				folders.put(category, folder);
				toolbarRoot.add(folder);

				folder.add(new ToolbarTreeNode(new Loadable(name, category, description, icon, web, className, jar)));
			}
			toolbarTree.updateUI();
			CommonLib.expandAll(toolbarTree, true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
// to be fixed
//		((QuantrGraphCanvas)((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponent(((JScrollPane) mainTabbedPane.getComponentAt(mainTabbedPane.getSelectedIndex())).getComponentCount() - 1)).setDropTarget(new DropTarget(this, new DropTargetListener() {
//			@Override
//			public void dragEnter(DropTargetDragEvent dtde) {
//				System.out.println("dragEnter");
//			}
//
//			@Override
//			public void dragOver(DropTargetDragEvent dtde) {
//				System.out.println("dragOver");
//			}
//
//			@Override
//			public void dropActionChanged(DropTargetDragEvent dtde) {
//				System.out.println("dropActionChanged");
//			}
//
//			@Override
//			public void dragExit(DropTargetEvent dte) {
//				System.out.println("dragExit");
//			}
//
//			@Override
//			public void drop(DropTargetDropEvent dtde) {
//				System.out.println("drop");
//			}
//		}));
	}

	private void initToolbar() {
		addToolbarLabel(toolbarPanel, "Basic");
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		toolbarPanel.add(panel);
		panel.add(new JButton("Pin"));
	}

	private void addToolbarLabel(JPanel panel, String str) {
		JPanel p = new JPanel();
		p.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
		p.add(new JLabel(str));
		panel.add(p);
	}

	public void clickDelete() {
		deleteButtonActionPerformed(null);
	}

	public QuantrGraphCanvas getCanvas() {
		return currentCanvas;
	}

	public QuantrGraphCanvas getCanvas(int n) {
		return (QuantrGraphCanvas) ((JScrollPane) mainTabbedPane.getComponentAt(n)).getViewport().getView();
	}

	public JTree getProjectTree() {
		return projectTree;
	}

	private void baToGates(hk.quantr.algebralib.data.Output output, String expr) {
		this.getCanvas().minX = 2000;
		this.getCanvas().maxX = 0;
		this.getCanvas().minY = 2000;
		this.getCanvas().maxY = 0;
		for (Vertex v : this.getCanvas().module.vertices) {
			if (v.x < this.getCanvas().minX) {
				this.getCanvas().minX = v.x;
			}
			if (v.x + v.width > this.getCanvas().maxX) {
				this.getCanvas().maxX = v.x + v.width;
			}
			if (v.y < this.getCanvas().minY) {
				this.getCanvas().minY = v.y;
			}
			if (v.y + v.height > this.getCanvas().maxY) {
				this.getCanvas().maxY = v.y + v.height;
			}
		}
		portConnectionMap = new HashMap();
		varsMap.clear();
		this.booleanAlgebraAutoPositionVertices = new ArrayList();
		for (int i = 0; i < 5; i++) {
			this.booleanAlgebraAutoPositionVertices.add(new ArrayList<Vertex>());
		}
		OutputPin outputPin = new OutputPin("OUTPUT{" + output.toString() + "}");
		outputPin.properties.put("Label", expr);
		outputPin.level = 4;
		outputPin.setLocation(outputPin.level * 10 + 3 + output.children.get(0).children.size(), 4 + this.getCanvas().maxY);
		this.getCanvas().module.vertices.add(outputPin);
		loopChildren(outputPin, output);
		levelB2G = new int[5];
	}

	private void loopChildren(Vertex parentGate, BooleanData b) {
		for (BooleanData child : b.children) {
			Vertex v = null;
			if (child instanceof hk.quantr.algebralib.data.And) {
				v = new AndGate("AND{" + child.toString() + "}");
				v.level = 2;
				v.setLocation(v.level * 10 + 3, levelB2G[0] * 10 + 3 + this.getCanvas().maxY);
				v.properties.put("No. Of Inputs", child.children.size());
				this.getCanvas().module.vertices.add(v);
			} else if (child instanceof hk.quantr.algebralib.data.Or) {
				v = new OrGate("OR{" + child.toString() + "}");
				v.level = 3;
				v.setLocation(v.level * 10 + 3 + child.children.size(), levelB2G[v.level] * 10 + 3 + this.getCanvas().maxY);
				v.properties.put("No. Of Inputs", child.children.size());
				this.getCanvas().module.vertices.add(v);
			} else if (child instanceof hk.quantr.algebralib.data.Not) {
				v = new NotGate("NOT{" + child.toString() + "}");
				v.level = 1;
				v.setLocation(v.level * 10 + 3, levelB2G[0] * 10 + 3 + this.getCanvas().maxY);
				this.getCanvas().module.vertices.add(v);
			} else if (child instanceof hk.quantr.algebralib.data.True) {
				v = newGate("TRUE", Constant.class);
				v.level = 0;
//				v.setLocation(v.level * 10 + 3, levelB2G[v.level] * 10 + 3);
				v.properties.put("Constant", "0x1");
			} else if (child instanceof hk.quantr.algebralib.data.False) {
				v = newGate("FALSE", Constant.class);
				v.level = 0;
//				v.setLocation(v.level * 10 + 3, levelB2G[v.level] * 10 + 3);
				v.properties.put("Constant", "0x0");
			} else if (child instanceof hk.quantr.algebralib.data.Node) {
				v = newGate("NODE{" + child.getName() + "}", Pin.class);
				v.level = 0;
				v.properties.put("Label", v.name);
//				v.setLocation(v.level * 10 + 3, levelB2G[v.level] * 10 + 3);
			}
			if (v != null) {
				v.updateProperty();
				levelB2G[v.level]++;
				this.booleanAlgebraAutoPositionVertices.get(v.level).add(v);
			}
//			this.getCanvas().recalculateLines();
//			autoPositionButtonActionPerformed(null);
			for (int i = 0; i < parentGate.inputs.size(); i++) {
				if (parentGate.inputs.get(i).edges.isEmpty()) {
					if (parentGate.inputs.get(i).vertexPort == null) {
						parentGate.inputs.get(i).vertexPort = this.getCanvas().getGrid().findPort(parentGate.inputs.get(i).getAbsolutionX(), parentGate.inputs.get(i).getAbsolutionY());
					} else {
						continue;
					}
					if (v != null) {
						if (v.outputs.get(0).vertexPort == null) {
							v.outputs.get(0).vertexPort = this.getCanvas().getGrid().findPort(v.outputs.get(0).getAbsolutionX(), v.outputs.get(0).getAbsolutionY());
						}
					}
					if (v instanceof NotGate) {
						v.setLocation(v.x, v.outputs.get(0).vertexPort.y - 1);
					}

//					System.out.println(parentGate.inputs.get(i) + ": " + parentGate.inputs.get(i).getAbsolutionX() + ", " + parentGate.inputs.get(i).getAbsolutionY());
//					System.out.println(v.outputs.get(0) + ": " + v.outputs.get(0).getAbsolutionX() + ", " + v.outputs.get(0).getAbsolutionY());
//					this.getCanvas().getGrid().autoEdgeSync(parentGate.inputs.get(i), v.outputs.get(0));
//					this.getCanvas().recalculateLines();
////					portConnectionMap.put(parentGate.inputs.get(i), v.outputs.get(0));
//					System.out.println("mapping" + v.outputs.get(0));
					if (portConnectionMap.get(v.outputs.get(0)) != null) {
						ArrayList<Port> temp = portConnectionMap.get(v.outputs.get(0));
//						System.out.println("not null " + temp);
						portConnectionMap.get(v.outputs.get(0)).add(parentGate.inputs.get(i));
					} else {
						ArrayList<Port> temp = new ArrayList();
						temp.add(parentGate.inputs.get(i));
//						System.out.println("null " + temp);
						portConnectionMap.put(v.outputs.get(0), temp);
					}
					break;
				}
			}
			if (!child.children.isEmpty()) {
				loopChildren(v, child);
			}
		}
	}

	private Vertex newGate(String name, Class c) {
		for (String n : varsMap.keySet()) {
			if (n.equals(name)) {
				levelB2G[0]--;
				return varsMap.get(n);
			}
		}
		try {
			Vertex v = (Vertex) c.getDeclaredConstructor(String.class).newInstance(name);
			varsMap.put(v.name, v);
			this.getCanvas().module.vertices.add(v);
			v.setLocation(3, levelB2G[0] * 10 + 3 + this.getCanvas().maxY);
			return v;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
			return null;
		}
	}
}
