/*
 * Copyright 2023 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic;

import hk.quantr.javalib.PropertyUtil;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.HyperlinkEvent;

/**
 *
 * @author peter
 */
public class AboutUsDialog extends javax.swing.JDialog {

	/**
	 * Creates new form AboutUsDialog
	 */
	public AboutUsDialog(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		try {
			initComponents();

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date d = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
			jEditorPane1.setText("<html><body>Version : " + PropertyUtil.getProperty("main.properties", "version") + "<br>Build date : " + format.format(d) + "<br><br>"
					+ "<a href=\"https://www.quantr.foundation/project/?project=Quantr%20Logic\">Homepage</a>"
					+ "<br /><a href=\"https://gitlab.com/quantr/eda/quantr-logic\">Gitlab</a>"
					+ "<br /><br /><img src=\"https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/image/member/peter.png\" width=50 /> Peter, System Architect<br>"
					+ "<img src=\"https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/image/member/ronald.jpeg\" width=50 /> Ronald, Student from Festival walk university<br>"
					+ "<img src=\"https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/image/member/michelle.jpeg\" width=50 /> Michelle, Student from Festival walk university<br>"
					+ "<img src=\"https://www.quantr.foundation/wp-content/plugins/quantr-foundation-wordpress-widgets/image/member/ken.png\" width=50 /> Ken, Student from Festival walk university<br></body></html>");
		} catch (ParseException ex) {
			Logger.getLogger(AboutUsDialog.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form
	 * Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("About us");

        jEditorPane1.setEditable(false);
        jEditorPane1.setContentType("text/html"); // NOI18N
        jEditorPane1.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
            public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {
                jEditorPane1HyperlinkUpdate(evt);
            }
        });
        jScrollPane1.setViewportView(jEditorPane1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(649, 512));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jEditorPane1HyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {//GEN-FIRST:event_jEditorPane1HyperlinkUpdate
		if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			try {
				Desktop.getDesktop().browse(evt.getURL().toURI());
			} catch (IOException | URISyntaxException ex) {
			}
		}
    }//GEN-LAST:event_jEditorPane1HyperlinkUpdate


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
