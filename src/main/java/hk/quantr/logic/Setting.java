package hk.quantr.logic;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import org.apache.commons.io.IOUtils;

public class Setting {

	private static Setting setting = null;
	public int x;
	public int y;
	public int width;
	public int height;
	public int dividerLocation1;
	public int dividerLocation2;
	public int dividerLocation3;
	public float scale;
	public String saveProjectPath;
	public String openProjectPath;
	public Color gateColor;
	public Color wireColor;
	public Color gridColor;
	public String componentPath;
	public boolean windowMaximized;
	public String username;
	public String ciperText;

	public Setting() {
		width = 800;
		height = 600;
		dividerLocation1 = 450;
		dividerLocation2 = 500;
		dividerLocation3 = 250;
		scale = 2;
		windowMaximized = false;
	}

	public static Setting getInstance() {
		if (setting == null) {
			setting = load();
		}
		return setting;
	}

	public static void main(String args[]) {
		Setting setting = Setting.getInstance();
		System.out.println(setting.x);
	}

	public void save() {
		XStream xstream = new XStream();
		xstream.alias("Setting", Setting.class);
		String xml = xstream.toXML(this);
		try {
			IOUtils.write(xml, new FileOutputStream(new File("setting.xml")), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Setting load() {
		try {
			XStream xstream = new XStream(new StaxDriver());
			xstream.addPermission(PrimitiveTypePermission.PRIMITIVES);
			xstream.allowTypesByWildcard(new String[]{"hk.quantr.logic.*"});
			xstream.alias("Setting", Setting.class);
			Setting setting = (Setting) xstream.fromXML(new File("setting.xml"));
			if (setting.dividerLocation1 == 0) {
				setting.dividerLocation1 = 400;
			}
			if (setting.dividerLocation2 == 0) {
				setting.dividerLocation2 = 400;
			}
			if (setting.dividerLocation3 == 0) {
				setting.dividerLocation3 = 400;
			}
			return setting;
		} catch (Exception ex) {
			new File("setting.xml").delete();
			setting = new Setting();
			setting.save();
			return setting;
		}
	}

}
