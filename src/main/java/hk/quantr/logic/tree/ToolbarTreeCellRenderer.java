/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.tree;

import hk.quantr.logic.Loadable;
import hk.quantr.logic.data.gate.AndGate;
import hk.quantr.logic.data.gate.OrGate;
import hk.quantr.javalib.CommonLib;
import hk.quantr.logic.arduino.ArduinoHeltecWifiOled;
import hk.quantr.logic.arduino.ArduinoSerial;
import hk.quantr.logic.arduino.ArduinoSerialOled;
import hk.quantr.logic.arduino.ArduinoWifi;
import hk.quantr.logic.arduino.ArduinoWifiOled;
import hk.quantr.logic.data.arithmetic.Adder;
import hk.quantr.logic.data.arithmetic.Shifter;
import hk.quantr.logic.data.arithmetic.Divider;
import hk.quantr.logic.data.arithmetic.Multiplier;
import hk.quantr.logic.data.arithmetic.Negator;
import hk.quantr.logic.data.arithmetic.Subtractor;
import hk.quantr.logic.data.basic.BitExtender;
import hk.quantr.logic.data.basic.BitstreamProgrammer;
import hk.quantr.logic.data.basic.Button;
import hk.quantr.logic.data.basic.Clock;
import hk.quantr.logic.data.basic.Combiner;
import hk.quantr.logic.data.basic.Constant;
import hk.quantr.logic.data.basic.DipSwitch;
import hk.quantr.logic.data.basic.Led;
import hk.quantr.logic.data.basic.LedBar;
import hk.quantr.logic.data.basic.OutputPin;
import hk.quantr.logic.data.basic.Pin;
import hk.quantr.logic.data.basic.Probe;
import hk.quantr.logic.data.basic.RGBLed;
import hk.quantr.logic.data.basic.Random;
import hk.quantr.logic.data.basic.Splitter;
import hk.quantr.logic.data.basic.Switch;
import hk.quantr.logic.data.basic.Transistor;
import hk.quantr.logic.data.basic.Tunnel;
import hk.quantr.logic.data.file.VCD;
import hk.quantr.logic.data.flipflop.DFlipflop;
import hk.quantr.logic.data.flipflop.DLatch;
import hk.quantr.logic.data.flipflop.JKFlipflop;
import hk.quantr.logic.data.flipflop.Ram;
import hk.quantr.logic.data.flipflop.SRFlipflop;
import hk.quantr.logic.data.flipflop.TFlipflop;
import hk.quantr.logic.data.gate.Buffer;
import hk.quantr.logic.data.gate.EvenParity;
import hk.quantr.logic.data.gate.NandGate;
import hk.quantr.logic.data.gate.NorGate;
import hk.quantr.logic.data.gate.NotGate;
import hk.quantr.logic.data.gate.OddParity;
import hk.quantr.logic.data.gate.TriStateBuffer;
import hk.quantr.logic.data.gate.TriStateNotBuffer;
import hk.quantr.logic.data.gate.XnorGate;
import hk.quantr.logic.data.gate.XorGate;
import hk.quantr.logic.data.graphics.ImageComponent;
import hk.quantr.logic.data.output.EightSegmentDisplay;
import hk.quantr.logic.data.output.HexDisplay;
import hk.quantr.logic.data.output.TTY;
import hk.quantr.logic.data.plexer.Multiplexer;
import hk.quantr.logic.data.plexer.Decoder;
import hk.quantr.logic.data.plexer.BitSelector;
import hk.quantr.logic.data.plexer.Demultiplexer;
import hk.quantr.logic.data.plexer.Encoder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ToolbarTreeCellRenderer extends JLabel implements TreeCellRenderer {

//	Icon folderOpen = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folderOpen.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon folderClose = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folderClose.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon orGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/or.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon andGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/and.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon notGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/not.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon norGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/nor.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon nandGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/nand.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon xorGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/xor.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon xnorGate = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/xnor.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon pin = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/pin.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon led = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/led.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon clock = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/clock.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon dLatch = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/d-latch.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon dFlipFlop = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/d-flipflop.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon srFlipFlop = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/sr-flipflop.png")), 20, 20, Image.SCALE_SMOOTH);
//	Icon jkFlipFlop = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/jk-flipflop.png")), 20, 20, Image.SCALE_SMOOTH);
	Icon folderOpen = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folderOpen.png"));
	Icon folderClose = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folderClose.png"));
	Icon orGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/or.png"));
	Icon andGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/and.png"));
	Icon notGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/not.png"));
	Icon norGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/nor.png"));
	Icon nandGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/nand.png"));
	Icon xorGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/xor.png"));
	Icon xnorGate = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/gate/xnor.png"));
	Icon pin = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/pin.png"));
	Icon led = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/led.png"));
	Icon tunnel = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/tunnel.png"));
	Icon random = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/random.png"));
	Icon clock = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/clock.png"));
	Icon splitter = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/splitter.png"));
	Icon combiner = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/combiner.png"));
	Icon dLatch = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/d-latch.png"));
	Icon dFlipFlop = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/d-flipflop.png"));
	Icon srFlipFlop = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/sr-flipflop.png"));
	Icon jkFlipFlop = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/jk-flipflop.png"));
	Icon ram = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/RAM.png"));
	Icon eightSegmentDisplay = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/output/8-segment-display.png"));
	Icon hexDisplay = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/output/hex-display.png"));
	Icon tty = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/output/TTY.png"));
	Icon numberDisplay = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/output/number-display.png"));
	Icon adder = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arithmetic/adder.png"));
	Icon subtractor = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arithmetic/subtractor.png"));
	Icon multiplier = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arithmetic/multiplier.png"));
	Icon divider = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arithmetic/divider.png"));
	Icon shifter = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arithmetic/shifter.png"));
	Icon vcd = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/vcd.png"));
	Icon dip = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/dip.png"));
	Icon switchIcon = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/switch.png"));
	Icon button = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/button.png"));
	Icon probe = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/probe.png"));
	Icon output = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/output.png"));
	Icon multiplexer = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/multiplexer.png"));
	Icon demultiplexer = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/demultiplexer.png"));
	Icon arduino = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/arduinoLogo.png"));
	Icon transistor = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/transistor.png"));
	Icon transistorWithState = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/transistorWithState.png"));
	Icon bitstreamProgrammer = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/bitstreamProgrammer.png"));

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		setIcon(null);
		ToolbarTreeNode node = (ToolbarTreeNode) value;
		if (leaf) {
			if (node.data instanceof OrGate) {
				setText("Or");
				setIcon(orGate);
			} else if (node.data instanceof AndGate) {
				setText("And");
				setIcon(andGate);
			} else if (node.data instanceof NotGate) {
				setText("Not");
				setIcon(notGate);
			} else if (node.data instanceof NorGate) {
				setText("Nor");
				setIcon(norGate);
			} else if (node.data instanceof NandGate) {
				setText("Nand");
				setIcon(nandGate);
			} else if (node.data instanceof XorGate) {
				setText("Xor");
				setIcon(xorGate);
			} else if (node.data instanceof XnorGate) {
				setText("Xnor");
				setIcon(xnorGate);
			} else if (node.data instanceof Pin) {
				setText("Pin");
				setIcon(pin);
			} else if (node.data instanceof Led) {
				setText("Led");
				setIcon(led);
			} else if (node.data instanceof Tunnel) {
				setText("Tunnel");
				setIcon(tunnel);
			} else if (node.data instanceof Random) {
				setText("Random");
				setIcon(random);
			} else if (node.data instanceof Clock) {
				setText("Clock");
				setIcon(clock);
			} else if (node.data instanceof Splitter) {
				setText("Splitter");
				setIcon(splitter);
			} else if (node.data instanceof Combiner) {
				setText("Combiner");
				setIcon(combiner);
			} else if (node.data instanceof DLatch) {
				setText("D Latch");
				setIcon(dLatch);
			} else if (node.data instanceof DFlipflop) {
				setText("D Flipflop");
				setIcon(dFlipFlop);
			} else if (node.data instanceof SRFlipflop) {
				setText("SR Flipflop");
				setIcon(srFlipFlop);
			} else if (node.data instanceof JKFlipflop) {
				setText("JK Flipflop");
				setIcon(jkFlipFlop);
			} else if (node.data instanceof TFlipflop) {
				setText("T Flipflop");
				setIcon(dFlipFlop);
			} else if (node.data instanceof Ram) {
				setText("Ram");
				setIcon(ram);
			} else if (node.data instanceof HexDisplay) {
				setText("Hex Display");
				setIcon(hexDisplay);
			} else if (node.data instanceof EightSegmentDisplay) {
				setText("8 Segment Display");
				setIcon(eightSegmentDisplay);
			} else if (node.data instanceof TTY) {
				setText("TTY");
				setIcon(tty);
			} else if (node.data instanceof Adder) {
				setText("Adder");
				setIcon(adder);
			} else if (node.data instanceof Subtractor) {
				setText("Subtractor");
				setIcon(subtractor);
			} else if (node.data instanceof Multiplier) {
				setText("Multiplier");
				setIcon(multiplier);
			} else if (node.data instanceof Divider) {
				setText("Divider");
				setIcon(divider);
			} else if (node.data instanceof Shifter) {
				setText("Shifter");
				setIcon(shifter);
			} else if (node.data instanceof Negator) {
				setText("Negator");
				setIcon(shifter);
			} else if (node.data instanceof BitExtender) {
				setText("Bit Extender");
				setIcon(shifter);
			} else if (node.data instanceof BitSelector) {
				setText("Bit Selector");
				setIcon(shifter);
			} else if (node.data instanceof Encoder) {
				setText("Encoder");
				setIcon(shifter);
			} else if (node.data instanceof Decoder) {
				setText("Decoder");
				setIcon(shifter);
			} else if (node.data instanceof Multiplexer) {
				setText("Multiplexer");
				setIcon(multiplexer);
			} else if (node.data instanceof Demultiplexer) {
				setText("Demultiplexer");
				setIcon(demultiplexer);
			} else if (node.data instanceof RGBLed) {
				setText("RGB LED");
				setIcon(led);
			} else if (node.data instanceof DipSwitch) {
				setText("Dip Switch");
				setIcon(dip);
			} else if (node.data instanceof Switch) {
				setText("Switch");
				setIcon(switchIcon);
			} else if (node.data instanceof Button) {
				setText("Button");
				setIcon(button);
			} else if (node.data instanceof LedBar) {
				setText("LED Bar");
				setIcon(led);
			} else if (node.data instanceof Probe) {
				setText("Probe");
				setIcon(probe);
			} else if (node.data instanceof OutputPin) {
				setText("Output Pin");
				setIcon(output);
			} else if (node.data instanceof Buffer) {
				setText("Buffer");
				setIcon(notGate);
			} else if (node.data instanceof EvenParity) {
				setText("EvenParity");
				setIcon(random);
			} else if (node.data instanceof OddParity) {
				setText("OddParity");
				setIcon(random);
			} else if (node.data instanceof VCD) {
				setText("VCD");
				setIcon(vcd);
			} else if (node.data instanceof Constant) {
				setText("Constant");
				setIcon(pin);
			} else if (node.data instanceof TriStateBuffer) {
				setText("Tri-State Buffer");
				setIcon(notGate);
			} else if (node.data instanceof TriStateNotBuffer) {
				setText("Tri-State Not Buffer");
				setIcon(notGate);
			} else if (node.data instanceof ImageComponent) {
				setText("Image Component");
				setIcon(tty);
			} else if (node.data instanceof ArduinoSerial) {
				setText("Arduino Serial");
				setIcon(arduino);
			} else if (node.data instanceof ArduinoWifi) {
				setText("Arduino Wifi");
				setIcon(arduino);
			} else if (node.data instanceof ArduinoSerialOled) {
				setText("Arduino Serial Oled");
				setIcon(arduino);
			} else if (node.data instanceof ArduinoWifiOled) {
				setText("Arduino Wifi Oled");
				setIcon(arduino);
			} else if (node.data instanceof ArduinoHeltecWifiOled) {
				setText("Arduino Heltec Wifi Oled");
				setIcon(arduino);
			} else if (node.data instanceof Transistor) {
				setText("Transistor");
				setIcon(transistor);
//			} else if (node.data instanceof TransistorWithState) {
//				setText("Transistor+");
//				setIcon(transistorWithState);
			} else if (node.data instanceof BitstreamProgrammer) {
				setText("Bitstream");
				setIcon(bitstreamProgrammer);
			} else if (node.data instanceof Loadable) {
				try {
					Loadable l = (Loadable) node.data;
					setText(l.name);
					URL url = new URL(l.icon);
					Image image = ImageIO.read(url);
//					ImageIcon icon = CommonLib.resizeImageToIcon(new ImageIcon(image), image.getWidth(null) * 30 / image.getHeight(null), 30, Image.SCALE_SMOOTH);
					ImageIcon icon = new ImageIcon(image);

					setIcon(icon);
				} catch (MalformedURLException ex) {
					Logger.getLogger(ToolbarTreeCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(ToolbarTreeCellRenderer.class.getName()).log(Level.SEVERE, null, ex);
				}
			} else {
				setText(node.data.toString());
			}
		} else {
			if (expanded) {
				setIcon(folderOpen);
			} else {
				setIcon(folderClose);
			}
			setText(node.data.toString());
		}
		return this;
	}

	@Override
	public void paint(Graphics g) {
//		BufferedImage qfLogo = CommonLib.toBufferedImage(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/d-flipflop.png")));
		BufferedImage image = CommonLib.toBufferedImage(CommonLib.iconToImage(this.getIcon()));

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		int imageX = image.getWidth(null) * 20 / image.getHeight(null);
		g2d.drawImage(image, 0, 0, imageX, 20, null);
		g.setColor(Color.black);
		int fontHeight = (int) getFont().createGlyphVector(g.getFontMetrics().getFontRenderContext(), getText()).getVisualBounds().getHeight();
		g.drawString(getText(), imageX + 4, getHeight() - ((getHeight() - fontHeight) / 2));
	}
}
