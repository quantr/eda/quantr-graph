/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.tree;

import hk.quantr.logic.data.gate.Module;
import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.Vertex;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ComponentTreeModel extends DefaultTreeModel {

	public Module module = new Module("Main");
	public DefaultMutableTreeNode root;

	public ComponentTreeModel(Module module, DefaultMutableTreeNode root) {
		super(root);
		this.root = root;
		this.module = module;

		refresh();
	}

	final public void refresh() {
		root.removeAllChildren();
		DefaultMutableTreeNode vertices = new DefaultMutableTreeNode("Vertex");
		if (this.module != null) {
			root.add(vertices);
			for (Vertex v : module.vertices) {
				vertices.add(new DefaultMutableTreeNode(v));
			}
			DefaultMutableTreeNode edges = new DefaultMutableTreeNode("Edge");
			for (Edge e : module.edges) {
				edges.add(new DefaultMutableTreeNode(e));
			}
			root.add(edges);
		}
	}

	@Override
	public Object getRoot() {
		return root;
	}

	@Override
	public Object getChild(Object parent, int index) {
		return ((DefaultMutableTreeNode) parent).getChildAt(index);
	}

	@Override
	public int getChildCount(Object parent) {
		return ((DefaultMutableTreeNode) parent).getChildCount();
	}

	@Override
	public boolean isLeaf(Object node) {
		return getChildCount(node) == 0;
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		return 0;
	}

}
