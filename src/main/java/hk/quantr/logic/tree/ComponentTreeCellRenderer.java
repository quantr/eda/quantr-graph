/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.tree;

import hk.quantr.logic.data.gate.Edge;
import hk.quantr.logic.data.gate.Vertex;
import hk.quantr.javalib.CommonLib;
import java.awt.Component;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ComponentTreeCellRenderer extends JLabel implements TreeCellRenderer {

	Icon shape_square = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/shape_square.png")), 13, 10, Image.SCALE_SMOOTH);
	Icon link = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/link.png")), 13, 10, Image.SCALE_SMOOTH);

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if (node.getUserObject() instanceof Vertex) {
			setIcon(shape_square);
			String name = ((Vertex) node.getUserObject()).name;
			String typeName = ((Vertex) node.getUserObject()).getTypeName();
			if (name == null) {
				setText("-");
			} else {
				setText(typeName + " - " + name);
			}
		} else if (node.getUserObject() instanceof Edge) {
			setText(((Edge) node.getUserObject()).name);
		} else {
			setIcon(null);
			setText(node.getUserObject().toString());
		}
		return this;
	}

}
