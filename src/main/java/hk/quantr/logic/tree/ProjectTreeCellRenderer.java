/*
 * Copyright 2023 Peter (peter@quantr.hk).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logic.tree;

import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class ProjectTreeCellRenderer extends JLabel implements TreeCellRenderer {

	Icon folderOpen = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folderOpen.png"));
	Icon folderClose = new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/folderClose.png"));

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		ProjectTreeNode node = (ProjectTreeNode) value;

		if (leaf) {
			setText(node.data.toString());
		} else {
			if (getIcon() == null) {
				if (expanded) {
					setIcon(folderOpen);
				} else {
					setIcon(folderClose);
				}
			} else {
				setIcon(node.icon);
			}
			setText(node.data.toString());
		}
		setPreferredSize(new Dimension(100, tree.getRowHeight()));
		return this;
	}

//	@Override
//	public Dimension getPreferredSize() {
//		int iconWidth = 0, iconHeight = 0;
//		if (getIcon() != null) {
//			iconWidth = getIcon().getIconWidth();
//			iconHeight = getIcon().getIconHeight();
//		}
//		FontMetrics fm = getFontMetrics(getFont());
//		int fontWidth = (int) getFont().createGlyphVector(fm.getFontRenderContext(), getText()).getVisualBounds().getWidth();
//		int fontHeight = (int) getFont().createGlyphVector(fm.getFontRenderContext(), getText()).getVisualBounds().getHeight();
//		return new Dimension(iconWidth + fontWidth + 400, iconHeight > fontHeight ? iconHeight : fontHeight);
//	}
	@Override
	public void paint(Graphics g) {
//		BufferedImage qfLogo = CommonLib.toBufferedImage(new ImageIcon(getClass().getResource("/hk/quantr/logic/icon/flipflop/d-flipflop.png")));
		int imageWidth = 0;
		if (this.getIcon() != null) {
			BufferedImage image = CommonLib.toBufferedImage(CommonLib.iconToImage(this.getIcon()));

			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

			imageWidth = image.getWidth(null) * 20 / image.getHeight(null);
			int imageY = (getHeight() - 20) / 2;
			g2d.drawImage(image, 0, imageY, imageWidth, 20, null);
		}
		g.setColor(Color.black);
		int fontHeight = (int) getFont().createGlyphVector(g.getFontMetrics().getFontRenderContext(), getText()).getVisualBounds().getHeight();
		if (this.getIcon() != null) {
			imageWidth += 4;
		}
		g.drawString(getText(), imageWidth, getHeight() - ((getHeight() - fontHeight) / 2));
	}
}
