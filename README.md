A logic simulator written in Java, it is an educational tool for student to learn/design/simulate their circuit.

**Features**

1. Completely open source
1. Support VCD dump format
1. Fast simulation
1. Completely in Java Swing, easy to embed it in other swing application

![](https://www.quantr.foundation/wp-content/uploads/2023/07/Screenshot-2023-07-01-at-4.32.17-PM.png)

![](https://www.quantr.foundation/wp-content/uploads/2023/07/Screenshot-2023-07-01-at-4.32.30-PM.png)

