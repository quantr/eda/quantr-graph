#include "heltec.h"

int numSplitInput = 0;
String** result;


void processSignal(String input) {
  String splitInput[50];
  numSplitInput = splitString(input, ',', splitInput, 50);
  result = new String*[numSplitInput];
  for (int i = 0; i < numSplitInput; i++) {
    result[i] = new String[2];
  }
  int index = 0;
  for (int i = 0; i < numSplitInput; i++) {
    String s = splitInput[i];
    if (s.indexOf('=') >= 0) {
      int equalsIndex = s.indexOf('=');
      result[index][0] = s.substring(0, equalsIndex);
      result[index][1] = s.substring(equalsIndex + 1);
      index++;
    }
  }
}

int splitString(String input, char delimiter, String output[], int maxOutput) {
  int numOutput = 0;
  int startIndex = 0;
  int endIndex = input.indexOf(delimiter);
  while (endIndex >= 0 && numOutput < maxOutput - 1) {
    output[numOutput] = input.substring(startIndex, endIndex);
    numOutput++;
    startIndex = endIndex + 1;
    endIndex = input.indexOf(delimiter, startIndex);
  }
  if (startIndex < input.length()) {
    output[numOutput] = input.substring(startIndex);
    numOutput++;
  }
  return numOutput;
}

String inputPin = "";

void handleDigitalPin(bool write) {
  if (!write) {
    inputPin = "";
  }
  for (int i = 0; i < numSplitInput; i++) {
    if (result[i][0].length() != 0 && result[i][1].length() != 0) {
      if (result[i]->indexOf("OLED") >= 0) {
        drawOLED(result[i]);
        continue;
      }
      for (char c : result[i][0]) {
        if (!isDigit(c)) {
          goto nextLoop;
        }
      }
      for (char c : result[i][1]) {
        if (!(c == '1' || c == '0' || c == '3')) {
          goto nextLoop;
        }
      }
      // Serial.println("");
      // Serial.println(result[i][0] + " = " + result[i][1]);
      if (write) {
        digitalWrite(result[i][0].toInt(), result[i][1].toInt());
      } else {
        pinMode(result[i][0].toInt(), result[i][1].toInt());
        if (result[i][1].toInt() == 1) {
          inputPin += result[i][0] + ",";
        }
      }
    }
nextLoop:
    continue;
  }
}

String readPin() {
  String output[50];
  int a = splitString(inputPin, ',', output, 50);
  String pin = "";
  for (int i = 0; i < a; i++) {
    pin += output[i] + "=" + digitalRead(output[i].toInt()) + ",";
  }
  // Serial.println(pin);
  return pin;
}

void freeResult(String** result, int size) {
  for (int i = 0; i < size; i++) {
    delete[] result[i];
  }
  delete[] result;
}
void drawOLED(String* signal) {
  Heltec.display->clear();
  removeNonNumeric(signal[0]);
  Serial.println("drawOLED");
  for (int c = 11; c >= 0; c--) {
    // Serial.println(signal[i]);
    if (signal[0].toInt() - 1 < c) {
      Heltec.display->drawRect((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), 17, 17);
      Heltec.display->drawLine((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), (c > 5 ? (11 - c) : (5 - c)) * 17 + 27, (c > 5 ? 10 : 32) + 17);
    } else if ((signal[1].toInt() & (1 << c)) != 0) {
      Heltec.display->fillRect((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), 17, 17);
    } else {
      Heltec.display->drawRect((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), 17, 17);
    }
  }
  Heltec.display->display();
}

void removeNonNumeric(String& str) {
  String cleanedString = "";
  int length = str.length();

  for (int i = 0; i < length; i++) {
    char c = str.charAt(i);
    if (isdigit(c)) {
      cleanedString += c;
    }
  }

  str = cleanedString;
}

void drawBits(String signal) {
  char signals[64];
  signal.toCharArray(signals, 64);
  for (int i = 0; i < strlen(signals); i++) {
    if (signal[i] >= 97 && signal[i] < 122) {  // check
      if ((i + 1) < strlen(signals)) {
        if (islower(signal[i + 1])) {
          Heltec.display->clear();
          Heltec.display->drawString(64, 0, String(signal[i]));
          // Heltec.display->drawString(64, 20, String(pinMap->search(String(signals[i]))));
          Heltec.display->drawString(64, 40, String(signal[i + 1]));
          Heltec.display->display();
          i = i + 2;
        }
      }
    }
  }
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Disable*/, true /*Serial Enable*/);
  Heltec.display->init();
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  // Heltec.display->clear();
  // Heltec.display->setFont(ArialMT_Plain_16);
  // Heltec.display->setTextAlignment(TEXT_ALIGN_CENTER);

  // Serial.println();
  // Heltec.display->drawString(64, 20, "life sucks T^T");
  // Heltec.display->display();

  // delay(1000);
  // Heltec.display->clear();
  // Heltec.display->drawString(64, 20, "life reli sucks T^T");
  // Heltec.display->display();
}


int k = 0;
void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {

    int noOfByte = Serial.read();
    // Serial.println(noOfByte);
    char buffer[noOfByte + 1];
    memset(buffer, 0, noOfByte + 1);
    Serial.readBytes(buffer, noOfByte);
    // String receivedData = Serial.readString();
    String receivedData = buffer;
    // Serial.println(receivedData);
    if (receivedData.indexOf("Setting") > 0) {
      // Serial.println("go into settings");
      processSignal(receivedData);
      handleDigitalPin(false);
      freeResult(result, numSplitInput);
      digitalWrite(13, k);
      k = k ^ 1;
      // Serial.println("Setting");
    } else if (receivedData.indexOf("Quantr-Logic") > 0) {
      processSignal(receivedData);
      handleDigitalPin(true);
      Serial.print((char)readPin().length());
      Serial.print(readPin());
      // displayOnOLED(receivedData);
      // drawBits(receivedData);
      freeResult(result, numSplitInput);
    } else if (receivedData.indexOf("Arduino") > 0) {
      Serial.print((char)noOfByte);
      Serial.print(receivedData);
    }
  }
}