#include <WiFi.h>
#include "HT_SSD1306Wire.h"

#ifdef WIRELESS_STICK_V3
static SSD1306Wire display(0x3c, 500000, SDA_OLED, SCL_OLED, GEOMETRY_64_32, RST_OLED);  // addr , freq , i2c group , resolution , rst
#else
static SSD1306Wire display(0x3c, 500000, SDA_OLED, SCL_OLED, GEOMETRY_128_64, RST_OLED);  // addr , freq , i2c group , resolution , rst
#endif

const char* ssid = "Quantr 2.4G";
const char* password = "quantrwifi";
// IPAddress addr = IPAddress(192, 168, 0, 150);
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  display.init();
  //Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Disable*/, true /*Serial Enable*/);
  display.clear();
  display.setFont(ArialMT_Plain_16);
  display.setTextAlignment(TEXT_ALIGN_CENTER);

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  display.clear();
  display.drawString(64, 0, "Connecting...");
  display.drawString(64, 20, String(ssid));
  display.display();
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  display.clear();
  display.drawString(64, 0, "Wifi Connected");
  display.drawString(64, 20, String(ssid));
  display.display();
  server.begin();

  delay(1000);
  display.clear();
  display.drawString(64, 0, "Quantr Logic");
  display.drawString(64, 20, "v1.0");
  display.drawString(64, 40, String(WiFi.localIP()[0]) + "." + String(WiFi.localIP()[1]) + "." + String(WiFi.localIP()[2]) + "." + String(WiFi.localIP()[3]) + ".");
  display.display();
  delay(1000);
  // We start by connecting to a WiFi network
}

int numSplitInput = 0;
String** result;

void processSignal(String input) {
  String splitInput[50];
  numSplitInput = splitString(input, ',', splitInput, 50);
  result = new String*[numSplitInput];
  for (int i = 0; i < numSplitInput; i++) {
    result[i] = new String[2];
  }
  int index = 0;
  for (int i = 0; i < numSplitInput; i++) {
    String s = splitInput[i];
    if (s.indexOf('=') >= 0) {
      int equalsIndex = s.indexOf('=');
      result[index][0] = s.substring(0, equalsIndex);
      result[index][1] = s.substring(equalsIndex + 1);
      index++;
    }
  }
}

int splitString(String input, char delimiter, String output[], int maxOutput) {
  int numOutput = 0;
  int startIndex = 0;
  int endIndex = input.indexOf(delimiter);
  while (endIndex >= 0 && numOutput < maxOutput - 1) {
    output[numOutput] = input.substring(startIndex, endIndex);
    numOutput++;
    startIndex = endIndex + 1;
    endIndex = input.indexOf(delimiter, startIndex);
  }
  if (startIndex < input.length()) {
    output[numOutput] = input.substring(startIndex);
    numOutput++;
  }
  return numOutput;
}

String inputPin = "";

void handleDigitalPin(bool write) {
  if (!write) {
    Serial.println("reset inputPin");
    inputPin = "";
  }
  for (int i = 0; i < numSplitInput; i++) {
    if (result[i][0].length() != 0 && result[i][1].length() != 0) {
      if (result[i]->indexOf("OLED") >= 0) {
        drawOLED(result[i]);
        continue;
      }
      for (char c : result[i][0]) {
        if (!isDigit(c)) {
          goto nextLoop;
        }
      }
      for (char c : result[i][1]) {
        if (!(c == '1' || c == '0' || c == '3')) {
          goto nextLoop;
        }
      }
      if (write) {
        //Serial.println("write " + result[i][0] + " = " + result[i][1]);
      } else {
        Serial.println("read " + result[i][0] + " = " + result[i][1]);
      }
      if (write) {
        digitalWrite(result[i][0].toInt(), result[i][1].toInt());
      } else {
        pinMode(result[i][0].toInt(), result[i][1].toInt());
        if (result[i][1].toInt() == 1) {
          inputPin += result[i][0] + ",";
        }
      }
    }
nextLoop:
    continue;
  }
}
void drawOLED(String* signal) {
  display.clear();
  removeNonNumeric(signal[0]);
  // Serial.println("drawOLED");
  for (int c = 11; c >= 0; c--) {
    // Serial.println(signal[i]);
    if (signal[0].toInt() - 1 < c) {
      display.drawRect((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), 17, 17);
      display.drawLine((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), (c > 5 ? (11 - c) : (5 - c)) * 17 + 27, (c > 5 ? 10 : 32) + 17);
    } else if ((signal[1].toInt() & (1 << c)) != 0) {
      display.fillRect((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), 17, 17);
    } else {
      display.drawRect((c > 5 ? (11 - c) : (5 - c)) * 17 + 10, (c > 5 ? 10 : 32), 17, 17);
    }
  }
  display.display();
}

void removeNonNumeric(String& str) {
  String cleanedString = "";
  int length = str.length();

  for (int i = 0; i < length; i++) {
    char c = str.charAt(i);
    if (isdigit(c)) {
      cleanedString += c;
    }
  }

  str = cleanedString;
}

String readPin() {
  Serial.println("readPin: " + inputPin);
  String output[50];
  int a = splitString(inputPin, ',', output, 50);
  String pin = "";
  for (int i = 0; i < a; i++) {
    pin += output[i] + "=" + digitalRead(output[i].toInt()) + ",";
  }
  Serial.println("readPin: " + pin);
  return pin;
}

void freeResult(String** result, int size) {
  for (int i = 0; i < size; i++) {
    delete[] result[i];
  }
  delete[] result;
}

void drawBits(String signal) {
  char signals[64];
  signal.toCharArray(signals, 64);
  for (int i = 0; i < strlen(signals); i++) {
    if (signal[i] >= 97 && signal[i] < 122) {  // check
      if ((i + 1) < strlen(signals)) {
        if (islower(signal[i + 1])) {
          display.clear();
          display.drawString(64, 0, String(signal[i]));
          // display.drawString(64, 20, String(pinMap->search(String(signals[i]))));
          display.drawString(64, 40, String(signal[i + 1]));
          display.display();
          i = i + 2;
        }
      }
    }
  }
}

void loop() {
  WiFiClient client = server.available();  // listen for incoming clients
  if (client) {                            // if you get a client,
    // Serial.println("New Client.");         // print a message out the serial port
    String currentLine = "";      // make a String to hold incoming data from the client
    while (client.connected()) {  // loop while the client's connected
      if (client.available()) {   // if there's bytes to read from the client,
        char c = client.read();   // read a byte, then
        // Serial.write(c);                   // print it out the serial monitor
        if (c == '\n') {  // if the byte is a newline character
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();
            // the content of the HTTP response follows the header:
            client.println(readPin());  //response to quantrlogic
            // Serial.println("responsed");
            break;
          } else {  // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
        if (currentLine.endsWith("Quantr-logic")) {
          processSignal(currentLine);
          handleDigitalPin(true);
          freeResult(result, numSplitInput);
        } else if (currentLine.endsWith("Setting")) {
          processSignal(currentLine);
          handleDigitalPin(false);
          freeResult(result, numSplitInput);
        }
      }
    }
    client.stop();
    // Serial.println("Client Disconnected.");
  }
}