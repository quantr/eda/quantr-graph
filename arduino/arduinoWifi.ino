#include <WiFi.h>

const char* ssid = "Quantr 2.4G";
const char* password = "quantrwifi";
// IPAddress addr = IPAddress(192, 168, 0, 150);
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(1000);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

int numSplitInput = 0;
String** result;

void processSignal(String input) {
  String splitInput[50];
  numSplitInput = splitString(input, ',', splitInput, 50);
  result = new String*[numSplitInput];
  for (int i = 0; i < numSplitInput; i++) {
    result[i] = new String[2];
  }
  int index = 0;
  for (int i = 0; i < numSplitInput; i++) {
    String s = splitInput[i];
    if (s.indexOf('=') >= 0) {
      int equalsIndex = s.indexOf('=');
      result[index][0] = s.substring(0, equalsIndex);
      result[index][1] = s.substring(equalsIndex + 1);
      index++;
    }
  }
}
int splitString(String input, char delimiter, String output[], int maxOutput) {
  int numOutput = 0;
  int startIndex = 0;
  int endIndex = input.indexOf(delimiter);
  while (endIndex >= 0 && numOutput < maxOutput - 1) {
    output[numOutput] = input.substring(startIndex, endIndex);
    numOutput++;
    startIndex = endIndex + 1;
    endIndex = input.indexOf(delimiter, startIndex);
  }
  if (startIndex < input.length()) {
    output[numOutput] = input.substring(startIndex);
    numOutput++;
  }
  return numOutput;
}

String inputPin = "";

void handleDigitalPin(bool write) {
  if (!write) {
    inputPin = "";
  }
  for (int i = 0; i < numSplitInput; i++) {
    if (result[i][0].length() != 0 && result[i][1].length() != 0) {
      for (char c : result[i][0]) {
        if (!isDigit(c)) {
          goto nextLoop;
        }
      }
      for (char c : result[i][1]) {
        if (!(c == '1' || c == '0' || c == '3')) {
          Serial.println(c + c == '3');
          goto nextLoop;
        }
      }
      Serial.println("");
      Serial.println(result[i][0] + " = " + result[i][1]);
      if (write) {
        digitalWrite(result[i][0].toInt(), result[i][1].toInt());
      } else {
        pinMode(result[i][0].toInt(), result[i][1].toInt());
        if (result[i][1].toInt() == 1) {
          inputPin += result[i][0] + ",";
        }
      }
    }
nextLoop:
    continue;
  }
}

String readPin() {
  String output[50];
  int a = splitString(inputPin, ',', output, 50);
  String pin = "";
  for (int i = 0; i < a; i++) {
    pin += output[i] + "=" + digitalRead(output[i].toInt()) + ",";
  }
  Serial.println(pin);
  return pin;
}

void freeResult(String** result, int size) {
  for (int i = 0; i < size; i++) {
    delete[] result[i];
  }
  delete[] result;
}

void loop() {
  WiFiClient client = server.available();  // listen for incoming clients
  if (client) {                            // if you get a client,
    Serial.println("New Client.");         // print a message out the serial port
    String currentLine = "";               // make a String to hold incoming data from the client
    while (client.connected()) {           // loop while the client's connected
      if (client.available()) {            // if there's bytes to read from the client,
        char c = client.read();            // read a byte, then
        Serial.write(c);                   // print it out the serial monitor
        if (c == '\n') {                   // if the byte is a newline character
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();
            // the content of the HTTP response follows the header:
            client.println(readPin());  //response to quantrlogic
            Serial.println("responsed");
            break;
          } else {  // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
        if (currentLine.endsWith("Quantr-logic")) {
          processSignal(currentLine);
          handleDigitalPin(true);
          freeResult(result, numSplitInput);
        } else if (currentLine.endsWith("Quantr-Setting")) {
          processSignal(currentLine);
          handleDigitalPin(false);
          freeResult(result, numSplitInput);
        }
      }
    }
    client.stop();
    Serial.println("Client Disconnected.");
  }
}
