int numSplitInput = 0;
String** result;

void processSignal(String input) {
  String splitInput[50];
  numSplitInput = splitString(input, ',', splitInput, 50);
  result = new String*[numSplitInput];
  for (int i = 0; i < numSplitInput; i++) {
    result[i] = new String[2];
  }
  int index = 0;
  for (int i = 0; i < numSplitInput; i++) {
    String s = splitInput[i];
    if (s.indexOf('=') >= 0) {
      int equalsIndex = s.indexOf('=');
      result[index][0] = s.substring(0, equalsIndex);
      result[index][1] = s.substring(equalsIndex + 1);
      index++;
    }
  }
}
int splitString(String input, char delimiter, String output[], int maxOutput) {
  int numOutput = 0;
  int startIndex = 0;
  int endIndex = input.indexOf(delimiter);
  while (endIndex >= 0 && numOutput < maxOutput - 1) {
    output[numOutput] = input.substring(startIndex, endIndex);
    numOutput++;
    startIndex = endIndex + 1;
    endIndex = input.indexOf(delimiter, startIndex);
  }
  if (startIndex < input.length()) {
    output[numOutput] = input.substring(startIndex);
    numOutput++;
  }
  return numOutput;
}

String inputPin = "";

void handleDigitalPin(bool write) {
  if (!write) {
    inputPin = "";
  }
  for (int i = 0; i < numSplitInput; i++) {
    if (result[i][0].length() != 0 && result[i][1].length() != 0) {
      for (char c : result[i][0]) {
        if (!isDigit(c)) {
          goto nextLoop;
        }
      }
      for (char c : result[i][1]) {
        if (!(c == '1' || c == '0' || c == '3')) {
          // Serial.println(c + c == '3');
          goto nextLoop;
        }
      }
      // Serial.println("");
      // Serial.println(result[i][0] + " = " + result[i][1]);
      if (write) {
        digitalWrite(result[i][0].toInt(), result[i][1].toInt());
      } else {
        pinMode(result[i][0].toInt(), result[i][1].toInt());
        if (result[i][1].toInt() == 1) {
          inputPin += result[i][0] + ",";
        }
      }
    }
nextLoop:
    continue;
  }
}

String readPin() {
  String output[50];
  int a = splitString(inputPin, ',', output, 50);
  String pin = "";
  for (int i = 0; i < a; i++) {
    pin += output[i] + "=" + digitalRead(output[i].toInt()) + ",";
  }
  // Serial.println(pin);
  return pin;
}

void freeResult(String** result, int size) {
  for (int i = 0; i < size; i++) {
    delete[] result[i];
  }
  delete[] result;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
}
int k = 0;
void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    int noOfByte = Serial.read();
    char buffer[noOfByte + 1];
    memset(buffer, 0, noOfByte + 1);
    Serial.readBytes(buffer, noOfByte);
    //String receivedData = Serial.readString();
    String receivedData = buffer;
    if (receivedData.indexOf("Setting") > 0) {
      // Serial.println("go into settings");
      processSignal(receivedData);
      handleDigitalPin(false);
      freeResult(result, numSplitInput);
      // Serial.println("Setting");
    } else {
      processSignal(receivedData);
      handleDigitalPin(true);

      Serial.print(readPin().length());
      Serial.print(readPin());

      freeResult(result, numSplitInput);
      // k = k ^ 1;
      digitalWrite(LED_BUILTIN, 0);
      delay(100);
      digitalWrite(LED_BUILTIN, 1);
    }
  }
}
