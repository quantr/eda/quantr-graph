<data>
  <vertices>
    <hk.quantr.logic.data.basic.DipSwitch>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 0 output</name>
          <deltaX>1</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../.."/>
              <endVertex class="hk.quantr.logic.data.plexer.Multiplexer">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Multiplexer 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>4</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.basic.Led">
                          <outputs/>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>Led 4</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
                              <name>Led 4 input 0</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>Led 4</string>
                            </entry>
                            <entry>
                              <string>Orientation</string>
                              <string>west</string>
                            </entry>
                          </properties>
                          <x>47</x>
                          <y>13</y>
                          <width>2</width>
                          <height>2</height>
                          <level>0</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <nextstate>false</nextstate>
                          <presentstate>false</presentstate>
                          <orientation>west</orientation>
                        </startVertex>
                        <endVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <startPort class="input" reference="../startVertex/inputs/input"/>
                        <endPort class="output" reference="../../.."/>
                        <edgeConnected/>
                        <name>AutoEdge {Led 4 input 0&lt;-&gt;Multiplexer 1 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>Multiplexer 1</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Multiplexer 1 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Multiplexer 1 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>2</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Multiplexer 1 input 1&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Multiplexer 1 input 2</name>
                    <deltaX>0</deltaX>
                    <deltaY>3</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>3</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Multiplexer 1 input 2&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Multiplexer 1 input 3</name>
                    <deltaX>0</deltaX>
                    <deltaY>4</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>4</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Multiplexer 1 input 3&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Input 4</name>
                    <deltaX>0</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>5</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Input 4&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Input 5</name>
                    <deltaX>0</deltaX>
                    <deltaY>6</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>6</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Input 5&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Input 6</name>
                    <deltaX>0</deltaX>
                    <deltaY>7</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>7</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Input 6&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Input 7</name>
                    <deltaX>0</deltaX>
                    <deltaY>8</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../.."/>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output">
                          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../../../../../../../.."/>
                          <name>DipSwitch 0 output</name>
                          <deltaX>8</deltaX>
                          <deltaY>0</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>true</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </endPort>
                        <edgeConnected/>
                        <name>AutoEdge {Input 7&lt;-&gt;DipSwitch 0 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../.."/>
                    <name>Multiplexer 1 input 4</name>
                    <deltaX>3</deltaX>
                    <deltaY>10</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Multiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.Pin">
                          <outputs>
                            <output>
                              <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
                              <name>Pin 2 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>3</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>true</isSelected>
                          <gridSize>10</gridSize>
                          <name>Pin 2</name>
                          <inputs/>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>Pin 2</string>
                            </entry>
                            <entry>
                              <string>Bits</string>
                              <int>3</int>
                            </entry>
                            <entry>
                              <string>Radix</string>
                              <string>bin</string>
                            </entry>
                            <entry>
                              <string>Value</string>
                              <long>0</long>
                            </entry>
                          </properties>
                          <x>27</x>
                          <y>29</y>
                          <width>6</width>
                          <height>2</height>
                          <level>0</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <radix>bin</radix>
                          <base>2</base>
                          <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
                          <hex>0000000000000000</hex>
                          <dec>00000000000000000000</dec>
                        </endVertex>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output" reference="../endVertex/outputs/output"/>
                        <edgeConnected/>
                        <name>AutoEdge {Multiplexer 1 input 4&lt;-&gt;Pin 2 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>3</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>Multiplexer 1</string>
                  </entry>
                  <entry>
                    <string>Select Bits</string>
                    <int>3</int>
                  </entry>
                </properties>
                <x>37</x>
                <y>13</y>
                <width>6</width>
                <height>10</height>
                <level>0</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
              </endVertex>
              <startPort class="output" reference="../../.."/>
              <endPort class="input" reference="../endVertex/inputs/input"/>
              <edgeConnected/>
              <name>AutoEdge {DipSwitch 0 output&lt;-&gt;Multiplexer 1 input 0}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[4]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[5]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[6]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[7]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <output reference="../output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[8]/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>DipSwitch 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>DipSwitch 0</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>north</string>
        </entry>
        <entry>
          <string>Number of Switch</string>
          <int>8</int>
        </entry>
      </properties>
      <x>14</x>
      <y>24</y>
      <width>9</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>north</orientation>
    </hk.quantr.logic.data.basic.DipSwitch>
    <hk.quantr.logic.data.plexer.Multiplexer reference="../hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.basic.Pin reference="../hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[9]/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.basic.LedBar>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>LedBar 3</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 0</name>
          <deltaX>1</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 1</name>
          <deltaX>2</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 2</name>
          <deltaX>3</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 3</name>
          <deltaX>4</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 4</name>
          <deltaX>5</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 5</name>
          <deltaX>6</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 6</name>
          <deltaX>7</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.LedBar" reference="../../.."/>
          <name>LedBar 3 input 7</name>
          <deltaX>8</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>LedBar 3</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>south</string>
        </entry>
        <entry>
          <string>Number of Segments</string>
          <int>8</int>
        </entry>
      </properties>
      <x>46</x>
      <y>9</y>
      <width>9</width>
      <height>3</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>south</orientation>
      <connectionState>false</connectionState>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.LedBar>
    <hk.quantr.logic.data.basic.Led reference="../hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[9]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[4]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[5]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[6]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[7]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[8]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
  </autoEdges>
</data>