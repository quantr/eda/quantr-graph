<data>
  <vertices>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 0 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <Edge>
              <startVertex class="hk.quantr.logic.data.basic.Pin" reference="../../../../.."/>
              <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <Edge>
                        <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                          <outputs>
                            <output>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 2 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <Edge>
                                  <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                                  <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                                    <outputs>
                                      <output>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 3 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <Edge>
                                            <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                                            <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                                              <outputs>
                                                <output>
                                                  <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                                  <name>DFlipflop 4 output</name>
                                                  <deltaX>6</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <Edge>
                                                      <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                                                      <startPort class="output" reference="../../.."/>
                                                      <edgeConnected/>
                                                      <Point>
                                                        <x>52</x>
                                                        <y>14</y>
                                                      </Point>
                                                      <Point>
                                                        <x>61</x>
                                                        <y>14</y>
                                                      </Point>
                                                      <Point>
                                                        <x>61</x>
                                                        <y>14</y>
                                                      </Point>
                                                      <Point>
                                                        <x>61</x>
                                                        <y>6</y>
                                                      </Point>
                                                      <name>edge 18</name>
                                                      <isSelected>false</isSelected>
                                                      <startPortExtended>false</startPortExtended>
                                                      <endPortExtended>false</endPortExtended>
                                                      <gridSize>10</gridSize>
                                                    </Edge>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                                <output>
                                                  <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                                  <name>DFlipflop 4 output</name>
                                                  <deltaX>6</deltaX>
                                                  <deltaY>5</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges/>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                              </outputs>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>DFlipflop 4</name>
                                              <inputs>
                                                <input>
                                                  <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                                  <name>DFlipflop 4 input 0</name>
                                                  <deltaX>3</deltaX>
                                                  <deltaY>0</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges/>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                                <input>
                                                  <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                                  <name>DFlipflop 4 input 1</name>
                                                  <deltaX>0</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <Edge reference="../../../../.."/>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                                <input>
                                                  <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                                  <name>DFlipflop 4 input 2</name>
                                                  <deltaX>0</deltaX>
                                                  <deltaY>5</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <Edge>
                                                      <startVertex class="hk.quantr.logic.data.basic.Clock">
                                                        <outputs>
                                                          <output>
                                                            <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
                                                            <name>Clock 5 output</name>
                                                            <deltaX>2</deltaX>
                                                            <deltaY>1</deltaY>
                                                            <x>0</x>
                                                            <y>0</y>
                                                            <edges>
                                                              <Edge reference="../../../../.."/>
                                                            </edges>
                                                            <state>false</state>
                                                            <presentstate>false</presentstate>
                                                            <bits>1</bits>
                                                            <value>0</value>
                                                          </output>
                                                        </outputs>
                                                        <isSelected>false</isSelected>
                                                        <gridSize>10</gridSize>
                                                        <name>Clock 5</name>
                                                        <inputs/>
                                                        <properties>
                                                          <entry>
                                                            <string>Name</string>
                                                            <string>Clock 5</string>
                                                          </entry>
                                                          <entry>
                                                            <string>Hertz</string>
                                                            <int>1</int>
                                                          </entry>
                                                        </properties>
                                                        <x>13</x>
                                                        <y>27</y>
                                                        <width>2</width>
                                                        <height>2</height>
                                                        <level>0</level>
                                                        <showLevel>false</showLevel>
                                                        <isFlipflop>false</isFlipflop>
                                                        <hertz>1</hertz>
                                                        <presentstate>false</presentstate>
                                                        <lastSystemTime>10258787512443</lastSystemTime>
                                                      </startVertex>
                                                      <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                                                      <startPort class="output" reference="../startVertex/outputs/output"/>
                                                      <endPort class="input" reference="../../.."/>
                                                      <edgeConnected>
                                                        <Edge>
                                                          <edgeConnected>
                                                            <Edge reference="../../../.."/>
                                                            <Edge>
                                                              <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../.."/>
                                                              <startPort class="input">
                                                                <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../.."/>
                                                                <name>DFlipflop 3 input 2</name>
                                                                <deltaX>0</deltaX>
                                                                <deltaY>5</deltaY>
                                                                <x>0</x>
                                                                <y>0</y>
                                                                <edges>
                                                                  <Edge reference="../../.."/>
                                                                  <Edge reference="../../../../.."/>
                                                                  <Edge>
                                                                    <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../../../../../../../.."/>
                                                                    <endPort class="input">
                                                                      <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../../../../../../../../.."/>
                                                                      <name>DFlipflop 2 input 2</name>
                                                                      <deltaX>0</deltaX>
                                                                      <deltaY>5</deltaY>
                                                                      <x>0</x>
                                                                      <y>0</y>
                                                                      <edges>
                                                                        <Edge reference="../../.."/>
                                                                      </edges>
                                                                      <state>false</state>
                                                                      <presentstate>false</presentstate>
                                                                      <bits>1</bits>
                                                                      <value>0</value>
                                                                    </endPort>
                                                                    <edgeConnected>
                                                                      <Edge reference="../../../../../../../../.."/>
                                                                    </edgeConnected>
                                                                    <Point>
                                                                      <x>30</x>
                                                                      <y>28</y>
                                                                    </Point>
                                                                    <Point>
                                                                      <x>30</x>
                                                                      <y>18</y>
                                                                    </Point>
                                                                    <Point>
                                                                      <x>30</x>
                                                                      <y>18</y>
                                                                    </Point>
                                                                    <Point>
                                                                      <x>30</x>
                                                                      <y>18</y>
                                                                    </Point>
                                                                    <name>edge 1</name>
                                                                    <isSelected>false</isSelected>
                                                                    <startPortExtended>false</startPortExtended>
                                                                    <endPortExtended>false</endPortExtended>
                                                                    <gridSize>10</gridSize>
                                                                  </Edge>
                                                                </edges>
                                                                <state>false</state>
                                                                <presentstate>false</presentstate>
                                                                <bits>1</bits>
                                                                <value>0</value>
                                                              </startPort>
                                                              <edgeConnected>
                                                                <Edge reference="../../../.."/>
                                                              </edgeConnected>
                                                              <Point>
                                                                <x>38</x>
                                                                <y>18</y>
                                                              </Point>
                                                              <Point>
                                                                <x>38</x>
                                                                <y>27</y>
                                                              </Point>
                                                              <Point>
                                                                <x>38</x>
                                                                <y>27</y>
                                                              </Point>
                                                              <Point>
                                                                <x>38</x>
                                                                <y>27</y>
                                                              </Point>
                                                              <name>edge 20</name>
                                                              <isSelected>false</isSelected>
                                                              <startPortExtended>false</startPortExtended>
                                                              <endPortExtended>false</endPortExtended>
                                                              <gridSize>10</gridSize>
                                                            </Edge>
                                                          </edgeConnected>
                                                          <Point>
                                                            <x>38</x>
                                                            <y>28</y>
                                                          </Point>
                                                          <Point>
                                                            <x>38</x>
                                                            <y>26</y>
                                                          </Point>
                                                          <Point>
                                                            <x>38</x>
                                                            <y>26</y>
                                                          </Point>
                                                          <Point>
                                                            <x>38</x>
                                                            <y>26</y>
                                                          </Point>
                                                          <name>edge 52</name>
                                                          <isSelected>false</isSelected>
                                                          <startPortExtended>false</startPortExtended>
                                                          <endPortExtended>false</endPortExtended>
                                                          <gridSize>10</gridSize>
                                                        </Edge>
                                                        <Edge reference="../Edge/edgeConnected/Edge[2]/startPort/edges/Edge[3]"/>
                                                        <Edge>
                                                          <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../../../../../../../.."/>
                                                          <startPort class="input">
                                                            <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../../../../../../../../.."/>
                                                            <name>DFlipflop 1 input 2</name>
                                                            <deltaX>0</deltaX>
                                                            <deltaY>5</deltaY>
                                                            <x>0</x>
                                                            <y>0</y>
                                                            <edges>
                                                              <Edge reference="../../.."/>
                                                            </edges>
                                                            <state>false</state>
                                                            <presentstate>false</presentstate>
                                                            <bits>1</bits>
                                                            <value>0</value>
                                                          </startPort>
                                                          <edgeConnected>
                                                            <Edge reference="../../../.."/>
                                                          </edgeConnected>
                                                          <Point>
                                                            <x>21</x>
                                                            <y>18</y>
                                                          </Point>
                                                          <Point>
                                                            <x>21</x>
                                                            <y>28</y>
                                                          </Point>
                                                          <Point>
                                                            <x>21</x>
                                                            <y>28</y>
                                                          </Point>
                                                          <Point>
                                                            <x>21</x>
                                                            <y>28</y>
                                                          </Point>
                                                          <name>edge 46</name>
                                                          <isSelected>false</isSelected>
                                                          <startPortExtended>false</startPortExtended>
                                                          <endPortExtended>false</endPortExtended>
                                                          <gridSize>10</gridSize>
                                                        </Edge>
                                                      </edgeConnected>
                                                      <Point>
                                                        <x>15</x>
                                                        <y>28</y>
                                                      </Point>
                                                      <Point>
                                                        <x>46</x>
                                                        <y>28</y>
                                                      </Point>
                                                      <Point>
                                                        <x>46</x>
                                                        <y>28</y>
                                                      </Point>
                                                      <Point>
                                                        <x>46</x>
                                                        <y>18</y>
                                                      </Point>
                                                      <name>edge 98</name>
                                                      <isSelected>false</isSelected>
                                                      <startPortExtended>false</startPortExtended>
                                                      <endPortExtended>false</endPortExtended>
                                                      <gridSize>10</gridSize>
                                                    </Edge>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                                <input>
                                                  <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                                  <name>DFlipflop 4 input 3</name>
                                                  <deltaX>3</deltaX>
                                                  <deltaY>6</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges/>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                              </inputs>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>DFlipflop 4</string>
                                                </entry>
                                              </properties>
                                              <x>46</x>
                                              <y>13</y>
                                              <width>6</width>
                                              <height>6</height>
                                              <level>4</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>true</isFlipflop>
                                            </endVertex>
                                            <startPort class="output" reference="../../.."/>
                                            <endPort class="input" reference="../endVertex/inputs/input[2]"/>
                                            <edgeConnected/>
                                            <Point>
                                              <x>44</x>
                                              <y>14</y>
                                            </Point>
                                            <Point>
                                              <x>46</x>
                                              <y>14</y>
                                            </Point>
                                            <Point>
                                              <x>46</x>
                                              <y>14</y>
                                            </Point>
                                            <Point>
                                              <x>46</x>
                                              <y>14</y>
                                            </Point>
                                            <name>edge 9</name>
                                            <isSelected>false</isSelected>
                                            <startPortExtended>false</startPortExtended>
                                            <endPortExtended>false</endPortExtended>
                                            <gridSize>10</gridSize>
                                          </Edge>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                      <output>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 3 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>5</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                    </outputs>
                                    <isSelected>false</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>DFlipflop 3</name>
                                    <inputs>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 3 input 0</name>
                                        <deltaX>3</deltaX>
                                        <deltaY>0</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 3 input 1</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <Edge reference="../../../../.."/>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                      <input reference="../../outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge/edgeConnected/Edge[2]/startPort"/>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 3 input 3</name>
                                        <deltaX>3</deltaX>
                                        <deltaY>6</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                    </inputs>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>DFlipflop 3</string>
                                      </entry>
                                    </properties>
                                    <x>38</x>
                                    <y>13</y>
                                    <width>6</width>
                                    <height>6</height>
                                    <level>3</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>true</isFlipflop>
                                  </endVertex>
                                  <startPort class="output" reference="../../.."/>
                                  <endPort class="input" reference="../endVertex/inputs/input[2]"/>
                                  <edgeConnected/>
                                  <Point>
                                    <x>36</x>
                                    <y>14</y>
                                  </Point>
                                  <Point>
                                    <x>38</x>
                                    <y>14</y>
                                  </Point>
                                  <Point>
                                    <x>38</x>
                                    <y>14</y>
                                  </Point>
                                  <Point>
                                    <x>38</x>
                                    <y>14</y>
                                  </Point>
                                  <name>edge 59</name>
                                  <isSelected>false</isSelected>
                                  <startPortExtended>false</startPortExtended>
                                  <endPortExtended>false</endPortExtended>
                                  <gridSize>10</gridSize>
                                </Edge>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                            <output>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 2 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>5</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>DFlipflop 2</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 2 input 0</name>
                              <deltaX>3</deltaX>
                              <deltaY>0</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 2 input 1</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <Edge reference="../../../../.."/>
                              </edges>
                              <state>true</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input reference="../../outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge/edgeConnected/Edge[2]/startPort/edges/Edge[3]/endPort"/>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 2 input 3</name>
                              <deltaX>3</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>DFlipflop 2</string>
                            </entry>
                          </properties>
                          <x>30</x>
                          <y>13</y>
                          <width>6</width>
                          <height>6</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>true</isFlipflop>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input[2]"/>
                        <edgeConnected/>
                        <Point>
                          <x>27</x>
                          <y>14</y>
                        </Point>
                        <Point>
                          <x>30</x>
                          <y>14</y>
                        </Point>
                        <Point>
                          <x>30</x>
                          <y>14</y>
                        </Point>
                        <Point>
                          <x>30</x>
                          <y>14</y>
                        </Point>
                        <name>edge 69</name>
                        <isSelected>false</isSelected>
                        <startPortExtended>false</startPortExtended>
                        <endPortExtended>false</endPortExtended>
                        <gridSize>10</gridSize>
                      </Edge>
                    </edges>
                    <state>true</state>
                    <presentstate>true</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>DFlipflop 1</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 1 input 0</name>
                    <deltaX>3</deltaX>
                    <deltaY>0</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 1 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <Edge reference="../../../../.."/>
                    </edges>
                    <state>true</state>
                    <presentstate>true</presentstate>
                    <bits>1</bits>
                    <value>1</value>
                  </input>
                  <input reference="../../outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge[3]/startPort"/>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 1 input 3</name>
                    <deltaX>3</deltaX>
                    <deltaY>6</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>DFlipflop 1</string>
                  </entry>
                </properties>
                <x>21</x>
                <y>13</y>
                <width>6</width>
                <height>6</height>
                <level>1</level>
                <showLevel>false</showLevel>
                <isFlipflop>true</isFlipflop>
              </endVertex>
              <startPort class="output" reference="../../.."/>
              <endPort class="input" reference="../endVertex/inputs/input[2]"/>
              <edgeConnected/>
              <Point>
                <x>14</x>
                <y>15</y>
              </Point>
              <Point>
                <x>21</x>
                <y>15</y>
              </Point>
              <Point>
                <x>21</x>
                <y>15</y>
              </Point>
              <Point>
                <x>21</x>
                <y>14</y>
              </Point>
              <name>edge 85</name>
              <isSelected>false</isSelected>
              <startPortExtended>false</startPortExtended>
              <endPortExtended>false</endPortExtended>
              <gridSize>10</gridSize>
            </Edge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>1</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 0</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>1</long>
        </entry>
      </properties>
      <x>12</x>
      <y>14</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex"/>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex"/>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex"/>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex"/>
    <hk.quantr.logic.data.basic.Clock reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/startVertex"/>
  </vertices>
  <edges>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge/edgeConnected/Edge[2]"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge/edgeConnected/Edge[2]/startPort/edges/Edge[3]"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/inputs/input[3]/edges/Edge/edgeConnected/Edge[3]"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge"/>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge/endVertex/outputs/output/edges/Edge"/>
  </edges>
  <autoEdges/>
</data>