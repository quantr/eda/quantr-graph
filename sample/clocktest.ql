<data>
  <vertices>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 0 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 3 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                          <outputs>
                            <output>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 1 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                                  <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop">
                                    <outputs>
                                      <output>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 2 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>true</state>
                                        <presentstate>true</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                      <output>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 2 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>5</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                    </outputs>
                                    <isSelected>false</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>DFlipflop 2</name>
                                    <inputs>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 2 input 0</name>
                                        <deltaX>3</deltaX>
                                        <deltaY>0</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 2 input 1</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 2 input 2</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>5</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="hk.quantr.logic.data.basic.Clock">
                                              <outputs>
                                                <output>
                                                  <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
                                                  <name>Clock 4 output</name>
                                                  <deltaX>2</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../../../../../.."/>
                                                      <endVertex class="hk.quantr.logic.data.basic.Clock" reference="../../../../.."/>
                                                      <startPort class="input">
                                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../../../../../../.."/>
                                                        <name>DFlipflop 3 input 2</name>
                                                        <deltaX>0</deltaX>
                                                        <deltaY>5</deltaY>
                                                        <x>0</x>
                                                        <y>0</y>
                                                        <edges>
                                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                                        </edges>
                                                        <state>true</state>
                                                        <presentstate>true</presentstate>
                                                        <bits>1</bits>
                                                        <value>1</value>
                                                      </startPort>
                                                      <endPort class="output" reference="../../.."/>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {DFlipflop 3 input 2&lt;-&gt;Clock 4 output}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../.."/>
                                                      <endVertex class="hk.quantr.logic.data.basic.Clock" reference="../../../../.."/>
                                                      <startPort class="input">
                                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../../../../../../../../../../../../.."/>
                                                        <name>DFlipflop 1 input 2</name>
                                                        <deltaX>0</deltaX>
                                                        <deltaY>5</deltaY>
                                                        <x>0</x>
                                                        <y>0</y>
                                                        <edges>
                                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                                        </edges>
                                                        <state>true</state>
                                                        <presentstate>true</presentstate>
                                                        <bits>1</bits>
                                                        <value>1</value>
                                                      </startPort>
                                                      <endPort class="output" reference="../../.."/>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {DFlipflop 1 input 2&lt;-&gt;Clock 4 output}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                  </edges>
                                                  <state>true</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                              </outputs>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>Clock 4</name>
                                              <inputs/>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>Clock 4</string>
                                                </entry>
                                                <entry>
                                                  <string>Hertz</string>
                                                  <int>1</int>
                                                </entry>
                                              </properties>
                                              <x>12</x>
                                              <y>24</y>
                                              <width>2</width>
                                              <height>2</height>
                                              <level>0</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>false</isFlipflop>
                                              <hertz>1</hertz>
                                              <presentstate>true</presentstate>
                                            </startVertex>
                                            <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                                            <startPort class="output" reference="../startVertex/outputs/output"/>
                                            <endPort class="input" reference="../../.."/>
                                            <edgeConnected/>
                                            <name>AutoEdge {Clock 4 output&lt;-&gt;DFlipflop 2 input 2}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                        </edges>
                                        <state>true</state>
                                        <presentstate>true</presentstate>
                                        <bits>1</bits>
                                        <value>1</value>
                                      </input>
                                      <input>
                                        <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                                        <name>DFlipflop 2 input 3</name>
                                        <deltaX>3</deltaX>
                                        <deltaY>6</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges/>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                    </inputs>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>DFlipflop 2</string>
                                      </entry>
                                    </properties>
                                    <x>35</x>
                                    <y>10</y>
                                    <width>6</width>
                                    <height>6</height>
                                    <level>3</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>true</isFlipflop>
                                  </endVertex>
                                  <startPort class="output" reference="../../.."/>
                                  <endPort class="input" reference="../endVertex/inputs/input[2]"/>
                                  <edgeConnected/>
                                  <name>AutoEdge {DFlipflop 1 output&lt;-&gt;DFlipflop 2 input 1}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                            <output>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 1 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>5</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>true</state>
                              <presentstate>true</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>DFlipflop 1</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 1 input 0</name>
                              <deltaX>3</deltaX>
                              <deltaY>0</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 1 input 1</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>true</state>
                              <presentstate>true</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startPort"/>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                              <name>DFlipflop 1 input 3</name>
                              <deltaX>3</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>DFlipflop 1</string>
                            </entry>
                          </properties>
                          <x>24</x>
                          <y>8</y>
                          <width>6</width>
                          <height>6</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>true</isFlipflop>
                        </startVertex>
                        <endVertex class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../../../.."/>
                        <startPort class="input" reference="../startVertex/inputs/input[2]"/>
                        <endPort class="output" reference="../../.."/>
                        <edgeConnected/>
                        <name>AutoEdge {DFlipflop 1 input 1&lt;-&gt;DFlipflop 3 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>true</state>
                    <presentstate>true</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 3 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>DFlipflop 3</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 3 input 0</name>
                    <deltaX>3</deltaX>
                    <deltaY>0</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 3 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>true</state>
                    <presentstate>true</presentstate>
                    <bits>1</bits>
                    <value>1</value>
                  </input>
                  <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startPort"/>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
                    <name>DFlipflop 3 input 3</name>
                    <deltaX>3</deltaX>
                    <deltaY>6</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>DFlipflop 3</string>
                  </entry>
                </properties>
                <x>15</x>
                <y>7</y>
                <width>6</width>
                <height>6</height>
                <level>1</level>
                <showLevel>false</showLevel>
                <isFlipflop>true</isFlipflop>
              </startVertex>
              <endVertex class="hk.quantr.logic.data.basic.Pin" reference="../../../../.."/>
              <startPort class="input" reference="../startVertex/inputs/input[2]"/>
              <endPort class="output" reference="../../.."/>
              <edgeConnected/>
              <name>AutoEdge {DFlipflop 3 input 1&lt;-&gt;Pin 0 output}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>1</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 0</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>1</long>
        </entry>
      </properties>
      <x>6</x>
      <y>12</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.flipflop.DFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.basic.Clock reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
  </autoEdges>
</data>