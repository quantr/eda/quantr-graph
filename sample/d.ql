<data>
  <vertices>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 0 output</name>
          <deltaX>8</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.plexer.Demultiplexer">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../.."/>
                    <name>Demultiplexer 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.output.HexDisplay">
                          <outputs/>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>HexDisplay 2</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.output.HexDisplay" reference="../../.."/>
                              <name>HexDisplay 2 input 0</name>
                              <deltaX>2</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <Edge>
                                  <edgeConnected/>
                                  <Point>
                                    <x>49</x>
                                    <y>10</y>
                                  </Point>
                                  <Point>
                                    <x>49</x>
                                    <y>5</y>
                                  </Point>
                                  <Point>
                                    <x>49</x>
                                    <y>5</y>
                                  </Point>
                                  <Point>
                                    <x>49</x>
                                    <y>5</y>
                                  </Point>
                                  <name>edge 33</name>
                                  <isSelected>true</isSelected>
                                  <startPortExtended>false</startPortExtended>
                                  <endPortExtended>false</endPortExtended>
                                  <gridSize>10</gridSize>
                                </Edge>
                                <Edge>
                                  <edgeConnected/>
                                  <Point>
                                    <x>46</x>
                                    <y>11</y>
                                  </Point>
                                  <Point>
                                    <x>57</x>
                                    <y>11</y>
                                  </Point>
                                  <Point>
                                    <x>57</x>
                                    <y>11</y>
                                  </Point>
                                  <Point>
                                    <x>57</x>
                                    <y>7</y>
                                  </Point>
                                  <name>edge 75</name>
                                  <isSelected>false</isSelected>
                                  <startPortExtended>false</startPortExtended>
                                  <endPortExtended>false</endPortExtended>
                                  <gridSize>10</gridSize>
                                </Edge>
                                <Edge>
                                  <edgeConnected>
                                    <Edge reference="../../../Edge[2]"/>
                                  </edgeConnected>
                                  <Point>
                                    <x>57</x>
                                    <y>11</y>
                                  </Point>
                                  <Point>
                                    <x>57</x>
                                    <y>8</y>
                                  </Point>
                                  <Point>
                                    <x>57</x>
                                    <y>8</y>
                                  </Point>
                                  <Point>
                                    <x>67</x>
                                    <y>8</y>
                                  </Point>
                                  <name>edge 4</name>
                                  <isSelected>true</isSelected>
                                  <startPortExtended>false</startPortExtended>
                                  <endPortExtended>false</endPortExtended>
                                  <gridSize>10</gridSize>
                                </Edge>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>4</bits>
                              <value>-1</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.output.HexDisplay" reference="../../.."/>
                              <name>HexDisplay 2 input 1</name>
                              <deltaX>4</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>HexDisplay 2</string>
                            </entry>
                            <entry>
                              <string>Decimal Point</string>
                              <string>true</string>
                            </entry>
                          </properties>
                          <x>33</x>
                          <y>3</y>
                          <width>5</width>
                          <height>6</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <dot>true</dot>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input"/>
                        <edgeConnected/>
                        <name>AutoEdge {Demultiplexer 1 output&lt;-&gt;HexDisplay 2 input 0}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>0</value>
                  </output>
                  <output>
                    <parent class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../.."/>
                    <name>Demultiplexer 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.output.HexDisplay">
                          <outputs/>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>HexDisplay 3</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.output.HexDisplay" reference="../../.."/>
                              <name>HexDisplay 3 input 0</name>
                              <deltaX>2</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>4</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.output.HexDisplay" reference="../../.."/>
                              <name>HexDisplay 3 input 1</name>
                              <deltaX>4</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>HexDisplay 3</string>
                            </entry>
                            <entry>
                              <string>Decimal Point</string>
                              <string>true</string>
                            </entry>
                          </properties>
                          <x>35</x>
                          <y>12</y>
                          <width>5</width>
                          <height>6</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <dot>true</dot>
                        </startVertex>
                        <endVertex class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../../../.."/>
                        <startPort class="input" reference="../startVertex/inputs/input"/>
                        <endPort class="output" reference="../../.."/>
                        <edgeConnected/>
                        <name>AutoEdge {HexDisplay 3 input 0&lt;-&gt;Demultiplexer 1 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>1</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>Demultiplexer 1</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../.."/>
                    <name>Demultiplexer 1 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>true</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>1</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../.."/>
                    <name>Demultiplexer 1 input 1</name>
                    <deltaX>3</deltaX>
                    <deltaY>4</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.plexer.Demultiplexer" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.Pin">
                          <outputs>
                            <output>
                              <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
                              <name>Pin 4 output</name>
                              <deltaX>2</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>Pin 4</name>
                          <inputs/>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>Pin 4</string>
                            </entry>
                            <entry>
                              <string>Bits</string>
                              <int>1</int>
                            </entry>
                            <entry>
                              <string>Radix</string>
                              <string>bin</string>
                            </entry>
                            <entry>
                              <string>Value</string>
                              <long>0</long>
                            </entry>
                          </properties>
                          <x>11</x>
                          <y>20</y>
                          <width>2</width>
                          <height>2</height>
                          <level>0</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <radix>bin</radix>
                          <base>2</base>
                          <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
                          <hex>0000000000000000</hex>
                          <dec>00000000000000000000</dec>
                        </endVertex>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output" reference="../endVertex/outputs/output"/>
                        <edgeConnected/>
                        <name>AutoEdge {Demultiplexer 1 input 1&lt;-&gt;Pin 4 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>Demultiplexer 1</string>
                  </entry>
                  <entry>
                    <string>Data Bits</string>
                    <int>4</int>
                  </entry>
                  <entry>
                    <string>Select Bits</string>
                    <int>1</int>
                  </entry>
                </properties>
                <x>18</x>
                <y>13</y>
                <width>6</width>
                <height>4</height>
                <level>1</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
              </startVertex>
              <endVertex class="hk.quantr.logic.data.basic.Pin" reference="../../../../.."/>
              <startPort class="input" reference="../startVertex/inputs/input"/>
              <endPort class="output" reference="../../.."/>
              <edgeConnected/>
              <name>AutoEdge {Demultiplexer 1 input 0&lt;-&gt;Pin 0 output}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>1</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 0</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>4</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>1</long>
        </entry>
      </properties>
      <x>6</x>
      <y>13</y>
      <width>8</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
    <hk.quantr.logic.data.plexer.Demultiplexer reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.output.HexDisplay reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.output.HexDisplay reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.basic.Pin reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
  </autoEdges>
</data>