<data>
  <vertices>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 0 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.basic.Pin" reference="../../../../.."/>
              <endVertex class="hk.quantr.logic.data.gate.AndGate">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.gate.AndGate" reference="../../.."/>
                    <name>AndGate 2 output</name>
                    <deltaX>5</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.gate.AndGate" reference="../../../../.."/>
                        <endVertex class="NandGate">
                          <outputs>
                            <output>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 3 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>2</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="hk.quantr.logic.data.gate.AndGate" reference="../../../../../../../../../.."/>
                                  <endVertex class="NandGate" reference="../../../../.."/>
                                  <startPort class="input">
                                    <parent class="hk.quantr.logic.data.gate.AndGate" reference="../../../../../../../../../../.."/>
                                    <name>AndGate 2 input 1</name>
                                    <deltaX>0</deltaX>
                                    <deltaY>3</deltaY>
                                    <x>0</x>
                                    <y>0</y>
                                    <edges>
                                      <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                    </edges>
                                    <state>true</state>
                                    <presentstate>false</presentstate>
                                    <bits>1</bits>
                                    <value>0</value>
                                  </startPort>
                                  <endPort class="output" reference="../../.."/>
                                  <edgeConnected/>
                                  <name>AutoEdge {AndGate 2 input 1&lt;-&gt;NandGate 3 output}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>true</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                              <ports>
                                <input reference="../../edges/hk.quantr.logic.data.gate.AutoEdge/startPort"/>
                              </ports>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>NandGate 3</name>
                          <inputs>
                            <input>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 3 input 0</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 3 input 1</name>
                              <deltaX>0</deltaX>
                              <deltaY>3</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="NandGate" reference="../../../../.."/>
                                  <endVertex class="hk.quantr.logic.data.basic.Pin">
                                    <outputs>
                                      <output>
                                        <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
                                        <name>Pin 1 output</name>
                                        <deltaX>2</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>1</value>
                                        <ports>
                                          <input reference="../../../../../../.."/>
                                        </ports>
                                      </output>
                                    </outputs>
                                    <isSelected>true</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>Pin 1</name>
                                    <inputs/>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>Pin 1</string>
                                      </entry>
                                      <entry>
                                        <string>Bits</string>
                                        <int>1</int>
                                      </entry>
                                      <entry>
                                        <string>Radix</string>
                                        <string>bin</string>
                                      </entry>
                                      <entry>
                                        <string>Value</string>
                                        <long>1</long>
                                      </entry>
                                    </properties>
                                    <x>6</x>
                                    <y>14</y>
                                    <width>2</width>
                                    <height>2</height>
                                    <level>0</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>false</isFlipflop>
                                    <radix>bin</radix>
                                    <base>2</base>
                                    <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
                                    <hex>0000000000000000</hex>
                                    <dec>00000000000000000000</dec>
                                  </endVertex>
                                  <startPort class="input" reference="../../.."/>
                                  <endPort class="output" reference="../endVertex/outputs/output"/>
                                  <edgeConnected/>
                                  <name>AutoEdge {NandGate 3 input 1&lt;-&gt;Pin 1 output}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>true</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>1</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>NandGate 3</string>
                            </entry>
                            <entry>
                              <string>No. Of Inputs</string>
                              <int>2</int>
                            </entry>
                          </properties>
                          <x>21</x>
                          <y>9</y>
                          <width>6</width>
                          <height>4</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input"/>
                        <edgeConnected/>
                        <name>AutoEdge {AndGate 2 output&lt;-&gt;NandGate 3 input 0}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                    <ports>
                      <input reference="../../edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input"/>
                    </ports>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>AndGate 2</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.gate.AndGate" reference="../../.."/>
                    <name>AndGate 2 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>true</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>1</value>
                  </input>
                  <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startPort"/>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>AndGate 2</string>
                  </entry>
                  <entry>
                    <string>No. Of Inputs</string>
                    <int>2</int>
                  </entry>
                </properties>
                <x>11</x>
                <y>6</y>
                <width>5</width>
                <height>4</height>
                <level>1</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
              </endVertex>
              <startPort class="output" reference="../../.."/>
              <endPort class="input" reference="../endVertex/inputs/input"/>
              <edgeConnected/>
              <name>AutoEdge {Pin 0 output&lt;-&gt;AndGate 2 input 0}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>1</value>
          <ports>
            <input reference="../../edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input"/>
          </ports>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 0</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>1</long>
        </entry>
      </properties>
      <x>6</x>
      <y>5</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
    <hk.quantr.logic.data.basic.Pin reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.gate.AndGate reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <NandGate reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
  </autoEdges>
</data>