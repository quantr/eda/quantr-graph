<data>
  <vertices>
    <NandGate>
      <outputs>
        <output>
          <parent class="NandGate" reference="../../.."/>
          <name>NandGate 0 output</name>
          <deltaX>6</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="NandGate" reference="../../../../.."/>
              <endVertex class="NandGate">
                <outputs>
                  <output>
                    <parent class="NandGate" reference="../../.."/>
                    <name>NandGate 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="NandGate" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.Led">
                          <outputs/>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>Led 6</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
                              <name>Led 6 input 0</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>Led 6</string>
                            </entry>
                            <entry>
                              <string>Orientation</string>
                              <string>west</string>
                            </entry>
                          </properties>
                          <x>43</x>
                          <y>4</y>
                          <width>2</width>
                          <height>2</height>
                          <level>3</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <nextstate>false</nextstate>
                          <presentstate>false</presentstate>
                          <orientation>west</orientation>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input"/>
                        <edgeConnected/>
                        <name>AutoEdge {NandGate 1 output&lt;-&gt;Led 6 input 0}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="NandGate" reference="../../../../.."/>
                        <endVertex class="NandGate">
                          <outputs>
                            <output>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 3 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>2</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="NandGate" reference="../../../../.."/>
                                  <endVertex class="NandGate" reference="../../../../../../../../../.."/>
                                  <startPort class="output" reference="../../.."/>
                                  <endPort class="input">
                                    <parent class="NandGate" reference="../../../../../../../../../../.."/>
                                    <name>NandGate 1 input 1</name>
                                    <deltaX>0</deltaX>
                                    <deltaY>3</deltaY>
                                    <x>0</x>
                                    <y>0</y>
                                    <edges>
                                      <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                    </edges>
                                    <state>true</state>
                                    <presentstate>false</presentstate>
                                    <bits>1</bits>
                                    <value>0</value>
                                  </endPort>
                                  <edgeConnected/>
                                  <name>AutoEdge {NandGate 3 output&lt;-&gt;NandGate 1 input 1}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="NandGate" reference="../../../../.."/>
                                  <endVertex class="hk.quantr.logic.data.basic.Led">
                                    <outputs/>
                                    <isSelected>false</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>Led 7</name>
                                    <inputs>
                                      <input>
                                        <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
                                        <name>Led 7 input 0</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>true</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                    </inputs>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>Led 7</string>
                                      </entry>
                                      <entry>
                                        <string>Orientation</string>
                                        <string>west</string>
                                      </entry>
                                    </properties>
                                    <x>42</x>
                                    <y>18</y>
                                    <width>2</width>
                                    <height>2</height>
                                    <level>4</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>false</isFlipflop>
                                    <nextstate>true</nextstate>
                                    <presentstate>true</presentstate>
                                    <orientation>west</orientation>
                                  </endVertex>
                                  <startPort class="output" reference="../../.."/>
                                  <endPort class="input" reference="../endVertex/inputs/input"/>
                                  <edgeConnected/>
                                  <name>AutoEdge {NandGate 3 output&lt;-&gt;Led 7 input 0}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>true</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>NandGate 3</name>
                          <inputs>
                            <input>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 3 input 0</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 3 input 1</name>
                              <deltaX>0</deltaX>
                              <deltaY>3</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="NandGate">
                                    <outputs>
                                      <output>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 2 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>2</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>true</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                    </outputs>
                                    <isSelected>false</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>NandGate 2</name>
                                    <inputs>
                                      <input>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 2 input 0</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="NandGate" reference="../../../../.."/>
                                            <endVertex class="notGate">
                                              <outputs>
                                                <output>
                                                  <parent class="notGate" reference="../../.."/>
                                                  <name>NotGate 4 output</name>
                                                  <deltaX>3</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                  </edges>
                                                  <state>true</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                              </outputs>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>NotGate 4</name>
                                              <inputs>
                                                <input>
                                                  <parent class="notGate" reference="../../.."/>
                                                  <name>NotGate 4 input 0</name>
                                                  <deltaX>0</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="hk.quantr.logic.data.basic.Pin">
                                                        <outputs>
                                                          <output>
                                                            <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
                                                            <name>Pin 5 output</name>
                                                            <deltaX>2</deltaX>
                                                            <deltaY>1</deltaY>
                                                            <x>0</x>
                                                            <y>0</y>
                                                            <edges>
                                                              <hk.quantr.logic.data.gate.AutoEdge>
                                                                <startVertex class="hk.quantr.logic.data.basic.Pin" reference="../../../../.."/>
                                                                <endVertex class="NandGate" reference="../../../../../../../../../../../../../../../../../../../../../../../../../../../../../.."/>
                                                                <startPort class="output" reference="../../.."/>
                                                                <endPort class="input">
                                                                  <parent class="NandGate" reference="../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../.."/>
                                                                  <name>NandGate 0 input 0</name>
                                                                  <deltaX>0</deltaX>
                                                                  <deltaY>1</deltaY>
                                                                  <x>0</x>
                                                                  <y>0</y>
                                                                  <edges>
                                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                                                  </edges>
                                                                  <state>false</state>
                                                                  <presentstate>false</presentstate>
                                                                  <bits>1</bits>
                                                                  <value>0</value>
                                                                </endPort>
                                                                <edgeConnected/>
                                                                <name>AutoEdge {Pin 5 output&lt;-&gt;NandGate 0 input 0}</name>
                                                                <isSelected>false</isSelected>
                                                                <fromWire>false</fromWire>
                                                                <x>0</x>
                                                                <y>0</y>
                                                              </hk.quantr.logic.data.gate.AutoEdge>
                                                              <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                            </edges>
                                                            <state>false</state>
                                                            <presentstate>false</presentstate>
                                                            <bits>1</bits>
                                                            <value>0</value>
                                                          </output>
                                                        </outputs>
                                                        <isSelected>true</isSelected>
                                                        <gridSize>10</gridSize>
                                                        <name>Pin 5</name>
                                                        <inputs/>
                                                        <properties>
                                                          <entry>
                                                            <string>Name</string>
                                                            <string>Pin 5</string>
                                                          </entry>
                                                          <entry>
                                                            <string>Bits</string>
                                                            <int>1</int>
                                                          </entry>
                                                          <entry>
                                                            <string>Radix</string>
                                                            <string>bin</string>
                                                          </entry>
                                                          <entry>
                                                            <string>Value</string>
                                                            <long>0</long>
                                                          </entry>
                                                        </properties>
                                                        <x>4</x>
                                                        <y>4</y>
                                                        <width>2</width>
                                                        <height>2</height>
                                                        <level>0</level>
                                                        <showLevel>false</showLevel>
                                                        <isFlipflop>false</isFlipflop>
                                                        <radix>bin</radix>
                                                        <base>2</base>
                                                        <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
                                                        <hex>0000000000000000</hex>
                                                        <dec>00000000000000000000</dec>
                                                      </startVertex>
                                                      <endVertex class="notGate" reference="../../../../.."/>
                                                      <startPort class="output" reference="../startVertex/outputs/output"/>
                                                      <endPort class="input" reference="../../.."/>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {Pin 5 output&lt;-&gt;NotGate 4 input 0}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                              </inputs>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>NotGate 4</string>
                                                </entry>
                                                <entry>
                                                  <string>No. Of Inputs</string>
                                                  <int>1</int>
                                                </entry>
                                              </properties>
                                              <x>12</x>
                                              <y>23</y>
                                              <width>3</width>
                                              <height>2</height>
                                              <level>1</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>false</isFlipflop>
                                            </endVertex>
                                            <startPort class="input" reference="../../.."/>
                                            <endPort class="output" reference="../endVertex/outputs/output"/>
                                            <edgeConnected/>
                                            <name>AutoEdge {NandGate 2 input 0&lt;-&gt;NotGate 4 output}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                        </edges>
                                        <state>true</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                      <input>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 2 input 1</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>3</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="hk.quantr.logic.data.basic.Clock">
                                              <outputs>
                                                <output>
                                                  <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
                                                  <name>Clock 8 output</name>
                                                  <deltaX>2</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="hk.quantr.logic.data.basic.Clock" reference="../../../../.."/>
                                                      <endVertex class="NandGate" reference="../../../../../../../../../../../../../../../../../../../../../../../../.."/>
                                                      <startPort class="output" reference="../../.."/>
                                                      <endPort class="input">
                                                        <parent class="NandGate" reference="../../../../../../../../../../../../../../../../../../../../../../../../../.."/>
                                                        <name>NandGate 0 input 1</name>
                                                        <deltaX>0</deltaX>
                                                        <deltaY>3</deltaY>
                                                        <x>0</x>
                                                        <y>0</y>
                                                        <edges>
                                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                                        </edges>
                                                        <state>false</state>
                                                        <presentstate>false</presentstate>
                                                        <bits>1</bits>
                                                        <value>0</value>
                                                      </endPort>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {Clock 8 output&lt;-&gt;NandGate 0 input 1}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                              </outputs>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>Clock 8</name>
                                              <inputs/>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>Clock 8</string>
                                                </entry>
                                                <entry>
                                                  <string>Hertz</string>
                                                  <int>1</int>
                                                </entry>
                                              </properties>
                                              <x>2</x>
                                              <y>15</y>
                                              <width>2</width>
                                              <height>2</height>
                                              <level>0</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>false</isFlipflop>
                                              <hertz>1</hertz>
                                              <presentstate>false</presentstate>
                                            </startVertex>
                                            <endVertex class="NandGate" reference="../../../../.."/>
                                            <startPort class="output" reference="../startVertex/outputs/output"/>
                                            <endPort class="input" reference="../../.."/>
                                            <edgeConnected/>
                                            <name>AutoEdge {Clock 8 output&lt;-&gt;NandGate 2 input 1}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                    </inputs>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>NandGate 2</string>
                                      </entry>
                                      <entry>
                                        <string>No. Of Inputs</string>
                                        <int>2</int>
                                      </entry>
                                    </properties>
                                    <x>18</x>
                                    <y>19</y>
                                    <width>6</width>
                                    <height>4</height>
                                    <level>2</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>false</isFlipflop>
                                  </startVertex>
                                  <endVertex class="NandGate" reference="../../../../.."/>
                                  <startPort class="output" reference="../startVertex/outputs/output"/>
                                  <endPort class="input" reference="../../.."/>
                                  <edgeConnected/>
                                  <name>AutoEdge {NandGate 2 output&lt;-&gt;NandGate 3 input 1}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>true</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>NandGate 3</string>
                            </entry>
                            <entry>
                              <string>No. Of Inputs</string>
                              <int>2</int>
                            </entry>
                          </properties>
                          <x>31</x>
                          <y>18</y>
                          <width>6</width>
                          <height>4</height>
                          <level>3</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input"/>
                        <edgeConnected/>
                        <name>AutoEdge {NandGate 1 output&lt;-&gt;NandGate 3 input 0}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>NandGate 1</name>
                <inputs>
                  <input>
                    <parent class="NandGate" reference="../../.."/>
                    <name>NandGate 1 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>true</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>NandGate 1</string>
                  </entry>
                  <entry>
                    <string>No. Of Inputs</string>
                    <int>2</int>
                  </entry>
                </properties>
                <x>27</x>
                <y>3</y>
                <width>6</width>
                <height>4</height>
                <level>2</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
              </endVertex>
              <startPort class="output" reference="../../.."/>
              <endPort class="input" reference="../endVertex/inputs/input"/>
              <edgeConnected/>
              <name>AutoEdge {NandGate 0 output&lt;-&gt;NandGate 1 input 0}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>true</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>NandGate 0</name>
      <inputs>
        <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
        <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>NandGate 0</string>
        </entry>
        <entry>
          <string>No. Of Inputs</string>
          <int>2</int>
        </entry>
      </properties>
      <x>10</x>
      <y>4</y>
      <width>6</width>
      <height>4</height>
      <level>1</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </NandGate>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex"/>
    <notGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.basic.Pin reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.basic.Led reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.basic.Led reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex"/>
    <hk.quantr.logic.data.basic.Clock reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
  </autoEdges>
</data>