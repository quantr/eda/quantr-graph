<data>
  <vertices>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 0 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.flipflop.JKFlipflop">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.JKFlipflop" reference="../../.."/>
                    <name>JKFlipflop 19 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.flipflop.JKFlipflop" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.flipflop.DLatch">
                          <outputs>
                            <output>
                              <parent class="hk.quantr.logic.data.flipflop.DLatch" reference="../../.."/>
                              <name>DLatch 17 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                            <output>
                              <parent class="hk.quantr.logic.data.flipflop.DLatch" reference="../../.."/>
                              <name>DLatch 17 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>5</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>DLatch 17</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DLatch" reference="../../.."/>
                              <name>DLatch 17 input 0</name>
                              <deltaX>3</deltaX>
                              <deltaY>0</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DLatch" reference="../../.."/>
                              <name>DLatch 17 input 1</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DLatch" reference="../../.."/>
                              <name>DLatch 17 input 2</name>
                              <deltaX>0</deltaX>
                              <deltaY>5</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="hk.quantr.logic.data.flipflop.DLatch" reference="../../.."/>
                              <name>DLatch 17 input 3</name>
                              <deltaX>3</deltaX>
                              <deltaY>6</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges/>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>DLatch 17</string>
                            </entry>
                          </properties>
                          <x>84</x>
                          <y>38</y>
                          <width>6</width>
                          <height>6</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>true</isFlipflop>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input[2]"/>
                        <edgeConnected/>
                        <name>AutoEdge {JKFlipflop 19 output&lt;-&gt;DLatch 17 input 1}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.JKFlipflop" reference="../../.."/>
                    <name>JKFlipflop 19 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>JKFlipflop 19</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.JKFlipflop" reference="../../.."/>
                    <name>JKFlipflop 19 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>true</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>1</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.JKFlipflop" reference="../../.."/>
                    <name>JKFlipflop 19 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>3</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.JKFlipflop" reference="../../.."/>
                    <name>JKFlipflop 19 input 2</name>
                    <deltaX>0</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>JKFlipflop 19</string>
                  </entry>
                </properties>
                <x>84</x>
                <y>28</y>
                <width>6</width>
                <height>6</height>
                <level>1</level>
                <showLevel>false</showLevel>
                <isFlipflop>true</isFlipflop>
              </startVertex>
              <endVertex class="hk.quantr.logic.data.basic.Pin" reference="../../../../.."/>
              <startPort class="input" reference="../startVertex/inputs/input"/>
              <endPort class="output" reference="../../.."/>
              <edgeConnected/>
              <name>AutoEdge {JKFlipflop 19 input 0&lt;-&gt;Pin 0 output}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>1</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 0</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>1</long>
        </entry>
      </properties>
      <x>4</x>
      <y>4</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
    <hk.quantr.logic.data.basic.Led>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Led 1</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
          <name>Led 1 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Led 1</string>
        </entry>
        <entry>
          <string>No. Of Inputs</string>
          <int>1</int>
        </entry>
      </properties>
      <x>4</x>
      <y>10</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.Led>
    <hk.quantr.logic.data.basic.Clock>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
          <name>Clock 2 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Clock 2</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Clock 2</string>
        </entry>
        <entry>
          <string>Hertz</string>
          <int>1</int>
        </entry>
      </properties>
      <x>4</x>
      <y>16</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <hertz>1</hertz>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.Clock>
    <hk.quantr.logic.data.basic.RGBLed>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>RGBLed 3</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 3 input 0</name>
          <deltaX>1</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 3 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <Edge>
              <startVertex class="hk.quantr.logic.data.arithmetic.Subtractor">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.arithmetic.Subtractor" reference="../../.."/>
                    <name>Subtractor 13 output</name>
                    <deltaX>4</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>0</value>
                  </output>
                  <output>
                    <parent class="hk.quantr.logic.data.arithmetic.Subtractor" reference="../../.."/>
                    <name>Subtractor 13 output</name>
                    <deltaX>2</deltaX>
                    <deltaY>4</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>Subtractor 13</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.arithmetic.Subtractor" reference="../../.."/>
                    <name>Subtractor 13 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.arithmetic.Subtractor" reference="../../.."/>
                    <name>Subtractor 13 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>3</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.arithmetic.Subtractor" reference="../../.."/>
                    <name>Subtractor 13 input 2</name>
                    <deltaX>2</deltaX>
                    <deltaY>0</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <Edge reference="../../../../.."/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Bits</string>
                    <int>4</int>
                  </entry>
                </properties>
                <x>50</x>
                <y>4</y>
                <width>4</width>
                <height>4</height>
                <level>0</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
              </startVertex>
              <endVertex class="hk.quantr.logic.data.basic.RGBLed" reference="../../../../.."/>
              <startPort class="input" reference="../startVertex/inputs/input[3]"/>
              <endPort class="input" reference="../../.."/>
              <edgeConnected/>
              <Point>
                <x>32</x>
                <y>11</y>
              </Point>
              <Point>
                <x>20</x>
                <y>11</y>
              </Point>
              <Point>
                <x>20</x>
                <y>11</y>
              </Point>
              <Point>
                <x>20</x>
                <y>14</y>
              </Point>
              <name>edge 42</name>
              <isSelected>false</isSelected>
              <startPortExtended>false</startPortExtended>
              <endPortExtended>false</endPortExtended>
              <gridSize>10</gridSize>
            </Edge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 3 input 2</name>
          <deltaX>1</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>RGBLed 3</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>west</string>
        </entry>
        <entry>
          <string>Active High/Low</string>
          <string>High</string>
        </entry>
        <entry>
          <string>Red Color depth</string>
          <int>211</int>
        </entry>
        <entry>
          <string>Green Color depth</string>
          <int>211</int>
        </entry>
        <entry>
          <string>Blue Color depth</string>
          <int>211</int>
        </entry>
      </properties>
      <x>4</x>
      <y>22</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>west</orientation>
      <activeHighLow>High</activeHighLow>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.RGBLed>
    <hk.quantr.logic.data.basic.Tunnel>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Tunnel 4</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.Tunnel" reference="../../.."/>
          <name>Tunnel 4 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Tunnel 4</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>west</string>
        </entry>
        <entry>
          <string>Data Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Label</string>
          <string> </string>
        </entry>
      </properties>
      <x>4</x>
      <y>28</y>
      <width>2</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>west</orientation>
      <databits>1</databits>
      <label> </label>
      <tunnelGroup/>
      <hasTunnelOutput>false</hasTunnelOutput>
    </hk.quantr.logic.data.basic.Tunnel>
    <hk.quantr.logic.data.basic.Splitter>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 5 output</name>
          <deltaX>2</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.basic.Combiner">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.basic.Combiner" reference="../../.."/>
                    <name>Combiner 6 output</name>
                    <deltaX>2</deltaX>
                    <deltaY>4</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>4</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>Combiner 6</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.basic.Combiner" reference="../../.."/>
                    <name>Combiner 6 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>0</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.basic.Combiner" reference="../../.."/>
                    <name>Combiner 6 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.basic.Combiner" reference="../../.."/>
                    <name>Combiner 6 input 2</name>
                    <deltaX>0</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.basic.Combiner" reference="../../.."/>
                    <name>Combiner 6 input 3</name>
                    <deltaX>0</deltaX>
                    <deltaY>3</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>Combiner 6</string>
                  </entry>
                  <entry>
                    <string>Orientation</string>
                    <string>west</string>
                  </entry>
                  <entry>
                    <string>Input Bits</string>
                    <int>4</int>
                  </entry>
                </properties>
                <x>4</x>
                <y>36</y>
                <width>2</width>
                <height>4</height>
                <level>0</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
                <orientation>west</orientation>
              </startVertex>
              <endVertex class="hk.quantr.logic.data.basic.Splitter" reference="../../../../.."/>
              <startPort class="input" reference="../startVertex/inputs/input[4]"/>
              <endPort class="output" reference="../../.."/>
              <edgeConnected/>
              <name>AutoEdge {Combiner 6 input 3&lt;-&gt;Splitter 5 output}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 5 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 5 output</name>
          <deltaX>2</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 5 output</name>
          <deltaX>2</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Splitter 5</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 5 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>4</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Splitter 5</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>east</string>
        </entry>
      </properties>
      <x>7</x>
      <y>31</y>
      <width>2</width>
      <height>4</height>
      <level>-1</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>east</orientation>
    </hk.quantr.logic.data.basic.Splitter>
    <hk.quantr.logic.data.basic.Combiner reference="../hk.quantr.logic.data.basic.Splitter/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.basic.DipSwitch>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>1</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>8</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>2</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.basic.DipSwitch" reference="../../../../.."/>
              <endVertex class="hk.quantr.logic.data.flipflop.SRFlipflop">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.SRFlipflop" reference="../../.."/>
                    <name>SRFlipflop 20 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                  <output>
                    <parent class="hk.quantr.logic.data.flipflop.SRFlipflop" reference="../../.."/>
                    <name>SRFlipflop 20 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>SRFlipflop 20</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.SRFlipflop" reference="../../.."/>
                    <name>SRFlipflop 20 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.SRFlipflop" reference="../../.."/>
                    <name>SRFlipflop 20 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>3</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.SRFlipflop" reference="../../.."/>
                    <name>SRFlipflop 20 input 2</name>
                    <deltaX>0</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.flipflop.SRFlipflop" reference="../../.."/>
                    <name>SRFlipflop 20 input 3</name>
                    <deltaX>0</deltaX>
                    <deltaY>5</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges/>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>SRFlipflop 20</string>
                  </entry>
                </properties>
                <x>50</x>
                <y>46</y>
                <width>6</width>
                <height>6</height>
                <level>0</level>
                <showLevel>false</showLevel>
                <isFlipflop>true</isFlipflop>
              </endVertex>
              <startPort class="output" reference="../../.."/>
              <endPort class="output" reference="../endVertex/outputs/output"/>
              <edgeConnected/>
              <name>AutoEdge {DipSwitch 7 output&lt;-&gt;SRFlipflop 20 output}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>3</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>4</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>5</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>6</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>7</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.DipSwitch" reference="../../.."/>
          <name>DipSwitch 7 output</name>
          <deltaX>8</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>DipSwitch 7</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>DipSwitch 7</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>north</string>
        </entry>
        <entry>
          <string>Number of Switch</string>
          <int>8</int>
        </entry>
      </properties>
      <x>4</x>
      <y>44</y>
      <width>9</width>
      <height>5</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>north</orientation>
    </hk.quantr.logic.data.basic.DipSwitch>
    <hk.quantr.logic.data.output.HexDisplay>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>HexDisplay 8</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.output.HexDisplay" reference="../../.."/>
          <name>HexDisplay 8 input 0</name>
          <deltaX>2</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.HexDisplay" reference="../../.."/>
          <name>HexDisplay 8 input 1</name>
          <deltaX>4</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>HexDisplay 8</string>
        </entry>
        <entry>
          <string>Decimal Point</string>
          <string>true</string>
        </entry>
      </properties>
      <x>17</x>
      <y>4</y>
      <width>5</width>
      <height>6</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <dot>true</dot>
    </hk.quantr.logic.data.output.HexDisplay>
    <hk.quantr.logic.data.output.EightSegmentDisplay>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>EightSegmentDisplay 9</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 0</name>
          <deltaX>1</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 1</name>
          <deltaX>2</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 2</name>
          <deltaX>3</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 3</name>
          <deltaX>4</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 4</name>
          <deltaX>1</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 5</name>
          <deltaX>2</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 6</name>
          <deltaX>3</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.EightSegmentDisplay" reference="../../.."/>
          <name>EightSegmentDisplay 9 input 7</name>
          <deltaX>4</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>EightSegmentDisplay 9</string>
        </entry>
      </properties>
      <x>17</x>
      <y>14</y>
      <width>5</width>
      <height>6</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </hk.quantr.logic.data.output.EightSegmentDisplay>
    <hk.quantr.logic.data.output.TTY>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>TTY 10</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.output.TTY" reference="../../.."/>
          <name>TTY 10 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>7</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.TTY" reference="../../.."/>
          <name>TTY 10 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>5</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.TTY" reference="../../.."/>
          <name>TTY 10 input 2</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.arithmetic.Subtractor" reference="../../../../../../hk.quantr.logic.data.basic.RGBLed/inputs/input[2]/edges/Edge/startVertex"/>
              <endVertex class="hk.quantr.logic.data.output.TTY" reference="../../../../.."/>
              <startPort class="input">
                <parent class="hk.quantr.logic.data.arithmetic.Multiplier">
                  <outputs>
                    <output>
                      <parent class="hk.quantr.logic.data.arithmetic.Multiplier" reference="../../.."/>
                      <name>Multiplier 14 output</name>
                      <deltaX>4</deltaX>
                      <deltaY>2</deltaY>
                      <x>0</x>
                      <y>0</y>
                      <edges/>
                      <state>false</state>
                      <presentstate>false</presentstate>
                      <bits>4</bits>
                      <value>0</value>
                    </output>
                    <output>
                      <parent class="hk.quantr.logic.data.arithmetic.Multiplier" reference="../../.."/>
                      <name>Multiplier 14 output</name>
                      <deltaX>2</deltaX>
                      <deltaY>4</deltaY>
                      <x>0</x>
                      <y>0</y>
                      <edges/>
                      <state>false</state>
                      <presentstate>false</presentstate>
                      <bits>4</bits>
                      <value>0</value>
                    </output>
                  </outputs>
                  <isSelected>false</isSelected>
                  <gridSize>10</gridSize>
                  <name>Multiplier 14</name>
                  <inputs>
                    <input reference="../../.."/>
                    <input>
                      <parent class="hk.quantr.logic.data.arithmetic.Multiplier" reference="../../.."/>
                      <name>Multiplier 14 input 1</name>
                      <deltaX>0</deltaX>
                      <deltaY>3</deltaY>
                      <x>0</x>
                      <y>0</y>
                      <edges/>
                      <state>false</state>
                      <presentstate>false</presentstate>
                      <bits>4</bits>
                      <value>0</value>
                    </input>
                    <input>
                      <parent class="hk.quantr.logic.data.arithmetic.Multiplier" reference="../../.."/>
                      <name>Multiplier 14 input 2</name>
                      <deltaX>2</deltaX>
                      <deltaY>0</deltaY>
                      <x>0</x>
                      <y>0</y>
                      <edges/>
                      <state>false</state>
                      <presentstate>false</presentstate>
                      <bits>4</bits>
                      <value>0</value>
                    </input>
                  </inputs>
                  <properties>
                    <entry>
                      <string>Bits</string>
                      <int>4</int>
                    </entry>
                  </properties>
                  <x>50</x>
                  <y>12</y>
                  <width>4</width>
                  <height>4</height>
                  <level>0</level>
                  <showLevel>false</showLevel>
                  <isFlipflop>false</isFlipflop>
                </parent>
                <name>Multiplier 14 input 0</name>
                <deltaX>0</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                </edges>
                <state>false</state>
                <presentstate>false</presentstate>
                <bits>4</bits>
                <value>0</value>
              </startPort>
              <endPort class="input" reference="../../.."/>
              <edgeConnected/>
              <name>AutoEdge {Multiplier 14 input 0&lt;-&gt;TTY 10 input 2}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.output.TTY" reference="../../.."/>
          <name>TTY 10 input 3</name>
          <deltaX>0</deltaX>
          <deltaY>10</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>TTY 10</string>
        </entry>
      </properties>
      <x>17</x>
      <y>24</y>
      <width>29</width>
      <height>12</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <text>
        <char>|</char>
      </text>
    </hk.quantr.logic.data.output.TTY>
    <orGate>
      <outputs>
        <output>
          <parent class="orGate" reference="../../.."/>
          <name>OrGate 11 output</name>
          <deltaX>5</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>OrGate 11</name>
      <inputs>
        <input>
          <parent class="orGate" reference="../../.."/>
          <name>OrGate 11 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="orGate" reference="../../.."/>
          <name>OrGate 11 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>OrGate 11</string>
        </entry>
        <entry>
          <string>No. Of Inputs</string>
          <int>2</int>
        </entry>
      </properties>
      <x>17</x>
      <y>40</y>
      <width>5</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </orGate>
    <hk.quantr.logic.data.arithmetic.Adder>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.arithmetic.Adder" reference="../../.."/>
          <name>Adder 12 output</name>
          <deltaX>4</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.arithmetic.Adder" reference="../../.."/>
          <name>Adder 12 output</name>
          <deltaX>2</deltaX>
          <deltaY>4</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Adder 12</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Adder" reference="../../.."/>
          <name>Adder 12 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.arithmetic.Adder" reference="../../../../.."/>
              <endVertex class="hk.quantr.logic.data.arithmetic.Adder" reference="../../../../.."/>
              <startPort class="input" reference="../../.."/>
              <endPort class="input">
                <parent class="hk.quantr.logic.data.arithmetic.Adder" reference="../../../../../.."/>
                <name>Adder 12 input 2</name>
                <deltaX>2</deltaX>
                <deltaY>0</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                </edges>
                <state>false</state>
                <presentstate>false</presentstate>
                <bits>1</bits>
                <value>0</value>
              </endPort>
              <edgeConnected/>
              <name>AutoEdge {Adder 12 input 0&lt;-&gt;Adder 12 input 2}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Adder" reference="../../.."/>
          <name>Adder 12 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input reference="../input/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
      </inputs>
      <properties>
        <entry>
          <string>Bits</string>
          <int>4</int>
        </entry>
      </properties>
      <x>17</x>
      <y>48</y>
      <width>4</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </hk.quantr.logic.data.arithmetic.Adder>
    <hk.quantr.logic.data.arithmetic.Subtractor reference="../hk.quantr.logic.data.basic.RGBLed/inputs/input[2]/edges/Edge/startVertex"/>
    <hk.quantr.logic.data.arithmetic.Multiplier reference="../hk.quantr.logic.data.output.TTY/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge/startPort/parent"/>
    <hk.quantr.logic.data.arithmetic.Divider>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.arithmetic.Divider" reference="../../.."/>
          <name>Divider 15 output</name>
          <deltaX>4</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.arithmetic.Divider" reference="../../.."/>
          <name>Divider 15 output</name>
          <deltaX>2</deltaX>
          <deltaY>4</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Divider 15</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Divider" reference="../../.."/>
          <name>Divider 15 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Divider" reference="../../.."/>
          <name>Divider 15 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Divider" reference="../../.."/>
          <name>Divider 15 input 2</name>
          <deltaX>2</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Bits</string>
          <int>4</int>
        </entry>
      </properties>
      <x>50</x>
      <y>20</y>
      <width>4</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </hk.quantr.logic.data.arithmetic.Divider>
    <hk.quantr.logic.data.arithmetic.Shifter>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.arithmetic.Shifter" reference="../../.."/>
          <name>Shifter 16 output</name>
          <deltaX>4</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.arithmetic.Shifter" reference="../../.."/>
          <name>Shifter 16 output</name>
          <deltaX>4</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Shifter 16</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Shifter" reference="../../.."/>
          <name>Shifter 16 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Shifter" reference="../../.."/>
          <name>Shifter 16 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.arithmetic.Shifter" reference="../../.."/>
          <name>Shifter 16 input 2</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Bits</string>
          <int>4</int>
        </entry>
        <entry>
          <string>Shift Type</string>
          <string>logical left</string>
        </entry>
      </properties>
      <x>50</x>
      <y>28</y>
      <width>4</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </hk.quantr.logic.data.arithmetic.Shifter>
    <hk.quantr.logic.data.flipflop.DLatch reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.flipflop.DFlipflop>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
          <name>DFlipflop 18 output</name>
          <deltaX>6</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
          <name>DFlipflop 18 output</name>
          <deltaX>6</deltaX>
          <deltaY>5</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>DFlipflop 18</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
          <name>DFlipflop 18 input 0</name>
          <deltaX>3</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
          <name>DFlipflop 18 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
          <name>DFlipflop 18 input 2</name>
          <deltaX>0</deltaX>
          <deltaY>5</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.DFlipflop" reference="../../.."/>
          <name>DFlipflop 18 input 3</name>
          <deltaX>3</deltaX>
          <deltaY>6</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>DFlipflop 18</string>
        </entry>
      </properties>
      <x>50</x>
      <y>36</y>
      <width>6</width>
      <height>6</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>true</isFlipflop>
    </hk.quantr.logic.data.flipflop.DFlipflop>
    <hk.quantr.logic.data.flipflop.JKFlipflop reference="../hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.flipflop.SRFlipflop reference="../hk.quantr.logic.data.basic.DipSwitch/outputs/output[2]/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.flipflop.Ram>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.flipflop.Ram" reference="../../.."/>
          <name>Ram 21 output</name>
          <deltaX>20</deltaX>
          <deltaY>5</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>8</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Ram 21</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.Ram" reference="../../.."/>
          <name>Ram 21 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>8</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.Ram" reference="../../.."/>
          <name>Ram 21 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>5</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>8</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.Ram" reference="../../.."/>
          <name>Ram 21 input 2</name>
          <deltaX>0</deltaX>
          <deltaY>7</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.flipflop.Ram" reference="../../.."/>
          <name>Ram 21 input 3</name>
          <deltaX>0</deltaX>
          <deltaY>9</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Ram 21</string>
        </entry>
        <entry>
          <string>Address Bit Width</string>
          <int>8</int>
        </entry>
        <entry>
          <string>Data Bit Width</string>
          <int>8</int>
        </entry>
        <entry>
          <string>Page</string>
          <int>1</int>
        </entry>
      </properties>
      <x>60</x>
      <y>4</y>
      <width>20</width>
      <height>27</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <addressBitWidth>8</addressBitWidth>
      <dataBitWidth>8</dataBitWidth>
      <hex>
        <string>0</string>
        <string>1</string>
        <string>2</string>
        <string>3</string>
        <string>4</string>
        <string>5</string>
        <string>6</string>
        <string>7</string>
        <string>8</string>
        <string>9</string>
        <string>a</string>
        <string>b</string>
        <string>c</string>
        <string>d</string>
        <string>e</string>
        <string>f </string>
      </hex>
      <data>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
        <long>0</long>
      </data>
      <page>1</page>
      <col>8</col>
    </hk.quantr.logic.data.flipflop.Ram>
    <hk.quantr.logic.data.basic.RGBLed>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>RGBLed 22</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 22 input 0</name>
          <deltaX>1</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 22 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 22 input 2</name>
          <deltaX>1</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>RGBLed 22</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>west</string>
        </entry>
        <entry>
          <string>Active High/Low</string>
          <string>High</string>
        </entry>
        <entry>
          <string>Red Color depth</string>
          <int>211</int>
        </entry>
        <entry>
          <string>Green Color depth</string>
          <int>211</int>
        </entry>
        <entry>
          <string>Blue Color depth</string>
          <int>211</int>
        </entry>
      </properties>
      <x>60</x>
      <y>35</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>west</orientation>
      <activeHighLow>High</activeHighLow>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.RGBLed>
    <hk.quantr.logic.data.basic.Splitter>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 23 output</name>
          <deltaX>2</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 23 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 23 output</name>
          <deltaX>2</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
        <output>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 23 output</name>
          <deltaX>2</deltaX>
          <deltaY>3</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Splitter 23</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.Splitter" reference="../../.."/>
          <name>Splitter 23 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>4</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>4</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Splitter 23</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>east</string>
        </entry>
      </properties>
      <x>60</x>
      <y>41</y>
      <width>2</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>east</orientation>
    </hk.quantr.logic.data.basic.Splitter>
    <hk.quantr.logic.data.basic.RGBLed>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>RGBLed 24</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 24 input 0</name>
          <deltaX>1</deltaX>
          <deltaY>0</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 24 input 1</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
        <input>
          <parent class="hk.quantr.logic.data.basic.RGBLed" reference="../../.."/>
          <name>RGBLed 24 input 2</name>
          <deltaX>1</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>RGBLed 24</string>
        </entry>
        <entry>
          <string>Orientation</string>
          <string>west</string>
        </entry>
        <entry>
          <string>Active High/Low</string>
          <string>High</string>
        </entry>
        <entry>
          <string>Red Color depth</string>
          <int>211</int>
        </entry>
        <entry>
          <string>Green Color depth</string>
          <int>211</int>
        </entry>
        <entry>
          <string>Blue Color depth</string>
          <int>211</int>
        </entry>
      </properties>
      <x>60</x>
      <y>49</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <orientation>west</orientation>
      <activeHighLow>High</activeHighLow>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.RGBLed>
    <hk.quantr.logic.data.basic.Led>
      <outputs/>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Led 25</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
          <name>Led 25 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Led 25</string>
        </entry>
        <entry>
          <string>No. Of Inputs</string>
          <int>1</int>
        </entry>
      </properties>
      <x>84</x>
      <y>4</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.Led>
    <hk.quantr.logic.data.basic.Led>
      <outputs/>
      <isSelected>true</isSelected>
      <gridSize>10</gridSize>
      <name>Led 26</name>
      <inputs>
        <input>
          <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
          <name>Led 26 input 0</name>
          <deltaX>0</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </input>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>Led 26</string>
        </entry>
        <entry>
          <string>No. Of Inputs</string>
          <int>1</int>
        </entry>
      </properties>
      <x>84</x>
      <y>10</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <nextstate>false</nextstate>
      <presentstate>false</presentstate>
    </hk.quantr.logic.data.basic.Led>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 27 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>1</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 27</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 27</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>1</long>
        </entry>
      </properties>
      <x>84</x>
      <y>16</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000001</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
    <hk.quantr.logic.data.basic.Pin>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
          <name>Pin 28 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges/>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Pin 28</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Pin 28</string>
        </entry>
        <entry>
          <string>Bits</string>
          <int>1</int>
        </entry>
        <entry>
          <string>Radix</string>
          <string>bin</string>
        </entry>
        <entry>
          <string>Value</string>
          <long>0</long>
        </entry>
      </properties>
      <x>84</x>
      <y>22</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <radix>bin</radix>
      <base>2</base>
      <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
      <hex>0000000000000000</hex>
      <dec>00000000000000000000</dec>
    </hk.quantr.logic.data.basic.Pin>
  </vertices>
  <edges>
    <Edge reference="../../vertices/hk.quantr.logic.data.basic.RGBLed/inputs/input[2]/edges/Edge"/>
  </edges>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.output.TTY/inputs/input[3]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.DipSwitch/outputs/output[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Splitter/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.arithmetic.Adder/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge>
      <startVertex class="hk.quantr.logic.data.flipflop.DLatch" reference="../../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
      <endVertex class="hk.quantr.logic.data.basic.Led" reference="../../../vertices/hk.quantr.logic.data.basic.Led[3]"/>
      <startPort class="output" reference="../../../vertices/hk.quantr.logic.data.basic.Pin/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output"/>
      <endPort class="input" reference="../../../vertices/hk.quantr.logic.data.basic.Led[3]/inputs/input"/>
      <edgeConnected/>
      <name>AutoEdge {DLatch 17 output&lt;-&gt;Led 26 input 0}</name>
      <isSelected>false</isSelected>
      <fromWire>false</fromWire>
      <x>0</x>
      <y>0</y>
    </hk.quantr.logic.data.gate.AutoEdge>
  </autoEdges>
</data>