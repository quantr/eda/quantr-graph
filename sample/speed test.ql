<data>
  <vertices>
    <hk.quantr.logic.data.basic.Clock>
      <outputs>
        <output>
          <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
          <name>Clock 0 output</name>
          <deltaX>2</deltaX>
          <deltaY>1</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="hk.quantr.logic.data.basic.Clock" reference="../../../../.."/>
              <endVertex class="hk.quantr.logic.data.gate.AndGate">
                <outputs>
                  <output>
                    <parent class="hk.quantr.logic.data.gate.AndGate" reference="../../.."/>
                    <name>AndGate 3 output</name>
                    <deltaX>5</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.gate.AndGate" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.Led">
                          <outputs/>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>Led 2</name>
                          <inputs>
                            <input>
                              <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
                              <name>Led 2 input 0</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>Led 2</string>
                            </entry>
                            <entry>
                              <string>Orientation</string>
                              <string>west</string>
                            </entry>
                          </properties>
                          <x>16</x>
                          <y>12</y>
                          <width>2</width>
                          <height>2</height>
                          <level>2</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <nextstate>false</nextstate>
                          <presentstate>false</presentstate>
                          <orientation>west</orientation>
                        </endVertex>
                        <startPort class="output" reference="../../.."/>
                        <endPort class="input" reference="../endVertex/inputs/input"/>
                        <edgeConnected/>
                        <name>AutoEdge {AndGate 3 output&lt;-&gt;Led 2 input 0}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>AndGate 3</name>
                <inputs>
                  <input>
                    <parent class="hk.quantr.logic.data.gate.AndGate" reference="../../.."/>
                    <name>AndGate 3 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input>
                    <parent class="hk.quantr.logic.data.gate.AndGate" reference="../../.."/>
                    <name>AndGate 3 input 1</name>
                    <deltaX>0</deltaX>
                    <deltaY>3</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="hk.quantr.logic.data.gate.AndGate" reference="../../../../.."/>
                        <endVertex class="hk.quantr.logic.data.basic.Clock">
                          <outputs>
                            <output>
                              <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
                              <name>Clock 1 output</name>
                              <deltaX>2</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>Clock 1</name>
                          <inputs/>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>Clock 1</string>
                            </entry>
                            <entry>
                              <string>Hertz</string>
                              <int>1</int>
                            </entry>
                          </properties>
                          <x>6</x>
                          <y>14</y>
                          <width>2</width>
                          <height>2</height>
                          <level>0</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                          <hertz>1</hertz>
                          <presentstate>false</presentstate>
                          <lastSystemTime>4797196581791</lastSystemTime>
                        </endVertex>
                        <startPort class="input" reference="../../.."/>
                        <endPort class="output" reference="../endVertex/outputs/output"/>
                        <edgeConnected/>
                        <name>AutoEdge {AndGate 3 input 1&lt;-&gt;Clock 1 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>AndGate 3</string>
                  </entry>
                  <entry>
                    <string>No. Of Inputs</string>
                    <int>2</int>
                  </entry>
                </properties>
                <x>9</x>
                <y>11</y>
                <width>5</width>
                <height>4</height>
                <level>1</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
                <ports>
                  <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input"/>
                </ports>
              </endVertex>
              <startPort class="output" reference="../../.."/>
              <endPort class="input" reference="../endVertex/inputs/input"/>
              <edgeConnected/>
              <name>AutoEdge {Clock 0 output&lt;-&gt;AndGate 3 input 0}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>Clock 0</name>
      <inputs/>
      <properties>
        <entry>
          <string>Name</string>
          <string>Clock 0</string>
        </entry>
        <entry>
          <string>Hertz</string>
          <int>1</int>
        </entry>
      </properties>
      <x>6</x>
      <y>10</y>
      <width>2</width>
      <height>2</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
      <hertz>1</hertz>
      <presentstate>false</presentstate>
      <lastSystemTime>4797196572815</lastSystemTime>
    </hk.quantr.logic.data.basic.Clock>
    <hk.quantr.logic.data.basic.Clock reference="../hk.quantr.logic.data.basic.Clock/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.basic.Led reference="../hk.quantr.logic.data.basic.Clock/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
    <hk.quantr.logic.data.gate.AndGate reference="../hk.quantr.logic.data.basic.Clock/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Clock/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Clock/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/hk.quantr.logic.data.basic.Clock/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
  </autoEdges>
</data>