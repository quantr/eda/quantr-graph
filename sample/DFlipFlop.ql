<data>
  <vertices>
    <NandGate>
      <outputs>
        <output>
          <parent class="NandGate" reference="../../.."/>
          <name>NandGate 0 output</name>
          <deltaX>6</deltaX>
          <deltaY>2</deltaY>
          <x>0</x>
          <y>0</y>
          <edges>
            <hk.quantr.logic.data.gate.AutoEdge>
              <startVertex class="NandGate">
                <outputs>
                  <output>
                    <parent class="NandGate" reference="../../.."/>
                    <name>NandGate 1 output</name>
                    <deltaX>6</deltaX>
                    <deltaY>2</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="NandGate">
                          <outputs>
                            <output>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 2 output</name>
                              <deltaX>6</deltaX>
                              <deltaY>2</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="NandGate">
                                    <outputs>
                                      <output>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 3 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>2</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="NandGate" reference="../../../../.."/>
                                            <endVertex class="NandGate" reference="../../../../../../../../../.."/>
                                            <startPort class="output" reference="../../.."/>
                                            <endPort class="input">
                                              <parent class="NandGate" reference="../../../../../../../../../../.."/>
                                              <name>NandGate 2 input 3</name>
                                              <deltaX>0</deltaX>
                                              <deltaY>3</deltaY>
                                              <x>0</x>
                                              <y>0</y>
                                              <edges>
                                                <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                              </edges>
                                              <state>false</state>
                                              <presentstate>false</presentstate>
                                              <bits>1</bits>
                                              <value>0</value>
                                            </endPort>
                                            <edgeConnected/>
                                            <name>AutoEdge {NandGate 3 output&lt;-&gt;NandGate 2 input 3}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="NandGate" reference="../../../../../../../../../../../../../../../../../../../.."/>
                                            <endVertex class="NandGate" reference="../../../../.."/>
                                            <startPort class="input">
                                              <parent class="NandGate" reference="../../../../../../../../../../../../../../../../../../../../.."/>
                                              <name>NandGate 0 input 0</name>
                                              <deltaX>0</deltaX>
                                              <deltaY>1</deltaY>
                                              <x>0</x>
                                              <y>0</y>
                                              <edges>
                                                <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                              </edges>
                                              <state>false</state>
                                              <presentstate>false</presentstate>
                                              <bits>1</bits>
                                              <value>0</value>
                                            </startPort>
                                            <endPort class="output" reference="../../.."/>
                                            <edgeConnected/>
                                            <name>AutoEdge {NandGate 0 input 0&lt;-&gt;NandGate 3 output}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                    </outputs>
                                    <isSelected>false</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>NandGate 3</name>
                                    <inputs>
                                      <input>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 3 input 0</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                      <input>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 3 input 1</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>3</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="hk.quantr.logic.data.basic.Pin">
                                              <outputs>
                                                <output>
                                                  <parent class="hk.quantr.logic.data.basic.Pin" reference="../../.."/>
                                                  <name>Pin 8 output</name>
                                                  <deltaX>2</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                              </outputs>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>Pin 8</name>
                                              <inputs/>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>Pin 8</string>
                                                </entry>
                                                <entry>
                                                  <string>Bits</string>
                                                  <int>1</int>
                                                </entry>
                                                <entry>
                                                  <string>Radix</string>
                                                  <string>bin</string>
                                                </entry>
                                                <entry>
                                                  <string>Value</string>
                                                  <long>0</long>
                                                </entry>
                                              </properties>
                                              <x>7</x>
                                              <y>26</y>
                                              <width>2</width>
                                              <height>2</height>
                                              <level>0</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>false</isFlipflop>
                                              <radix>bin</radix>
                                              <base>2</base>
                                              <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
                                              <hex>0000000000000000</hex>
                                              <dec>00000000000000000000</dec>
                                            </startVertex>
                                            <endVertex class="NandGate" reference="../../../../.."/>
                                            <startPort class="output" reference="../startVertex/outputs/output"/>
                                            <endPort class="input" reference="../../.."/>
                                            <edgeConnected/>
                                            <name>AutoEdge {Pin 8 output&lt;-&gt;NandGate 3 input 1}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                    </inputs>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>NandGate 3</string>
                                      </entry>
                                      <entry>
                                        <string>No. Of Inputs</string>
                                        <int>2</int>
                                      </entry>
                                    </properties>
                                    <x>24</x>
                                    <y>27</y>
                                    <width>6</width>
                                    <height>4</height>
                                    <level>0</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>false</isFlipflop>
                                  </startVertex>
                                  <endVertex class="NandGate" reference="../../../../.."/>
                                  <startPort class="input" reference="../startVertex/inputs/input"/>
                                  <endPort class="output" reference="../../.."/>
                                  <edgeConnected/>
                                  <name>AutoEdge {NandGate 3 input 0&lt;-&gt;NandGate 2 output}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="NandGate">
                                    <outputs>
                                      <output>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 5 output</name>
                                        <deltaX>6</deltaX>
                                        <deltaY>2</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="NandGate">
                                              <outputs>
                                                <output>
                                                  <parent class="NandGate" reference="../../.."/>
                                                  <name>NandGate 4 output</name>
                                                  <deltaX>6</deltaX>
                                                  <deltaY>2</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="NandGate" reference="../../../../.."/>
                                                      <endVertex class="NandGate" reference="../../../../../../../../../.."/>
                                                      <startPort class="output" reference="../../.."/>
                                                      <endPort class="input">
                                                        <parent class="NandGate" reference="../../../../../../../../../../.."/>
                                                        <name>NandGate 5 input 0</name>
                                                        <deltaX>0</deltaX>
                                                        <deltaY>1</deltaY>
                                                        <x>0</x>
                                                        <y>0</y>
                                                        <edges>
                                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                                        </edges>
                                                        <state>false</state>
                                                        <presentstate>false</presentstate>
                                                        <bits>1</bits>
                                                        <value>0</value>
                                                      </endPort>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {NandGate 4 output&lt;-&gt;NandGate 5 input 0}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="NandGate" reference="../../../../.."/>
                                                      <endVertex class="hk.quantr.logic.data.basic.Led">
                                                        <outputs/>
                                                        <isSelected>false</isSelected>
                                                        <gridSize>10</gridSize>
                                                        <name>Q</name>
                                                        <inputs>
                                                          <input>
                                                            <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
                                                            <name>Led 6 input 0</name>
                                                            <deltaX>0</deltaX>
                                                            <deltaY>1</deltaY>
                                                            <x>0</x>
                                                            <y>0</y>
                                                            <edges>
                                                              <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                            </edges>
                                                            <state>false</state>
                                                            <presentstate>false</presentstate>
                                                            <bits>1</bits>
                                                            <value>0</value>
                                                          </input>
                                                        </inputs>
                                                        <properties>
                                                          <entry>
                                                            <string>Name</string>
                                                            <string>Q</string>
                                                          </entry>
                                                          <entry>
                                                            <string>Orientation</string>
                                                            <string>west</string>
                                                          </entry>
                                                        </properties>
                                                        <x>52</x>
                                                        <y>13</y>
                                                        <width>2</width>
                                                        <height>2</height>
                                                        <level>0</level>
                                                        <showLevel>false</showLevel>
                                                        <isFlipflop>false</isFlipflop>
                                                        <nextstate>false</nextstate>
                                                        <presentstate>false</presentstate>
                                                        <orientation>west</orientation>
                                                      </endVertex>
                                                      <startPort class="output" reference="../../.."/>
                                                      <endPort class="input" reference="../endVertex/inputs/input"/>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {NandGate 4 output&lt;-&gt;Led 6 input 0}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </output>
                                              </outputs>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>NandGate 4</name>
                                              <inputs>
                                                <input>
                                                  <parent class="NandGate" reference="../../.."/>
                                                  <name>NandGate 4 input 0</name>
                                                  <deltaX>0</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge>
                                                      <startVertex class="NandGate" reference="../../../../.."/>
                                                      <endVertex class="NandGate" reference="../../../../../../../../../../../../../../../../../../../.."/>
                                                      <startPort class="input" reference="../../.."/>
                                                      <endPort class="output" reference="../../../../../../../../../../../../../../../../../.."/>
                                                      <edgeConnected/>
                                                      <name>AutoEdge {NandGate 4 input 0&lt;-&gt;NandGate 1 output}</name>
                                                      <isSelected>false</isSelected>
                                                      <fromWire>false</fromWire>
                                                      <x>0</x>
                                                      <y>0</y>
                                                    </hk.quantr.logic.data.gate.AutoEdge>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                                <input>
                                                  <parent class="NandGate" reference="../../.."/>
                                                  <name>NandGate 4 input 1</name>
                                                  <deltaX>0</deltaX>
                                                  <deltaY>3</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                              </inputs>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>NandGate 4</string>
                                                </entry>
                                                <entry>
                                                  <string>No. Of Inputs</string>
                                                  <int>2</int>
                                                </entry>
                                              </properties>
                                              <x>40</x>
                                              <y>11</y>
                                              <width>6</width>
                                              <height>4</height>
                                              <level>0</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>false</isFlipflop>
                                            </startVertex>
                                            <endVertex class="NandGate" reference="../../../../.."/>
                                            <startPort class="input" reference="../startVertex/inputs/input[2]"/>
                                            <endPort class="output" reference="../../.."/>
                                            <edgeConnected/>
                                            <name>AutoEdge {NandGate 4 input 1&lt;-&gt;NandGate 5 output}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="hk.quantr.logic.data.basic.Led">
                                              <outputs/>
                                              <isSelected>false</isSelected>
                                              <gridSize>10</gridSize>
                                              <name>Led 7</name>
                                              <inputs>
                                                <input>
                                                  <parent class="hk.quantr.logic.data.basic.Led" reference="../../.."/>
                                                  <name>Led 7 input 0</name>
                                                  <deltaX>0</deltaX>
                                                  <deltaY>1</deltaY>
                                                  <x>0</x>
                                                  <y>0</y>
                                                  <edges>
                                                    <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                                  </edges>
                                                  <state>false</state>
                                                  <presentstate>false</presentstate>
                                                  <bits>1</bits>
                                                  <value>0</value>
                                                </input>
                                              </inputs>
                                              <properties>
                                                <entry>
                                                  <string>Name</string>
                                                  <string>Led 7</string>
                                                </entry>
                                                <entry>
                                                  <string>Orientation</string>
                                                  <string>west</string>
                                                </entry>
                                              </properties>
                                              <x>53</x>
                                              <y>23</y>
                                              <width>2</width>
                                              <height>2</height>
                                              <level>0</level>
                                              <showLevel>false</showLevel>
                                              <isFlipflop>false</isFlipflop>
                                              <nextstate>false</nextstate>
                                              <presentstate>false</presentstate>
                                              <orientation>west</orientation>
                                            </startVertex>
                                            <endVertex class="NandGate" reference="../../../../.."/>
                                            <startPort class="input" reference="../startVertex/inputs/input"/>
                                            <endPort class="output" reference="../../.."/>
                                            <edgeConnected/>
                                            <name>AutoEdge {Led 7 input 0&lt;-&gt;NandGate 5 output}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                    </outputs>
                                    <isSelected>true</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>NandGate 5</name>
                                    <inputs>
                                      <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
                                      <input>
                                        <parent class="NandGate" reference="../../.."/>
                                        <name>NandGate 5 input 1</name>
                                        <deltaX>0</deltaX>
                                        <deltaY>3</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </input>
                                    </inputs>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>NandGate 5</string>
                                      </entry>
                                      <entry>
                                        <string>No. Of Inputs</string>
                                        <int>2</int>
                                      </entry>
                                    </properties>
                                    <x>40</x>
                                    <y>22</y>
                                    <width>6</width>
                                    <height>4</height>
                                    <level>0</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>false</isFlipflop>
                                  </startVertex>
                                  <endVertex class="NandGate" reference="../../../../.."/>
                                  <startPort class="input" reference="../startVertex/inputs/input[2]"/>
                                  <endPort class="output" reference="../../.."/>
                                  <edgeConnected/>
                                  <name>AutoEdge {NandGate 5 input 1&lt;-&gt;NandGate 2 output}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </output>
                          </outputs>
                          <isSelected>false</isSelected>
                          <gridSize>10</gridSize>
                          <name>NandGate 2</name>
                          <inputs>
                            <input>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 2 input 0</name>
                              <deltaX>0</deltaX>
                              <deltaY>1</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input>
                              <parent class="NandGate" reference="../../.."/>
                              <name>NandGate 2 input 1</name>
                              <deltaX>0</deltaX>
                              <deltaY>2</deltaY>
                              <x>0</x>
                              <y>0</y>
                              <edges>
                                <hk.quantr.logic.data.gate.AutoEdge>
                                  <startVertex class="hk.quantr.logic.data.basic.Clock">
                                    <outputs>
                                      <output>
                                        <parent class="hk.quantr.logic.data.basic.Clock" reference="../../.."/>
                                        <name>Clock 9 output</name>
                                        <deltaX>2</deltaX>
                                        <deltaY>1</deltaY>
                                        <x>0</x>
                                        <y>0</y>
                                        <edges>
                                          <hk.quantr.logic.data.gate.AutoEdge>
                                            <startVertex class="hk.quantr.logic.data.basic.Clock" reference="../../../../.."/>
                                            <endVertex class="NandGate" reference="../../../../../../../../../../../../../../.."/>
                                            <startPort class="output" reference="../../.."/>
                                            <endPort class="input">
                                              <parent class="NandGate" reference="../../../../../../../../../../../../../../../.."/>
                                              <name>NandGate 1 input 1</name>
                                              <deltaX>0</deltaX>
                                              <deltaY>3</deltaY>
                                              <x>0</x>
                                              <y>0</y>
                                              <edges>
                                                <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                                              </edges>
                                              <state>false</state>
                                              <presentstate>false</presentstate>
                                              <bits>1</bits>
                                              <value>0</value>
                                            </endPort>
                                            <edgeConnected/>
                                            <name>AutoEdge {Clock 9 output&lt;-&gt;NandGate 1 input 1}</name>
                                            <isSelected>false</isSelected>
                                            <fromWire>false</fromWire>
                                            <x>0</x>
                                            <y>0</y>
                                          </hk.quantr.logic.data.gate.AutoEdge>
                                          <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                                        </edges>
                                        <state>false</state>
                                        <presentstate>false</presentstate>
                                        <bits>1</bits>
                                        <value>0</value>
                                      </output>
                                    </outputs>
                                    <isSelected>false</isSelected>
                                    <gridSize>10</gridSize>
                                    <name>Clock 9</name>
                                    <inputs/>
                                    <properties>
                                      <entry>
                                        <string>Name</string>
                                        <string>Clock 9</string>
                                      </entry>
                                      <entry>
                                        <string>Hertz</string>
                                        <int>1</int>
                                      </entry>
                                    </properties>
                                    <x>9</x>
                                    <y>14</y>
                                    <width>2</width>
                                    <height>2</height>
                                    <level>0</level>
                                    <showLevel>false</showLevel>
                                    <isFlipflop>false</isFlipflop>
                                    <hertz>1</hertz>
                                    <presentstate>false</presentstate>
                                  </startVertex>
                                  <endVertex class="NandGate" reference="../../../../.."/>
                                  <startPort class="output" reference="../startVertex/outputs/output"/>
                                  <endPort class="input" reference="../../.."/>
                                  <edgeConnected/>
                                  <name>AutoEdge {Clock 9 output&lt;-&gt;NandGate 2 input 1}</name>
                                  <isSelected>false</isSelected>
                                  <fromWire>false</fromWire>
                                  <x>0</x>
                                  <y>0</y>
                                </hk.quantr.logic.data.gate.AutoEdge>
                              </edges>
                              <state>false</state>
                              <presentstate>false</presentstate>
                              <bits>1</bits>
                              <value>0</value>
                            </input>
                            <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
                          </inputs>
                          <properties>
                            <entry>
                              <string>Name</string>
                              <string>NandGate 2</string>
                            </entry>
                            <entry>
                              <string>No. Of Inputs</string>
                              <int>3</int>
                            </entry>
                          </properties>
                          <x>25</x>
                          <y>20</y>
                          <width>6</width>
                          <height>4</height>
                          <level>0</level>
                          <showLevel>false</showLevel>
                          <isFlipflop>false</isFlipflop>
                        </startVertex>
                        <endVertex class="NandGate" reference="../../../../.."/>
                        <startPort class="input" reference="../startVertex/inputs/input"/>
                        <endPort class="output" reference="../../.."/>
                        <edgeConnected/>
                        <name>AutoEdge {NandGate 2 input 0&lt;-&gt;NandGate 1 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                      <hk.quantr.logic.data.gate.AutoEdge>
                        <startVertex class="NandGate" reference="../../../../../../../../../.."/>
                        <endVertex class="NandGate" reference="../../../../.."/>
                        <startPort class="input">
                          <parent class="NandGate" reference="../../../../../../../../../../.."/>
                          <name>NandGate 0 input 1</name>
                          <deltaX>0</deltaX>
                          <deltaY>3</deltaY>
                          <x>0</x>
                          <y>0</y>
                          <edges>
                            <hk.quantr.logic.data.gate.AutoEdge reference="../../.."/>
                          </edges>
                          <state>false</state>
                          <presentstate>false</presentstate>
                          <bits>1</bits>
                          <value>0</value>
                        </startPort>
                        <endPort class="output" reference="../../.."/>
                        <edgeConnected/>
                        <name>AutoEdge {NandGate 0 input 1&lt;-&gt;NandGate 1 output}</name>
                        <isSelected>false</isSelected>
                        <fromWire>false</fromWire>
                        <x>0</x>
                        <y>0</y>
                      </hk.quantr.logic.data.gate.AutoEdge>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge"/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </output>
                </outputs>
                <isSelected>false</isSelected>
                <gridSize>10</gridSize>
                <name>NandGate 1</name>
                <inputs>
                  <input>
                    <parent class="NandGate" reference="../../.."/>
                    <name>NandGate 1 input 0</name>
                    <deltaX>0</deltaX>
                    <deltaY>1</deltaY>
                    <x>0</x>
                    <y>0</y>
                    <edges>
                      <hk.quantr.logic.data.gate.AutoEdge reference="../../../../.."/>
                    </edges>
                    <state>false</state>
                    <presentstate>false</presentstate>
                    <bits>1</bits>
                    <value>0</value>
                  </input>
                  <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/endPort"/>
                </inputs>
                <properties>
                  <entry>
                    <string>Name</string>
                    <string>NandGate 1</string>
                  </entry>
                  <entry>
                    <string>No. Of Inputs</string>
                    <int>2</int>
                  </entry>
                </properties>
                <x>26</x>
                <y>13</y>
                <width>6</width>
                <height>4</height>
                <level>0</level>
                <showLevel>false</showLevel>
                <isFlipflop>false</isFlipflop>
              </startVertex>
              <endVertex class="NandGate" reference="../../../../.."/>
              <startPort class="input" reference="../startVertex/inputs/input"/>
              <endPort class="output" reference="../../.."/>
              <edgeConnected/>
              <name>AutoEdge {NandGate 1 input 0&lt;-&gt;NandGate 0 output}</name>
              <isSelected>false</isSelected>
              <fromWire>false</fromWire>
              <x>0</x>
              <y>0</y>
            </hk.quantr.logic.data.gate.AutoEdge>
          </edges>
          <state>false</state>
          <presentstate>false</presentstate>
          <bits>1</bits>
          <value>0</value>
        </output>
      </outputs>
      <isSelected>false</isSelected>
      <gridSize>10</gridSize>
      <name>NandGate 0</name>
      <inputs>
        <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startPort"/>
        <input reference="../../outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startPort"/>
      </inputs>
      <properties>
        <entry>
          <string>Name</string>
          <string>NandGate 0</string>
        </entry>
        <entry>
          <string>No. Of Inputs</string>
          <int>2</int>
        </entry>
      </properties>
      <x>24</x>
      <y>6</y>
      <width>6</width>
      <height>4</height>
      <level>0</level>
      <showLevel>false</showLevel>
      <isFlipflop>false</isFlipflop>
    </NandGate>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <NandGate reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex"/>
    <hk.quantr.logic.data.basic.Led reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/endVertex"/>
    <hk.quantr.logic.data.basic.Led reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex"/>
    <hk.quantr.logic.data.basic.Pin reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
    <hk.quantr.logic.data.basic.Clock reference="../NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex"/>
  </vertices>
  <edges/>
  <autoEdges>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input[2]/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/inputs/input/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
    <hk.quantr.logic.data.gate.AutoEdge reference="../../vertices/NandGate/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]/startVertex/outputs/output/edges/hk.quantr.logic.data.gate.AutoEdge[2]"/>
  </autoEdges>
</data>